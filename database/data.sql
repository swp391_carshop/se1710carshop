-- User data
-- Admins
INSERT INTO Users (Fullname, Username, Password, Email, Phone, Role)
VALUES
('Robert Johnson', 'admin001', 'adminpass', 'admin001@example.com', '111111111', 1),
('Jennifer Wilson', 'admin002', 'adminpass', 'admin002@example.com', '222222222', 1),
('David Thompson', 'admin003', 'adminpass', 'admin003@example.com', '333333333', 1),
('Emily Davis', 'admin004', 'adminpass', 'admin004@example.com', '444444444', 1),
('Daniel Martinez', 'admin005', 'adminpass', 'admin005@example.com', '555555555', 1);

-- Marketers
INSERT INTO Users (Fullname, Username, Password, Email, Phone, Role)
VALUES
('Jessica Lee', 'marketer001', 'marketerpass', 'marketer001@example.com', '111111111', 2),
('Ryan Brown', 'marketer002', 'marketerpass', 'marketer002@example.com', '222222222', 2),
('Sophia Wilson', 'marketer003', 'marketerpass', 'marketer003@example.com', '333333333', 2),
('Ethan Johnson', 'marketer004', 'marketerpass', 'marketer004@example.com', '444444444', 2),
('Olivia Thompson', 'marketer005', 'marketerpass', 'marketer005@example.com', '555555555', 2);

-- Sales
INSERT INTO Users (Fullname, Username, Password, Email, Phone, Role)
VALUES
('Jacob Smith', 'sale001', 'salepass', 'sale001@example.com', '111111111', 3),
('Isabella Taylor', 'sale002', 'salepass', 'sale002@example.com', '222222222', 3),
('William Moore', 'sale003', 'salepass', 'sale003@example.com', '333333333', 3),
('Mia Anderson', 'sale004', 'salepass', 'sale004@example.com', '444444444', 3),
('James Martinez', 'sale005', 'salepass', 'sale005@example.com', '555555555', 3);

-- Lecturers
INSERT INTO Users (Fullname, Username, Password, Email, Phone, Role)
VALUES
('Emma Johnson', 'lecturer001', 'lecturerpass', 'lecturer001@example.com', '111111111', 4),
('Noah Wilson', 'lecturer002', 'lecturerpass', 'lecturer002@example.com', '222222222', 4),
('Ava Davis', 'lecturer003', 'lecturerpass', 'lecturer003@example.com', '333333333', 4),
('Liam Thompson', 'lecturer004', 'lecturerpass', 'lecturer004@example.com', '444444444', 4),
('Sophia Brown', 'lecturer005', 'lecturerpass', 'lecturer005@example.com', '555555555', 4);

-- Students
INSERT INTO Users (Fullname, Username, Password, Email, Phone, Role)
VALUES
('Nguyen Dinh Nghia','nghia1708','Nightdark9#','piddfae@gmail.com','0352963942',5),
('Alexander Davis', 'student001', 'studentpass', 'student001@example.com', '111111111', 5),
('Charlotte Johnson', 'student002', 'studentpass', 'student002@example.com', '222222222', 5),
('Benjamin Wilson', 'student003', 'studentpass', 'student003@example.com', '333333333', 5),
('Amelia Thompson', 'student004', 'studentpass', 'student004@example.com', '444444444', 5),
('Elijah Martinez', 'student005', 'studentpass', 'student005@example.com', '555555555', 5);

-- Blog data
INSERT [dbo].[Blogs] ([Title], [Content], [Description], [Image], [Link], [Created_at], [Status]) VALUES (N'Mastering Python: A Step-by-Step Guide', N'Just updated for 2023! It''s time to become a modern and complete Python developer! Join a live online community of over 900,000+ developers and a course taught by an industry expert that has actually worked both in Silicon Valley and Toronto. Graduates of Andrei’s courses are now working at Google, Tesla, Amazon, Apple, IBM, JP Morgan, Meta, + other top tech companies.

Learn Python from scratch, get hired, and have fun along the way with the most modern, up-to-date Python course (we use the latest version of Python). This course is focused on efficiency: never spend time on confusing, out of date, incomplete Python tutorials anymore.

This comprehensive and project based course will introduce you to all of the modern skills of a Python developer (Python 3) and along the way, we will build over 12 real world projects to add to your portfolio (You will get access to all the the code from the 12+ projects we build, so that you can put them on your portfolio right away)!', N'Explore the ins and outs of Python programming and become a proficient developer with this comprehensive guide.', N'blog_1.jpg', N'https://www.youtube.com/watch?v=fAuh0gBjjDI', CAST(N'2023-06-06T22:08:50.837' AS DateTime), 1)
INSERT [dbo].[Blogs] ([Title], [Content], [Description], [Image], [Link], [Created_at], [Status]) VALUES (N'The Art of Web Development: HTML, CSS, and JavaScript', N'This is important to remember. Love isn''t like pie. You don''t need to divide it among all your friends and loved ones. No matter how much love you give, you can always give more. It doesn''t run out, so don''t try to hold back giving it as if it may one day run out. Give it freely and as much as you want.', N'Dive into the world of web development and learn the essential skills of HTML, CSS, and JavaScript to create stunning websites.', N'blog_2.jpg', N'https://www.youtube.com/watch?v=fAuh0gBjjDI', CAST(N'2023-06-06T22:08:50.837' AS DateTime), 1)
INSERT [dbo].[Blogs] ([Title], [Content], [Description], [Image], [Link], [Created_at], [Status]) VALUES (N'Data Science: Unveiling the Power of Data', N'I''m going to hire professional help tomorrow. I can''t handle this anymore. She fell over the coffee table and now there is blood in her catheter. This is much more than I ever signed up to do.

"So, what do you think?" he asked nervously. He wanted to know the answer, but at the same time, he didn''t. He''d put his heart and soul into the project and he wasn''t sure he''d be able to recover if they didn''t like what he produced. The silence from the others in the room seemed to last a lifetime even though it had only been a moment since he asked the question. "So, what do you think?" he asked again.', N'Discover the fascinating realm of data science and unlock valuable insights by analyzing and interpreting data.', N'blog_3.jpg', N'https://www.youtube.com/watch?v=fAuh0gBjjDI', CAST(N'2023-06-06T22:08:50.837' AS DateTime), 1)
INSERT [dbo].[Blogs] ([Title], [Content], [Description], [Image], [Link], [Created_at], [Status]) VALUES (N'The Ultimate Guide to Machine Learning Algorithms', N'She never liked cleaning the sink. It was beyond her comprehension how it got so dirty so quickly. It seemed that she was forced to clean it every other day. Even when she was extra careful to keep things clean and orderly, it still ended up looking like a mess in a couple of days. What she didn''t know was there was a tiny creature living in it that didn''t like things neat.

It really didn''t matter what they did to him. He''s already made up his mind. Whatever came his way, he was prepared for the consequences. He knew in his heart that the sacrifice he made was done with love and not hate no matter how others decided to spin it.

It went through such rapid contortions that the little bear was forced to change his hold on it so many times he became confused in the darkness, and could not, for the life of him, tell whether he held the sheep right side up, or upside down. But that point was decided for him a moment later by the animal itself, who, with a sudden twist, jabbed its horns so hard into his lowest ribs that he gave a grunt of anger and disgust.', N'Delve into the realm of machine learning and understand the inner workings of various algorithms to build intelligent systems.', N'blog_4.jpg', N'https://www.youtube.com/watch?v=fAuh0gBjjDI', CAST(N'2023-06-06T22:08:50.837' AS DateTime), 1)
INSERT [dbo].[Blogs] ([Title], [Content], [Description], [Image], [Link], [Created_at], [Status]) VALUES (N'Becoming a Successful Entrepreneur: Key Strategies', N'They needed to find a place to eat. The kids were beginning to get grumpy in the back seat and if they didn''t find them food soon, it was just a matter of time before they were faced with a complete meltdown. Even knowing this, the solution wasn''t easy. Everyone in the car had a different opinion on where the best place to eat would be with nobody agreeing with the suggestions of the others. It seemed to be an impossible no-win situation where not everyone would be happy no matter where they decided to eat which in itself would lead to a meltdown. Yet a decision needed to be made and it needed to be made quickly.

She sat in the darkened room waiting. It was now a standoff. He had the power to put her in the room, but not the power to make her repent. It wasn''t fair and no matter how long she had to endure the darkness, she wouldn''t change her attitude. At three years old, Sandy''s stubborn personality had already bloomed into full view.

To the two friends, the treehouse was much more than a treehouse. It was a sanctuary away from the other kids where they could be themselves without being teased or bullied. It was their secret fortress hidden high in the branches of a huge oak that only they knew existed. At least that is what they thought. They were more than a little annoyed when their two younger sisters decided to turn the treehouse into a princess castle by painting the inside pink and putting glitter everywhere.', N'Embark on the journey of entrepreneurship and learn the key strategies to establish and grow a successful business.', N'blog_5.jpg', N'https://www.youtube.com/watch?v=fAuh0gBjjDI', CAST(N'2023-06-06T22:08:50.837' AS DateTime), 1)
INSERT [dbo].[Blogs] ([Title], [Content], [Description], [Image], [Link], [Created_at], [Status]) VALUES (N'Mastering Digital Marketing: Reaching Your Target Audience', N'They needed to find a place to eat. The kids were beginning to get grumpy in the back seat and if they didn''t find them food soon, it was just a matter of time before they were faced with a complete meltdown. Even knowing this, the solution wasn''t easy. Everyone in the car had a different opinion on where the best place to eat would be with nobody agreeing with the suggestions of the others. It seemed to be an impossible no-win situation where not everyone would be happy no matter where they decided to eat which in itself would lead to a meltdown. Yet a decision needed to be made and it needed to be made quickly.

She sat in the darkened room waiting. It was now a standoff. He had the power to put her in the room, but not the power to make her repent. It wasn''t fair and no matter how long she had to endure the darkness, she wouldn''t change her attitude. At three years old, Sandy''s stubborn personality had already bloomed into full view.

To the two friends, the treehouse was much more than a treehouse. It was a sanctuary away from the other kids where they could be themselves without being teased or bullied. It was their secret fortress hidden high in the branches of a huge oak that only they knew existed. At least that is what they thought. They were more than a little annoyed when their two younger sisters decided to turn the treehouse into a princess castle by painting the inside pink and putting glitter everywhere.', N'Learn the art of digital marketing and explore effective strategies to reach and engage your target audience online.', N'blog_6.jpg', N'https://www.youtube.com/watch?v=fAuh0gBjjDI', CAST(N'2023-06-06T22:08:50.837' AS DateTime), 1)
INSERT [dbo].[Blogs] ([Title], [Content], [Description], [Image], [Link], [Created_at], [Status]) VALUES (N'Unlocking the World of Cryptocurrency: Bitcoin and Beyond', N'They needed to find a place to eat. The kids were beginning to get grumpy in the back seat and if they didn''t find them food soon, it was just a matter of time before they were faced with a complete meltdown. Even knowing this, the solution wasn''t easy. Everyone in the car had a different opinion on where the best place to eat would be with nobody agreeing with the suggestions of the others. It seemed to be an impossible no-win situation where not everyone would be happy no matter where they decided to eat which in itself would lead to a meltdown. Yet a decision needed to be made and it needed to be made quickly.
She sat in the darkened room waiting. It was now a standoff. He had the power to put her in the room, but not the power to make her repent. It wasn''t fair and no matter how long she had to endure the darkness, she wouldn''t change her attitude. At three years old, Sandy''s stubborn personality had already bloomed into full view.
To the two friends, the treehouse was much more than a treehouse. It was a sanctuary away from the other kids where they could be themselves without being teased or bullied. It was their secret fortress hidden high in the branches of a huge oak that only they knew existed. At least that is what they thought. They were more than a little annoyed when their two younger sisters decided to turn the treehouse into a princess castle by painting the inside pink and putting glitter everywhere.', N'Dive into the realm of cryptocurrencies and blockchain and understand the technology behind Bitcoin and other digital currencies.', N'blog_7.jpg', N'https://www.youtube.com/watch?v=fAuh0gBjjDI', CAST(N'2023-06-06T22:08:50.837' AS DateTime), 1)
INSERT [dbo].[Blogs] ([Title], [Content], [Description], [Image], [Link], [Created_at], [Status]) VALUES (N'The Power of UX Design: Creating Engaging User Experiences', N'They needed to find a place to eat. The kids were beginning to get grumpy in the back seat and if they didn''t find them food soon, it was just a matter of time before they were faced with a complete meltdown. Even knowing this, the solution wasn''t easy. Everyone in the car had a different opinion on where the best place to eat would be with nobody agreeing with the suggestions of the others. It seemed to be an impossible no-win situation where not everyone would be happy no matter where they decided to eat which in itself would lead to a meltdown. Yet a decision needed to be made and it needed to be made quickly.

She sat in the darkened room waiting. It was now a standoff. He had the power to put her in the room, but not the power to make her repent. It wasn''t fair and no matter how long she had to endure the darkness, she wouldn''t change her attitude. At three years old, Sandy''s stubborn personality had already bloomed into full view.

To the two friends, the treehouse was much more than a treehouse. It was a sanctuary away from the other kids where they could be themselves without being teased or bullied. It was their secret fortress hidden high in the branches of a huge oak that only they knew existed. At least that is what they thought. They were more than a little annoyed when their two younger sisters decided to turn the treehouse into a princess castle by painting the inside pink and putting glitter everywhere.', N'Discover the importance of UX design and learn how to create intuitive and engaging user experiences for digital products.', N'blog_8.jpg', N'https://www.youtube.com/watch?v=fAuh0gBjjDI', CAST(N'2023-06-06T22:08:50.837' AS DateTime), 1)
INSERT [dbo].[Blogs] ([Title], [Content], [Description], [Image], [Link], [Created_at], [Status]) VALUES (N'Effective Project Management: From Planning to Execution', N'They needed to find a place to eat. The kids were beginning to get grumpy in the back seat and if they didn''t find them food soon, it was just a matter of time before they were faced with a complete meltdown. Even knowing this, the solution wasn''t easy. Everyone in the car had a different opinion on where the best place to eat would be with nobody agreeing with the suggestions of the others. It seemed to be an impossible no-win situation where not everyone would be happy no matter where they decided to eat which in itself would lead to a meltdown. Yet a decision needed to be made and it needed to be made quickly.

She sat in the darkened room waiting. It was now a standoff. He had the power to put her in the room, but not the power to make her repent. It wasn''t fair and no matter how long she had to endure the darkness, she wouldn''t change her attitude. At three years old, Sandy''s stubborn personality had already bloomed into full view.

To the two friends, the treehouse was much more than a treehouse. It was a sanctuary away from the other kids where they could be themselves without being teased or bullied. It was their secret fortress hidden high in the branches of a huge oak that only they knew existed. At least that is what they thought. They were more than a little annoyed when their two younger sisters decided to turn the treehouse into a princess castle by painting the inside pink and putting glitter everywhere.', N'Master the art of project management and learn how to effectively plan, execute, and deliver successful projects.', N'blog_9.jpg', N'https://www.youtube.com/watch?v=fAuh0gBjjDI', CAST(N'2023-06-06T22:08:50.837' AS DateTime), 1)
INSERT [dbo].[Blogs] ([Title], [Content], [Description], [Image], [Link], [Created_at], [Status]) VALUES (N'The World of Artificial Intelligence: Future Possibilities', N'They needed to find a place to eat. The kids were beginning to get grumpy in the back seat and if they didn''t find them food soon, it was just a matter of time before they were faced with a complete meltdown. Even knowing this, the solution wasn''t easy. Everyone in the car had a different opinion on where the best place to eat would be with nobody agreeing with the suggestions of the others. It seemed to be an impossible no-win situation where not everyone would be happy no matter where they decided to eat which in itself would lead to a meltdown. Yet a decision needed to be made and it needed to be made quickly.

She sat in the darkened room waiting. It was now a standoff. He had the power to put her in the room, but not the power to make her repent. It wasn''t fair and no matter how long she had to endure the darkness, she wouldn''t change her attitude. At three years old, Sandy''s stubborn personality had already bloomed into full view.

To the two friends, the treehouse was much more than a treehouse. It was a sanctuary away from the other kids where they could be themselves without being teased or bullied. It was their secret fortress hidden high in the branches of a huge oak that only they knew existed. At least that is what they thought. They were more than a little annoyed when their two younger sisters decided to turn the treehouse into a princess castle by painting the inside pink and putting glitter everywhere.', N'Explore the vast possibilities of artificial intelligence and understand how it is shaping the future in various domains.', N'blog_10.jpg', N'https://www.youtube.com/watch?v=fAuh0gBjjDI', CAST(N'2023-06-06T22:08:50.837' AS DateTime), 1)
INSERT [dbo].[Blogs] ([Title], [Content], [Description], [Image], [Link], [Created_at], [Status]) VALUES (N'Mastering JavaScript: A Step-by-Step Guide', N'Just updated for 2023! It''s time to become a modern and complete Python developer! Join a live online community of over 900,000+ developers and a course taught by an industry expert that has actually worked both in Silicon Valley and Toronto. Graduates of Andrei’s courses are now working at Google, Tesla, Amazon, Apple, IBM, JP Morgan, Meta, + other top tech companies. Learn Python from scratch, get hired, and have fun along the way with the most modern, up-to-date Python course (we use the latest version of Python). This course is focused on efficiency: never spend time on confusing, out of date, incomplete Python tutorials anymore. This comprehensive and project based course will introduce you to all of the modern skills of a Python developer (Python 3) and along the way, we will build over 12 real world projects to add to your portfolio (You will get access to all the the code from the 12+ projects we build, so that you can put them on your portfolio right away)!', N'Explore the ins and outs of Python programming and become a proficient developer with this comprehensive guide.', N'blog_2.jpg', N'https://www.youtube.com/watch?v=fAuh0gBjjDI', CAST(N'2023-06-12T11:38:31.863' AS DateTime), 1)
INSERT [dbo].[Blogs] ([Title], [Content], [Description], [Image], [Link], [Created_at], [Status]) VALUES (N'The World of Artificial Intelligence: Future Possibilities', N'They needed to find a place to eat. The kids were beginning to get grumpy in the back seat and if they didn''t find them food soon, it was just a matter of time before they were faced with a complete meltdown. Even knowing this, the solution wasn''t easy. Everyone in the car had a different opinion on where the best place to eat would be with nobody agreeing with the suggestions of the others. It seemed to be an impossible no-win situation where not everyone would be happy no matter where they decided to eat which in itself would lead to a meltdown. Yet a decision needed to be made and it needed to be made quickly. She sat in the darkened room waiting. It was now a standoff. He had the power to put her in the room, but not the power to make her repent. It wasn''t fair and no matter how long she had to endure the darkness, she wouldn''t change her attitude. At three years old, Sandy''s stubborn personality had already bloomed into full view. To the two friends, the treehouse was much more than a treehouse. It was a sanctuary away from the other kids where they could be themselves without being teased or bullied. It was their secret fortress hidden high in the branches of a huge oak that only they knew existed. At least that is what they thought. They were more than a little annoyed when their two younger sisters decided to turn the treehouse into a princess castle by painting the inside pink and putting glitter everywhere.', N'Explore the vast possibilities of artificial intelligence and understand how it is shaping the future in various domains.', N'blog_3.jpg', N'https://www.youtube.com/watch?v=fAuh0gBjjDI', CAST(N'2023-06-16T23:53:00.273' AS DateTime), 1)
GO
SET IDENTITY_INSERT [dbo].[Slider] ON
INSERT [dbo].[Slider] ([Id], [Title], [Description], [Image], [Link], [Status]) VALUES (1, N'HTML CSS Pro Course', N'This is the most complete and detailed course you can find on the Internet!', N'slider_1.jpg', N'https://youtu.be/bv16wjxgV4U', 1)
INSERT [dbo].[Slider] ([Id], [Title], [Description], [Image], [Link], [Status]) VALUES (2, N'Learn ReactJS for free', N'ReactJS course from basic to advanced. The result of this course is that you can do most common projects with ReactJS.', N'slider_2.jpg', N'https://youtu.be/bv16wjxgV4U', 1)
INSERT [dbo].[Slider] ([Id], [Title], [Description], [Image], [Link], [Status]) VALUES (3, N'Student achievements', N'To achieve good results in everything we need to define clear goals for it. ', N'slider_3.jpg', N'https://youtu.be/bv16wjxgV4U', 1)
INSERT [dbo].[Slider] ([Id], [Title], [Description], [Image], [Link], [Status]) VALUES (4, N'EDUBIN on Youtube', N'EDUBIN is mentioned everywhere, where there are job opportunities for IT profession and there are people who love EDUBIN will be there.', N'slider_4.jpg', N'https://youtu.be/bv16wjxgV4U', 1)
INSERT [dbo].[Slider] ([Id], [Title], [Description], [Image], [Link], [Status]) VALUES (5, N'EDUBIN on Facebook', N'EDUBIN is mentioned everywhere, where there are job opportunities for IT profession and there are people who love EDUBIN will be there.', N'slider_5.jpg', N'https://youtu.be/bv16wjxgV4U', 1)
SET IDENTITY_INSERT [dbo].[Slider] OFF
GO

	-- Insert data into Subjects table
INSERT INTO Subjects (name, description)
VALUES ('Mathematics', 'The study of numbers, quantities, and shapes'),
       ('English', 'The study of the English language and literature'),
       ('Biology', 'The study of living organisms and their interactions'),
       ('Chemistry', 'The study of matter and its properties'),
       ('Physics', 'The study of matter and energy and their interactions'),
       ('History', 'The study of past events and societies'),
       ('Art History', 'The study of visual art and its history'),
       ('Philosophy', 'The study of fundamental questions about existence, knowledge, values, reason, mind, and language'),
       ('Computer Science', 'The study of computers and computational systems'),
       ('Psychology', 'The study of the human mind and behavior'),
       ('Economics', 'The study of the production, distribution, and consumption of goods and services'),
       ('Sociology', 'The study of human society and social behavior'),
       ('Geography', 'The study of the Earth''s physical features, climate, and human populations'),
       ('Political Science', 'The study of government systems, political behavior, and public policies');




-- FAKE COURSE DATA
INSERT INTO Courses (Name_course, Description, Objective, Price, Active, Subject_Id)
VALUES
('Algebra', 'Introduction to algebraic concepts and equations.', 'To introduce students to algebraic concepts and equations.', 0, 1, 1),
('Calculus', 'Study of change and motion through mathematical methods.', 'To provide students with a comprehensive understanding of calculus and its applications.', 0, 1, 1),
('Mechanics', 'Study of forces and motion in the physical world.', 'To explore the principles of mechanics and their relevance in the physical world.', 0, 1, 5),
('Programming Fundamentals', 'Introduction to programming concepts and problem-solving.', 'To introduce students to programming concepts and develop problem-solving skills.', 0, 1, 9),
('Statistics', 'Study of data collection, analysis, interpretation, and presentation.', 'To provide students with a solid foundation in statistical analysis and data interpretation.', 0, 1, 1),
('Differential Equations', 'Study of equations involving derivatives and their applications.', 'To explore differential equations and their various applications.', 0, 1, 1),
('Electromagnetism', 'Study of electromagnetic fields and their effects.', 'To understand electromagnetic fields and their effects.', 0, 1, 2),
('Database Management', 'Introduction to the design and management of relational databases.', 'To introduce students to database design and management.', 0, 1, 9),
('Web Development', 'Building websites and web applications using HTML, CSS, and JavaScript.', 'To teach students web development using HTML, CSS, and JavaScript.', 0, 1, 9),
('Artificial Intelligence', 'Study of intelligent agents and machine learning algorithms.', 'To explore artificial intelligence concepts and machine learning algorithms.', 0, 1, 9)


-- Material_types Data
INSERT INTO [dbo].[Material_types] ([Type_name])
VALUES
   ('Textbook'),
   ('Lecture Notes'),
   ('Video'),
   ('Reference');

-- FAKE CHAPTER DATA

--- !Algebra ---
INSERT INTO [dbo].[Chapters] ([Name], [Chapter_no], [Objective], [Courses_Id])
VALUES
   ('Introduction to Variables and Expressions', 1, 'Learn about variables and expressions in algebra.', 1),
   ('Solving Linear Equations', 2, 'Explore methods for solving linear equations.', 1),
   ('Graphing Linear Equations', 3, 'Learn how to graph linear equations on a coordinate plane.', 1),
   ('Systems of Equations', 4, 'Study systems of equations and their solutions.', 1),
   ('Exponents and Polynomials', 5, 'Understand exponents and polynomials in algebra.', 1);

	-- Algebra Lessons
INSERT INTO [dbo].[Lessons] ([Lesson_no], [Name], [Chapters_Id])
VALUES
   -- Introduction to Variables and Expressions
   (1, 'Introduction to Variables', 1),
   (2, 'Introduction to Expressions', 1),
   (3, 'Evaluating Expressions', 1),
   
   -- Solving Linear Equations
   (1, 'Solving Linear Equations: One Variable', 2),
   (2, 'Solving Linear Equations: Multi-Variable', 2),
   (3, 'Applications of Linear Equations', 2),
   
   -- Graphing Linear Equations
   (1, 'Plotting Points on a Coordinate Plane', 3),
   (2, 'Graphing Linear Equations: Slope-Intercept Form', 3),
   (3, 'Graphing Linear Equations: Standard Form', 3),
   
   -- Systems of Equations
   (1, 'Solving Systems of Equations: Substitution Method', 4),
   (2, 'Solving Systems of Equations: Elimination Method', 4),
   (3, 'Applications of Systems of Equations', 4),
   
   -- Exponents and Polynomials
   (1, 'Introduction to Exponents', 5),
   (2, 'Properties of Exponents', 5),
   (3, 'Polynomials and Polynomial Operations', 5);

-- Algebra Lesson Materials
-- Introduction to Variables and Expressions
INSERT INTO [dbo].[Materials] ([Material_no], [Text], [Material_link], [Lesson_Id], [Material_type_Id])
VALUES
   (1, 'Worksheet: Variables and Expressions', 'https://example.com/worksheet_variables_expressions.pdf', 1, 1),
   (2, 'Video: Introduction to Variables', 'https://www.youtube.com/embed/tHYis-DP0oU', 2, 3),
   (3, 'Article: Understanding Expressions in Algebra', 'https://example.com/article_expressions_algebra', 3, 4);

-- Solving Linear Equations
INSERT INTO [dbo].[Materials] ([Material_no], [Text], [Material_link], [Lesson_Id], [Material_type_Id])
VALUES
   (1, 'Practice Problems: Solving Linear Equations', 'https://example.com/practice_problems_linear_equations.pdf', 4, 1),
   (2, 'Video Tutorial: Solving Linear Equations Step-by-Step', 'https://www.youtube.com/watch?v=7DPWeBszNSM&pp=ygUlU29sdmluZyBMaW5lYXIgRXF1YXRpb25zIFN0ZXAtYnktU3RlcA%3D%3D', 5, 3),
   (3, 'Interactive Activity: Solving Linear Equations Game', 'https://example.com/interactive_activity_linear_equations_game', 6, 4);

-- Graphing Linear Equations
INSERT INTO [dbo].[Materials] ([Material_no], [Text], [Material_link], [Lesson_Id], [Material_type_Id])
VALUES
   (1, 'Worksheet: Graphing Linear Equations', 'https://example.com/worksheet_graphing_linear_equations.pdf', 7, 1),
   (2, 'Video: Introduction to Graphing Linear Equations', 'https://example.com/video_intro_graphing_linear_equations', 8, 3),
   (3, 'Online Graphing Tool: Plotting Linear Equations', 'https://example.com/online_graphing_tool_linear_equations', 9, 4);

-- Systems of Equations
INSERT INTO [dbo].[Materials] ([Material_no], [Text], [Material_link], [Lesson_Id], [Material_type_Id])
VALUES
   (1, 'Worksheet: Systems of Equations Practice', 'https://example.com/worksheet_systems_equations_practice.pdf', 10, 1),
   (2, 'Video Tutorial: Solving Systems of Equations with Substitution', 'https://example.com/video_tutorial_systems_equations_substitution', 11, 3),
   (3, 'Article: Real-World Applications of Systems of Equations', 'https://example.com/article_systems_equations_applications', 12, 4);

-- Exponents and Polynomials
INSERT INTO [dbo].[Materials] ([Material_no], [Text], [Material_link], [Lesson_Id], [Material_type_Id])
VALUES
   (1, 'Worksheet: Exponents and Polynomials', 'https://example.com/worksheet_exponents_polynomials.pdf', 13, 1),
   (2, 'Video: Introduction to Exponents', 'https://example.com/video_intro_exponents', 14, 3),
   (3, 'Article: Polynomial Operations Explained', 'https://example.com/article_polynomial_operations', 15, 4);


--- !END OF ALGEBRA----

-- Calculus
INSERT INTO [dbo].[Chapters] ([Name], [Chapter_no], [Objective], [Courses_Id])
VALUES
   ('Limits and Continuity', 1, 'Learn about limits and continuity in calculus.', 2),
   ('Differentiation', 2, 'Study the concept of differentiation.', 2),
   ('Applications of Differentiation', 3, 'Explore various applications of differentiation.', 2),
   ('Integration', 4, 'Understand the process of integration.', 2),
   ('Applications of Integration', 5, 'Learn about different applications of integration.', 2);

-- Mechanics
INSERT INTO [dbo].[Chapters] ([Name], [Chapter_no], [Objective], [Courses_Id])
VALUES
   ('Introduction to Motion', 1, 'Get an introduction to the concept of motion.', 3),
   ('Newton''s Laws of Motion', 2, 'Study Newton''s laws of motion.', 3),
   ('Forces and Friction', 3, 'Learn about forces and friction.', 3),
   ('Work, Energy, and Power', 4, 'Understand the concepts of work, energy, and power.', 3),
   ('Momentum and Collisions', 5, 'Explore the concepts of momentum and collisions.', 3);

-- Programming Fundamentals
INSERT INTO [dbo].[Chapters] ([Name], [Chapter_no], [Objective], [Courses_Id])
VALUES
   ('Introduction to Programming Concepts', 1, 'Get an introduction to programming concepts.', 4),
   ('Variables and Data Types', 2, 'Learn about variables and data types in programming.', 4),
   ('Control Structures (if statements, loops)', 3, 'Study control structures like if statements and loops.', 4),
   ('Functions and Modular Programming', 4, 'Understand functions and modular programming.', 4),
   ('Arrays and Lists', 5, 'Explore arrays and lists in programming.', 4);

-- Statistics
INSERT INTO [dbo].[Chapters] ([Name], [Chapter_no], [Objective], [Courses_Id])
VALUES
   ('Introduction to Data and Data Collection', 1, 'Get an introduction to data and data collection in statistics.', 5),
   ('Descriptive Statistics', 2, 'Learn about descriptive statistics.', 5),
   ('Probability', 3, 'Study probability theory and concepts.', 5),
   ('Statistical Distributions', 4, 'Explore different statistical distributions.', 5),
   ('Hypothesis Testing', 5, 'Understand the concept of hypothesis testing.', 5);

-- Differential Equations
INSERT INTO [dbo].[Chapters] ([Name], [Chapter_no], [Objective], [Courses_Id])
VALUES
   ('First-Order Differential Equations', 1, 'Study first-order differential equations.', 6),
   ('Second-Order Differential Equations', 2, 'Explore second-order differential equations.', 6),
   ('Systems of Differential Equations', 3, 'Understand systems of differential equations.', 6),
   ('Power Series Solutions', 4, 'Learn about power series solutions in differential equations.', 6),
   ('Boundary Value Problems', 5, 'Study boundary value problems in differential equations.', 6);

-- Electromagnetism
INSERT INTO [dbo].[Chapters] ([Name], [Chapter_no], [Objective], [Courses_Id])
VALUES
   ('Electric Fields and Forces', 1, 'Study electric fields and forces.', 7),
   ('Gauss''s Law', 2, 'Learn about Gauss''s law in electromagnetism.', 7),
   ('Electric Potential', 3, 'Understand electric potential in electromagnetism.', 7),
   ('Magnetic Fields and Forces', 4, 'Explore magnetic fields and forces.', 7),
   ('Electromagnetic Waves', 5, 'Study electromagnetic waves and their properties.', 7);

-- Database Management
INSERT INTO [dbo].[Chapters] ([Name], [Chapter_no], [Objective], [Courses_Id])
VALUES
   ('Introduction to Databases', 1, 'Get an introduction to databases.', 8),
   ('Relational Data Model', 2, 'Learn about the relational data model.', 8),
   ('SQL Queries and Joins', 3, 'Study SQL queries and joins.', 8),
   ('Database Design and Normalization', 4, 'Understand database design and normalization.', 8),
   ('Transaction Management', 5, 'Explore transaction management in databases.', 8);

-- Web Development
INSERT INTO [dbo].[Chapters] ([Name], [Chapter_no], [Objective], [Courses_Id])
VALUES
   ('Introduction to HTML', 1, 'Get an introduction to HTML.', 9),
   ('CSS Styling and Layout', 2, 'Learn about CSS styling and layout.', 9),
   ('JavaScript Fundamentals', 3, 'Study JavaScript fundamentals.', 9),
   ('DOM Manipulation', 4, 'Explore DOM manipulation using JavaScript.', 9),
   ('Web Development Frameworks', 5, 'Learn about web development frameworks.', 9);
   
-- Artificial Intelligence
INSERT INTO [dbo].[Chapters] ([Name], [Chapter_no], [Objective], [Courses_Id])
VALUES
   ('Introduction to AI', 1, 'Get an introduction to artificial intelligence.', 10),
   ('Search Algorithms', 2, 'Study search algorithms in AI.', 10),
   ('Machine Learning Basics', 3, 'Learn about the basics of machine learning.', 10),
   ('Neural Networks', 4, 'Explore neural networks in AI.', 10),
   ('Natural Language Processing', 5, 'Study natural language processing techniques.', 10);


