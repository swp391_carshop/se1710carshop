-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2023-06-24 14:07:21.624
CREATE DATABASE Online_Learning
USE Online_Learning
-- tables
-- Table: Blogs
CREATE TABLE Blogs (
    Id int  NOT NULL IDENTITY(1, 1),
    Title nvarchar(max)  NOT NULL,
    Content ntext  NOT NULL,
    Description nvarchar(max)  NOT NULL,
    Image nvarchar(max)  NOT NULL,
    Link nvarchar(max)  NOT NULL,
    Created_at datetime  NOT NULL DEFAULT getdate(),
	Status bit NOT NULL,
    CONSTRAINT Blogs_pk PRIMARY KEY  (Id)
);

-- Table: Chapters
CREATE TABLE Chapters (
    Id int  NOT NULL IDENTITY(1, 1),
    Name nvarchar(max)  NOT NULL,
    Chapter_no int  NOT NULL,
    Objective nvarchar(max)  NOT NULL,
    Courses_Id int  NOT NULL,
    CONSTRAINT Chapters_pk PRIMARY KEY  (Id)
);

-- Table: Courses
CREATE TABLE Courses (
    Id int  NOT NULL IDENTITY(1, 1),
    Name_course nvarchar(max)  NOT NULL,
    Description ntext  NOT NULL,
    Objective ntext  NOT NULL,
    Price decimal(8,2)  NOT NULL,
    Active tinyint  NOT NULL,
    Subject_Id int  NOT NULL,
    CONSTRAINT Courses_pk PRIMARY KEY  (Id)
);

-- Table: Enrolled_course
CREATE TABLE Enrolled_course (
    Course_Id int  NOT NULL,
    Students_Id int  NOT NULL,
    Enrollment_Date datetime  NOT NULL DEFAULT getdate(),
    Status int  NOT NULL,
    CONSTRAINT Enrolled_course_pk PRIMARY KEY  (Course_Id,Students_Id)
);

-- Table: Lessons
CREATE TABLE Lessons (
    Id int  NOT NULL IDENTITY(1, 1),
    Lesson_no int  NOT NULL,
    Name nvarchar(max)  NOT NULL,
    Chapters_Id int  NOT NULL,
    CONSTRAINT Lessons_pk PRIMARY KEY  (Id)
);

-- Table: Material_types
CREATE TABLE Material_types (
    Id int  NOT NULL IDENTITY(1, 1),
    Type_name nvarchar(max)  NOT NULL,
    CONSTRAINT Material_types_pk PRIMARY KEY  (Id)
);

-- Table: Materials
CREATE TABLE Materials (
    Material_Id int  NOT NULL IDENTITY(1, 1),
    Material_no int  NOT NULL,
    Text ntext  NOT NULL,
    Material_link nvarchar(max)  NOT NULL,
    Lesson_Id int  NOT NULL,
    Material_type_Id int  NOT NULL,
    CONSTRAINT Materials_pk PRIMARY KEY  (Material_Id)
);

-- Table: On_course
CREATE TABLE On_course (
    Courses_Id int  NOT NULL,
    Users_Id int  NOT NULL,
    CONSTRAINT On_course_pk PRIMARY KEY  (Courses_Id,Users_Id)
);

-- Table: Question
CREATE TABLE Question (
    Id int  NOT NULL IDENTITY(1, 1),
    Quiz_Id int  NOT NULL,
    Question_Text nvarchar(max)  NOT NULL,
    OptionA ntext  NOT NULL,
    OptionB ntext  NOT NULL,
    OptionC ntext  NOT NULL,
    OptionD ntext  NOT NULL,
    Answer ntext  NOT NULL,
    CONSTRAINT Question_pk PRIMARY KEY  (Id)
);

-- Table: Quiz
CREATE TABLE Quiz (
    Id int  NOT NULL IDENTITY(1, 1),
    Name nvarchar(max)  NOT NULL,
    Chapters_Id int  NOT NULL,
    CONSTRAINT Quiz_pk PRIMARY KEY  (Id)
);

-- Table: Quiz_Results
CREATE TABLE Quiz_Results (
    Users_Id int  NOT NULL,
    Quiz_Id int  NOT NULL,
    Is_Pass bit  NOT NULL,
    CONSTRAINT Quiz_Results_pk PRIMARY KEY  (Users_Id,Quiz_Id)
);

-- Table: Slider
CREATE TABLE Slider (
    Id int  NOT NULL IDENTITY(1, 1),
    Title nvarchar(max)  NOT NULL,
	Description nvarchar(max) NULL,
    Image nvarchar(max)  NOT NULL,
    Link nvarchar(max)  NOT NULL,
    Status bit  NOT NULL,
    CONSTRAINT Slider_pk PRIMARY KEY  (Id)
);

-- Table: Subjects
CREATE TABLE Subjects (
    Id int  NOT NULL IDENTITY(1, 1),
    Name nvarchar(max)  NOT NULL,
    Description nvarchar(max)  NOT NULL,
    CONSTRAINT Subjects_pk PRIMARY KEY  (Id)
);

-- Table: Users
CREATE TABLE Users (
    Id int  NOT NULL IDENTITY(1, 1),
    Fullname nvarchar(max)  NOT NULL,
    Username nvarchar(max)  NOT NULL,
    Password nvarchar(max)  NOT NULL,
    Email nvarchar(max)  NOT NULL,
    Phone nvarchar(max)  NOT NULL,
    Role int  NOT NULL,
    CONSTRAINT Users_pk PRIMARY KEY  (Id)
);

-- foreign keys
-- Reference: Chaper_Course (table: Chapters)
ALTER TABLE Chapters ADD CONSTRAINT Chaper_Course
    FOREIGN KEY (Courses_Id)
    REFERENCES Courses (Id);

-- Reference: Course_Subject (table: Courses)
ALTER TABLE Courses ADD CONSTRAINT Course_Subject
    FOREIGN KEY (Subject_Id)
    REFERENCES Subjects (Id);

-- Reference: Enrolled_course_Course (table: Enrolled_course)
ALTER TABLE Enrolled_course ADD CONSTRAINT Enrolled_course_Course
    FOREIGN KEY (Course_Id)
    REFERENCES Courses (Id);

-- Reference: Enrolled_course_Students (table: Enrolled_course)
ALTER TABLE Enrolled_course ADD CONSTRAINT Enrolled_course_Students
    FOREIGN KEY (Students_Id)
    REFERENCES Users (Id);

-- Reference: Lesson_Chaper (table: Lessons)
ALTER TABLE Lessons ADD CONSTRAINT Lesson_Chaper
    FOREIGN KEY (Chapters_Id)
    REFERENCES Chapters (Id);

-- Reference: Material_Chapter (table: Materials)
ALTER TABLE Materials ADD CONSTRAINT Material_Chapter
    FOREIGN KEY (Lesson_Id)
    REFERENCES Lessons (Id);

-- Reference: Material_Material_type (table: Materials)
ALTER TABLE Materials ADD CONSTRAINT Material_Material_type
    FOREIGN KEY (Material_type_Id)
    REFERENCES Material_types (Id);

-- Reference: On_course_Users (table: On_course)
ALTER TABLE On_course ADD CONSTRAINT On_course_Users
    FOREIGN KEY (Users_Id)
    REFERENCES Users (Id);

-- Reference: Question_Quiz (table: Question)
ALTER TABLE Question ADD CONSTRAINT Question_Quiz
    FOREIGN KEY (Quiz_Id)
    REFERENCES Quiz (Id);

-- Reference: Quiz_Chapters (table: Quiz)
ALTER TABLE Quiz ADD CONSTRAINT Quiz_Chapters
    FOREIGN KEY (Chapters_Id)
    REFERENCES Chapters (Id);

-- Reference: Quiz_Results_Quiz (table: Quiz_Results)
ALTER TABLE Quiz_Results ADD CONSTRAINT Quiz_Results_Quiz
    FOREIGN KEY (Quiz_Id)
    REFERENCES Quiz (Id);

-- Reference: Quiz_Results_Users (table: Quiz_Results)
ALTER TABLE Quiz_Results ADD CONSTRAINT Quiz_Results_Users
    FOREIGN KEY (Users_Id)
    REFERENCES Users (Id);

-- Reference: on_course_Course (table: On_course)
ALTER TABLE On_course ADD CONSTRAINT on_course_Course
    FOREIGN KEY (Courses_Id)
    REFERENCES Courses (Id);

-- End of file.

-- Create Full Text Catalog for Blogs
CREATE FULLTEXT CATALOG Search_Blog WITH ACCENT_SENSITIVITY = OFF;

-- Create Full Text Index in Blogs table
CREATE FULLTEXT INDEX ON Blogs (Title LANGUAGE 1056) -- Assuming Title is the column name for the title
   KEY INDEX Blogs_pk
      ON Search_Blog
   WITH (CHANGE_TRACKING = AUTO);