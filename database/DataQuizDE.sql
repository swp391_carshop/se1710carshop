insert into Quiz(Name, Chapters_Id)
values
('Quiz DE - 1', 1),
('Quiz DE - 2', 2),
('Quiz DE - 3', 3),
('Quiz DE - 4', 4),
('Quiz DE - 5', 5);

insert into Question(Quiz_Id, Question_Text, OptionA, OptionB, OptionC, OptionD, Answer)
values
(1,'Which one of these skills is essential to the role of a Data Engineer?','To setup and manage the infrastructure required for the ingestion, processing, and storage of data','Proficiency in creating Deep Learning models','To inspect analytics-ready data for deriving insights','Proficiency in Statistics','To setup and manage the infrastructure required for the ingestion, processing, and storage of data'),
(1,'What according to Sarah Flinch, needs to be tracked and analyzed in order to keep business updated on the overall sentiment of the consumers?','Social media posts, customer reviews and ratings on eCommerce platforms, and product reviews on blogging sites','eCommerce platforms','Social media sites','Blogging sites','Social media posts, customer reviews and ratings on eCommerce platforms, and product reviews on blogging sites'),
(1,'Which one of these functional skills is essential to the role of a Data Engineer?','The ability to work with the software development lifecycle','Proficiency in working with ETL Tools','Proficiency in Mathematics','Inspect analytics-ready data for deriving insights','The ability to work with the software development lifecycle'),
(1,'Oracle Exadata, IBM Db2 Warehouse on Cloud, IBM Netezza Performance Server, and Amazon RedShift are some of the popular __________________ in use today.','Data Warehouses','ETL Tools','NoSQL Databases','Big Data Platforms','Data Warehouses'),
(1,'To ensure business stakeholders can see real-time data each time they log into the dashboard, Sarah decided to build _______________ to extract, transform, and load data on an ongoing basis.','A Data Pipeline','A sentiment analysis algorithm','APIs','A Python program','A Data Pipeline'),
(2,'Automated tools, frameworks, and processes for all stages of the data analytics process are part of the Data Engineer�s ecosystem. What role do data integration tools play in this ecosystem?','Combine data from multiple sources into a unified view that is accessed by data consumers to query and manipulate data','Cover the entire journey of data from source to destination','Store high-volume day-to-day operational data in data repositories','Conduct complex data analytics','Combine data from multiple sources into a unified view that is accessed by data consumers to query and manipulate data'),
(2,'Which of these data sources is an example of semi-structured data?','Emails','Documents','Network and web logs','Social media feeds','Emails'),
(2,'Which one of the provided file formats is commonly used by APIs and Web Services to return data?','JSON','XML','XLS','Delimited file','JSON'),
(2,'What is one example of the relational databases discussed in the video?','SQL Server','Flat files','XML','Spreadsheet','SQL Server'),
(2,'Which of the following languages is one of the most popular querying languages in use today?','SQL','Python','Java','R','SQL');


insert into Question(Quiz_Id, Question_Text,OptionA,OptionB,OptionC,OptionD,Answer)
values
(3, 'Data Marts and Data Warehouses have typically been relational, but the emergence of what technology has helped to let these be used for non-relational data?','NoSQL','SQL','Data Lake','ETL','NoSQL'),
(3, 'What is one of the most significant advantages of an RDBMS?','Enforces a limit on the length of data fields','Can store only structured data','Is ACID-Compliant','Requires source and destination tables to be identical for migrating data','Is ACID-Compliant'),
(3, 'Which one of the NoSQL database types uses a graphical model to represent and store data, and is particularly useful for visualizing, analyzing, and finding connections between different pieces of data?','Key value store','Document-based','Column-based','Graph-based','Graph-based'),
(3, 'Which of the data repositories serves as a pool of raw data and stores large amounts of structured, semi-structured, and unstructured data in their native formats?','Data Warehouses','Data Marts','Relational Databases','Data Lakes','Data Lakes'),
(3, 'While data integration combines disparate data into a unified view of the data, a data pipeline covers the entire data movement journey from source to destination systems, and ETL is a process within data integration.','True','False','','','True');

insert into Question(Quiz_Id, Question_Text,OptionA,OptionB,OptionC,OptionD,Answer)
values
(4, 'What does the attribute �Veracity� imply in the context of Big Data?', 'Scale of data', 'The speed at which data accumulates', 'Accuracy and conformity of data to facts', 'Diversity of the type and sources of data', 'Accuracy and conformity of data to facts'),
(4, '____________, in the context of Big Data, is the speed at which data accumulates.', 'Velocity', 'Volume', 'Value', 'Variety', 'Velocity'),
(4, 'What does the attribute �Value� imply in the context of Big Data?', 'The diversity of the type and sources of data', 'The accuracy and conformity of data to facts', 'Our ability and need to turn data into value', 'The speed at which data accumulates', 'Our ability and need to turn data into value'),
(4, 'Apache Spark is a general-purpose data processing engine designed to extract and process Big Data for a wide range of applications. What is one of its key use cases?', 'Consolidate data across the organization', 'Perform complex analytics in real-time', 'Scalable and reliable Big Data storage', 'Fast recovery from hardware failures', 'Scalable and reliable Big Data storage'),
(4, 'Which of the Big Data processing tools is used for reading, writing, and managing large data set files that are stored in either HDFS or Apache HBase?', 'Hive', 'ETL', 'Hadoop', 'Spark', 'Hive'),
(5, 'Which one of these steps is an intrinsic part of the �Data Processing Layer� of a data platform?', 'Transform and merge extracted data, either logically or physically', 'Read data in batch or streaming modes from storage and apply transformations', 'Deliver processed data to data consumers', 'Transfer data from data sources to the data platform in streaming, batch, or both modes', 'Read data in batch or streaming modes from storage and apply transformations'),
(5, 'Systems that are used for capturing high-volume transactional data need to be designed for high-speed read, write, and update operations.', 'True', 'False', '', '', 'True'),
(5, 'What is the role of �Network Access Control� systems in the area of network security?', 'To inspect incoming network traffic for intrusion attempts and vulnerabilities', 'To ensure attackers cannot tap into data while it is in transit', 'To ensure endpoint security by allowing only authorized devices to connect to the network', 'To create silos, or virtual local area networks, within a network so that you can segregate your assets', 'To ensure endpoint security by allowing only authorized devices to connect to the network'),
(5, '__________ ensures that users access information based on their roles and the privileges assigned to their roles.', 'Firewalls', 'Authorization', 'Authentication', 'Security Monitoring', 'Authorization'),
(5, 'Security Monitoring and Intelligence systems:', 'Ensure users access information based on their role and privileges', 'Create virtual local area networks within a network so that you can segregate your assets', 'Create an audit history for triage and compliance purposes', 'Ensure only authorized devices can connect to a network', 'Create an audit history for triage and compliance purposes');

