/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import entity.Slider;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.DBContext;

/**
 *
 * @author Asus
 */
public class DAOSlider extends DBContext {

    public int getTotalSlider() {
        String sql = "select count(*) from Slider";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return 0;
    }

    public List<Slider> pagingSlider(int index) {
        List<Slider> list = new ArrayList<>();
        String sql = "SELECT * FROM Slider\n"
                + "ORDER BY Id\n"
                + "OFFSET ? ROWS FETCH NEXT 3 ROWS ONLY";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, (index - 1) * 3);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Slider s = new Slider();
                s.setId(rs.getString("id"));
                s.setTitle(rs.getString("title"));
                s.setDescription(rs.getString("description"));
                s.setImage(rs.getString("image"));
                s.setLink(rs.getString("link"));
                s.setStatus(rs.getString("status"));
                list.add(s);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public Slider getSliderById(String sId) {
        String sql = "select *from Slider\n"
                + "where Id= ? ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, sId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Slider s = new Slider();
                s.setId(rs.getString("id"));
                s.setTitle(rs.getString("title"));
                s.setDescription(rs.getString("description"));
                s.setImage(rs.getString("image"));
                s.setLink(rs.getString("link"));
                s.setStatus(rs.getString("status"));
                return s;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public void editSlider(String title, String describe, String image,
            String link, String status, String sid) {
        String sql = "UPDATE [dbo].[Slider]\n"
                + "   SET [Title] = ?\n"
                + "      ,[Description] = ?\n"
                + "      ,[Image] = ?\n"
                + "      ,[link] = ?\n"
                + "      ,[Status] = ?\n"
                + " WHERE Id = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, title);
            st.setString(2, describe);
            st.setString(3, image);
            st.setString(4, link);
            st.setString(5, status);
            st.setString(6, sid);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void deleteSlider(String sid) {
        String sql = "delete from Slider "
                + "where Id = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, sid);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void insertSlider(String title, String describe, String image,
            String link, String status) {
        String sql = "INSERT INTO [dbo].[Slider]\n"
                + "           ([Title]\n"
                + "           ,[Description]\n"
                + "           ,[Image]\n"
                + "           ,[link]\n"
                + "           ,[Status])\n"
                + "     VALUES\n"
                + "           (?,?,?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, title);
            st.setString(2, describe);
            st.setString(3, image);
            st.setString(4, link);
            st.setString(5, status);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }
    
    public List<Slider> searchTitleSlider(String key) {
        List<Slider> list = new ArrayList<>();
        String sql = "select * from Slider "
                + "where Title like ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%" + key + "%");
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                Slider s = new Slider();
                s.setId(rs.getString("id"));
                s.setTitle(rs.getString("title"));
                s.setDescription(rs.getString("description"));
                s.setImage(rs.getString("image"));
                s.setLink(rs.getString("link"));
                s.setStatus(rs.getString("status"));
                list.add(s);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }
    
    public List<Slider> getAllSlider() {
        List<Slider> list = new ArrayList<>();
        String sql = "select *from Slider\n"
                + "where status = 1";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Slider s = new Slider();
                s.setId(rs.getString("id"));
                s.setTitle(rs.getString("title"));
                s.setDescription(rs.getString("description"));
                s.setImage(rs.getString("image"));
                s.setLink(rs.getString("link"));
                s.setStatus(rs.getString("status"));
                list.add(s);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public static void main(String[] args) {
        DAOSlider d = new DAOSlider();
        List<Slider> a = d.getAllSlider();
        for (Slider o : a) {
            System.out.println(o.getTitle());
        }
//        List<Slider> a = d.pagingSlider(2);
//        for (Slider o : a) {
//            System.out.println(o);
//        }
    }

    /*    public List<Slider> searchTitleSlider(String txtSearch) {
    throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }*/
}
