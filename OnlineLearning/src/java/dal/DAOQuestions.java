/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import entity.QuestionAnswer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import model.DBContext;

/**
 *
 * @author Admin MSI
 */
public class DAOQuestions extends DBContext {

    Connection cnn;
    Statement stm;
    ResultSet rs;
    PreparedStatement pstm;
    public ArrayList<QuestionAnswer> getQuestions(String quizId){
//        String query = "select top 10 q.Id, q.Question_Text, q.Answers, q.OptionA, q.OptionB, q.OptionC, q.OptionD, q1.Name  from Question q join Quiz q1 on q.Quiz_Id=q1.Id \n" +
//"where q1.Name = '"+quizName+"'\n"+"ORDER BY CHECKSUM(NEWID())";
        String query = "select top 5 q.Id, q.Question_Text, q.OptionA, q.OptionB, q.OptionC, q.OptionD, q.Answer "
                + "from Question q join Quiz q1 on q.Quiz_Id=q1.Id "
                + "where q.Quiz_Id = " + quizId;
        try{
            PreparedStatement pstm = connection.prepareStatement(query);
            ResultSet rs;
            rs = pstm.executeQuery();
            ArrayList<QuestionAnswer> ls = new ArrayList<>();
            while (rs.next()) {
                QuestionAnswer listQues = 
                        new QuestionAnswer(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7));
                ls.add(listQues);
            }
            return ls;
        } catch (Exception e) {
            System.out.println("getQuestions: " + e.getMessage());
        }
        return null;
    }
    
    public void addQuestion(String quizId, String question, String optionA, String optionB, String optionC, String optionD, String answer){
        try{
            String sql = "insert into Question(Quiz_Id, Question_Text, OptionA, OptionB, OptionC, OptionD, Answer)\n" +
            "values (?,?,?,?,?,?,?)";
            pstm = connection.prepareStatement(sql);
            pstm.setInt(1, Integer.parseInt(quizId));
            pstm.setString(2, question);
            pstm.setString(3, optionA);
            pstm.setString(4, optionB);
            pstm.setString(5, optionC);
            pstm.setString(6, optionD);
            pstm.setString(7, answer);
            pstm.executeUpdate();
        }catch(Exception e){
            System.out.println("add question: " + e.getMessage());
        }
    }
    public ArrayList<QuestionAnswer> showQuestionLecturer(int quizId){
        String sql = "select * from Question\n" +
                "where Question.Quiz_Id = ?";
        try{
            PreparedStatement pstm = connection.prepareStatement(sql);
            pstm.setInt(1, quizId);
            ResultSet rs;
            rs = pstm.executeQuery();
            ArrayList<QuestionAnswer> ls = new ArrayList<>();
            while (rs.next()) {
                QuestionAnswer listQues= new QuestionAnswer(
                        rs.getInt(1),
                        rs.getInt(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getString(8)
                );
                ls.add(listQues);
            }
            return ls;
        }catch(Exception e){
            System.out.println("showQuestionLecturer: " + e.getMessage());
        }
        return null;
    }
    
    public void DeleteQuestion(int quizId) {
        String sql = "DELETE FROM Question WHERE Quiz_Id = ?";
        try {
            PreparedStatement pstm = connection.prepareStatement(sql);
            pstm.setInt(1, quizId);
            int rowsAffected = pstm.executeUpdate();
            System.out.println(rowsAffected + " question(s) deleted for Quiz with Id " + quizId);
        } catch (Exception e) {
            System.out.println("Delete quiz: " + e.getMessage());
        }
    }

    public void DeleteQuizResult(int quizId) {
        String sql = "DELETE FROM Quiz_Results WHERE Quiz_Id = ?";
        try {
            PreparedStatement pstm = connection.prepareStatement(sql);
            pstm.setInt(1, quizId);
            int rowsAffected = pstm.executeUpdate();
            System.out.println(rowsAffected + " result(s) deleted for Quiz with Id " + quizId);
        } catch (Exception e) {
            System.out.println("Delete quiz result: " + e.getMessage());
        }
    }

}
