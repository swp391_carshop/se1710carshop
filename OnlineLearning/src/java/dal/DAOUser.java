/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import entity.SaleUser;
import entity.Subject;
import entity.User;
import entity.UserEnrollment;
import java.sql.Connection;
import model.DBContext;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Asus
 */
public class DAOUser extends DBContext {

    public User login(String user, String pass) {
        String sql = "select * from Users where username=? and password=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, user);
            st.setString(2, pass);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                User a = new User();
                a.setId(rs.getInt("id"));
                a.setUsername(rs.getString("username"));
                a.setFullname(rs.getString("fullname"));
                a.setPassword(rs.getString("password"));
                a.setRole(rs.getInt("role"));
                return a;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }
    
    
    /**
     * Get user by username and password
     * @param user
     * @param pass
     * @return 
     */
    public User getUserByUsernameAndPassword(String user, String pass){
                String sql = "select * from Users where username=? and password=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, user);
            st.setString(2, pass);
            ResultSet rs = st.executeQuery();

            if (rs.next()) {
                User a = new User();
                a.setId(rs.getInt("id"));
                a.setUsername(rs.getString("username"));
                a.setFullname(rs.getString("fullname"));
                a.setPassword(rs.getString("password"));
                a.setPhone(rs.getString("phone"));
                a.setEmail(rs.getString("email"));
                a.setRole(rs.getInt("role"));
                return a;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public int findUserId(String username) {
        String sql = "select * from Users where username = ?";
        int userId = -1;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, username);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                userId = rs.getInt(1);
                return userId;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return userId;
    }

    public User checkAccountExit(String user, String email) {
        String sql = "select * from Users where username = ? or email= ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, user);
            st.setString(2, email);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                User a = new User();
                a.setId(rs.getInt("id"));
                a.setUsername(rs.getString("username"));
                a.setPassword(rs.getString("password"));
                a.setEmail(rs.getString("email"));
                return a;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public String authenticateUser(User loginBean) {
        String userName = loginBean.getUsername();
        String password = loginBean.getPassword();
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        String userNameDB = "";
        String passwordDB = "";
        String roleDB = "";
        try {
            connection = DBContext.createConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT username, password, role FROM Users");
            while (resultSet.next()) {
                userNameDB = resultSet.getString("username");
                passwordDB = resultSet.getString("password");
                roleDB = resultSet.getString("role");
                if (userName.equals(userNameDB) && password.equals(passwordDB) && roleDB.equals("1")) {
                    return "1";
                } else if (userName.equals(userNameDB) && password.equals(passwordDB) && roleDB.equals("2")) {
                    return "2";
                } else if (userName.equals(userNameDB) && password.equals(passwordDB) && roleDB.equals("3")) {
                    return "3";
                } else if (userName.equals(userNameDB) && password.equals(passwordDB) && roleDB.equals("4")) {
                    return "4";
                } else if (userName.equals(userNameDB) && password.equals(passwordDB) && roleDB.equals("5")) {
                    return "5";
                }
            }
        } catch (SQLException e) {
        } finally {
            // Close the database resources
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            }
        }
        return "Invalid user credentials";
    }

    public void register(String fullName, String user, String pass, String email, String phone, int role) {
        String sql = "INSERT INTO [dbo].[Users]\n"
                + "           ([Fullname]\n"
                + "           ,[Username]\n"
                + "           ,[Password]\n"
                + "           ,[Email]\n"
                + "           ,[Phone]\n"
                + "           ,[Role])\n"
                + "     VALUES\n"
                + "           (?,?,?,?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, fullName);
            st.setString(2, user);
            st.setString(3, pass);
            st.setString(4, email);
            st.setString(5, phone);
            st.setInt(6, role);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public int updatePassword(User user) {
        int n = 0;
        String sql = "UPDATE [Users]\n"
                + "      SET [Password] = ?"
                + "      WHERE  [Email] = ?";

        try {
            PreparedStatement pre = connection.prepareStatement(sql);
            //set parameter
            pre.setString(1, user.getPassword());
            pre.setString(2, user.getEmail());
            //run
            n = pre.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAOUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return n;
    }

    public User getUserById(int id) {
        try {
            String sql = "SELECT *\n"
                    + "  FROM [dbo].[Users] where Id = ?";
            PreparedStatement ptm = connection.prepareStatement(sql);
            ptm.setInt(1, id);
            ResultSet rs = ptm.executeQuery();
            if (rs.next()) {
                User u = new User();
                u.setId(rs.getInt(1));
                u.setFullname(rs.getString(2));
                u.setUsername(rs.getString(3));
                u.setPassword(rs.getString(4));
                u.setEmail(rs.getString(5));
                u.setPhone(rs.getString(6));
                u.setRole(rs.getInt(7));
                return u;
            }

        } catch (SQLException ex) {
            Logger.getLogger(DAOCourse.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    public void updateUser(String id, String username, String fullname, String phone) {
        try {
            String sql = "UPDATE [dbo].[Users]\n"
                    + "   SET [Fullname] = ?\n"
                    + "      ,[Username] = ?\n"
                    + "      ,[Phone] = ?\n"
                    + " WHERE Id = ?";
            PreparedStatement ptm = connection.prepareStatement(sql);
            ptm.setString(1, fullname);
            ptm.setString(2, username);
            ptm.setString(3, phone);
            ptm.setInt(4, Integer.parseInt(id));
            ptm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAOCourse.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setPassWord(int id, String newPassword) {
        try {
            String sql = "UPDATE [dbo].[Users]\n"
                    + "   SET \n"
                    + "      [Password] = ?\n"
                    + " WHERE Id = ?";
            PreparedStatement ptm = connection.prepareStatement(sql);
            ptm.setString(1, newPassword);
            ptm.setInt(2, id);
            ptm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAOCourse.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int loadUserIdByUsername(String userName) {
        int id = -1;
        String sql = "SELECT id FROM Users WHERE username LIKE ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, userName);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                id = rs.getInt(1);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return id;
    }

    public int getTotalUser() {
        String sql = "select count(*) from Users";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return 0;
    }

    public int getTotalSearchUser(String username) {
        String sql = "select count(*) from Users WHERE Username like ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%" + username + "%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return 0;
    }

    public List<User> pagingUserList(int index) {
        List<User> list = new ArrayList<>();
        String sql = "SELECT * FROM Users"
                + "                ORDER BY Id\n"
                + "                OFFSET ? ROWS FETCH NEXT 8 ROWS ONLY";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, (index - 1) * 8);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                User a = new User();
                a.setId(rs.getInt("id"));
                a.setFullname(rs.getString("fullname"));
                a.setUsername(rs.getString("username"));
                a.setEmail(rs.getString("email"));
                a.setPhone(rs.getString("phone"));
                a.setRole(rs.getInt("role"));
                list.add(a);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }
    
    public List<User> pagingSearchUserList(String username, int index) {
        List<User> list = new ArrayList<>();
        String sql = "SELECT * FROM Users"
                + "                WHERE Username like ?\n"
                + "                ORDER BY Id\n"
                + "                OFFSET ? ROWS FETCH NEXT 8 ROWS ONLY";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%" + username + "%");
            st.setInt(2, (index - 1) * 8);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                User a = new User();
                a.setId(rs.getInt("id"));
                a.setFullname(rs.getString("fullname"));
                a.setUsername(rs.getString("username"));
                a.setEmail(rs.getString("email"));
                a.setPhone(rs.getString("phone"));
                a.setRole(rs.getInt("role"));
                list.add(a);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }
    
    public List<User> UserList(int role) {
        List<User> list = new ArrayList<>();
        String sql = "SELECT * FROM Users where role = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, role);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                User a = new User();
                a.setId(rs.getInt("id"));
                a.setFullname(rs.getString("fullname"));
                a.setUsername(rs.getString("username"));
                a.setEmail(rs.getString("email"));
                a.setPhone(rs.getString("phone"));
                a.setRole(rs.getInt("role"));
                list.add(a);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }


    public List<User> searchUsername(String key, int role) {
        List<User> list = new ArrayList<>();
        String sql = "select * from Users "
                + "where Username like ? and role = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%" + key + "%");
            st.setInt(2, role);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                User a = new User();
                a.setId(rs.getInt("id"));
                a.setFullname(rs.getString("fullname"));
                a.setUsername(rs.getString("username"));
                a.setEmail(rs.getString("email"));
                a.setPhone(rs.getString("phone"));
                a.setRole(rs.getInt("Role"));
                list.add(a);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }


    public User getUserByIdRole(int aId, int role) {
        String sql = "select *from Users\n"
                + "where Id= ? and role = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, aId);
            st.setInt(2, role);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                User a = new User();
                a.setId(rs.getInt("id"));
                a.setFullname(rs.getString("fullname"));
                a.setUsername(rs.getString("username"));
                a.setEmail(rs.getString("email"));
                a.setPhone(rs.getString("phone"));
                return a;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public List<SaleUser> getCourseUserById(int aId) {
        List<SaleUser> list = new ArrayList<>();
        String sql = "select e.Students_Id as id, FORMAT(e.Enrollment_Date, 'yyyy-MM-dd') as enrollmentDate, c.Name_course as nameCourse, c.Description\n"
                + "from Enrolled_course e join Courses c on e.Course_Id = c.Id where e.Students_Id = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, aId);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                SaleUser a = new SaleUser();
                a.setId(rs.getInt("id"));
                a.setNameCourse(rs.getString("nameCourse"));
                a.setDescription(rs.getString("description"));
                a.setEnrollmentDate(rs.getString("enrollmentDate"));
                list.add(a);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public ArrayList<UserEnrollment> loadUserEnrollment(int userId, int offset, int fetch) {
        ArrayList<UserEnrollment> enrollList = new ArrayList();
        String sql = "select Name_course as course_name, Subjects.Name as "
                + "subject_name, CONVERT(DATE, Enrollment_Date) as enroll_date , "
                + "Status from Enrolled_course \n"
                + "JOIN Courses ON Course_Id = Courses.Id AND Students_Id = ? \n"
                + "JOIN Subjects ON Subject_Id = Subjects.Id \n"
                + "ORDER BY Status OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, userId);
            ps.setInt(2, offset);
            ps.setInt(3, fetch);
            ResultSet rs = ps.executeQuery();
            //get data
            while (rs.next()) {
                String courseName = rs.getString(1);
                String subjectName = rs.getString(2);
                String enrollmentDate = rs.getString(3);
                int status = rs.getInt(4);
                enrollList.add(new UserEnrollment(courseName, subjectName, enrollmentDate, status));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return enrollList;
    }

    public int loadTotalUserEnrollment(int userId) {
        String sql = "select COUNT(*) from Enrolled_course \n"
                + "JOIN Courses ON Course_Id = Courses.Id AND Students_Id = ? \n"
                + "JOIN Subjects ON Subject_Id = Subjects.Id";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, userId);
            ResultSet rs = ps.executeQuery();
            //get data
            while (rs.next()) {
                int totalCount = rs.getInt(1);
                return totalCount;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return 0;
    }

    public ArrayList<Subject> loadUserEnrollmentSubject(int userId) {
        ArrayList<Subject> listSubject = new ArrayList();
        String sql = "SELECT DISTINCT Subjects.Id, Subjects.Name as subject_name "
                + "FROM Enrolled_course \n"
                + "JOIN Courses ON Course_Id = Courses.Id AND Students_Id = ? \n"
                + "JOIN Subjects ON Subject_Id = Subjects.Id ";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, userId);
            ResultSet rs = ps.executeQuery();
            //get data
            while (rs.next()) {
                int id = rs.getInt(1);
                String name = rs.getString(2);;
                listSubject.add(new Subject(id, name));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return listSubject;
    }


    /**
     * This function return an arraylist of UserEnrollment based on subjectId
     * and userId paginated by offset and fetch variables
     *
     * This function return an arraylist of UserEnrollment based on subjectId
     * and userId paginated by offset and fetch variables
     *
     * @param subjectId
     * @param userId
     * @param offset
     * @param fetch
     * @return
     */
    public ArrayList<UserEnrollment> loadUserEnrollmentBySubjectId(int subjectId,
            int userId, int offset, int fetch) {
        ArrayList<UserEnrollment> enrollList = new ArrayList();
        String sql = "select Name_course as course_name, Subjects.Name "
                + "as subject_name, CONVERT(DATE, Enrollment_Date) as enroll_date, "
                + "Status from Enrolled_course \n"
                + "JOIN Courses ON Course_Id = Courses.Id AND Students_Id = ? \n"
                + "JOIN Subjects ON Subject_Id = Subjects.Id \n"
                + "where Subject_Id = ?\n"
                + "ORDER BY Status OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, userId);
            ps.setInt(2, subjectId);
            ps.setInt(3, offset);
            ps.setInt(4, fetch);
            ResultSet rs = ps.executeQuery();
            //get data
            while (rs.next()) {
                String courseName = rs.getString(1);
                String subjectName = rs.getString(2);
                String enrollmentDate = rs.getString(3);
                int status = rs.getInt(4);
                enrollList.add(new UserEnrollment(courseName, subjectName, enrollmentDate, status));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return enrollList;
    }

    public int loadTotalUserEnrollmentBySubjectId(int subjectId, int userId) {
        String sql = "select COUNT(*) from Enrolled_course \n"
                + "JOIN Courses ON Course_Id = Courses.Id AND Students_Id = ? \n"
                + "JOIN Subjects ON Subject_Id = Subjects.Id \n"
                + "where Subject_Id = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, userId);
            ps.setInt(2, subjectId);
            ResultSet rs = ps.executeQuery();
            //get data
            while (rs.next()) {
                int totalCount = rs.getInt(1);
                return totalCount;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return 0;
    }


    /**
     * This function update Enrolled_Course table
     *
     * @param courseId - Id of course
     * @param studentId - Id of Student
     * @param status - status of enrolled course: 0 - for In progress, 1 - for
     * Completed
     */
    public void updateEnrollmentStatus(int courseId, int studentId, int status) {
        String sql = "UPDATE [dbo].[Enrolled_course]\n"
                + "   SET [Status] = ?\n"
                + " WHERE Course_Id = ? and Students_Id = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, status);
            ps.setInt(2, courseId);
            ps.setInt(3, studentId);
            int n = ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public UserEnrollment loadUserEnrollment(int courseId, int userId) {
        UserEnrollment enrollment = new UserEnrollment();
        String sql = "SELECT * FROM Enrolled_course WHERE Course_Id = ? and Students_Id = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, courseId);
            ps.setInt(2, userId);
            ResultSet rs = ps.executeQuery();
            //get data
            while (rs.next()) {
                String date = rs.getString(3);
                int status = rs.getInt(4);
                enrollment = new UserEnrollment(courseId, userId, date, status);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return enrollment;
    }

    public void deleteUser(int id) {
        User user = this.getUserById(id);
        String sql;
        //If user is a lecturer
        if (user.getRole() == 4) {
            //Delete all on course of the lecturer first
            sql = "DELETE FROM [dbo].[On_course]\n"
                    + "      WHERE Users_Id = ?";
            try {
                PreparedStatement ps = connection.prepareStatement(sql);
                ps.setInt(1, id);
                int n = ps.executeUpdate();
            } catch (SQLException e) {
                System.out.println(e);
            }
        }
        //If user is a student
        if (user.getRole() == 5) {
            //Delete all enrolled course of the student 
            sql = "DELETE FROM [dbo].[Enrolled_course]\n"
                    + "      WHERE Students_Id = ?";
            try {
                PreparedStatement ps = connection.prepareStatement(sql);
                ps.setInt(1, id);
                int n = ps.executeUpdate();
            } catch (SQLException e) {
                System.out.println(e);
            }
            //Delete all quiz result of the student 
            sql = "DELETE FROM [dbo].[Quiz_Results]\n"
                    + "      WHERE Users_Id = ?";
            try {
                PreparedStatement ps = connection.prepareStatement(sql);
                ps.setInt(1, id);
                int n = ps.executeUpdate();
            } catch (SQLException e) {
                System.out.println(e);
            }
        }

        //Delete User
        sql = "DELETE FROM [dbo].[Users]\n"
                + "      WHERE Id = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, id);
            int n = ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }
    
    public static void main(String[] args) {
        DAOUser d = new DAOUser();
        System.out.println(d.loadUserEnrollment(0, 0).getEnrollmentDate()==null);
    }
}
