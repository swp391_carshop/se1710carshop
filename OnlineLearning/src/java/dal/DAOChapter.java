/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import entity.Chapter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.DBContext;

/**
 *
 * @author ADMIN
 */
public class DAOChapter extends DBContext {

    public Vector<Chapter> getAllChapters() {
        Vector<Chapter> vector = new Vector<>();
        try {
            ResultSet rs = this.getData("select * from Chapters");
            while (rs.next()) {
                int chapterId = rs.getInt(1);
                String chapterName = rs.getString(2);
                int chapterNo = rs.getInt(3);
                String objective = rs.getString(4);
                int courseId = rs.getInt(5);
                Chapter chap = new Chapter(chapterName, chapterNo, objective, courseId);
                vector.add(chap);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOCourse.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        return vector;
    }


    public int getCourseChapterNumber(int courseId) {
        int num = 0;
        try {
            ResultSet rs = this.getData("select COUNT(Id) from Chapters where Courses_Id =" + courseId);
            if (rs.next()) {
                num = rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOCourse.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return num;
    }

    public Chapter getChapterByChapterId(int chapterId) {
        try {
            ResultSet rs = this.getData("SELECT * FROM Chapters WHERE Id = " + chapterId);
            if (rs.next()) {
                String chapterName = rs.getString(2);
                int chapterNo = rs.getInt(3);
                String objective = rs.getString(4);
                int courseId = rs.getInt(5);
                return new Chapter(chapterId, chapterName, chapterNo, objective, courseId);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOCourse.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }
    
        public Vector<Chapter> getChaptersByCourseId(int courseId) {
        Vector<Chapter> vector = new Vector<>();
        try {
            ResultSet rs = this.getData("select * from Chapters where Courses_Id = " + courseId + "ORDER BY Chapter_no");
            while (rs.next()) {
                int chapterId = rs.getInt(1);
                String chapterName = rs.getString(2);
                int chapterNo = rs.getInt(3);
                String objective = rs.getString(4);
                Chapter chap = new Chapter(chapterId,chapterName, chapterNo, objective, courseId);
                vector.add(chap);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOCourse.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        return vector;
    }
        
    public int insertChapter(String chapterName, String chapterNo, String chapterObjective, int courseId){
        try{
            String sql = "insert into Chapters(Name, Chapter_no, Objective, Courses_Id)\n" +
            "values (?, ?, ?, ?)";
            PreparedStatement pstm = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            pstm.setString(1, chapterName);
            pstm.setInt(2, Integer.parseInt(chapterNo));
            pstm.setString(3, chapterObjective);
            pstm.setInt(4, courseId);
            pstm.executeUpdate();
            try (ResultSet generatedKeys = pstm.getGeneratedKeys()){
                System.out.println(generatedKeys);
                   if (generatedKeys.next()) {
                    int newChapterId = generatedKeys.getInt(1);
                    // Use the newCourseId as needed
                    System.out.println("Newly generated ID: " + newChapterId);
                    return newChapterId;
                }
            }catch(Exception e){
                System.out.println("generateKey " + e.getMessage());
            }
        }catch(Exception e){
            System.out.println("insert chapter: " + e.getMessage());
        }
        return -1;
    }
    
    public ArrayList<Chapter> getChapterByCourseId(int courseId){
        ArrayList<Chapter> list = new ArrayList<>();
        try{
            String sql = "select * from Chapters\n" +
                "where Courses_Id = ?;";
            PreparedStatement pstm = connection.prepareStatement(sql);
            pstm.setInt(1, courseId);
            ResultSet rs = pstm.executeQuery();
            while(rs.next()){
                list.add(new Chapter(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getString(4), rs.getInt(5)));
            }
        }catch(Exception e){
            System.out.println("Get chapter by course id " + e.getMessage());
        }      
        return list;
    }    

        
    public void deleteChapter(int chapterId) {
        String sql = "DELETE FROM Chapters WHERE Id = ?";
        try {
            PreparedStatement pstm = connection.prepareStatement(sql);
            pstm.setInt(1, chapterId);
            int rowsAffected = pstm.executeUpdate();
            if (rowsAffected > 0) {
                System.out.println("Quiz with Id " + chapterId + " deleted successfully.");
            } else {
                System.out.println("No quiz found with Id " + chapterId + ".");
            }
        } catch (Exception e) {
            System.out.println("Delete quiz: " + e.getMessage());
        }
    }
    
    public static void main(String[] args) {
        DAOChapter d = new DAOChapter();
        System.out.println(d.getChapterByChapterId(1).getChapterName());
    }

}
