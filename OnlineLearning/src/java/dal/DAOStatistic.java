/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Vector;
import model.DBContext;

/**
 *
 * @author LENOVO
 */
public class DAOStatistic {

    private Connection con;
    private String status = "ok";

    public DAOStatistic() {
        try {
            con = new DBContext().connection;
        } catch (Exception e) {
            status = "Error" + e.getMessage();
        }
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double loadWeeklyRevenue(int option) {
        //option = 0: load total revenue in a week
        //option = 1: load change percentage
        if (option == 0) {
            double revenue = 0;
            String sql = "SELECT COALESCE(SUM(Courses.Price), 0) AS Revenue "
                    + "FROM Enrolled_Course INNER JOIN Courses "
                    + "ON Enrolled_Course.Course_Id = Courses.Id "
                    + "WHERE DATEPART(wk, Enrollment_Date) = (DATEPART(wk, GETDATE()));";
            try {
                PreparedStatement ps = con.prepareStatement(sql);
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    revenue = rs.getDouble(1);
                }
                rs.close();
                ps.close();
            } catch (Exception e) {
                status = "Error " + e.getMessage();
            }
            return revenue;
        } else {
            double changePercentage = 0;
            String sql = "WITH CTE_Revenue AS (\n"
                    + "    SELECT \n"
                    + "        DATEPART(wk, Enrollment_Date) AS RevenueWeek,\n"
                    + "        SUM(Courses.Price) AS Revenue\n"
                    + "    FROM Enrolled_Course\n"
                    + "    INNER JOIN Courses\n"
                    + "    ON Enrolled_Course.Course_Id = Courses.Id\n"
                    + "    WHERE DATEPART(wk, Enrollment_Date) "
                    + "IN (DATEPART(wk, GETDATE()), DATEPART(wk, GETDATE()) - 1)\n"
                    + "    GROUP BY DATEPART(wk, Enrollment_Date)\n"
                    + "),\n"
                    + "CTE_RevenueChange AS (\n"
                    + "    SELECT \n"
                    + "        CAST((((MAX(CASE WHEN RevenueWeek "
                    + "= DATEPART(wk, GETDATE()) THEN Revenue END) "
                    + "- MAX(CASE WHEN RevenueWeek "
                    + "= DATEPART(wk, GETDATE()) - 1 THEN Revenue END)) "
                    + "/ MAX(CASE WHEN RevenueWeek "
                    + "= DATEPART(wk, GETDATE()) - 1 THEN Revenue END)) * 100.0) "
                    + "AS DECIMAL(5,2)) AS RevenueChange\n"
                    + "    FROM CTE_Revenue\n"
                    + ")\n"
                    + "SELECT * FROM CTE_RevenueChange; ";
            try {
                PreparedStatement ps = con.prepareStatement(sql);
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    changePercentage = rs.getDouble(1);
                }
                rs.close();
                ps.close();
            } catch (Exception e) {
                status = "Error " + e.getMessage();
            }
            return changePercentage;
        }
    }

    public double loadWeeklyEnrollment(int option) {
        //option = 0: load total enrollment in a week
        //option = 1: load change percentage
        if (option == 0) {
            double enrollment = 0;
            String sql = "SELECT \n"
                    + "    COUNT(*) AS EnrollmentCount\n"
                    + "FROM Enrolled_Course\n"
                    + "WHERE DATEPART(wk, Enrollment_Date) = DATEPART(wk, GETDATE());";
            try {
                PreparedStatement ps = con.prepareStatement(sql);
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    enrollment = rs.getDouble(1);
                }
                rs.close();
                ps.close();
            } catch (Exception e) {
                status = "Error " + e.getMessage();
            }
            return enrollment;
        } else {
            double changePercentage = 0;
            String sql = "WITH CTE_EnrollmentCount AS (\n"
                    + "    SELECT \n"
                    + "        DATEPART(wk, Enrollment_Date) AS EnrollmentWeek,\n"
                    + "        COUNT(*) AS EnrollmentCount\n"
                    + "    FROM Enrolled_Course\n"
                    + "    WHERE DATEPART(wk, Enrollment_Date) IN "
                    + "(DATEPART(wk, GETDATE()), DATEPART(wk, GETDATE()) - 1)\n"
                    + "    GROUP BY DATEPART(wk, Enrollment_Date)\n"
                    + "),\n"
                    + "CTE_EnrollmentChange AS (\n"
                    + "    SELECT \n"
                    + "        CAST((CAST((MAX(CASE WHEN EnrollmentWeek "
                    + "= DATEPART(wk, GETDATE()) THEN EnrollmentCount END)"
                    + "- MAX(CASE WHEN EnrollmentWeek "
                    + "= DATEPART(wk, GETDATE()) - 1 THEN EnrollmentCount END)) "
                    + "AS DECIMAL(10,2)) / CAST((MAX(CASE WHEN EnrollmentWeek "
                    + "= DATEPART(wk, GETDATE()) - 1 THEN EnrollmentCount END)) "
                    + "AS DECIMAL(10,2))*100) AS DECIMAL(5,2)) AS EnrollmentChange\n"
                    + "    FROM CTE_EnrollmentCount\n"
                    + ")\n"
                    + "SELECT * FROM CTE_EnrollmentChange;";
            try {
                PreparedStatement ps = con.prepareStatement(sql);
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    changePercentage = rs.getDouble(1);
                }
                rs.close();
                ps.close();
            } catch (Exception e) {
                status = "Error " + e.getMessage();
            }
            return changePercentage;
        }
    }

    public Map<String, Double> loadDailyRevenue() {
        Map<String, Double> revenues = new LinkedHashMap<String, Double>();
        String sql = "WITH CTE_Dates AS (\n"
                + "    SELECT TOP (7) \n"
                + "   DATEADD(day, -ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) "
                + "+ 1, CAST(GETDATE() AS DATE)) AS Date\n"
                + "    FROM sys.all_objects\n"
                + "),\n"
                + "CTE_Revenue AS (\n"
                + "    SELECT \n"
                + "        CAST(Enrollment_Date AS DATE) AS RevenueDate,\n"
                + "        SUM(Courses.price) AS Revenue\n"
                + "    FROM Enrolled_Course\n"
                + "    INNER JOIN Courses\n"
                + "    ON Enrolled_Course.Course_Id = Courses.Id\n"
                + "WHERE Enrollment_Date >= DATEADD(day, -6, CAST(GETDATE() AS DATE))\n"
                + "    GROUP BY CAST(Enrollment_Date AS DATE)\n"
                + ")\n"
                + "SELECT \n"
                + "    CTE_Dates.Date,\n"
                + "    ISNULL(CTE_Revenue.Revenue, 0) AS Revenue\n"
                + "FROM CTE_Dates\n"
                + "LEFT JOIN CTE_Revenue\n"
                + "ON CTE_Dates.Date = CTE_Revenue.RevenueDate\n"
                + "ORDER BY CTE_Dates.Date;";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                revenues.put(rs.getString(1), rs.getDouble(2));
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            status = "Error " + e.getMessage();
        }
        return revenues;
    }

    public Map<String, Integer> loadDailyEnrollment() {
        Map<String, Integer> enrollments = new LinkedHashMap<>();
        String sql = "WITH CTE_Dates AS (\n"
                + "    SELECT TOP (7) \n"
                + "        DATEADD(day, -ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) + 1, CAST(GETDATE() AS DATE)) AS Date\n"
                + "    FROM sys.all_objects\n"
                + "),\n"
                + "CTE_Revenue AS (\n"
                + "    SELECT \n"
                + "        CAST(Enrollment_Date AS DATE) AS RevenueDate,\n"
                + "        COUNT(*) AS Enrollment\n"
                + "    FROM Enrolled_Course\n"
                + "    WHERE Enrollment_Date >= DATEADD(day, -6, CAST(GETDATE() AS DATE))\n"
                + "    GROUP BY CAST(Enrollment_Date AS DATE)\n"
                + ")\n"
                + "SELECT \n"
                + "    CTE_Dates.Date,\n"
                + "    ISNULL(CTE_Revenue.Enrollment, 0) AS Enrollment\n"
                + "FROM CTE_Dates\n"
                + "LEFT JOIN CTE_Revenue\n"
                + "ON CTE_Dates.Date = CTE_Revenue.RevenueDate\n"
                + "ORDER BY CTE_Dates.Date;";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                enrollments.put(rs.getString(1), rs.getInt(2));
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            status = "Error " + e.getMessage();
        }
        return enrollments;
    }

    public Map<String, Double> loadMonthlyRevenue() {
        Map<String, Double> revenues = new LinkedHashMap<String, Double>();
        String sql = "WITH CTE_Dates AS (\n"
                + "    SELECT TOP (12) \n"
                + "        DATEADD(month, -ROW_NUMBER() OVER (ORDER BY (SELECT NULL))"
                + " + 1, EOMONTH(GETDATE())) AS Date\n"
                + "    FROM sys.all_objects\n"
                + "),\n"
                + "CTE_Revenue AS (\n"
                + "    SELECT \n"
                + "        EOMONTH(Enrollment_Date) AS RevenueDate,\n"
                + "        SUM(Courses.price) AS Revenue\n"
                + "    FROM Enrolled_Course\n"
                + "    INNER JOIN Courses\n"
                + "    ON Enrolled_Course.Course_Id = Courses.Id\n"
                + "    WHERE Enrollment_Date >= DATEADD(month, -11, EOMONTH(GETDATE()))\n"
                + "    GROUP BY EOMONTH(Enrollment_Date)\n"
                + ")\n"
                + "SELECT \n"
                + "    CTE_Dates.Date,\n"
                + "    ISNULL(CTE_Revenue.Revenue, 0) AS Revenue\n"
                + "FROM CTE_Dates\n"
                + "LEFT JOIN CTE_Revenue\n"
                + "ON CTE_Dates.Date = CTE_Revenue.RevenueDate\n"
                + "ORDER BY CTE_Dates.Date;";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                revenues.put(rs.getString(1).substring(0, 7), rs.getDouble(2));
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            status = "Error " + e.getMessage();
        }
        return revenues;
    }

    public Map<String, Integer> loadMonthlyEnrollment() {
        Map<String, Integer> enrollments = new LinkedHashMap<>();
        String sql = "WITH CTE_Dates AS (\n"
                + "    SELECT TOP (12) \n"
                + "        DATEADD(month, -ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) + 1, EOMONTH(GETDATE())) AS Date\n"
                + "    FROM sys.all_objects\n"
                + "),\n"
                + "CTE_Revenue AS (\n"
                + "    SELECT \n"
                + "        EOMONTH(Enrollment_Date) AS RevenueDate,\n"
                + "        COUNT(*) AS Enrollment\n"
                + "    FROM Enrolled_Course\n"
                + "    WHERE Enrollment_Date >= DATEADD(month, -11, EOMONTH(GETDATE()))\n"
                + "    GROUP BY EOMONTH(Enrollment_Date)\n"
                + ")\n"
                + "SELECT \n"
                + "    CTE_Dates.Date,\n"
                + "    ISNULL(CTE_Revenue.Enrollment, 0) AS Revenue\n"
                + "FROM CTE_Dates\n"
                + "LEFT JOIN CTE_Revenue\n"
                + "ON CTE_Dates.Date = CTE_Revenue.RevenueDate\n"
                + "ORDER BY CTE_Dates.Date;";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                enrollments.put(rs.getString(1), rs.getInt(2));
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            status = "Error " + e.getMessage();
        }
        return enrollments;
    }

    public Map<String, Double> loadYearlyRevenue() {
        Map<String, Double> revenues = new LinkedHashMap<String, Double>();
        String sql = "WITH CTE_Dates AS (\n"
                + "    SELECT TOP (5) \n"
                + "        DATEADD(year, -ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) "
                + "+ 1, DATEFROMPARTS(YEAR(GETDATE()), 1, 1)) AS Date\n"
                + "    FROM sys.all_objects\n"
                + "),\n"
                + "CTE_Revenue AS (\n"
                + "    SELECT \n"
                + "        DATEFROMPARTS(YEAR(Enrollment_Date), 1, 1) AS RevenueDate,\n"
                + "        SUM(Courses.price) AS Revenue\n"
                + "    FROM Enrolled_Course\n"
                + "    INNER JOIN Courses\n"
                + "    ON Enrolled_Course.Course_Id = Courses.Id\n"
                + "    WHERE Enrollment_Date >= DATEADD(year, -4, DATEFROMPARTS(YEAR(GETDATE()), 1, 1))\n"
                + "    GROUP BY DATEFROMPARTS(YEAR(Enrollment_Date), 1, 1)\n"
                + ")\n"
                + "SELECT \n"
                + "    YEAR(CTE_Dates.Date),\n"
                + "    ISNULL(CTE_Revenue.Revenue, 0) AS Revenue\n"
                + "FROM CTE_Dates\n"
                + "LEFT JOIN CTE_Revenue\n"
                + "ON CTE_Dates.Date = CTE_Revenue.RevenueDate\n"
                + "ORDER BY CTE_Dates.Date;";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                revenues.put(rs.getString(1), rs.getDouble(2));
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            status = "Error " + e.getMessage();
        }
        return revenues;
    }

    public Map<String, Integer> loadYearlyEnrollment() {
        Map<String, Integer> enrollments = new LinkedHashMap<>();
        String sql = "WITH CTE_Dates AS (\n"
                + "    SELECT TOP (5) \n"
                + "        DATEADD(year, -ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) + 1, DATEFROMPARTS(YEAR(GETDATE()), 1, 1)) AS Date\n"
                + "    FROM sys.all_objects\n"
                + "),\n"
                + "CTE_Revenue AS (\n"
                + "    SELECT \n"
                + "        DATEFROMPARTS(YEAR(Enrollment_Date), 1, 1) AS RevenueDate,\n"
                + "        COUNT(*) AS Enrollment\n"
                + "    FROM Enrolled_Course\n"
                + "    WHERE Enrollment_Date >= DATEADD(year, -4, DATEFROMPARTS(YEAR(GETDATE()), 1, 1))\n"
                + "    GROUP BY DATEFROMPARTS(YEAR(Enrollment_Date), 1, 1)\n"
                + ")\n"
                + "SELECT \n"
                + "    YEAR(CTE_Dates.Date),\n"
                + "    ISNULL(CTE_Revenue.Enrollment, 0) AS Enrollment\n"
                + "FROM CTE_Dates\n"
                + "LEFT JOIN CTE_Revenue\n"
                + "ON CTE_Dates.Date = CTE_Revenue.RevenueDate\n"
                + "ORDER BY CTE_Dates.Date;";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                enrollments.put(rs.getString(1), rs.getInt(2));
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            status = "Error " + e.getMessage();
        }
        return enrollments;
    }

    public Map<String, Integer> loadDailyEnrollmentPerCourse() {
        Map<String, Integer> enrollments = new LinkedHashMap<String, Integer>();
        String sql = "SELECT \n"
                + "    Courses.Name_course AS CourseName,\n"
                + "    ISNULL(COUNT(Enrolled_Course.Course_Id), 0) AS EnrollmentCount\n"
                + "FROM Courses\n"
                + "LEFT JOIN Enrolled_Course\n"
                + "ON Courses.Id = Enrolled_Course.Course_Id\n"
                + "AND Enrollment_Date >= DATEADD(day, -6, CAST(GETDATE() AS DATE))\n"
                + "GROUP BY Courses.Name_course\n"
                + "ORDER BY EnrollmentCount DESC;";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                enrollments.put(rs.getString(1), rs.getInt(2));
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            status = "Error " + e.getMessage();
        }
        return enrollments;
    }

    public Map<String, Integer> loadMonthlyEnrollmentPerCourse() {
        Map<String, Integer> enrollments = new LinkedHashMap<String, Integer>();
        String sql = "SELECT \n"
                + "    Courses.Name_course AS CourseName,\n"
                + "    ISNULL(COUNT(Enrolled_Course.Course_Id), 0) AS EnrollmentCount\n"
                + "FROM Courses\n"
                + "LEFT JOIN Enrolled_Course\n"
                + "ON Courses.Id = Enrolled_Course.Course_Id\n"
                + "AND Enrollment_Date >= DATEADD(month, -11, EOMONTH(GETDATE()))\n"
                + "GROUP BY Courses.Name_course\n"
                + "ORDER BY EnrollmentCount DESC;";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                enrollments.put(rs.getString(1), rs.getInt(2));
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            status = "Error " + e.getMessage();
        }
        return enrollments;
    }

    public Map<String, Integer> loadYearlyEnrollmentPerCourse() {
        Map<String, Integer> enrollments = new LinkedHashMap<String, Integer>();
        String sql = "SELECT \n"
                + "    Courses.Name_course AS CourseName,\n"
                + "    ISNULL(COUNT(Enrolled_Course.Course_Id), 0) AS EnrollmentCount\n"
                + "FROM Courses\n"
                + "LEFT JOIN Enrolled_Course\n"
                + "ON Courses.Id = Enrolled_Course.Course_Id\n"
                + "AND Enrollment_Date >= DATEADD(year, -4, DATEFROMPARTS(YEAR(GETDATE()), 1, 1))\n"
                + "GROUP BY Courses.Name_course\n"
                + "ORDER BY EnrollmentCount DESC;";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                enrollments.put(rs.getString(1), rs.getInt(2));
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            status = "Error " + e.getMessage();
        }
        return enrollments;
    }

    public Map<String, Double> loadDailyEnrollmentPerSubject() {
        Map<String, Double> enrollments = new LinkedHashMap<>();
        String sql = "WITH TotalEn AS(\n"
                + "SELECT COUNT(*) AS total FROM Enrolled_Course WHERE Enrollment_Date >= DATEADD(day, -7, GETDATE()))\n"
                + "\n"
                + "SELECT Subjects.Name, CAST((COUNT(Enrolled_Course.Course_Id)*100.0)/TotalEn.total AS DECIMAL(5,2)) as Enrollment\n"
                + "FROM Courses\n"
                + "JOIN Enrolled_Course ON Enrolled_Course.Course_Id = Courses.Id AND Enrolled_Course.Enrollment_Date >= DATEADD(day, -7, GETDATE())\n"
                + "JOIN Subjects ON Courses.Subject_Id = Subjects.Id\n"
                + "CROSS JOIN TotalEn\n"
                + "GROUP BY Subjects.Name, TotalEn.total;";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                enrollments.put(rs.getString(1), rs.getDouble(2));
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            status = "Error " + e.getMessage();
        }
        return enrollments;
    }

    public Map<String, Double> loadMonthlyEnrollmentPerSubject() {
        Map<String, Double> enrollments = new LinkedHashMap<>();
        String sql = "WITH TotalEn AS(\n"
                + "SELECT COUNT(*) AS total FROM Enrolled_Course WHERE Enrollment_Date >= DATEADD(month, -12, GETDATE()))\n"
                + "\n"
                + "SELECT Subjects.Name, CAST((COUNT(Enrolled_Course.Course_Id)*100.0)/TotalEn.total AS DECIMAL(5,2)) as Enrollment\n"
                + "FROM Courses\n"
                + "JOIN Enrolled_Course ON Enrolled_Course.Course_Id = Courses.Id AND Enrolled_Course.Enrollment_Date >= DATEADD(month, -12, GETDATE())\n"
                + "JOIN Subjects ON Courses.Subject_Id = Subjects.Id\n"
                + "CROSS JOIN TotalEn\n"
                + "GROUP BY Subjects.Name, TotalEn.total;";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                enrollments.put(rs.getString(1), rs.getDouble(2));
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            status = "Error " + e.getMessage();
        }
        return enrollments;
    }

    public Map<String, Double> loadYearlyEnrollmentPerSubject() {
        Map<String, Double> enrollments = new LinkedHashMap<>();
        String sql = "WITH TotalEn AS(\n"
                + "SELECT COUNT(*) AS total FROM Enrolled_Course WHERE Enrollment_Date >= DATEADD(year, -5, GETDATE()))\n"
                + "\n"
                + "SELECT Subjects.Name, CAST((COUNT(Enrolled_Course.Course_Id)*100.0)/TotalEn.total AS DECIMAL(5,2)) as Enrollment\n"
                + "FROM Courses\n"
                + "JOIN Enrolled_Course ON Enrolled_Course.Course_Id = Courses.Id AND Enrolled_Course.Enrollment_Date >= DATEADD(year, -5, GETDATE())\n"
                + "JOIN Subjects ON Courses.Subject_Id = Subjects.Id\n"
                + "CROSS JOIN TotalEn\n"
                + "GROUP BY Subjects.Name, TotalEn.total;";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                enrollments.put(rs.getString(1), rs.getDouble(2));
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            status = "Error " + e.getMessage();
        }
        return enrollments;
    }

    public Map<String, Vector> loadCourseStat() {
        Map<String, Vector> courseStats = new LinkedHashMap<>();
        Vector<Integer> courseId = new Vector<>();
        Vector<String> name = new Vector<>();
        Vector<String> subject = new Vector<>();
        Vector<Integer> totalEnroll = new Vector<>();
        Vector<Double> completeRate = new Vector<>();
        Vector<Integer> active = new Vector<>();

        String sql = "SELECT Courses.Id,\n"
                + "Courses.Name_course, \n"
                + "Subjects.Name AS Subject,\n"
                + "       COUNT(*) as Total,\n"
                + "       CAST(SUM(CASE WHEN Enrolled_Course.Status = 1 THEN 1 ELSE 0 END) * 100.0 / COUNT(*)AS DECIMAL(5,2)) as CompletionRate,\n"
                + "	   Active\n"
                + "FROM Enrolled_Course\n"
                + "JOIN Courses ON Enrolled_Course.Course_Id = Courses.Id\n"
                + "JOIN Subjects ON Courses.Subject_Id = Subjects.Id\n"
                + "GROUP BY Courses.Name_course, Courses.Id, Subjects.Name,Active";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                courseId.add(rs.getInt(1));
                name.add(rs.getString(2));
                subject.add(rs.getString(3));
                totalEnroll.add(rs.getInt(4));
                completeRate.add(rs.getDouble(5));
                active.add(rs.getInt(6));
                
            }

            courseStats.put("id", courseId);
            courseStats.put("name", name);
            courseStats.put("subject", subject);
            courseStats.put("total", totalEnroll);
            courseStats.put("complete", completeRate);
            courseStats.put("active", active);

            rs.close();
            ps.close();
        } catch (Exception e) {
            status = "Error " + e.getMessage();
        }
        return courseStats;
    }
    
        /**
     *
     * @param option
     *
     * @return option = 0 return sort by id, option = 1 return sort by
     * enrollments, option = 2 return sort by completion.
     * size = 0 return all else get TOP size in query
     */
    public Map<String, Vector> loadCourseStat(int option, int size) {
        Map<String, Vector> courseStats = new LinkedHashMap<>();
        Vector<Integer> courseId = new Vector<>();
        Vector<String> name = new Vector<>();
        Vector<String> subject = new Vector<>();
        Vector<Integer> totalEnroll = new Vector<>();
        Vector<Double> completeRate = new Vector<>();
        Vector<Integer> active = new Vector<>();
        String orderQuery = "";
        String topQuery = "";
        //check option
        if(option == 1){
            orderQuery = "ORDER BY Total DESC";
        }else if(option == 2){
            orderQuery = "ORDER BY CompletionRate DESC";
        }
        
        //check size
        if(size != 0){
            topQuery = "TOP " + size;
        }

        String sql = "SELECT "+topQuery+" Courses.Id,\n"
                + "Courses.Name_course, \n"
                + "Subjects.Name AS Subject,\n"
                + "       COUNT(*) as Total,\n"
                + "       CAST(SUM(CASE WHEN Enrolled_Course.Status = 1 THEN 1 ELSE 0 END) * 100.0 / COUNT(*)AS DECIMAL(5,2)) as CompletionRate,\n"
                + "	   Active\n"
                + "FROM Enrolled_Course\n"
                + "JOIN Courses ON Enrolled_Course.Course_Id = Courses.Id\n"
                + "JOIN Subjects ON Courses.Subject_Id = Subjects.Id\n"
                + "GROUP BY Courses.Name_course, Courses.Id, Subjects.Name,Active\n"
                + orderQuery;
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                courseId.add(rs.getInt(1));
                name.add(rs.getString(2));
                subject.add(rs.getString(3));
                totalEnroll.add(rs.getInt(4));
                completeRate.add(rs.getDouble(5));
                active.add(rs.getInt(6));

            }

            courseStats.put("id", courseId);
            courseStats.put("name", name);
            courseStats.put("subject", subject);
            courseStats.put("total", totalEnroll);
            courseStats.put("complete", completeRate);
            courseStats.put("active", active);

            rs.close();
            ps.close();
        } catch (Exception e) {
            status = "Error " + e.getMessage();
        }
        return courseStats;
    }


    public static void main(String[] args) {
        DAOStatistic d = new DAOStatistic();
        System.out.println(d.loadWeeklyRevenue(1));
        System.out.println(d.loadWeeklyEnrollment(1));
        Map<String, Double> revenues1 = d.loadDailyRevenue();
        for (Map.Entry<String, Double> entry : revenues1.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }
        System.out.println("-----------------------");
        Map<String, Double> revenues2 = d.loadMonthlyRevenue();
        for (Map.Entry<String, Double> entry : revenues2.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }
        System.out.println("-----------------------");
        Map<String, Double> revenues3 = d.loadYearlyRevenue();
        for (Map.Entry<String, Double> entry : revenues3.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }

        System.out.println("-----------------------");
        Map<String, Integer> enrollment1 = d.loadDailyEnrollmentPerCourse();
        for (Map.Entry<String, Integer> entry : enrollment1.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }

        System.out.println("-----------------------");
        Map<String, Integer> enrollment2 = d.loadMonthlyEnrollmentPerCourse();
        for (Map.Entry<String, Integer> entry : enrollment2.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }

        System.out.println("-----------------------");
        Map<String, Integer> enrollment3 = d.loadYearlyEnrollmentPerCourse();
        for (Map.Entry<String, Integer> entry : enrollment3.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }
        System.out.println("****************************");

//        StringBuilder days = new StringBuilder();
//        ArrayList<Entry<String, Double>> entryList
//                = new ArrayList<Map.Entry<String, Double>>(revenues1.entrySet());
//        for (int i = 0; i < entryList.size(); i++) {
//            days.append("'" + entryList.get(i).getValue() + "'");
//            if(!entryList.get(i).equals(entryList.get(entryList.size()-1))){
//                days.append(", ");
//            }
//        }
//        System.out.println(days);
        StringBuilder courses = new StringBuilder();
        ArrayList<Map.Entry<String, Integer>> courseEnrollments = new ArrayList<>(enrollment1.entrySet());
        for (int i = 0; i < courseEnrollments.size(); i++) {
            courses.append("'" + courseEnrollments.get(i).getKey() + "'");
            if (!courseEnrollments.get(i).equals(courseEnrollments.get(courseEnrollments.size() - 1))) {
                courses.append(", ");
            }
        }

        StringBuilder dayEnrolls = new StringBuilder();
        ArrayList<Entry<String, Integer>> dayEnrList = new ArrayList<>(enrollment1.entrySet());
        for (int i = 0; i < dayEnrList.size(); i++) {
            dayEnrolls.append("'" + dayEnrList.get(i).getValue() + "'");
            if (!dayEnrList.get(i).equals(dayEnrList.get(dayEnrList.size() - 1))) {
                dayEnrolls.append(", ");
            }
        }
        System.out.println(courses);
        System.out.println(dayEnrolls);

        System.out.println("-------------daily enroll----------");
        Map<String, Integer> enrollment11 = d.loadDailyEnrollment();
        for (Map.Entry<String, Integer> entry : enrollment11.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }
        System.out.println("-------------monthly enroll----------");
        Map<String, Integer> enrollment12 = d.loadMonthlyEnrollment();
        for (Map.Entry<String, Integer> entry : enrollment12.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }

        System.out.println("-------------yearly enroll----------");
        Map<String, Double> enrollment13 = d.loadDailyEnrollmentPerSubject();
        for (Map.Entry<String, Double> entry : enrollment13.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }

    }
}
