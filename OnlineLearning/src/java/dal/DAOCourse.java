/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import entity.Course;
import entity.CourseSubject;
import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.DBContext;

/**
 *
 * @author ADMIN
 */
public class DAOCourse extends DBContext {

    public Vector<Course> getAllCourseses() {
        Vector<Course> vector = new Vector<>();

        try {
            ResultSet rs = this.getData("select * from Courses");
            while (rs.next()) {
                int courseID = rs.getInt(1);
                String courseName = rs.getString(2);
                String description = rs.getString(3);
                String objective = rs.getString(4);
                double price = rs.getDouble(5);
                int active = rs.getInt(6);
                int subjectID = rs.getInt(7);
                Course course = new Course(courseID, courseName, description, objective, price, active, subjectID);
                vector.add(course);
            }

        } catch (SQLException ex) {
            Logger.getLogger(DAOCourse.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        return vector;
    }

    public Vector<Course> getAllActiveCourseses() {
        Vector<Course> vector = new Vector<>();

        try {
            ResultSet rs = this.getData("select * from Courses where Active = 1");
            while (rs.next()) {
                int courseID = rs.getInt(1);
                String courseName = rs.getString(2);
                String description = rs.getString(3);
                String objective = rs.getString(4);
                double price = rs.getDouble(5);
                int active = rs.getInt(6);
                int subjectID = rs.getInt(7);
                Course course = new Course(courseID, courseName, description, objective, price, active, subjectID);
                vector.add(course);
            }

        } catch (SQLException ex) {
            Logger.getLogger(DAOCourse.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        return vector;
    }

    public Vector<Course> getCourseses(String sql) {
        Vector<Course> vector = new Vector<>();

        try {
            ResultSet rs = this.getData(sql);
            while (rs.next()) {
                int courseID = rs.getInt(1);
                String courseName = rs.getString(2);
                String description = rs.getString(3);
                String objective = rs.getString(4);
                double price = rs.getDouble(5);
                int active = rs.getInt(6);
                int subjectID = rs.getInt(7);
                Course course = new Course(courseID, courseName, description, objective, price, active, subjectID);
                vector.add(course);
            }

        } catch (SQLException ex) {
            Logger.getLogger(DAOCourse.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        return vector;
    }

    public Vector<Course> getFeatureCourseses(int numCourse) {
        Vector<Course> vector = new Vector<>();
        String sql = "WITH topcourse as(\n"
                    + "SELECT TOP(?) Courses.Id AS CourseId, ISNULL(COUNT(Enrolled_Course.Course_Id), 0) AS EnrollmentCount\n"
                    + "              FROM Courses\n"
                    + "               LEFT JOIN Enrolled_Course\n"
                    + "               ON Courses.Id = Enrolled_Course.Course_Id\n"
                    + "			   WHERE Active = 1\n"
                    + "                GROUP BY Courses.Id\n"
                    + "               ORDER BY EnrollmentCount DESC)\n"
                    + "select * from Courses join topcourse on Courses.Id = topcourse.CourseId";
        try {
            PreparedStatement pre = connection.prepareStatement(sql);
            //set parameter
            pre.setInt(1, numCourse);
            ResultSet rs = pre.executeQuery();
            while (rs.next()) {
                int courseID = rs.getInt(1);
                String courseName = rs.getString(2);
                String description = rs.getString(3);
                String objective = rs.getString(4);
                double price = rs.getDouble(5);
                int active = rs.getInt(6);
                int subjectID = rs.getInt(7);
                Course course = new Course(courseID, courseName, description, objective, price, active, subjectID);
                vector.add(course);
            }

        } catch (SQLException ex) {
            Logger.getLogger(DAOCourse.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        return vector;
    }

    public int getCourseStudentTotal(int courseId) {
        int num = 0;
        try {
            ResultSet rs = this.getData("Select COUNT(Students_Id) from Enrolled_course where Course_Id =" + courseId);
            if (rs.next()) {
                num = rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOCourse.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return num;
    }

    public boolean checkStudentEnroll(int courseId, int StudentId) {
        boolean check = false;
        try {
            ResultSet rs = this.getData("Select * from Enrolled_course where "
                    + "Course_Id = " + courseId + "and Students_Id = " + StudentId);
            if (rs.next()) {
                check = true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOCourse.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return check;
    }

    public int enrollStudent(int courseId, int StudentId) {
        int n = 0;
        String sql = "INSERT INTO [dbo].[Enrolled_course]\n"
                + "           ([Course_Id]\n"
                + "           ,[Students_Id]\n"
                + "           ,[Status])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,0)";

        try {
            PreparedStatement pre = connection.prepareStatement(sql);
            //set parameter
            pre.setInt(1, courseId);
            pre.setInt(2, StudentId);
            //run
            n = pre.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAOCourse.class.getName()).log(Level.SEVERE, null, ex);
        }
        return n;
    }

    public Course loadCourseByCourseId(int courseId) {
        try {
            ResultSet rs = this.getData("select * from Courses WHERE Id = " + courseId);
            while (rs.next()) {
                String courseName = rs.getString(2);
                String description = rs.getString(3);
                String objective = rs.getString(4);
                double price = rs.getDouble(5);
                int active = rs.getInt(6);
                int subjectID = rs.getInt(7);
                return new Course(courseId, courseName, description, objective,
                        price, active, subjectID);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOCourse.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public List<CourseSubject> courseSubjectList(String active) {
        List<CourseSubject> list = new ArrayList<>();
        String sql = "SELECT c.Id, c.Name_course, c.Description, c.Objective, c.Price, c.Active, s.Name, c2.sales, c3.studying, c4.completed\n"
                + "FROM Courses c\n"
                + "JOIN (\n"
                + "    SELECT c.Id, COALESCE(ec.sales, 0) AS sales\n"
                + "FROM Courses c\n"
                + "LEFT JOIN (\n"
                + "    SELECT Course_Id, COUNT(*) AS sales FROM Enrolled_course GROUP BY Course_Id) ec ON c.Id = ec.Course_Id\n"
                + ") c2 ON c.Id = c2.Id\n"
                + "JOIN (\n"
                + "    SELECT c.Id, COALESCE(ec.studying, 0) AS studying FROM Courses c\n"
                + "    LEFT JOIN (\n"
                + "        SELECT Course_Id, COUNT(*) AS studying FROM Enrolled_course WHERE Status = 0 GROUP BY Course_Id) ec ON c.Id = ec.Course_Id\n"
                + ") c3 ON c.Id = c3.Id\n"
                + "JOIN (\n"
                + "    SELECT c.Id, COALESCE(ec.completed, 0) AS completed FROM Courses c\n"
                + "    LEFT JOIN (\n"
                + "        SELECT Course_Id, COUNT(*) AS completed FROM Enrolled_course WHERE Status = 1 GROUP BY Course_Id) ec ON c.Id = ec.Course_Id\n"
                + ") c4 ON c.Id = c4.Id\n"
                + "JOIN Subjects s ON s.Id = c.Id where c.Active = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, active);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new CourseSubject(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getString(8),
                        rs.getString(9),
                        rs.getString(10)
                ));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }
    
    public List<CourseSubject> pagingCourseSubject(int index) {
        List<CourseSubject> list = new ArrayList<>();
        String sql = "SELECT c.Id, c.Name_course, c.Description, c.Objective, c.Price, c.Active, s.Name, c2.sales, c3.studying, c4.completed\n"
                + "FROM Courses c\n"
                + "JOIN (\n"
                + "    SELECT c.Id, COALESCE(ec.sales, 0) AS sales\n"
                + "FROM Courses c\n"
                + "LEFT JOIN (\n"
                + "    SELECT Course_Id, COUNT(*) AS sales FROM Enrolled_course GROUP BY Course_Id) ec ON c.Id = ec.Course_Id\n"
                + ") c2 ON c.Id = c2.Id\n"
                + "JOIN (\n"
                + "    SELECT c.Id, COALESCE(ec.studying, 0) AS studying FROM Courses c\n"
                + "    LEFT JOIN (\n"
                + "        SELECT Course_Id, COUNT(*) AS studying FROM Enrolled_course WHERE Status = 0 GROUP BY Course_Id) ec ON c.Id = ec.Course_Id\n"
                + ") c3 ON c.Id = c3.Id\n"
                + "JOIN (\n"
                + "    SELECT c.Id, COALESCE(ec.completed, 0) AS completed FROM Courses c\n"
                + "    LEFT JOIN (\n"
                + "        SELECT Course_Id, COUNT(*) AS completed FROM Enrolled_course WHERE Status = 1 GROUP BY Course_Id) ec ON c.Id = ec.Course_Id\n"
                + ") c4 ON c.Id = c4.Id\n"
                + "JOIN Subjects s ON s.Id = c.Id\n"
                + "ORDER BY Id\n"
                + "OFFSET ? ROWS FETCH NEXT 5 ROWS ONLY";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, (index - 1) * 5);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new CourseSubject(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getString(8),
                        rs.getString(9),
                        rs.getString(10)
                ));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }


    public List<CourseSubject> searchCourseSubject(String active, String key) {
        List<CourseSubject> list = new ArrayList<>();
        String sql = "SELECT c.Id, c.Name_course, c.Description, c.Objective, c.Price, c.Active, s.Name, c2.sales, c3.studying, c4.completed\n"
                + "FROM Courses c\n"
                + "JOIN (\n"
                + "    SELECT c.Id, COALESCE(ec.sales, 0) AS sales\n"
                + "FROM Courses c\n"
                + "LEFT JOIN (\n"
                + "    SELECT Course_Id, COUNT(*) AS sales FROM Enrolled_course GROUP BY Course_Id) ec ON c.Id = ec.Course_Id\n"
                + ") c2 ON c.Id = c2.Id\n"
                + "JOIN (\n"
                + "    SELECT c.Id, COALESCE(ec.studying, 0) AS studying FROM Courses c\n"
                + "    LEFT JOIN (\n"
                + "        SELECT Course_Id, COUNT(*) AS studying FROM Enrolled_course WHERE Status = 0 GROUP BY Course_Id) ec ON c.Id = ec.Course_Id\n"
                + ") c3 ON c.Id = c3.Id\n"
                + "JOIN (\n"
                + "    SELECT c.Id, COALESCE(ec.completed, 0) AS completed FROM Courses c\n"
                + "    LEFT JOIN (\n"
                + "        SELECT Course_Id, COUNT(*) AS completed FROM Enrolled_course WHERE Status = 1 GROUP BY Course_Id) ec ON c.Id = ec.Course_Id\n"
                + ") c4 ON c.Id = c4.Id\n"
                + "JOIN Subjects s ON s.Id = c.Id where c.Active = ? and c.Name_course like ?\n";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, active);
            st.setString(2, "%" + key + "%");
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                list.add(new CourseSubject(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getString(8),
                        rs.getString(9),
                        rs.getString(10)
                ));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<CourseSubject> getSaleCourseSubjectAsc(String active) {
        List<CourseSubject> list = new ArrayList<>();
        String sql = "SELECT c.Id, c.Name_course, c.Description, c.Objective, c.Price, c.Active, s.Name, c2.sales, c3.studying, c4.completed\n"
                + "FROM Courses c\n"
                + "JOIN (\n"
                + "    SELECT c.Id, COALESCE(ec.sales, 0) AS sales\n"
                + "FROM Courses c\n"
                + "LEFT JOIN (\n"
                + "    SELECT Course_Id, COUNT(*) AS sales FROM Enrolled_course GROUP BY Course_Id) ec ON c.Id = ec.Course_Id\n"
                + ") c2 ON c.Id = c2.Id\n"
                + "JOIN (\n"
                + "    SELECT c.Id, COALESCE(ec.studying, 0) AS studying FROM Courses c\n"
                + "    LEFT JOIN (\n"
                + "        SELECT Course_Id, COUNT(*) AS studying FROM Enrolled_course WHERE Status = 0 GROUP BY Course_Id) ec ON c.Id = ec.Course_Id\n"
                + ") c3 ON c.Id = c3.Id\n"
                + "JOIN (\n"
                + "    SELECT c.Id, COALESCE(ec.completed, 0) AS completed FROM Courses c\n"
                + "    LEFT JOIN (\n"
                + "        SELECT Course_Id, COUNT(*) AS completed FROM Enrolled_course WHERE Status = 1 GROUP BY Course_Id) ec ON c.Id = ec.Course_Id\n"
                + ") c4 ON c.Id = c4.Id\n"
                + "JOIN Subjects s ON s.Id = c.Id where c.Active = ?\n"
                + "ORDER BY sales asc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, active);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new CourseSubject(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getString(8),
                        rs.getString(9),
                        rs.getString(10)
                ));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<CourseSubject> getSaleCourseSubjectDesc(String active) {
        List<CourseSubject> list = new ArrayList<>();
        String sql = "SELECT c.Id, c.Name_course, c.Description, c.Objective, c.Price, c.Active, s.Name, c2.sales, c3.studying, c4.completed\n"
                + "FROM Courses c\n"
                + "JOIN (\n"
                + "    SELECT c.Id, COALESCE(ec.sales, 0) AS sales\n"
                + "FROM Courses c\n"
                + "LEFT JOIN (\n"
                + "    SELECT Course_Id, COUNT(*) AS sales FROM Enrolled_course GROUP BY Course_Id) ec ON c.Id = ec.Course_Id\n"
                + ") c2 ON c.Id = c2.Id\n"
                + "JOIN (\n"
                + "    SELECT c.Id, COALESCE(ec.studying, 0) AS studying FROM Courses c\n"
                + "    LEFT JOIN (\n"
                + "        SELECT Course_Id, COUNT(*) AS studying FROM Enrolled_course WHERE Status = 0 GROUP BY Course_Id) ec ON c.Id = ec.Course_Id\n"
                + ") c3 ON c.Id = c3.Id\n"
                + "JOIN (\n"
                + "    SELECT c.Id, COALESCE(ec.completed, 0) AS completed FROM Courses c\n"
                + "    LEFT JOIN (\n"
                + "        SELECT Course_Id, COUNT(*) AS completed FROM Enrolled_course WHERE Status = 1 GROUP BY Course_Id) ec ON c.Id = ec.Course_Id\n"
                + ") c4 ON c.Id = c4.Id\n"
                + "JOIN Subjects s ON s.Id = c.Id where c.Active = ?\n"
                + "ORDER BY sales desc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, active);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new CourseSubject(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getString(8),
                        rs.getString(9),
                        rs.getString(10)
                ));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    
    public int insertCourse(String courseName, String description, String objective, String price, String active, String subjectId){
        try{
            String sql = "insert into Courses (Name_course, Description, Objective, Price, Active, Subject_Id)\n" +
            "values(?, ?, ?, ?, ?, ?)";
            PreparedStatement pstm = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            pstm.setString(1, courseName);
            pstm.setString(2, description);
            pstm.setString(3, objective);
            pstm.setDouble(4, Double.parseDouble(price));
            pstm.setInt(5, Integer.parseInt(active));
            pstm.setInt(6, Integer.parseInt(subjectId));
            pstm.executeUpdate();
            try (ResultSet generatedKeys = pstm.getGeneratedKeys()){
                System.out.println(generatedKeys);
                   if (generatedKeys.next()) {
                    int newCourseId = generatedKeys.getInt(1);
                    // Use the newCourseId as needed
                    System.out.println("Newly generated ID: " + newCourseId);
                    return newCourseId;
                }
            }catch(Exception e){
                System.out.println("generateKey " + e.getMessage());
            }
        }catch(Exception e){
            System.out.println("insert course: " + e.getMessage());
        }
        return -1;
    }
    
    public void addOnCourse(int courseId, int lecturerId){
        try{
            String sql = "INSERT INTO [dbo].[On_course]\n"
                    + "([Courses_Id]\n"
                    + ",[Users_Id])\n"
                    + "VALUES (?,?)";
            PreparedStatement pstm = connection.prepareStatement(sql);
            pstm.setInt(1, courseId);
            pstm.setInt(2, lecturerId);
            pstm.executeUpdate();
        }catch(Exception e){
            System.out.println("insert on course: " + e.getMessage());
        }
    }
    
    public ArrayList<Course> getCourseByLecturerId(int lectureId){
        ArrayList<Course> list = new ArrayList<>();
        try{
            String sql = "select c.Id, c.Name_course, c.Description, c.Objective from Courses c join On_course on c.Id = On_course.Courses_Id\n" + 
                    "where On_course.Users_Id = ?";
            PreparedStatement pstm = connection.prepareStatement(sql);
            pstm.setInt(1,lectureId);
            ResultSet rs = pstm.executeQuery();
            while(rs.next()){
                list.add(new Course(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4)));
            }
        }catch(Exception e){
            System.out.println("Get course by lecturer Id: " + e.getMessage());
        }
        return list;
    }

    public int updateActive(int active, int courseId) {
        int n = 0;
        String sql = "UPDATE [dbo].[Courses]\n"
                + "   SET [Active] = ?\n"
                + "      \n"
                + " WHERE Id = ?";

        try {
            PreparedStatement pre = connection.prepareStatement(sql);
            //set parameter
            pre.setInt(1, active);
            pre.setInt(2, courseId);
            //run
            n = pre.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAOCourse.class.getName()).log(Level.SEVERE, null, ex);
        }
        return n;
    }

    public static void main(String[] args) {
        DAOCourse d = new DAOCourse();
        List<Course> a = d.getFeatureCourseses(5);
        for (Course course : a) {
            System.out.println(course.getCourseName());
        }
    }

}
