/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import entity.Lesson;
import entity.Material;
import entity.MaterialType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.DBContext;

/**
 *
 * @author LENOVO
 */
public class DAOMaterial {

    private Connection con;
    private String status = "ok";

    public DAOMaterial() {
        try {
            con = new DBContext().connection;
        } catch (Exception e) {
            status = "Error" + e.getMessage();
        }
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Material getMaterialByLessonId(int lessonId) {
        Material mat = new Material();
        String sql = "SELECT * FROM Materials WHERE Lesson_Id = ?";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, lessonId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                int no = rs.getInt(2);
                String text = rs.getString(3);
                String link = rs.getString(4);
                int typeId = rs.getInt(6);
                mat = new Material(id, no, text, link, lessonId, typeId);
            }
        } catch (Exception e) {
            status = "Error " + e.getMessage();
        }
        return mat;
    }

    public Vector<MaterialType> getAllMaterialTypes() {
        String sql = "SELECT *\n"
                + "  FROM [Online_Learning].[dbo].[Material_types]";
        Vector<MaterialType> matType = new Vector<>();
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                String typeName = rs.getString(2);
                matType.add(new MaterialType(id, typeName));
            }
        } catch (Exception e) {
            status = "Error " + e.getMessage();
        }
        return matType;
    }

    public int addMaterial(Material material) {
        //Add material detail
        String sql = "INSERT INTO [dbo].[Materials]\n"
                + "           ([Material_no]\n"
                + "           ,[Text]\n"
                + "           ,[Material_link]\n"
                + "           ,[Lesson_Id]\n"
                + "           ,[Material_type_Id])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?)";

        try {
            PreparedStatement pre = con.prepareStatement(sql);
            //set parameter
            pre.setInt(1, material.getNo());
            pre.setString(2, material.getText());
            pre.setString(3, material.getLink());
            pre.setInt(4, material.getLessonId());
            pre.setInt(5, material.getTypeId());
            //run
            int n = pre.executeUpdate();
            return n;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return -1;
    }
    

    public static void main(String[] args) {
        DAOMaterial d = new DAOMaterial();
        System.out.println(d.getMaterialByLessonId(1).getLink());
        System.out.println(d.getAllMaterialTypes().size());
    }
}
