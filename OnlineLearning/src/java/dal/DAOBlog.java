/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import entity.Blog;
import java.sql.SQLException;
import java.util.List;
import model.DBContext;

/**
 *
 * @author LENOVO
 */
public class DAOBlog extends DBContext {

    private Connection con;
    private String status = "1";

    public DAOBlog() {
        try {
            con = new DBContext().connection;
        } catch (Exception e) {
            status = "Error" + e.getMessage();
        }
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<Blog> loadBlog() {
        ArrayList<Blog> blogs = new ArrayList();
        String sql = "SELECT Id, Title, [Description], [Image], Link, Created_at"
                + "FROM [Blogs] where status = 1";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                String title = rs.getString(2);
                String description = rs.getString(3);
                String image = rs.getString(4);
                String link = rs.getString(5);
                String created_at = rs.getString(6).substring(0, 16);
                blogs.add(new Blog(id, title, description, image, link, created_at));
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            status = "Error " + e.getMessage();
        }
        return blogs;
    }

    public ArrayList<Blog> searchBlogByTitle(String textSearch, int offset, int pageSize) {
        ArrayList<Blog> blogs = new ArrayList();
        String sql = "SELECT Id, Title, [Description], [Image], Link, Created_at"
                + " FROM Blogs WHERE FREETEXT(Title, ?) and Status = 1"
                + " ORDER BY Created_at DESC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, textSearch);
            ps.setInt(2, offset);
            ps.setInt(3, pageSize);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                String title = rs.getString(2);
                String description = rs.getString(3);
                String image = rs.getString(4);
                String link = rs.getString(5);
                String created_at = rs.getString(6).substring(0, 16);
                blogs.add(new Blog(id, title, description, image, link, created_at));
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            status = "Error " + e.getMessage();
        }
        return blogs;
    }

    public int loadTotalBlogSearch(String textSearch) {
        String sql = "SELECT COUNT(*) FROM [Blogs] "
                + "WHERE FREETEXT(Title, ?) and Status = 1";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, textSearch);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                int totalCount = rs.getInt(1);
                return totalCount;
            }
        } catch (Exception e) {
            status = "Error " + e.getMessage();
        }
        return 0;
    }

    public Blog loadBlogDetailById(String blogId) {
        Blog blog = new Blog();
        String sql = "SELECT Title, Content, [Image], Link, Created_at "
                + "FROM [Blogs] WHERE Id LIKE ? and Status = 1";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, blogId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String title = rs.getString(1);
                String connectiontent = rs.getString(2);
                String description = "";
                String image = rs.getString(3);
                String link = rs.getString(4);
                String created_at = rs.getString(5).substring(0, 16);
                blog = new Blog(Integer.parseInt(blogId), title, connectiontent, description, image, link, created_at);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
        }
        return blog;
    }

    public ArrayList<Blog> loadBlog(int offset, int pageSize) {
        ArrayList<Blog> blogs = new ArrayList();
        String sql = "SELECT Id, Title, [Description], [Image], Link, Created_at"
                + " FROM Blogs where Status = 1 ORDER BY Created_at DESC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, offset);
            ps.setInt(2, pageSize);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                String title = rs.getString(2);
                String description = rs.getString(3);
                String image = rs.getString(4);
                String link = rs.getString(5);
                String created_at = rs.getString(6).substring(0, 16);
                blogs.add(new Blog(id, title, description, image, link, created_at));
            }
        } catch (Exception e) {
            status = "Error " + e.getMessage();
        }
        return blogs;
    }
    /**
     * 
     * @param size
     * @return size = 0 return all, else return TOP size
     */
        public ArrayList<Blog> loadBlogOrderByDate(int size) {
            String topQuery = "";
            //check size
            if(size != 0){
                topQuery = "TOP "+size; 
            }
        ArrayList<Blog> blogs = new ArrayList();
        String sql = "SELECT "+topQuery+" Id, Title, [Description], [Image], Link, Created_at"
                + " FROM Blogs where Status = 1 ORDER BY Created_at DESC";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                String title = rs.getString(2);
                String description = rs.getString(3);
                String image = rs.getString(4);
                String link = rs.getString(5);
                String created_at = rs.getString(6).substring(0, 16);
                blogs.add(new Blog(id, title, description, image, link, created_at));
            }
        } catch (Exception e) {
            status = "Error " + e.getMessage();
        }
        return blogs;
    }

    public int loadTotalBlog() {
        String sql = "SELECT COUNT(*) FROM [Blogs] where Status = 1";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                int totalCount = rs.getInt(1);
                return totalCount;
            }
        } catch (Exception e) {
            status = "Error " + e.getMessage();
        }
        return 0;
    }

    public int getTotalBlog() {
        String sql = "select count(*) from Blogs";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return 0;
    }

    public List<Blog> pagingBlog(int index) {
        List<Blog> list = new ArrayList<>();
        String sql = "SELECT * FROM Blogs\n"
                + "ORDER BY Id\n"
                + "OFFSET ? ROWS FETCH NEXT 3 ROWS ONLY";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, (index - 1) * 3);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Blog b = new Blog();
                b.setId(rs.getInt("id"));
                b.setTitle(rs.getString("title"));
                b.setContent(rs.getString("content"));
                b.setDescription(rs.getString("description"));
                b.setImage(rs.getString("image"));
                b.setLink(rs.getString("link"));
                b.setCreated_at(rs.getString("created_at"));
                b.setStatus(rs.getString("status"));
                list.add(b);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public Blog getBlogById(String bId) {
        String sql = "select *from Blogs\n"
                + "where Id= ? ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, bId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Blog b = new Blog();
                b.setId(rs.getInt("id"));
                b.setTitle(rs.getString("title"));
                b.setContent(rs.getString("content"));
                b.setDescription(rs.getString("description"));
                b.setImage(rs.getString("image"));
                b.setLink(rs.getString("link"));
                b.setCreated_at(rs.getString("created_at"));
                b.setStatus(rs.getString("status"));
                return b;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public void editBlog(String title, String content, String describe, String image,
            String link, String create, String Status, String bid) {
        String sql = "UPDATE [dbo].[Blogs]\n"
                + "   SET [Title] = ?\n"
                + "      ,[Content] = ?\n"
                + "      ,[Description] = ?\n"
                + "      ,[Image] = ?\n"
                + "      ,[Link] = ?\n"
                + "      ,[Created_at] = ?\n"
                + "      ,[Status] = ?\n"
                + " WHERE Id = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, title);
            st.setString(2, content);
            st.setString(3, describe);
            st.setString(4, image);
            st.setString(5, link);
            st.setString(6, create);
            st.setString(7, Status);
            st.setString(8, bid);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void deleteBlog(String bid) {
        String sql = "delete from Blogs "
                + "where Id = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, bid);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void insertBlog(String title, String content, String description,
            String image, String link, String status) {
        String sql = "INSERT INTO [dbo].[Blogs]\n"
                + "           ([Title]\n"
                + "           ,[Content]\n"
                + "           ,[Description]\n"
                + "           ,[Image]\n"
                + "           ,[Link]\n"
                + "           ,[Status])\n"
                + "     VALUES\n"
                + "           (?,?,?,?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, title);
            st.setString(2, content);
            st.setString(3, description);
            st.setString(4, image);
            st.setString(5, link);
            st.setString(6, status);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public List<Blog> searchTitleBlog(String key) {
        List<Blog> list = new ArrayList<>();
        String sql = "select * from Blogs "
                + "where Title like ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%" + key + "%");
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                Blog b = new Blog();
                b.setId(rs.getInt("id"));
                b.setTitle(rs.getString("title"));
                b.setContent(rs.getString("content"));
                b.setDescription(rs.getString("description"));
                b.setImage(rs.getString("image"));
                b.setLink(rs.getString("link"));
                b.setCreated_at(rs.getString("created_at"));
                b.setStatus(rs.getString("status"));
                list.add(b);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public static void main(String[] args) {
        DAOBlog d = new DAOBlog();
        ArrayList<Blog> blogs = d.searchBlogByTitle("mastering",0, 10);
        for (int i = 0; i < blogs.size(); i++) {
            System.out.println(blogs.get(i).getCreated_at());
        }
//        System.out.println(d.loadTotalBlogSearch("Mastering Marketing"));
//        System.out.println(d.loadBlogDetailById("1").getId());

//        d.getBlogById("1");
//        System.out.println(d);
    }
}
