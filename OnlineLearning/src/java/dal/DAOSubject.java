/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;


import entity.Subject;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.DBContext;

/**
 *
 * @author ADMIN
 */
public class DAOSubject extends DBContext{
    
    public String getSubjectName(int subId){
        String subName = null;
        try {
            String sql = "Select * from Subjects where Id = " + subId;
            ResultSet rs = this.getData(sql);
            if(rs.next()) {
                subName = rs.getString(2);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOCourse.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        return subName;
    }
    
        public List<Subject> getSubject(){
        List<Subject> list = new ArrayList<>();
        String sql = "Select * from Subjects";
        try{
            PreparedStatement pstm = connection.prepareStatement(sql);
            ResultSet rs = pstm.executeQuery();
            while(rs.next()){
                list.add(new Subject(rs.getInt(1), rs.getString(2)));
            }
        }catch(Exception e){
            System.out.println("get subjects: " + e.getMessage());
        }
        return list;
    }
    
    public void addSubjet() {
        try {
            String sql = "INSERT INTO Subjects  (Name, Description)\n"
                    + "VALUES (?,?);";
            PreparedStatement pstm = connection.prepareStatement(sql);

            pstm.setString(1, "Name");
            pstm.setString(2, "Description");
            pstm.executeUpdate();
        } catch (Exception e) {
            System.out.println("addSubject: " + e.getMessage());
        }
    }
    
}
