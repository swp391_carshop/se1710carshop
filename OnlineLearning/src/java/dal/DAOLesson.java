/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import entity.Blog;
import entity.Lesson;
import entity.Material;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.DBContext;

/**
 *
 * @author LENOVO
 */
public class DAOLesson {

    private Connection con;
    private String status = "ok";

    public DAOLesson() {
        try {
            con = new DBContext().connection;
        } catch (Exception e) {
            status = "Error" + e.getMessage();
        }
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Lesson getLessonById(int lessonId) {
        Lesson lesson = new Lesson();
        String sql = "SELECT * FROM Lessons WHERE Id = ?";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, lessonId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                int lessonNo = rs.getInt(2);
                String name = rs.getString(3);
                int chapterId = rs.getInt(4);
                lesson = new Lesson(id, lessonNo, name, chapterId);
            }
        } catch (Exception e) {
            status = "Error " + e.getMessage();
        }
        return lesson;
    }

    public Vector<Lesson> getLessonByCourseId(int courseId) {
        Vector<Lesson> lessons = new Vector<>();
        String sql = "WITH chapId AS (SELECT Id, Chapter_no FROM Chapters WHERE Courses_Id = ?)\n"
                + "SELECT * FROM Lessons \n"
                + " JOIN \n"
                + "chapId\n"
                + "ON Chapters_Id = chapId.Id ORDER BY Chapter_no, Lesson_no;";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, courseId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                int lessonNo = rs.getInt(2);
                String name = rs.getString(3);
                int chapterId = rs.getInt(4);
                lessons.add(new Lesson(id, lessonNo, name, chapterId));
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            status = "Error " + e.getMessage();
        }
        return lessons;
    }

    public int getLastLessonNo(int chapterId) {
        String sql = "Select Top (1) [Lesson_no]  FROM "
                + "[Online_Learning].[dbo].[Lessons] WHERE Chapters_Id = ? "
                + "ORDER BY Lesson_no DESC";
        int id = 0;
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, chapterId);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                id = rs.getInt(1);
            }
        } catch (Exception e) {
            System.out.println("getLastLessonNo: " + e.getMessage());
        }
        return id;
    }

    public Vector<Lesson> getLessonByChapterId(int chapterId) {
        Vector<Lesson> lessons = new Vector<>();
        String sql = "SELECT *\n"
                + "  FROM [Online_Learning].[dbo].[Lessons] WHERE Chapters_Id = ?";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, chapterId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                int lessonNo = rs.getInt(2);
                String name = rs.getString(3);
                lessons.add(new Lesson(id, lessonNo, name, chapterId));
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            System.out.println("getLessonByChapterId Error: " + e.getMessage());
        }
        return lessons;
    }

    public int addLesson(Lesson lesson) {

        //Add lesson detail
        String sql = "INSERT INTO [dbo].[Lessons]\n"
                + "           ([Lesson_no]\n"
                + "           ,[Name]\n"
                + "           ,[Chapters_Id])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?)";

        try {
            PreparedStatement pre = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            //set parameter
            pre.setInt(1, lesson.getLessonNo());
            pre.setString(2, lesson.getName());
            pre.setInt(3, lesson.getChapterId());
            //run
            pre.executeUpdate();
            try ( ResultSet generatedKeys = pre.getGeneratedKeys()) {
                System.out.println(generatedKeys);
                if (generatedKeys.next()) {
                    int newLessonId = generatedKeys.getInt(1);
                    // Use the newLessonId as needed
                    System.out.println("Newly generated ID: " + newLessonId);
                    return newLessonId;
                }
            } catch (Exception e) {
                System.out.println("add lesson error:  " + e.getMessage());
                e.printStackTrace();
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return -1;
    }

    public void deleteLesson(int lessonId) {
        //Delete all Material of Lesson
        String sql = "DELETE FROM [dbo].[Materials]\n"
                + "      WHERE Lesson_Id = ?";

        try {
            PreparedStatement pre = con.prepareStatement(sql);
            //set parameter
            pre.setInt(1, lessonId);
            //run
            pre.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        //Delete Lesson
        sql = "DELETE FROM [dbo].[Lessons]\n"
                + "      WHERE Id = ?";

        try {
            PreparedStatement pre = con.prepareStatement(sql);
            //set parameter
            pre.setInt(1, lessonId);
            //run
            pre.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
