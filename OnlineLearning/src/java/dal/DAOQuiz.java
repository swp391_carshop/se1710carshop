/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import entity.Quiz;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import model.DBContext;
import java.sql.*;
import java.util.List;
import java.util.Collections;
import java.util.Vector;

/**
 *
 * @author Admin MSI
 */
public class DAOQuiz extends DBContext {

    private Connection con;
    private String status = "ok";

    public DAOQuiz() {
        try {
            con = new DBContext().connection;
        } catch (Exception e) {
            status = "Error" + e.getMessage();
        }
    }

    public ArrayList<Quiz> showListQuiz() {
        String str = "Select name from quiz";
        ArrayList<Quiz> quizzes = new ArrayList<>();

        try {
            PreparedStatement pstm = connection.prepareStatement(str);
            ResultSet rs;
            rs = pstm.executeQuery();
            while (rs.next()) {
                Quiz quizList = new Quiz(rs.getInt(1), rs.getString(2), rs.getInt(3));
                quizzes.add(quizList);
            }
            return quizzes;
        } catch (Exception e) {
            System.out.println("Get list Quiz: " + e.getMessage());
        }

        return null;
    }

    public Vector<Quiz> getQuizByCourseId(int courseId) {
        Vector<Quiz> quizzes = new Vector<>();
        String sql = "WITH chapId AS (SELECT Id, Chapter_no FROM Chapters WHERE Courses_Id = ?)\n"
                + "SELECT * FROM Quiz JOIN chapId ON Chapters_Id = chapId.Id";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, courseId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                String quizName = rs.getString(2);
                int chapterId = rs.getInt(3);
                quizzes.add(new Quiz(id, quizName, chapterId));
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            status = "Error " + e.getMessage();
        }
        return quizzes;
    }

    /**
     *
     * @param quizId
     * @param userId
     * @return 0 if not exist, return 1 if isPass = 0, return 2 if isPass = 1
     */
    public int checkQuizResult(int quizId, int userId) {
        String sql = "SELECT * FROM Quiz_Results WHERE Users_Id = ? AND Quiz_Id = ?";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, userId);
            ps.setInt(2, quizId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                quizId = rs.getInt(1);
                userId = rs.getInt(2);
                int isPass = rs.getInt(3);
                if (isPass == 0) {
                    return 1;
                } else if (isPass == 1) {
                    return 2;
                }
            } else {
                return 0;
            }

        } catch (Exception e) {
            status = "Error " + e.getMessage();
        }

        return 0;
    }

    public void addQuizResult(int quizId, int userId, boolean isPass) {
        String sql = "INSERT INTO Quiz_Results (Users_Id, Quiz_Id, Is_Pass) "
                + "VALUES (?, ?, ?)";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, userId);
            ps.setInt(2, quizId);
            //isPass == true => 1 else => 0
            ps.setInt(3, isPass ? 1 : 0);
            ResultSet rs = ps.executeQuery();
        } catch (Exception e) {
            status = "Error " + e.getMessage();
        }

    }

    public void updateQuizResult(int quizId, int userId, boolean isPass) {
        String sql = "UPDATE [dbo].[Quiz_Results] SET [Is_Pass] = ? "
                + "WHERE Users_Id = ? AND Quiz_Id = ? ";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, isPass ? 1 : 0);
            ps.setInt(2, userId);
            ps.setInt(3, quizId);
            ResultSet rs = ps.executeQuery();
        } catch (Exception e) {
            status = "Error " + e.getMessage();
        }
    }

    public int getCourseIdByQuizId(int quizId) {
        String sql = "SELECT Courses_Id from Quiz_Results qr join Quiz q  on qr.Quiz_Id = q.Id\n"
                + "join Chapters chap on q.Chapters_Id = chap.Id where Quiz_Id = ?";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, quizId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
            status = "Error " + e.getMessage();
        }
        return 0;
    }

    public Quiz getQuizByQuizId(int quizId) {
        Quiz quiz = new Quiz();
        String sql = "SELECT * FROM Quiz WHERE Id = ?";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, quizId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                String quizName = rs.getString(2);
                int chapterId = rs.getInt(3);
                quiz = new Quiz(id, quizName, chapterId);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            status = "Error " + e.getMessage();
        }
        return quiz;
    }

    public int getQuizPassedInCourse(int courseId, int userId) {
        String sql = "SELECT COUNT(*) from Quiz_Results qr join Quiz q  on qr.Quiz_Id = q.Id\n"
                + "join Chapters chap on q.Chapters_Id = chap.Id where Courses_Id = ? AND Is_Pass = 1 AND Users_Id = ?";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, courseId);
            ps.setInt(2, userId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
            status = "Error " + e.getMessage();
        }
        return 0;
    }
    
    public int addQuiz(String quizName, int chapterId){
        try{
            String sql = "insert into Quiz(Name, Chapters_Id)\n" +
            "values (?,?);";
            PreparedStatement pstm = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            pstm.setString(1, quizName);
            pstm.setInt(2, chapterId);
            pstm.executeUpdate();
            try (ResultSet generatedKeys = pstm.getGeneratedKeys()){
                System.out.println(generatedKeys);
                   if (generatedKeys.next()) {
                    int newQuizId = generatedKeys.getInt(1);
                    // Use the newCourseId as needed
                    System.out.println("Newly generated ID: " + newQuizId);
                    return newQuizId;
                }
            }catch(Exception e){
                System.out.println("generateKey " + e.getMessage());
            }
        }catch(Exception e){
            System.out.println("insert quiz: " + e.getMessage());
        }
        return -1;
    } 

    public static void main(String[] args) {
        DAOQuiz d = new DAOQuiz();
//        d.updateQuizResult(1, 26, true);
        System.out.println(d.getQuizByChapterId(1));
    }

    public Vector<Quiz> getQuizByChapterId(int chapterId) {
        Vector<Quiz> quizzes = new Vector<>();
        String sql = "SELECT *\n"
                + "  FROM [Online_Learning].[dbo].[Quiz] WHERE Chapters_Id = ?";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, chapterId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                String quizName = rs.getString(2);
                quizzes.add(new Quiz(id, quizName, chapterId));
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            status = "Error " + e.getMessage();
            e.printStackTrace();
        }
        return quizzes;
    }
    
    public int getQuizIdByNameAndChpterId(String quizName, int chapterId){
        String sql = "select * from Quiz\n" +
                    "where Name=? and Chapters_Id=?";
        try{
            PreparedStatement pstm = con.prepareStatement(sql);
            pstm.setString(1, quizName);
            pstm.setInt(2, chapterId);
            ResultSet rs = pstm.executeQuery();
            while(rs.next()){
                return rs.getInt(1);
            }
        }catch(Exception e){
            System.out.println("getQuizIdByNameAndChpterId: " + e.getMessage());
        }
        return 0;
    }
    public void DeleteQuiz(int quizId) {
        String sql = "DELETE FROM Quiz WHERE Id = ?";
        try {
            PreparedStatement pstm = con.prepareStatement(sql);
            pstm.setInt(1, quizId);
            int rowsAffected = pstm.executeUpdate();
            if (rowsAffected > 0) {
                System.out.println("Quiz with Id " + quizId + " deleted successfully.");
            } else {
                System.out.println("No quiz found with Id " + quizId + ".");
            }
        } catch (Exception e) {
            System.out.println("Delete quiz: " + e.getMessage());
        }
    }

}
