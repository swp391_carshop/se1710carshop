/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller_blog;

import dal.DAOBlog;
import entity.Blog;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 *
 * @author LENOVO
 */
@WebServlet(name = "BlogDetailControl", urlPatterns = {"/blogdetail"})
public class BlogDetailControl extends HttpServlet {

    DAOBlog d;

    public void init() {
        d = new DAOBlog();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try{
        // Extract the news article title and ID from the request URL
        Blog blog = d.loadBlogDetailById(request.getParameter("id"));
        //check if blog id is not found by check if blog title is empty, 
        //redirect to error page
        if(blog.getTitle().isEmpty()){
            Exception e;
        }
        request.setAttribute("blog", blog);
        request.getRequestDispatcher("blog-detail.jsp").forward(request, response);
        }catch(Exception e){
            response.sendRedirect("error404.html");
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
