/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller_blog;

import dal.DAOBlog;
import entity.Blog;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import utils.libs;

/**
 *
 * @author LENOVO
 */
@WebServlet(name = "BlogSearchControl", urlPatterns = {"/blogsearch"})
public class BlogSearchControl extends HttpServlet {

    DAOBlog d;

    public void init() {
        d = new DAOBlog();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //------------------------------search----------------------------------
        String textSearch = request.getParameter("searchbar");
        textSearch = libs.removeExtraWhitespaces(textSearch);
        //check if search input is empty
        if (!textSearch.isEmpty()) {
            request.setAttribute("textSearch", textSearch);//for display textSearch after searching
        }
        request.getRequestDispatcher("blogs").forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
