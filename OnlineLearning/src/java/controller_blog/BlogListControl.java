/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller_blog;

import dal.DAOBlog;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import entity.Blog;
import entity.PageView;
import jakarta.servlet.http.HttpSession;

/**
 *
 * @author LENOVO
 */
@WebServlet(name = "BlogListControl", urlPatterns = {"/blogs"})
public class BlogListControl extends HttpServlet {

    DAOBlog d;

    public void init() {
        d = new DAOBlog();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int index = 0;
        try {
            index = Integer.parseInt(request.getParameter("index"));
        } catch (Exception e) {
            index = 0;
        }
        int nrpp = 3;
        int offset = index * nrpp;
        int totalCount = 0;
        int totalPages = 0;

        ArrayList<Blog> blogs = new ArrayList<>();
        //check if user use search and is using search with pagination
        if (request.getAttribute("textSearch") != null
                || request.getParameter("search")!=null) {
            String textSearch = (request.getAttribute("textSearch") != null)
                    ? (String) request.getAttribute("textSearch") : request.getParameter("search");
            
            request.setAttribute("textS", textSearch);//set value search input 
            //field so that input will be displayed after search
            
            //get search blogs
            totalCount = d.loadTotalBlogSearch(textSearch);
            totalPages = (int) Math.ceil((double) totalCount / nrpp);
            blogs = d.searchBlogByTitle(textSearch, offset, nrpp);
        } else {
            
            //get all blogs
            totalCount = d.loadTotalBlog();
            totalPages = (int) Math.ceil((double) totalCount / nrpp);
            blogs = d.loadBlog(offset, nrpp);
        }

        request.setAttribute("totalPages", totalPages);
        request.setAttribute("index", index);
        request.setAttribute("blogs", blogs);
        request.getRequestDispatcher("blog-list.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
