/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller_sale;

import dal.DAOCourse;
import entity.CourseSubject;
import entity.PageView;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;

/**
 *
 * @author Asus
 */
@WebServlet(name = "ViewCourseList", urlPatterns = {"/viewcourse"})
public class ViewCourseList extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet VIewCourseList</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet VIewCourseList at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DAOCourse d = new DAOCourse();
        PrintWriter out = response.getWriter();

        String go = request.getParameter("go");

        if (go == null) {
            go = "courselist";
        }

        if ("courselist".equals(go)) {
            int index = 0;
            try {
                index = Integer.parseInt(request.getParameter("index"));
            } catch (Exception e) {
                index = 0;
            }

            int nrpp = 5;
            try {
                nrpp = Integer.parseInt(request.getParameter("nrpp"));
                nrpp = nrpp < 0 ? 0 : nrpp;
            } catch (Exception e) {
            }

            List<CourseSubject> list = d.courseSubjectList("1");

            PageView p = new PageView(list.size(), nrpp, index);
            p.calculate();
            request.setAttribute("page", p);
            request.setAttribute("go", go);
            request.setAttribute("saleCourseSubject", list);
            request.getRequestDispatcher("sale-courselist.jsp").forward(request, response);
        }

        if ("searchcourse".equals(go)) {

            String txtSearch = request.getParameter("txtC");

            int index = 0;
            try {
                index = Integer.parseInt(request.getParameter("index"));
            } catch (Exception e) {
                index = 0;
            }

            int nrpp = 5;
            try {
                nrpp = Integer.parseInt(request.getParameter("nrpp"));
                nrpp = nrpp < 0 ? 0 : nrpp;
            } catch (Exception e) {
            }

            List<CourseSubject> list = d.searchCourseSubject("1", txtSearch);
            PageView p = new PageView(list.size(), nrpp, index);
            p.calculate();
            request.setAttribute("page", p);
            request.setAttribute("saleCourseSubject", list);
            request.setAttribute("go", go);
            request.setAttribute("txtC", txtSearch);

            request.getRequestDispatcher("sale-courselist.jsp").forward(request, response);
        }

        if ("sortsalecourse".equals(go)) {
            String o = request.getParameter("type");
            List<CourseSubject> list = null;
            int index = 0;
            try {
                index = Integer.parseInt(request.getParameter("index"));
            } catch (Exception e) {
                index = 0;
            }

            int nrpp = 5;
            try {
                nrpp = Integer.parseInt(request.getParameter("nrpp"));
                nrpp = nrpp < 0 ? 0 : nrpp;
            } catch (Exception e) {
            }

            if (o.equalsIgnoreCase("giam")) {
                list = d.getSaleCourseSubjectDesc("1");
                PageView p = new PageView(list.size(), nrpp, index);
                p.calculate();
                request.setAttribute("page", p);
            } else if (o.equalsIgnoreCase("tang")) {
                list = d.getSaleCourseSubjectAsc("1");
                PageView p = new PageView(list.size(), nrpp, index);
                p.calculate();
                request.setAttribute("page", p);
            }
            
            request.setAttribute("type", o);
            request.setAttribute("saleCourseSubject", list);
            request.setAttribute("go", go);
            request.getRequestDispatcher("sale-courselist.jsp").forward(request, response);
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
