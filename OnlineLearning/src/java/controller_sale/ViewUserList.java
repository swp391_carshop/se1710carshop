/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller_sale;

import dal.DAOUser;
import entity.PageView;
import entity.User;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Asus
 */
@WebServlet(name = "ViewUserList", urlPatterns = {"/viewuser"})
public class ViewUserList extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ViewUserList</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ViewUserList at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DAOUser d = new DAOUser();
        PrintWriter out = response.getWriter();

        String go = request.getParameter("go");

        if (go == null) {
            go = "userlist";
        }

        if ("userlist".equals(go)) {
            int index = 0;
            try {
                index = Integer.parseInt(request.getParameter("index"));
            } catch (Exception e) {
                index = 0;
            }

            int nrpp = 11;
            try {
                nrpp = Integer.parseInt(request.getParameter("nrpp"));
                nrpp = nrpp < 0 ? 0 : nrpp;
            } catch (Exception e) {
            }

            List<User> userList = d.UserList(5);

            PageView p = new PageView(userList.size(), nrpp, index);
            p.calculate();
            request.setAttribute("page", p);
            request.setAttribute("go", go);
            request.setAttribute("saleUserList", userList);
            request.getRequestDispatcher("sale-userlist.jsp").forward(request, response);
        }

        if ("searchusersale".equals(go)) {

            String txtSearch = request.getParameter("txtB");

            int index = 0;
            try {
                index = Integer.parseInt(request.getParameter("index"));
            } catch (Exception e) {
                index = 0;
            }

            int nrpp = 11;
            try {
                nrpp = Integer.parseInt(request.getParameter("nrpp"));
                nrpp = nrpp < 0 ? 0 : nrpp;
            } catch (Exception e) {
            }
            List<User> list = d.searchUsername(txtSearch, 5);
            PageView p = new PageView(list.size(), nrpp, index);
            p.calculate();
            request.setAttribute("page", p);
            request.setAttribute("saleUserList", list);
            request.setAttribute("go", go);
            request.setAttribute("txtB", txtSearch);

            request.getRequestDispatcher("sale-userlist.jsp").forward(request, response);
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String txtSearch = request.getParameter("txt");
        DAOUser d = new DAOUser();
        List<User> list = d.searchUsername(txtSearch, 5);
        PrintWriter out = response.getWriter();
        out.println("<table width=\"100%\" class=\"table table-hover\" id=\"dataTables-example\">\n"
                + "                                    <thead>\n"
                + "                                        <tr>\n"
                + "                                            <th>Id</th>\n"
                + "                                            <th>Full Name</th>\n"
                + "                                            <th>Username</th>\n"
                + "                                            <th>Email</th>\n"
                + "                                            <th>Phone</th>\n"
                + "                                        </tr>\n"
                + "                                    </thead>\n"
                + "                                    <tbody>");
        boolean hasResult = false;
        for (User a : list) {
            out.println("<tr>");
            out.println("    <td>" + a.getId() + "</td>");
            out.println("    <td>" + a.getFullname() + "</td>");
            out.println("    <td>");
            out.println("        <form action=\"loadusersale\" method=\"post\" id=\"userForm\">");
            out.println("            <input type=\"button\" value=\"" + a.getUsername() + "\" class=\"link-button\" onmouseover=\"this.style.fontWeight = 'bold'; this.style.color = 'blue'\" onmouseout=\"this.style.fontWeight = 'normal'; this.style.color = 'black'\" onclick=\"submitForm('" + a.getId() + "')\">");
            out.println("            <input type=\"hidden\" id=\"aid\" name=\"aid\">");
            out.println("        </form>");
            out.println("    </td>");
            out.println("    <td>" + a.getEmail() + "</td>");
            out.println("    <td>" + a.getPhone() + "</td>");
            out.println("</tr>");

            hasResult = true;
        }

        out.println("</tbody>\n"
                + "                                </table>");
        if (!hasResult) {
            out.println("<div class=\"no-result-found\">");
            out.println("    <h3 style=\"color: black\">No Result Found</h3>");
            out.println("</div>");
            }
        }

        /**
         * Returns a short description of the servlet.
         *
         * @return a String containing servlet description
         */
        @Override
        public String getServletInfo
        
            () {
        return "Short description";
        }// </editor-fold>

    }
