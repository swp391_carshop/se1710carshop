
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller_sale;

import dal.DAOStatistic;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Map;
import java.util.Vector;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;




/**
 *
 * @author LENOVO
 */
@WebServlet(name = "SaleDashboardControl", urlPatterns = {"/sale-dashboard"})
public class SaleDashboardControl extends HttpServlet {

    DAOStatistic d;

    public void init() {
        d = new DAOStatistic();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //get weekly enrollment
        int enrollments = (int) d.loadWeeklyEnrollment(0);
        double enrollChangePercent = d.loadWeeklyEnrollment(1);

        //----------------------ENROLLMENT LINE CHART---------------------------
        //get data of daily, monthly, yearly enrollment        
        Map<String, Integer> dailyEnrollment = d.loadDailyEnrollment();
        Map<String, Integer> monthlyEnrollment = d.loadMonthlyEnrollment();
        Map<String, Integer> yearlyEnrollment = d.loadYearlyEnrollment();

        //create string of a list of latest 7 days enrollment
        StringBuilder days = new StringBuilder();
        StringBuilder dayRevenue = new StringBuilder();
        ArrayList<Map.Entry<String, Integer>> dayList
                = new ArrayList<>(dailyEnrollment.entrySet());

        //loop to access to each item in map
        for (int i = 0; i < dayList.size(); i++) {
            //create string contain date
            days.append("'").append(dayList.get(i).getKey()).append("'");
            //check if last item, no append ','
            if (!dayList.get(i).equals(dayList.get(dayList.size() - 1))) {
                days.append(", ");
            }
            //create string contain num of enrollments
            dayRevenue.append("'").append(dayList.get(i).getValue()).append("'");
            //check if last item, no append ','
            if (!dayList.get(i).equals(dayList.get(dayList.size() - 1))) {
                dayRevenue.append(", ");
            }
        }

        //create string of a list of latest 12 months enrollment
        StringBuilder months = new StringBuilder();
        StringBuilder monthRevenue = new StringBuilder();
        ArrayList<Map.Entry<String, Integer>> monthList
                = new ArrayList<>(monthlyEnrollment.entrySet());

        for (int i = 0; i < monthList.size(); i++) {
            months.append("'").append(monthList.get(i).getKey().subSequence(0, 7)).append("'");
            //check if last item, no append ','
            if (!monthList.get(i).equals(monthList.get(monthList.size() - 1))) {
                months.append(", ");
            }
            monthRevenue.append("'").append(monthList.get(i).getValue()).append("'");
            //check if last item, no append ','
            if (!monthList.get(i).equals(monthList.get(monthList.size() - 1))) {
                monthRevenue.append(", ");
            }
        }

        //create string of a list of latest 5 years enrollment
        StringBuilder years = new StringBuilder();
        StringBuilder yearRevenue = new StringBuilder();
        ArrayList<Map.Entry<String, Integer>> yearList
                = new ArrayList<>(yearlyEnrollment.entrySet());

        for (int i = 0; i < yearList.size(); i++) {
            years.append("'").append(yearList.get(i).getKey()).append("'");
            //check if last item, no append ','
            if (!yearList.get(i).equals(yearList.get(yearList.size() - 1))) {
                years.append(", ");
            }
            yearRevenue.append("'").append(yearList.get(i).getValue()).append("'");
            //check if last item, no append ','
            if (!yearList.get(i).equals(yearList.get(yearList.size() - 1))) {
                yearRevenue.append(", ");
            }
        }

        //create js query to draw enrollment line charts
        String revenueChart = "<script>var revenueOption = document.getElementById(\"revenueOption\");\n"
                + "var revenueChart = document.getElementById(\"revenueChart\");\n"
                + "var revContainer = null;\n"
                + "\n"
                + "revenueDailyChart();\n"
                + "\n"
                + "function revenueDailyChart() {\n"
                + "    revContainer = new Chart(revenueChart, {\n"
                + "        type: 'line',\n"
                + "        data: {\n"
                + "            labels: [" + days + "],\n"
                + "            datasets: [{\n"
                + "                    data: [" + dayRevenue + "],\n"
                + "                    backgroundColor: \"rgba(48, 164, 255, 0.2)\",\n"
                + "                    borderColor: \"rgba(48, 164, 255, 0.8)\",\n"
                + "                    fill: true,\n"
                + "                    borderWidth: 1\n"
                + "                }]\n"
                + "        },\n"
                + "        options: {\n"
                + "            animation: {\n"
                + "                duration: 2000,\n"
                + "                easing: 'easeOutQuart',\n"
                + "            },\n"
                + "            plugins: {\n"
                + "                legend: {\n"
                + "                    display: false,\n"
                + "                    position: 'right',\n"
                + "                },\n"
                + "                title: {\n"
                + "                    display: true,\n"
                + "                    text: 'Enrollments',\n"
                + "                    position: 'left',\n"
                + "                },\n"
                + "            },\n"
                + "        }\n"
                + "    });\n"
                + "}\n"
                + "\n"
                + "function revenueMonthlyChart() {\n"
                + "    revContainer = new Chart(revenueChart, {\n"
                + "        type: 'line',\n"
                + "        data: {\n"
                + "            labels: [" + months + "],\n"
                + "            datasets: [{\n"
                + "                    data: [" + monthRevenue + "],\n"
                + "                    backgroundColor: \"rgba(48, 164, 255, 0.2)\",\n"
                + "                    borderColor: \"rgba(48, 164, 255, 0.8)\",\n"
                + "                    fill: true,\n"
                + "                    borderWidth: 1\n"
                + "                }]\n"
                + "        },\n"
                + "        options: {\n"
                + "            animation: {\n"
                + "                duration: 2000,\n"
                + "                easing: 'easeOutQuart',\n"
                + "            },\n"
                + "            plugins: {\n"
                + "                legend: {\n"
                + "                    display: false,\n"
                + "                    position: 'right',\n"
                + "                },\n"
                + "                title: {\n"
                + "                    display: true,\n"
                + "                    text: 'Enrollments',\n"
                + "                    position: 'left',\n"
                + "                },\n"
                + "            },\n"
                + "        }\n"
                + "    });\n"
                + "}\n"
                + "\n"
                + "function revenueYearlyChart() {\n"
                + "    revContainer = new Chart(revenueChart, {\n"
                + "        type: 'line',\n"
                + "        data: {\n"
                + "            labels: [" + years + "],\n"
                + "            datasets: [{\n"
                + "                    data: [" + yearRevenue + "],\n"
                + "                    backgroundColor: \"rgba(48, 164, 255, 0.2)\",\n"
                + "                    borderColor: \"rgba(48, 164, 255, 0.8)\",\n"
                + "                    fill: true,\n"
                + "                    borderWidth: 1\n"
                + "                }]\n"
                + "        },\n"
                + "        options: {\n"
                + "            animation: {\n"
                + "                duration: 2000,\n"
                + "                easing: 'easeOutQuart',\n"
                + "            },\n"
                + "            plugins: {\n"
                + "                legend: {\n"
                + "                    display: false,\n"
                + "                    position: 'right',\n"
                + "                },\n"
                + "                title: {\n"
                + "                    display: true,\n"
                + "                    text: 'Enrollments',\n"
                + "                    position: 'left',\n"
                + "                },\n"
                + "            },\n"
                + "        }\n"
                + "    });\n"
                + "}\n"
                + "\n"
                + "// Event listener for dropdown change\n"
                + "revenueOption.addEventListener('change', function () {\n"
                + "    // Clear the previous chart if it exists\n"
                + "    if (revContainer !== null) {\n"
                + "        revContainer.destroy();\n"
                + "    }\n"
                + "\n"
                + "    // Create the selected chart based on the dropdown value\n"
                + "    if (this.value === 'daily') {\n"
                + "        revenueDailyChart();\n"
                + "    } else if (this.value === 'monthly') {\n"
                + "        revenueMonthlyChart();\n"
                + "    } else {\n"
                + "        revenueYearlyChart();\n"
                + "    }\n"
                + "});</script>";

        //----------------------SUBJECT ENROLLMENT PIE CHART--------------------
        //get data of daily, monthly, yearly enrollment        
        Map<String, Double> dailySubEnrollment = d.loadDailyEnrollmentPerSubject();
        Map<String, Double> monthlySubEnrollment = d.loadMonthlyEnrollmentPerSubject();
        Map<String, Double> yearlySubEnrollment = d.loadYearlyEnrollmentPerSubject();

        //create string of a list of latest 7 days subject enrollment
        StringBuilder daySubjects = new StringBuilder();
        StringBuilder daySubEnroll = new StringBuilder();
        ArrayList<Map.Entry<String, Double>> daySubList
                = new ArrayList<>(dailySubEnrollment.entrySet());

        //loop to access to each item in map
        for (int i = 0; i < daySubList.size(); i++) {
            //create string contain date
            daySubjects.append("'").append(daySubList.get(i).getKey()).append("'");
            //check if last item, no append ','
            if (!daySubList.get(i).equals(daySubList.get(daySubList.size() - 1))) {
                daySubjects.append(", ");
            }
            //create string contain num of enrollments
            daySubEnroll.append("'").append(daySubList.get(i).getValue()).append("'");
            //check if last item, no append ','
            if (!daySubList.get(i).equals(daySubList.get(daySubList.size() - 1))) {
                daySubEnroll.append(", ");
            }
        }

        //create string of a list of latest 12 months subject enrollment
        StringBuilder monthSubjects = new StringBuilder();
        StringBuilder monthSubEnroll = new StringBuilder();
        ArrayList<Map.Entry<String, Double>> monthSubList
                = new ArrayList<>(monthlySubEnrollment.entrySet());

        for (int i = 0; i < monthSubList.size(); i++) {
            monthSubjects.append("'").append(monthSubList.get(i).getKey()).append("'");
            //check if last item, no append ','
            if (!monthSubList.get(i).equals(monthSubList.get(monthSubList.size() - 1))) {
                monthSubjects.append(", ");
            }
            monthSubEnroll.append("'").append(monthSubList.get(i).getValue()).append("'");
            //check if last item, no append ','
            if (!monthSubList.get(i).equals(monthSubList.get(monthSubList.size() - 1))) {
                monthSubEnroll.append(", ");
            }
        }

        //create string of a list of latest 5 years enrollment
        StringBuilder yearSubjects = new StringBuilder();
        StringBuilder yearSubEnroll = new StringBuilder();
        ArrayList<Map.Entry<String, Double>> yearSubList
                = new ArrayList<>(yearlySubEnrollment.entrySet());

        for (int i = 0; i < yearSubList.size(); i++) {
            yearSubjects.append("'").append(yearSubList.get(i).getKey()).append("'");
            //check if last item, no append ','
            if (!yearSubList.get(i).equals(yearSubList.get(yearSubList.size() - 1))) {
                yearSubjects.append(", ");
            }
            yearSubEnroll.append("'").append(yearSubList.get(i).getValue()).append("'");
            //check if last item, no append ','
            if (!yearSubList.get(i).equals(yearSubList.get(yearSubList.size() - 1))) {
                yearSubEnroll.append(", ");
            }
        }

        String subEnrollPieChart = "<script>\n"
                + "            var subOption = document.getElementById(\"subEnrollOption\");\n"
                + "            var subChart = document.getElementById(\"enroll-piechart\");\n"
                + "            var pieContainer = null;\n"
                + "            colors = [\"#0074D9\", \"#FF4136\", \"#2ECC40\", \"#FF851B\", \"#7FDBFF\", \"#B10DC9\", \"#FFDC00\", \"#001f3f\", \"#39CCCC\", \"#01FF70\", \"#85144b\", \"#F012BE\", \"#3D9970\", \"#111111\", \"#AAAAAA\"];\n"
                + "\n"
                + "            subDailyChart();\n"
                + "\n"
                + "            function subDailyChart() {\n"
                + "                pieContainer = new Chart(subChart, {\n"
                + "                    type: 'pie',\n"
                + "                    data: {\n"
                + "                        labels: [" + daySubjects + "],\n"
                + "                        datasets: [{\n"
                + "                                data: [" + daySubEnroll + "],\n"
                + "                                backgroundColor: colors,\n"
                + "                                hoverOffset: 4\n"
                + "                            }]\n"
                + "                    },\n"
                + "                    options: {\n"
                + "                        animation: {\n"
                + "                            duration: 2000,\n"
                + "                            easing: 'easeOutQuart',\n"
                + "                        },\n"
                + "                        plugins: {\n"
                + "                            legend: {\n"
                + "                                display: true,\n"
                + "                                position: 'right',\n"
                + "                            },\n"
                + "                            title: {\n"
                + "                                display: false,\n"
                + "                                text: 'Total Value',\n"
                + "                                position: 'left',\n"
                + "                            },\n"
                + "                        },\n"
                + "                    }\n"
                + "                });\n"
                + "            }\n"
                + "\n"
                + "            function subMonthlyChart() {\n"
                + "                pieContainer = new Chart(subChart, {\n"
                + "                    type: 'pie',\n"
                + "                    data: {\n"
                + "                        labels: [" + monthSubjects + "],\n"
                + "                        datasets: [{\n"
                + "                                data: [" + monthSubEnroll + "],\n"
                + "                                backgroundColor: colors,\n"
                + "                                hoverOffset: 4\n"
                + "                            }]\n"
                + "                    },\n"
                + "                    options: {\n"
                + "                        animation: {\n"
                + "                            duration: 2000,\n"
                + "                            easing: 'easeOutQuart',\n"
                + "                        },\n"
                + "                        plugins: {\n"
                + "                            legend: {\n"
                + "                                display: true,\n"
                + "                                position: 'right',\n"
                + "                            },\n"
                + "                            title: {\n"
                + "                                display: false,\n"
                + "                                text: 'Total Value',\n"
                + "                                position: 'left',\n"
                + "                            },\n"
                + "                        },\n"
                + "                    }\n"
                + "                });\n"
                + "            }\n"
                + "\n"
                + "            function subYearlyChart() {\n"
                + "                pieContainer = new Chart(subChart, {\n"
                + "                    type: 'pie',\n"
                + "                    data: {\n"
                + "                        labels: [" + yearSubjects + "],\n"
                + "                        datasets: [{\n"
                + "                                data: [" + yearSubEnroll + "],\n"
                + "                                backgroundColor: colors,\n"
                + "                                hoverOffset: 4\n"
                + "                            }]\n"
                + "                    },\n"
                + "                    options: {\n"
                + "                        animation: {\n"
                + "                            duration: 2000,\n"
                + "                            easing: 'easeOutQuart',\n"
                + "                        },\n"
                + "                        plugins: {\n"
                + "                            legend: {\n"
                + "                                display: true,\n"
                + "                                position: 'right',\n"
                + "                            },\n"
                + "                            title: {\n"
                + "                                display: false,\n"
                + "                                text: 'Total Value',\n"
                + "                                position: 'left',\n"
                + "                            },\n"
                + "                        },\n"
                + "                    }\n"
                + "                });\n"
                + "            }\n"
                + "\n"
                + "// Event listener for dropdown change\n"
                + "            subOption.addEventListener('change', function () {\n"
                + "                // Clear the previous chart if it exists\n"
                + "                if (pieContainer !== null) {\n"
                + "                    pieContainer.destroy();\n"
                + "                }\n"
                + "\n"
                + "                // Create the selected chart based on the dropdown value\n"
                + "                if (this.value === 'daily') {\n"
                + "                    subDailyChart();\n"
                + "                } else if (this.value === 'monthly') {\n"
                + "                    subMonthlyChart();\n"
                + "                } else {\n"
                + "                    subYearlyChart();\n"
                + "                }\n"
                + "            });\n"
                + "        </script>";

        //------------------COURSE STATISTICS-----------------------------------
        //get sort by id
        Map<String, Vector> courseStats = d.loadCourseStat(0,5);
        Vector<Integer> id = courseStats.get("id");
        Vector<String> name = courseStats.get("name");
        Vector<String> subject = courseStats.get("subject");
        Vector<Integer> total = courseStats.get("total");
        Vector<Double> complete = courseStats.get("complete");
        Vector<Integer> active = courseStats.get("active");

        request.setAttribute("courseId", id);
        request.setAttribute("name", name);
        request.setAttribute("subject", subject);
        request.setAttribute("total", total);
        request.setAttribute("complete", complete);
        request.setAttribute("active", active);

        //get sort by enrollments
        courseStats = d.loadCourseStat(1,5);
        id = courseStats.get("id");
        name = courseStats.get("name");
        subject = courseStats.get("subject");
        total = courseStats.get("total");
        complete = courseStats.get("complete");
        active = courseStats.get("active");

        request.setAttribute("courseId1", id);
        request.setAttribute("name1", name);
        request.setAttribute("subject1", subject);
        request.setAttribute("total1", total);
        request.setAttribute("complete1", complete);
        request.setAttribute("active1", active);

        //get sort by completion
        courseStats = d.loadCourseStat(2,5);
        id = courseStats.get("id");
        name = courseStats.get("name");
        subject = courseStats.get("subject");
        total = courseStats.get("total");
        complete = courseStats.get("complete");
        active = courseStats.get("active");

        request.setAttribute("courseId2", id);
        request.setAttribute("name2", name);
        request.setAttribute("subject2", subject);
        request.setAttribute("total2", total);
        request.setAttribute("complete2", complete);
        request.setAttribute("active2", active);

        request.setAttribute("subEnrollPieChart", subEnrollPieChart);
        request.setAttribute("revenueChart", revenueChart);
        request.setAttribute("enrollments", enrollments);
        request.setAttribute("enrollChangePercent", enrollChangePercent);
        request.getRequestDispatcher("sale-dashboard.jsp").forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setHeader("Content-Disposition", "attachment; filename=course-statistic.xlsx");

        Map<String, Vector> courseStats = d.loadCourseStat(0,0);
        Vector<Integer> id = courseStats.get("id");
        Vector<String> name = courseStats.get("name");
        Vector<String> subject = courseStats.get("subject");
        Vector<Integer> total = courseStats.get("total");
        Vector<Double> complete = courseStats.get("complete");
        Vector<Integer> active = courseStats.get("active");

//creating an instance of HSSFWorkbook class  
        Workbook  workbook = new XSSFWorkbook();
//invoking creatSheet() method and passing the name of the sheet to be created   
        Sheet sheet = workbook.createSheet("Course Statistics");
//creating the 0th row using the createRow() method  
        Row  rowhead = sheet.createRow((short) 0);
//creating cell by using the createCell() method and setting the values to the cell by using the setCellValue() method  
        rowhead.createCell(0).setCellValue("Id");
        rowhead.createCell(1).setCellValue("Course Name");
        rowhead.createCell(2).setCellValue("Subject Name");
        rowhead.createCell(3).setCellValue("Number of Enrollments");
        rowhead.createCell(4).setCellValue("Completion rates");
        rowhead.createCell(5).setCellValue("Active");

        for (int i = 0; i < id.size(); i++) {
            //creating row  
            Row  row = sheet.createRow((short) i + 1);
            //inserting data in the row  
            row.createCell(0).setCellValue(id.get(i));
            row.createCell(1).setCellValue(name.get(i));
            row.createCell(2).setCellValue(subject.get(i));
            row.createCell(3).setCellValue(total.get(i));
            row.createCell(4).setCellValue(complete.get(i));
            row.createCell(5).setCellValue(active.get(i)==1?"Active":"Inactive");
        }

//closing the workbook  
        workbook.write(response.getOutputStream()); // Write workbook to response.
        workbook.close();
        System.out.println("Excel file has been generated successfully.");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
