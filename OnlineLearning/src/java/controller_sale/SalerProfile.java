/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller_sale;

import dal.DAOUser;
import entity.User;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import utils.libs;

/**
 *
 * @author Asus
 */
@WebServlet(name="SalerProfile", urlPatterns={"/saleprofile"})
public class SalerProfile extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SalerProfile</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SalerProfile at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        HttpSession session = request.getSession();
        String go = request.getParameter("go");
        DAOUser userDAO = new DAOUser();
        try {
            //Get user information to display
            int userId = userDAO.findUserId(String.valueOf(session.getAttribute("Saler")));
            User userData = userDAO.getUserById(userId);
            request.setAttribute("userData", userData);

        } catch (Exception e) {
            e.printStackTrace();
            response.sendRedirect("login");
        }
        if (go == null) {
            go = "profiledetail";
        } 
        System.out.println(go);
        if ("profiledetail".equals(go)) {
            request.getRequestDispatcher("sale-profiledetail.jsp").forward(request, response);
            return;
        }
        if ("editprofile".equals(go)) {
            request.setAttribute("go", go);
            request.getRequestDispatcher("sale-edit-profile.jsp").forward(request, response);
            return;
        }
        if ("changepass".equals(go)) {
            request.setAttribute("go", go);
            request.getRequestDispatcher("sale-edit-profile.jsp").forward(request, response);
            return;
        }
        
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        DAOUser userDAO = new DAOUser();
        String go = request.getParameter("go");
        
        if ("editprofile".equals(go)) {

            try {
                String id = request.getParameter("id");
                String fullname = request.getParameter("fullname");
                String username = request.getParameter("username");
                String email = request.getParameter("email");
                String phone = request.getParameter("phonenumber");

                //Check if all fiels are 
                if (fullname == null || fullname.trim().isEmpty()
                        || username == null || username.trim().isEmpty()
                        || email == null || email.trim().isEmpty()
                        || phone == null || phone.trim().isEmpty()) {
                    request.setAttribute("error", "You must fill all field!");
                    doGet(request, response);
                    return;
                }

                //Check full name has only alphabetical letter and no space at the beginning
                if (!libs.isValidFullname(fullname)) {
                    request.setAttribute("error", "Invalid fullname!");
                    doGet(request, response);
                    return;
                }

                //Check phone number format
                if (!libs.isValidPhoneNumber(phone)) {
                    request.setAttribute("error", "Invalid Phone Number!");
                    doGet(request, response);
                    return;
                }

                userDAO.updateUser(id, username, fullname, phone);
                
                response.sendRedirect("saleprofile?go=profiledetail");
            } catch (Exception e) {
                e.printStackTrace();
                response.sendRedirect("saleprofile?go=profiledetail");
            }

        }
        
        if ("changepass".equals(go)) {
            HttpSession session = request.getSession();
            try {
                //Get user information
                int userId = (Integer) session.getAttribute("userId");
                User userData = userDAO.getUserById(userId);

                //Get input
                String currentPass = request.getParameter("currentPassword");
                String newPassword = request.getParameter("newPassword");
                String confirmPassword = request.getParameter("confirmPassword");

                //check if inputed password is equal to current password
                if (!userData.getPassword().equals(currentPass)) {
                    request.setAttribute("error", "Current password is incorrect!");
                    doGet(request, response);
                    return;
                }

                //check if new password contains letters, digits, special char, not contain whitespace, atleast 8 characters
                if (!libs.isValidPassword(newPassword)) {
                    request.setAttribute("error", "Password must contain letters, digits, special characters and at least 8 characters");
                    doGet(request, response);
                    return;
                }

                //Check if confirmPassword and newPassword is equal
                if (!newPassword.equals(confirmPassword)) {
                    request.setAttribute("error", "Confirm password is not equal to new password");
                    doGet(request, response);
                    return;
                }

                //Change password
                userDAO.setPassWord(userId, newPassword);
                request.setAttribute("status", "Change password successful!");
                
               request.getRequestDispatcher("sale-edit-profile.jsp").forward(request, response);
            } catch (Exception e) {
                System.out.println("Change password error: " + e.getMessage());
                request.setAttribute("error", "There has been an error occured, please try again");
                response.sendRedirect("loadusersale?go=profiledetail");
            }

        }
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
