/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller_lecturer;

import dal.DAOQuestions;
import dal.DAOQuiz;
import entity.QuestionAnswer;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Admin MSI
 */
public class view_list_question extends HttpServlet{

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        DAOQuiz daoQuiz = new DAOQuiz();
        DAOQuestions daoQuestion = new DAOQuestions();
        int chapterId = Integer.parseInt(req.getParameter("chapterId"));
        String quizName = req.getParameter("quizName");
        System.out.println(quizName);
        System.out.println(chapterId);
        int quizId = daoQuiz.getQuizIdByNameAndChpterId(quizName, chapterId);
        System.out.println(quizId);
        String go = req.getParameter("go");
        System.out.println(go);
        if (go.equals("viewQuiz")) {
            ArrayList<QuestionAnswer> ls = daoQuestion.showQuestionLecturer(quizId);
            System.out.println(ls);
            req.setAttribute("listQuestion", ls);
            req.setAttribute("chapterId", chapterId);
            req.getRequestDispatcher("lecturer-view-question.jsp").forward(req, resp);
        }
        if (go.equals("delete")) {
            daoQuestion.DeleteQuestion(quizId);
            daoQuestion.DeleteQuizResult(quizId);
            daoQuiz.DeleteQuiz(quizId);
            resp.sendRedirect(req.getContextPath() + "/addlesson?chapterId=" + chapterId);
        }

    }
}
