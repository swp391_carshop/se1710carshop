/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller_lecturer;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import dal.DAOQuestions;
import entity.Chapter;
import entity.QuestionAnswer;
import java.util.ArrayList;

/**
 *
 * @author Admin MSI
 */
public class add_questions extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            // Get quizId and chapterId from request
            String quizId = request.getParameter("quizId");
            int chapterId = Integer.parseInt(request.getParameter("chapterId"));

            // Get question details from request
            String[] questionTexts = request.getParameterValues("question");
            String[] optionAs = request.getParameterValues("optionA");
            String[] optionBs = request.getParameterValues("optionB");
            String[] optionCs = request.getParameterValues("optionC");
            String[] optionDs = request.getParameterValues("optionD");
            String[] optionCorrectChoices = request.getParameterValues("correctChoice");

            // Process each question and add to the database
            DAOQuestions daoQuestion = new DAOQuestions();
            for (int i = 0; i < questionTexts.length; i++) {
                String question = questionTexts[i];
                String optionA = optionAs[i];
                String optionB = optionBs[i];
                String optionC = optionCs[i];
                String optionD = optionDs[i];
                String correctAnswer;
                switch (optionCorrectChoices[i]) {
                    case "A":
                        correctAnswer = optionA;
                        break;
                    case "B":
                        correctAnswer = optionB;
                        break;
                    case "C":
                        correctAnswer = optionC;
                        break;
                    default:
                        correctAnswer = optionD;
                }

                // Add question to the database
                daoQuestion.addQuestion(quizId, question, optionA, optionB, optionC, optionD, correctAnswer);
            }

            // Redirect back to the "addlesson" page with the appropriate chapterId parameter
            response.sendRedirect("addlesson?chapterId=" + chapterId);
        } catch (Exception e) {
            e.printStackTrace();
            // Handle any errors or exceptions here
            // You can redirect to an error page or show an error message to the user
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            int chapterId = Integer.parseInt(req.getParameter("chapterId"));
            req.setAttribute("chapterId", chapterId);
            int quizId = Integer.parseInt(req.getParameter("quizId"));
            req.setAttribute("quizId", quizId);
            req.getRequestDispatcher("lecturer-add-question.jsp").forward(req, resp);
        } catch (Exception e) {
            e.getStackTrace();
            req.getRequestDispatcher("error404.html").forward(req, resp);
        }

    }

}
