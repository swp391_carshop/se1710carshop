/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller_lecturer;

import dal.DAOChapter;
import dal.DAOCourse;
import dal.DAOLesson;
import dal.DAOMaterial;
import dal.DAOQuiz;
import entity.Lesson;
import entity.Material;
import entity.Quiz;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.Vector;

/**
 *
 * @author ADMIN
 */
@WebServlet(name = "add_lesson", urlPatterns = {"/addlesson"})
public class add_lesson extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DAOLesson daoLesson = new DAOLesson();
        DAOChapter daoChapter = new DAOChapter();
        DAOMaterial daoMaterial = new DAOMaterial();
        DAOQuiz daoQuiz = new DAOQuiz();
        try {
            String go = request.getParameter("go");
            if (go == null) {
                go = "listAll";
            }
            
            if (go.equals("listAll")) {
                //Get all lesson for this chapter
                int chapterId = Integer.parseInt(request.getParameter("chapterId"));
                Vector<Lesson> lessonList = daoLesson.getLessonByChapterId(chapterId);
                
                //Get all material of each lesson
                Vector<Material> materialList = new Vector<>();
                for (Lesson less : lessonList) {
                    materialList.add(daoMaterial.getMaterialByLessonId(less.getId()));
                }
                
                //Get all quiz for this chapter
                Vector<Quiz> quizList  = (Vector<Quiz>) daoQuiz.getQuizByChapterId(chapterId);
                
                //Send data to jsp
                request.setAttribute("lessonList", lessonList);
                request.setAttribute("materialList", materialList);
                request.setAttribute("quizList", quizList);
                request.setAttribute("chapterData", daoChapter.getChapterByChapterId(chapterId));
                request.getRequestDispatcher("lecturer-view-lesson.jsp").forward(request, response);
            }
            
            if (go.equals("addLesson")) {
                //Get chapter information
                int chapterId = Integer.parseInt(request.getParameter("chapterId"));
                request.setAttribute("chapterData", daoChapter.getChapterByChapterId(chapterId));
                request.setAttribute("materialTypes", daoMaterial.getAllMaterialTypes());
                request.getRequestDispatcher("lecturer-add-lesson.jsp").forward(request, response);
            }
            
            if (go.equals("delete")) {
                int lessonId = Integer.parseInt(request.getParameter("lid"));
                //Get chapterId of Lesson
                int chapterId = daoLesson.getLessonById(lessonId).getChapterId();
                daoLesson.deleteLesson(lessonId);
                response.sendRedirect("addlesson?chapterId="+chapterId);
                
            }
            
            
        } catch (Exception e) {
            System.out.println("View lesson: " + e.getMessage());
            e.printStackTrace();
            response.sendRedirect("loaddatalecturer");
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DAOLesson daoLesson = new DAOLesson();
        DAOChapter daoChapter = new DAOChapter();
        DAOMaterial daoMaterial = new DAOMaterial();
        try {
            String go = request.getParameter("go");
            if (go.equals("addLesson")) {
                //Get all input value to add
                String chapterName = request.getParameter("chapterName");
                String materialText = request.getParameter("materialText");
                String materialLink = request.getParameter("materialLink");
                int materialType = Integer.parseInt(request.getParameter("materialType"));
                int chapterId = Integer.parseInt(request.getParameter("chapterId"));
                
                //Create lesson object
                int lessonNo = daoLesson.getLastLessonNo(chapterId) + 1;
                Lesson lesson = new Lesson(lessonNo, chapterName, chapterId);
                int lessonId = daoLesson.addLesson(lesson);
                //Create material object
                Material material = new Material(1, materialText, materialLink, lessonId, materialType);
                daoMaterial.addMaterial(material);
                response.sendRedirect("addlesson?chapterId="+chapterId);
            }
            
        } catch (Exception e) {
            System.out.println("Add lesson: " + e.getMessage());
            e.printStackTrace();
            response.sendRedirect("loaddatalecturer");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
