/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller_lecturer;

import dal.DAOCourse;
import dal.DAOUser;
import entity.Course;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Admin MSI
 */
public class load_data_lecturer extends HttpServlet{

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        DAOUser daoUser = new DAOUser();
        int lecturerId = daoUser.findUserId(String.valueOf(session.getAttribute("Lecturer")));
        DAOCourse daoCourse = new DAOCourse();
        ArrayList<Course> listCourse = daoCourse.getCourseByLecturerId(lecturerId);
        req.setAttribute("listCourse", listCourse);
        req.getRequestDispatcher("lecturer.jsp").forward(req, resp);
    }
    
}
