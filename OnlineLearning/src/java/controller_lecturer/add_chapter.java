/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller_lecturer;

import dal.DAOChapter;
import dal.DAOLesson;
import dal.DAOQuestions;
import dal.DAOQuiz;
import entity.Chapter;
import entity.Lesson;
import entity.Quiz;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Vector;

/**
 *
 * @author Admin MSI
 */
public class add_chapter extends HttpServlet{

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String chapterName = req.getParameter("chapter-name");
        String chapterNo = req.getParameter("chapter-no");
        String chapterObjective = req.getParameter("chapter-objective");
        String CourseId = req.getParameter("cid");
        
        
        DAOChapter daoChapter = new DAOChapter();
        int chapterId = daoChapter.insertChapter(chapterName, chapterNo, chapterObjective, Integer.parseInt(CourseId));
        req.setAttribute("chapterId", chapterId);
        resp.sendRedirect("addlesson?chapterId="+chapterId);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        DAOChapter daoChapter = new DAOChapter();
        DAOLesson daoLesson = new DAOLesson();
        DAOQuiz daoQuiz = new DAOQuiz();
        DAOQuestions daoQuestion = new DAOQuestions();
        try {
            String go = req.getParameter("go");
            if (go == null) {
                go = "listAll";
            }

            if (go.equals("listAll")) {
                int courseId = Integer.parseInt(req.getParameter("cid"));
                Vector<Chapter> listChapter = daoChapter.getChaptersByCourseId(courseId);
                req.setAttribute("cid", courseId);
                req.setAttribute("listChapter", listChapter);
                req.getRequestDispatcher("lecturer-view-chapter.jsp").forward(req, resp);
                return;
            }
            if (go.equals("add")) {
                req.getRequestDispatcher("lecturer-add-chapter.jsp").forward(req, resp);
                return;
            }
            if (go.equals("delete")) {
                //Get chapter Id to delete
                int chapterId = Integer.parseInt(req.getParameter("chapterId"));
                int courseId = daoChapter.getChapterByChapterId(chapterId).getCourses_Id();
                //Get all lesson of chapter
                Vector<Lesson> listLesson = daoLesson.getLessonByChapterId(chapterId);
                //Delete each lesson of chapter
                for (Lesson lesson : listLesson) {
                    daoLesson.deleteLesson(lesson.getId());
                }
                //Get all quiz of chapter
                Vector<Quiz> listQuiz =  daoQuiz.getQuizByChapterId(chapterId);
                //Delete each quiz of chapter
                for (Quiz quiz : listQuiz) {
                    int quizId = quiz.getId();
                    daoQuestion.DeleteQuestion(quizId);
                    daoQuestion.DeleteQuizResult(quizId);
                    daoQuiz.DeleteQuiz(quizId);
                }
                
                //Delete chapter
                daoChapter.deleteChapter(chapterId);
                
                resp.sendRedirect("addchapter?cid=" + courseId);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Add chapter: " + e.getMessage());
            resp.sendRedirect("error404.html");
        }
    } 
}
