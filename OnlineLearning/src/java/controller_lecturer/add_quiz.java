/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller_lecturer;

import dal.DAOQuiz;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 *
 * @author Admin MSI
 */
public class add_quiz extends HttpServlet{

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String quizName = req.getParameter("quiz-name");
        int chapterId = Integer.parseInt(req.getParameter("chapterId"));
        System.out.println(chapterId+ "add_quiz servlet");
        DAOQuiz daoQuiz = new DAOQuiz();
        int quizId = daoQuiz.addQuiz(quizName, chapterId);
        req.setAttribute("quizId", quizId);
        resp.sendRedirect("addquestion?chapterId="+chapterId+"&quizId="+quizId);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    }
    
}
