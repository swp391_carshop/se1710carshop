/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller_lecturer;

import dal.DAOCourse;
import dal.DAOSubject;
import dal.DAOUser;
import entity.Course;
import entity.Subject;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Admin MSI
 */
public class add_course extends HttpServlet{

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String courseName = request.getParameter("couresName");
        String descripton = request.getParameter("description");
        String objective = request.getParameter("objective");
        String price = request.getParameter("price");
        String subjectId = request.getParameter("subjectId");
        String active = "0";

        HttpSession session = request.getSession();
        DAOCourse daoCourse = new DAOCourse();
        int newCourseId = daoCourse.insertCourse(courseName, descripton, objective, price, active, subjectId);
        
        DAOUser daoUser = new DAOUser();
        int lecturerId = daoUser.findUserId(String.valueOf(session.getAttribute("Lecturer")));
        daoCourse.addOnCourse(newCourseId, lecturerId);
        request.setAttribute("courseId", newCourseId);
        request.setAttribute("lecturerId", lecturerId);
        response.sendRedirect("addchapter?cid="+newCourseId);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        DAOSubject getListSubject = new DAOSubject();
        List<Subject> list = getListSubject.getSubject();
        DAOUser daoUser = new DAOUser();
        int lecturerId = daoUser.findUserId(String.valueOf(session.getAttribute("Lecturer")));
        DAOCourse daoCourse = new DAOCourse();
        ArrayList<Course> listCourse = daoCourse.getCourseByLecturerId(lecturerId);
//        int courseId = Integer.parseInt(request.getParameter("cid"));
//        request.setAttribute("cid", courseId);
        request.setAttribute("listCourse", listCourse);
        request.setAttribute("listSubject", list);
        request.getRequestDispatcher("lecturer-add-course.jsp").forward(request, response);        
    }
    
}
