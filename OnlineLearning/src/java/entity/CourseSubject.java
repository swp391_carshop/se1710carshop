/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

/**
 *
 * @author Asus
 */
public class CourseSubject {
    private int id;
    private String name_course;
    private String description;
    private String objective;
    private String price;
    private String active;
    private String name;
    private String sale;
    private String studying;
    private String completed;

    public CourseSubject() {
    }

    public CourseSubject(int id, String name_course, String description, String objective, String price, String active, String name, String sale, String studying, String completed) {
        this.id = id;
        this.name_course = name_course;
        this.description = description;
        this.objective = objective;
        this.price = price;
        this.active = active;
        this.name = name;
        this.sale = sale;
        this.studying = studying;
        this.completed = completed;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName_course() {
        return name_course;
    }

    public void setName_course(String name_course) {
        this.name_course = name_course;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getObjective() {
        return objective;
    }

    public void setObjective(String objective) {
        this.objective = objective;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSale() {
        return sale;
    }

    public void setSale(String sale) {
        this.sale = sale;
    }

    public String getStudying() {
        return studying;
    }

    public void setStudying(String studying) {
        this.studying = studying;
    }

    public String getCompleted() {
        return completed;
    }

    public void setCompleted(String completed) {
        this.completed = completed;
    }
}
