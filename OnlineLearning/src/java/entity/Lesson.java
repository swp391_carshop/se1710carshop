/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

/**
 *
 * @author LENOVO
 */
public class Lesson {
    private int id;
    private int lessonNo;
    private String name;
    private int chapterId;

    public Lesson() {
    }

    public Lesson(int lessonNo, String name, int chapterId) {
        this.lessonNo = lessonNo;
        this.name = name;
        this.chapterId = chapterId;
    }
    
    
    //For view
    public Lesson(int id, int lessonNo, String name, int chapterId) {
        this.id = id;
        this.lessonNo = lessonNo;
        this.name = name;
        this.chapterId = chapterId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLessonNo() {
        return lessonNo;
    }

    public void setLessonNo(int lessonNo) {
        this.lessonNo = lessonNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getChapterId() {
        return chapterId;
    }

    public void setChapterId(int chapterId) {
        this.chapterId = chapterId;
    }
    
    
}
