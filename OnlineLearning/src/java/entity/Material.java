/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

/**
 *
 * @author LENOVO
 */
public class Material {
    private int id;
    private int no;
    private String text;
    private String link;
    private int lessonId;
    private int typeId;

    public Material(int no, String text, String link, int lessonId, int typeId) {
        this.no = no;
        this.text = text;
        this.link = link;
        this.lessonId = lessonId;
        this.typeId = typeId;
    }

    public Material(int id, int no, String text, String link, int lessonId, int typeId) {
        this.id = id;
        this.no = no;
        this.text = text;
        this.link = link;
        this.lessonId = lessonId;
        this.typeId = typeId;
    }

    public Material() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public int getLessonId() {
        return lessonId;
    }

    public void setLessonId(int lessonId) {
        this.lessonId = lessonId;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }
    
    
}
