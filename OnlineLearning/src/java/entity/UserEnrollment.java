/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author LENOVO
 */
public class UserEnrollment {
    private int courseId;
    private int userId;
    private String enrollmentDate;
    private int status;
    private String courseName;
    private String subjectName;

    public UserEnrollment() {
    }
    

    public UserEnrollment(int courseId, int userId, String enrollmentDate, int status) {
        this.courseId = courseId;
        this.userId = userId;
        this.enrollmentDate = enrollmentDate;
        this.status = status;
    }
    
    
    public UserEnrollment(String courseName, String subjectName, String enrollmentDate, int status) {
        this.courseName = courseName;
        this.subjectName = subjectName;
        this.enrollmentDate = enrollmentDate;
        this.status = status;
    }

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getEnrollmentDate() {
        return enrollmentDate;
    }

    public void setEnrollmentDate(String enrollmentDate) {
        this.enrollmentDate = enrollmentDate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }


}
