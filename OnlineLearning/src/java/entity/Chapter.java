/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

/**
 *
 * @author ADMIN
 */
public class Chapter {
    private int chapterId;
    private String chapterName;
    private int chapterNo;
    private String objective;
    private int Courses_Id;

    public Chapter() {
    }

    
    //FOr insert
    public Chapter(String chapterName, int chapterNo, String objective, int Courses_Id) {
        this.chapterName = chapterName;
        this.chapterNo = chapterNo;
        this.objective = objective;
        this.Courses_Id = Courses_Id;
    }

    //For update
    public Chapter(int chapterId, String chapterName, int chapterNo, String objective, int Courses_Id) {
        this.chapterId = chapterId;
        this.chapterName = chapterName;
        this.chapterNo = chapterNo;
        this.objective = objective;
        this.Courses_Id = Courses_Id;
    }
    

    public int getChapterId() {
        return chapterId;
    }

    public void setChapterId(int chapterId) {
        this.chapterId = chapterId;
    }

    public String getChapterName() {
        return chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    public int getCourses_Id() {
        return Courses_Id;
    }

    public void setCourses_Id(int Courses_Id) {
        this.Courses_Id = Courses_Id;
    }

    public int getChapterNo() {
        return chapterNo;
    }

    public void setChapterNo(int chapterNo) {
        this.chapterNo = chapterNo;
    }

    public String getObjective() {
        return objective;
    }

    public void setObjective(String objective) {
        this.objective = objective;
    }
    
    
    
    
}
