/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

/**
 *
 * @author ADMIN
 */
public class Course {
    private int courseID;
    private String courseName;
    private String description;
    private String objective;
    private double price;
    private int active;
    private int subjectID;

    public Course(){
    }

    public Course(int courseID, String courseName, String description, String objective) {
        this.courseID = courseID;
        this.courseName = courseName;
        this.description = description;
        this.objective = objective;
    }

    public Course(int courseID, String courseName) {
        this.courseID = courseID;
        this.courseName = courseName;
    }

    public Course(String courseName) {
        this.courseName = courseName;
    }
    
    //For insert
    public Course(String courseName, String description, String objective, double price, int active, int subjectID) {
        this.courseName = courseName;
        this.description = description;
        this.objective = objective;
        this.price = price;
        this.active = active;
        this.subjectID = subjectID;
    }
    
    
    //For update
    public Course(int courseID, String courseName, String description, String objective, double price, int active, int subjectID) {
        this.courseID = courseID;
        this.courseName = courseName;
        this.description = description;
        this.objective = objective;
        this.price = price;
        this.active = active;
        this.subjectID = subjectID;
    }

    public String getObjective() {
        return objective;
    }

    public void setObjective(String objective) {
        this.objective = objective;
    }
    
    
    public int getCourseID() {
        return courseID;
    }

    public void setCourseID(int courseID) {
        this.courseID = courseID;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public int getSubjectID() {
        return subjectID;
    }

    public void setSubjectID(int subjectID) {
        this.subjectID = subjectID;
    }
    
    
}
