/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller_quiz;

import dal.DAOQuestions;
import entity.QuestionAnswer;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 *
 * @author Admin MSI
 */
public class GetQuestionsServlet extends HttpServlet{

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
         request.getRequestDispatcher("show-questions.jsp").forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String quizId = request.getParameter("quiz");
        session.setAttribute("quizId", quizId);
        DAOQuestions questionAnswer = new DAOQuestions();
        PrintWriter out = response.getWriter();
        out.println("Clicked value: " + quizId);
        ArrayList<QuestionAnswer> listQA = questionAnswer.getQuestions(quizId);
        session.setAttribute("listQuestions", listQA);
        request.setAttribute("listQuestions", listQA);
        
        request.getRequestDispatcher("show-questions.jsp").forward(request, response);
    }
    
}
