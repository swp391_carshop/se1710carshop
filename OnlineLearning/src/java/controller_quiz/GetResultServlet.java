/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller_quiz;

import dal.DAOCourse;
import dal.DAOQuestions;
import dal.DAOQuiz;
import dal.DAOUser;
import entity.QuestionAnswer;
import entity.User;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.util.ArrayList;
import utils.SendMail;

public class GetResultServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            DAOQuestions sq = new DAOQuestions();
            DAOQuiz daoQuiz = new DAOQuiz();
            DAOUser daoUser = new DAOUser();
            DAOCourse daoCourse = new DAOCourse();
            ArrayList<QuestionAnswer> list = new ArrayList<>();
//        String ExamCode = request.getParameter("code");
            HttpSession session = request.getSession();
            String ExamCode = (String) session.getAttribute("quizId");
//        int TotalQues = (int) session.getAttribute("totalQues");
//        String[] code = ExamCode.split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)");
            list = sq.getQuestions(ExamCode);
            int count = 0;
            for (QuestionAnswer s : list) {
                String x = String.valueOf(s.getId());
                if (request.getParameter(x) != null) {
                    String userAnswer = request.getParameter(x);
                    if (userAnswer.equals(s.getAnswer())) {
                        count++;
                    }
                }
            }
//        String time = request.getParameter("time");
//        request.setAttribute("time", time);

//        request.setAttribute("cauDung", count);
//        request.getRequestDispatcher("ExamClient.jsp").forward(request, response);
            PrintWriter out = response.getWriter();
            float grade = ((float) count / 5) * 10;
            request.setAttribute("sum", String.valueOf(count));
            request.setAttribute("Grade", String.valueOf(grade));

            //check grade 
            boolean pass = false;
            if (grade >= 8) {
                pass = true;
            }

            int studentId = (int) session.getAttribute("userId");
            int quizId = Integer.parseInt(ExamCode);
            int check = daoQuiz.checkQuizResult(quizId, studentId);
            //check user result exist or is pass
            if (check == 0) {
                //if not exist ==> add
                daoQuiz.addQuizResult(quizId, studentId, pass);
            } else if (check == 1) {
                //if exist but not pass ==> update
                daoQuiz.updateQuizResult(quizId, studentId, pass);
            }

            int courseId = daoQuiz.getCourseIdByQuizId(quizId);

            //send mail to user if user complete the course after passing this quiz
            int coursePassed = daoUser.loadUserEnrollment(courseId, studentId).getStatus();
            //check if user pass the course
            if (coursePassed != 1) {
                int totalQuizInCourse = (daoQuiz.getQuizByCourseId(courseId)).size();
                int quizPassed = daoQuiz.getQuizPassedInCourse(courseId, studentId);
                //check if user has completed all quiz in course
                if (quizPassed == totalQuizInCourse) {
                    System.out.println("Passed");
                    //set enrollment status: completed
                    daoUser.updateEnrollmentStatus(courseId, studentId, 1);
                    User user = daoUser.getUserById(studentId);
                    //get user email
                    String email = user.getEmail();
                    //send email
                    SendMail send = new SendMail();
                    String courseName = daoCourse.loadCourseByCourseId(courseId).getCourseName();
                    String fullName = user.getFullname();
                    send.sendEmail(email, "You completed " + courseName + "!",
                            "<!DOCTYPE html\n"
                            + "	PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n"
                            + "<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:v=\"urn:schemas-microsoft-com:vml\"\n"
                            + "	xmlns:o=\"urn:schemas-microsoft-com:office:office\">\n"
                            + "\n"
                            + "<head>\n"
                            + "	<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n"
                            + "	<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n"
                            + "	<style type=\"text/css\">\n"
                            + "		/* ----- Custom Font Import ----- */\n"
//                            + "		@import url(https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic&subset=latin,latin-ext);\n"
                            + "\n"
                            + "		/* ----- Text Styles ----- */\n"
                            + "		table {\n"
                            + "			font-family: 'Lato', Arial, sans-serif;\n"
                            + "			-webkit-font-smoothing: antialiased;\n"
                            + "			-moz-font-smoothing: antialiased;\n"
                            + "			font-smoothing: antialiased;\n"
                            + "		}\n"
                            + "\n"
                            + "		@media only screen and (max-width: 700px) {\n"
                            + "\n"
                            + "			/* ----- Base styles ----- */\n"
                            + "			.full-width-container {\n"
                            + "				padding: 0 !important;\n"
                            + "			}\n"
                            + "\n"
                            + "			.container {\n"
                            + "				width: 100% !important;\n"
                            + "			}\n"
                            + "\n"
                            + "			/* ----- Header ----- */\n"
                            + "			.header td {\n"
                            + "				padding: 30px 15px 30px 15px !important;\n"
                            + "			}\n"
                            + "\n"
                            + "			/* ----- Projects list ----- */\n"
                            + "			.projects-list {\n"
                            + "				display: block !important;\n"
                            + "			}\n"
                            + "\n"
                            + "			.projects-list tr {\n"
                            + "				display: block !important;\n"
                            + "			}\n"
                            + "\n"
                            + "			.projects-list td {\n"
                            + "				display: block !important;\n"
                            + "			}\n"
                            + "\n"
                            + "			.projects-list tbody {\n"
                            + "				display: block !important;\n"
                            + "			}\n"
                            + "\n"
                            + "			.projects-list img {\n"
                            + "				margin: 0 auto 25px auto;\n"
                            + "			}\n"
                            + "\n"
                            + "			/* ----- Half block ----- */\n"
                            + "			.half-block {\n"
                            + "				display: block !important;\n"
                            + "			}\n"
                            + "\n"
                            + "			.half-block tr {\n"
                            + "				display: block !important;\n"
                            + "			}\n"
                            + "\n"
                            + "			.half-block td {\n"
                            + "				display: block !important;\n"
                            + "			}\n"
                            + "\n"
                            + "			.half-block__image {\n"
                            + "				width: 100% !important;\n"
                            + "				background-size: cover;\n"
                            + "			}\n"
                            + "\n"
                            + "			.half-block__content {\n"
                            + "				width: 100% !important;\n"
                            + "				box-sizing: border-box;\n"
                            + "				padding: 25px 15px 25px 15px !important;\n"
                            + "			}\n"
                            + "\n"
                            + "			/* ----- Hero subheader ----- */\n"
                            + "			.hero-subheader__title {\n"
                            + "				padding: 80px 15px 15px 15px !important;\n"
                            + "				font-size: 35px !important;\n"
                            + "			}\n"
                            + "\n"
                            + "			.hero-subheader__content {\n"
                            + "				padding: 0 15px 90px 15px !important;\n"
                            + "			}\n"
                            + "\n"
                            + "			/* ----- Title block ----- */\n"
                            + "			.title-block {\n"
                            + "				padding: 0 15px 0 15px;\n"
                            + "			}\n"
                            + "\n"
                            + "			/* ----- Paragraph block ----- */\n"
                            + "			.paragraph-block__content {\n"
                            + "				padding: 25px 15px 18px 15px !important;\n"
                            + "			}\n"
                            + "\n"
                            + "			/* ----- Info bullets ----- */\n"
                            + "			.info-bullets {\n"
                            + "				display: block !important;\n"
                            + "			}\n"
                            + "\n"
                            + "			.info-bullets tr {\n"
                            + "				display: block !important;\n"
                            + "			}\n"
                            + "\n"
                            + "			.info-bullets td {\n"
                            + "				display: block !important;\n"
                            + "			}\n"
                            + "\n"
                            + "			.info-bullets tbody {\n"
                            + "				display: block;\n"
                            + "			}\n"
                            + "\n"
                            + "			.info-bullets__icon {\n"
                            + "				text-align: center;\n"
                            + "				padding: 0 0 15px 0 !important;\n"
                            + "			}\n"
                            + "\n"
                            + "			.info-bullets__content {\n"
                            + "				text-align: center;\n"
                            + "			}\n"
                            + "\n"
                            + "			.info-bullets__block {\n"
                            + "				padding: 25px !important;\n"
                            + "			}\n"
                            + "\n"
                            + "			/* ----- CTA block ----- */\n"
                            + "			.cta-block__title {\n"
                            + "				padding: 35px 15px 0 15px !important;\n"
                            + "			}\n"
                            + "\n"
                            + "			.cta-block__content {\n"
                            + "				padding: 20px 15px 27px 15px !important;\n"
                            + "			}\n"
                            + "\n"
                            + "			.cta-block__button {\n"
                            + "				padding: 0 15px 0 15px !important;\n"
                            + "			}\n"
                            + "		}\n"
                            + "	</style>\n"
                            + "\n"
                            + "	<!--[if gte mso 9]><xml>\n"
                            + "			<o:OfficeDocumentSettings>\n"
                            + "				<o:AllowPNG/>\n"
                            + "				<o:PixelsPerInch>96</o:PixelsPerInch>\n"
                            + "			</o:OfficeDocumentSettings>\n"
                            + "		</xml><![endif]-->\n"
                            + "</head>\n"
                            + "\n"
                            + "<body style=\"padding: 0; margin: 0;\" bgcolor=\"#eeeeee\">\n"
                            + "	<span\n"
                            + "		style=\"color:transparent !important; overflow:hidden !important; display:none !important; line-height:0px !important; height:0 !important; opacity:0 !important; visibility:hidden !important; width:0 !important; mso-hide:all;\">This\n"
                            + "		is your preheader text for this email (Read more about email preheaders here - https://goo.gl/e60hyK)</span>\n"
                            + "\n"
                            + "	<!-- / Full width container -->\n"
                            + "	<table class=\"full-width-container\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height=\"100%\" width=\"100%\"\n"
                            + "		bgcolor=\"#eeeeee\" style=\"width: 100%; height: 100%; padding: 30px 0 30px 0;\">\n"
                            + "		<tr>\n"
                            + "			<td align=\"center\" valign=\"top\">\n"
                            + "				<!-- / 700px container -->\n"
                            + "				<table class=\"container\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"700\" bgcolor=\"#ffffff\"\n"
                            + "					style=\"width: 700px;\">\n"
                            + "					<tr>\n"
                            + "						<td align=\"center\" valign=\"top\">\n"
                            + "							<!-- / Header -->\n"
                            + "							<table class=\"container header\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"620\"\n"
                            + "								style=\"width: 620px;\">\n"
                            + "								<tr>\n"
                            + "									<td style=\"padding: 30px 0 30px 0; border-bottom: solid 1px #eeeeee;\" align=\"left\">\n"
                            + "										<a href=\"#\"\n"
                            + "											style=\"font-size: 30px; text-decoration: none; color: #000000;\">EDUBIN</a>\n"
                            + "									</td>\n"
                            + "								</tr>\n"
                            + "							</table>\n"
                            + "							<!-- /// Header -->\n"
                            + "\n"
                            + "							<!-- / Hero subheader -->\n"
                            + "							<table class=\"container hero-subheader\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"\n"
                            + "								width=\"620\" style=\"width: 620px;\">\n"
                            + "								<tr>\n"
                            + "									<td class=\"hero-subheader__title\"\n"
                            + "										style=\"font-size: 43px; font-weight: bold; padding: 40px 0 40px 0;\"\n"
                            + "										align=\"left\">"+fullName+", Congratulations!</td>\n"
                            + "								</tr>\n"
                            + "\n"
                            + "								<tr>\n"
                            + "									<td class=\"hero-subheader__content\"\n"
                            + "										style=\"font-size: 16px; line-height: 27px; color: #969696;\" align=\"left\">\n"
                            + "										Completing an online course is no simple endeavor. It requires time, dedication, and commitment, so when we say \"Congratulations\" - we mean it! Take a moment to reflect on your hard work and enjoy your completion of "+courseName+". You’ve earned it.</td>\n"
                            + "								</tr>\n"
                            + "							</table>\n"
                            + "							<!-- /// Hero subheader -->\n"
                            + "\n"
                            + "							<table class=\"container title-block\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"\n"
                            + "								width=\"100%\">\n"
                            + "								<tr>\n"
                            + "									<td align=\"center\" valign=\"top\">\n"
                            + "										<table class=\"container\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"620\"\n"
                            + "											style=\"width: 620px;\">\n"
                            + "											<tr>\n"
                            + "												<td style=\" padding: 35px 0 0px 0; font-size: 26px;\" align=\"left\">What's next?</td>\n"
                            + "											</tr>\n"
                            + "										</table>\n"
                            + "									</td>\n"
                            + "								</tr>\n"
                            + "							</table>\n"
                            + "							<!-- /// Title -->\n"
                            + "\n"
                            + "							<!-- / Paragraph -->\n"
                            + "							<table class=\"container paragraph-block\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"\n"
                            + "								width=\"100%\">\n"
                            + "								<tr>\n"
                            + "									<td align=\"center\" valign=\"top\">\n"
                            + "										<table class=\"container\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"620\"\n"
                            + "											style=\"width: 620px;\">\n"
                            + "											<tr>\n"
                            + "												<td class=\"paragraph-block__content\"\n"
                            + "													style=\"padding: 25px 0 18px 0; font-size: 16px; line-height: 27px; color: #969696;\"\n"
                            + "													align=\"left\">Whenever you're ready, there's more to learn! Keep your momentum going by finding your next course in our course catalog.\n"
                            + "												</td>\n"
                            + "\n"
                            + "											</tr>\n"
                            + "											<tr>\n"
                            + "												<td class=\"cta-block__button\" width=\"230\" align=\"center\"\n"
                            + "													style=\"width: 200px;\">\n"
                            + "													<a href=\"http://localhost:9999/OnlineLearning/courses\"\n"
                            + "														style=\"border: 3px solid #eeeeee; color: #969696; text-decoration: none; padding: 15px 45px; text-transform: uppercase; display: block; text-align: center; font-size: 16px;\">Browse Catalog</a>\n"
                            + "												</td>\n"
                            + "											</tr>\n"
                            + "										</table>\n"
                            + "									</td>\n"
                            + "								</tr>\n"
                            + "							</table>\n"
                            + "							<!-- /// Paragraph -->\n"
                            + "\n"
                            + "\n"
                            + "							<!-- / Divider -->\n"
                            + "							<table class=\"container\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\"\n"
                            + "								style=\"padding-top: 25px;\" align=\"center\">\n"
                            + "								<tr>\n"
                            + "									<td align=\"center\">\n"
                            + "										<table class=\"container\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"620\"\n"
                            + "											align=\"center\" style=\"border-bottom: solid 1px #eeeeee; width: 620px;\">\n"
                            + "											<tr>\n"
                            + "												<td align=\"center\">&nbsp;</td>\n"
                            + "											</tr>\n"
                            + "										</table>\n"
                            + "									</td>\n"
                            + "								</tr>\n"
                            + "							</table>\n"
                            + "							<!-- /// Divider -->\n"
                            + "						</td>\n"
                            + "					</tr>\n"
                            + "				</table>\n"
                            + "			</td>\n"
                            + "		</tr>\n"
                            + "	</table>\n"
                            + "	<script src=\"https://kit.fontawesome.com/ad151c54e9.js\" crossorigin=\"anonymous\"></script>\n"
                            + "</body>\n"
                            + "\n"
                            + "</html>");
                }

            }
            // Code show detail quiz
            ArrayList<String> idAnswers = new ArrayList<>();
            ArrayList<String> userAnswers = new ArrayList<String>();

            for (QuestionAnswer s : list) {
                String x = String.valueOf(s.getId());
                if (request.getParameter(x) != null) {
                    String answer = request.getParameter(x);
                    idAnswers.add(x);
                    userAnswers.add(answer);
                    System.out.println(idAnswers);
                    System.out.println(userAnswers);
                }
            }
            session.setAttribute("idAnswers", idAnswers);
            session.setAttribute("userAnswers", userAnswers);

            Cookie[] cookies = request.getCookies();
            for (Cookie cooky : cookies) {
                System.out.println(cooky.getName());
                System.out.println(URLDecoder.decode(cooky.getValue(), "UTF-8"));
            }
            request.setAttribute("cook", cookies);

            //Get lesson 
            request.setAttribute("courseId", courseId);
            request.getRequestDispatcher("show-result.jsp").forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect("error403.html");
    }

}
