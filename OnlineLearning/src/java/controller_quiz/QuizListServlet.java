/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller_quiz;

import dal.DAOQuiz;
import entity.Quiz;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Admin MSI
 */
public class QuizListServlet extends HttpServlet{

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DAOQuiz getListQuiz = new DAOQuiz();
        ArrayList<Quiz> quizzes = getListQuiz.showListQuiz();
        HttpSession session = request.getSession();
        session.setAttribute("listQuiz", quizzes);
        request.setAttribute("quizzes", quizzes);
        request.getRequestDispatcher("quiz_list.jsp").forward(request, response);
    }
    
}
