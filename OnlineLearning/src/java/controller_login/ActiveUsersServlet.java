/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller_login;

import clientController.*;
import dal.DAOUser;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

/**
 *
 * @author Asus
 */
@WebServlet(name = "ActiveUsersServlet", urlPatterns = {"/active"})
public class ActiveUsersServlet extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected boolean processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        DAOUser uDao = new DAOUser();
        HttpSession session = request.getSession();
        String email = (String) session.getAttribute("email");
        String code = (String) session.getAttribute("code");

        String trueCode = request.getParameter("validateCode");

        if (trueCode.equals(code)) {
            return true;
        }
        return false;
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        if (processRequest(request, response)) {
            DAOUser uDao = new DAOUser();
            HttpSession session = request.getSession();
            String name = (String) session.getAttribute("fullname");
            String user = (String) session.getAttribute("user");
            String pass = (String) session.getAttribute("pass");
            String email = (String) session.getAttribute("email");
            String phoneNumber = (String) session.getAttribute("phone");
            
            //Register
            //5 for role user
            uDao.register(name, user, pass, email, phoneNumber, 5);
            response.sendRedirect("successfulRegister.jsp");
        } else {
            request.setAttribute("error", "Your code is wrong");
            request.getRequestDispatcher("registerCodeMail.jsp").forward(request, response);
        }
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
