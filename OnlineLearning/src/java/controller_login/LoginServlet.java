/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller_login;

import dal.DAOUser;
//import dao.LoginDAO;
//import entity.LoginBean;
import entity.User;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;

/**
 *
 * @author Asus
 */
@WebServlet(name = "LoginSeverlet", urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet LoginSeverlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet LoginSeverlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String userName = request.getParameter("username").trim();
        String password = request.getParameter("password").trim();
        User loginBean = new User();
        loginBean.setUsername(userName);
        loginBean.setPassword(password);
        DAOUser daoUser = new DAOUser();
        HttpSession session = request.getSession(); //Creating a session
        try {
            String userValidate = daoUser.authenticateUser(loginBean);
            if (userValidate.equals("1")) {
                System.out.println("Admin's Home");
                User dataUser = daoUser.getUserByUsernameAndPassword(userName, password);
                session.setAttribute("Admin", userName);
                session.setAttribute("userName", userName);
                session.setAttribute("dataUser", dataUser);
                session.setAttribute("userId", dataUser.getId());
                response.sendRedirect("adminuser");
            } else if (userValidate.equals("2")) {
                System.out.println("Maketer's Home");
                User dataUser = daoUser.getUserByUsernameAndPassword(userName, password);
                session.setAttribute("fullName", dataUser);
                session.setAttribute("Maketer", userName);
                session.setAttribute("userName", userName);
                session.setAttribute("dataUser", dataUser);
                session.setAttribute("userId", dataUser.getId());
                response.sendRedirect("manageblog");
            } else if (userValidate.equals("3")) {
                System.out.println("Saler's Home");
                User dataUser = daoUser.getUserByUsernameAndPassword(userName, password);
                session.setAttribute("Saler", userName);
                session.setAttribute("userName", userName);
                session.setAttribute("dataUser", dataUser);
                session.setAttribute("userId", dataUser.getId());
                response.sendRedirect("sale-dashboard");
            } else if (userValidate.equals("4")) {
                System.out.println("Lecturer's Home");
                User dataUser = daoUser.getUserByUsernameAndPassword(userName, password);
                session.setAttribute("Lecturer", userName);
                session.setAttribute("userName", userName);
                session.setAttribute("dataUser", dataUser);
                session.setAttribute("userId", dataUser.getId());
                response.sendRedirect("loaddatalecturer");
            } else if (userValidate.equals("5")) {
                System.out.println("User's Home");
                User dataUser = daoUser.getUserByUsernameAndPassword(userName, password);
                session.setAttribute("Learner", userName);
                session.setAttribute("userName", userName);
                session.setAttribute("dataUser", dataUser);
                session.setAttribute("userId", dataUser.getId());
                response.sendRedirect("homepage");
            } else {
                System.out.println("Error message = " + userValidate);
                request.setAttribute("error", userValidate);
                request.getRequestDispatcher("/login.jsp").forward(request, response);
            }
        } catch (IOException e1) {
            e1.printStackTrace();
            response.sendRedirect("homepage");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
