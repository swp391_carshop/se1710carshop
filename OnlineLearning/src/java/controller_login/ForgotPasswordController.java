package controller_login;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
import dal.DAOUser;
import entity.User;
import jakarta.servlet.RequestDispatcher;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.security.SecureRandom;
import java.util.Random;
import utils.SendMail;

/**
 *
 * @author ADMIN
 */
@WebServlet(name = "ForgotPasswordController", urlPatterns = {"/forgotpassword"})
public class ForgotPasswordController extends HttpServlet {

    private static final String CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()";
    private static final int PASSWORD_LENGTH = 10;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        DAOUser dao = new DAOUser();
        try ( PrintWriter out = response.getWriter()) {
            // Get user information
            String userEmail = request.getParameter("email");
            ResultSet rs = dao.getData("select * from Users where Email = '" + userEmail + "'");
            if (!rs.next()) {
                request.setAttribute("error", "Your email does not exist");
                dispatch(request, response, "forgotpass.jsp");
            } else {
                try {
                    // Send email
                    String newPassword = generateRandomPassword();
                    String subject = "NEW GENERATED PASSWORD FOR YOUR EDUBIN ONLINE LEARNING";
                    String message = "<div style=\"font-family: Helvetica,Arial,sans-serif;min-width:1000px;overflow:auto;line-height:2\">\n"
                            + "  <div style=\"margin:50px auto;width:70%;padding:20px 0\">\n"
                            + "    <div style=\"border-bottom:1px solid #eee\">\n"
                            + "      <a href=\"\" style=\"font-size:1.4em;color: #00466a;text-decoration:none;font-weight:600\">EDUBIN</a>\n"
                            + "    </div>\n"
                            + "    <p style=\"font-size:1.1em\">Xin Chào,</p>\n"
                            + "    <p>You have just sent a request to generate new password. Here is your new generated password:</p>\n"
                            + "    <h2 style=\"background: #00466a;margin: 0 auto;width: max-content;padding: 0 10px;color: #fff;border-radius: 4px;\">" + newPassword  + "</h2>\n"
                            + "    <p style=\"font-size:0.9em;\">If it wasn't you who sent it, please check your account immediately.<br /><b>EDUBIN_ONLINE_LEARNING</b></p>\n"
                            + "    <hr style=\"border:none;border-top:1px solid #eee\" />\n"
                            + "    <div style=\"float:right;padding:8px 0;color:#aaa;font-size:0.8em;line-height:1;font-weight:300\">\n"
                            + "    </div>\n"
                            + "  </div>\n"
                            + "</div>";
                    
                    SendMail sendMail = new SendMail();
                    sendMail.sendEmail(userEmail, subject, message);
                    User user = new User(newPassword, userEmail);
                    int n = dao.updatePassword(user);
                    if (n > 1) {
                        System.out.println("Update password Successfully!");
                    } else {
                        request.setAttribute("error", "There is some mistake, please try again!");
                        System.err.println("Update password failed!");
                    }
                    request.setAttribute("sendEmailSuccess", true);
                } catch (Exception e) {
                    System.err.println("Error sending email: " + e.getMessage());
                }
                dispatch(request, response, "forgotpass.jsp");
            }
        } catch (SQLException ex) {
            Logger.getLogger(ForgotPasswordController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static String generateRandomPassword() {
        StringBuilder sb = new StringBuilder(PASSWORD_LENGTH);
        Random random = new SecureRandom();
        for (int i = 0; i < PASSWORD_LENGTH; i++) {
            int randomIndex = random.nextInt(CHARACTERS.length());
            char randomChar = CHARACTERS.charAt(randomIndex);
            sb.append(randomChar);
        }
        return sb.toString();
    }

    void dispatch(HttpServletRequest request, HttpServletResponse response, String url)
            throws ServletException, IOException {
        RequestDispatcher disp = request.getRequestDispatcher(url);
        //run
        disp.forward(request, response);
    }


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
