/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */


package controller_login;

import dal.DAOUser;
import helper.RandomCode;
import utils.SendMail;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import utils.libs;

/**
 *
 * @author Asus
 */
@WebServlet(name = "RegisterServlet", urlPatterns = {"/register"})
public class RegisterServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet LoginSeverlet</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet LoginSeverlet at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("register.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        DAOUser uDAO = new DAOUser();
        String fullName = request.getParameter("fullname");
        String user = request.getParameter("user");
        String pass = request.getParameter("pass");
        String repass = request.getParameter("repass");
        String email = request.getParameter("email");
        String phoneNumber = request.getParameter("phone");
        
        
        if (fullName == null || fullName.trim().isEmpty()
                || user == null || user.trim().isEmpty()
                || email == null || email.trim().isEmpty()
                || phoneNumber == null || phoneNumber.trim().isEmpty()
                || pass == null || pass.trim().isEmpty()
                || repass == null || repass.trim().isEmpty()) {
            request.setAttribute("error", "You must fill all field!");
            request.getRequestDispatcher("register.jsp").forward(request, response);
        } else {
            //fullname chua moi letter va ko co dau cach dau ten
            if (!libs.isValidFullname(fullName)) {
                request.setAttribute("fullnameError", fullName);
                request.setAttribute("usernameError", user);
                request.setAttribute("emailError", email);
                request.setAttribute("phoneNumberError", phoneNumber);
                request.setAttribute("error", "Invalid fullname!");
                request.getRequestDispatcher("register.jsp").forward(request, response);
            } else {
                //check if username has character and number 
                if (user.length() < 15 || !libs.isIncludeAlphabet(user) || !libs.isIncludeDigits(user) || libs.isIncludeSpecialChars(user)) {
                    request.setAttribute("fullnameError", fullName);
                    request.setAttribute("usernameError", user);
                    request.setAttribute("emailError", email);
                    request.setAttribute("phoneNumberError", phoneNumber);
                    request.setAttribute("error", "Username must contain at most 15 characters and contain only letters and numbers");
                    request.getRequestDispatcher("register.jsp").forward(request, response);
                } else {
                    if (!libs.isValidEmail(email)) {
                        request.setAttribute("fullnameError", fullName);
                        request.setAttribute("usernameError", user);
                        request.setAttribute("emailError", email);
                        request.setAttribute("phoneNumberError", phoneNumber);
                        request.setAttribute("error", "Email has already exist!");
                        request.getRequestDispatcher("register.jsp").forward(request, response);
                    } else {
                        //kiem tra username da ton tai trong database
                        if (uDAO.checkAccountExit(user, email) != null) {
                            request.setAttribute("fullnameError", fullName);
                            request.setAttribute("usernameError", user);
                            request.setAttribute("emailError", email);
                            request.setAttribute("phoneNumberError", phoneNumber);
                            request.setAttribute("error", "Username has already exist!");
                            request.getRequestDispatcher("register.jsp").forward(request, response);
                        } else {

                            //kiem tra phone number dung
                            if (!libs.isValidPhoneNumber(phoneNumber)) {
                                request.setAttribute("fullnameError", fullName);
                                request.setAttribute("usernameError", user);
                                request.setAttribute("emailError", email);
                                request.setAttribute("phoneNumberError", phoneNumber);
                                request.setAttribute("error", "Invalid Phone Number!");
                                request.getRequestDispatcher("register.jsp").forward(request, response);
                            } else {
                                //check if password contains letters, digits, special char, not contain whitespace, atleast 8 characters
                                if (!libs.isValidPassword(pass)) {
                                    request.setAttribute("fullnameError", fullName);
                                    request.setAttribute("usernameError", user);
                                    request.setAttribute("emailError", email);
                                    request.setAttribute("phoneNumberError", phoneNumber);
                                    request.setAttribute("error", "Password must contain letters, digits, special characters and at least 8 characters");
                                    request.getRequestDispatcher("register.jsp").forward(request, response);
                                } else {
                                    //kiem tra pass trung voi confirm
                                    if (!pass.equals(repass)) {
                                        request.setAttribute("fullnameError", fullName);
                                        request.setAttribute("usernameError", user);
                                        request.setAttribute("emailError", email);
                                        request.setAttribute("phoneNumberError", phoneNumber);
                                        request.setAttribute("error", "Password and Confirm Password is not the same!");
                                        request.getRequestDispatcher("register.jsp").forward(request, response);
                                    } else {
                                        RandomCode getRandom = new RandomCode();
                                        String randomCode = getRandom.generateRandomCode(6);

                                        HttpSession session = request.getSession();
                                        session.setAttribute("fullname", fullName);
                                        session.setAttribute("email", email);
                                        session.setAttribute("user", user);
                                        session.setAttribute("code", randomCode);
                                        session.setAttribute("pass", pass);
                                        session.setAttribute("phone", phoneNumber);
                                        session.setMaxInactiveInterval(300);

                                        String subject = "Mã đăng kí tài khoản EDUBIN của bạn";
                                        String message = "<div style=\"font-family: Helvetica,Arial,sans-serif;min-width:1000px;overflow:auto;line-height:2\">\n"
                                                + "  <div style=\"margin:50px auto;width:70%;padding:20px 0\">\n"
                                                + "    <div style=\"border-bottom:1px solid #eee\">\n"
                                                + "      <a href=\"\" style=\"font-size:1.4em;color: #00466a;text-decoration:none;font-weight:600\">EDUBIN</a>\n"
                                                + "    </div>\n"
                                                + "    <p style=\"font-size:1.1em\">Xin Chào,</p>\n"
                                                + "    <p>Bạn vừa đăng ký tài khoản với địa chỉ email này. Vui lòng nhập mã bên dưới để xác minh email của bạn:</p>\n"
                                                + "    <h2 style=\"background: #00466a;margin: 0 auto;width: max-content;padding: 0 10px;color: #fff;border-radius: 4px;\">" + randomCode + "</h2>\n"
                                                + "    <p style=\"font-size:0.9em;\">Nếu bạn không đăng ký tài khoản, vui lòng bỏ qua email này.<br /><b>EDUBIN_ONLINE_LEARNING</b></p>\n"
                                                + "    <hr style=\"border:none;border-top:1px solid #eee\" />\n"
                                                + "    <div style=\"float:right;padding:8px 0;color:#aaa;font-size:0.8em;line-height:1;font-weight:300\">\n"
                                                + "    </div>\n"
                                                + "  </div>\n"
                                                + "</div>";

                                        SendMail send = new SendMail();
                                        send.sendEmail(email, subject, message);
                                        request.getRequestDispatcher("registerCodeMail.jsp").forward(request, response);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
