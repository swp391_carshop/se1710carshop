/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller_login;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;

/**
 *
 * @author Admin MSI
 */
public class LogoutServlet extends HttpServlet{
    private static final long serialVersionUID = 1L;
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false); //Fetch session object
        if (session != null) //If session is not null
        {
            session.invalidate(); //removes all session attributes bound to the session
//            request.setAttribute("errMessage", "You have logged out successfully");
//            RequestDispatcher requestDispatcher = request.getRequestDispatcher("/login.jsp");
//            requestDispatcher.forward(request, response);
            response.sendRedirect("homepage");
            System.out.println("Logged out");
        }
    }
    
}
