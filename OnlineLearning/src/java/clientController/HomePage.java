/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package clientController;

import dal.DAOBlog;
import dal.DAOCourse;
import dal.DAOSlider;
import entity.Blog;
import entity.Course;
import entity.Slider;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 *
 * @author Admin MSI
 */
public class HomePage extends HttpServlet{

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
        try {
            //-------------------------- SLIDER ------------------------------------
            DAOSlider daoSlider = new DAOSlider();
            List<Slider> sliderHome = daoSlider.getAllSlider();

            //------------------------- BLOG ---------------------------------------
            DAOBlog daoBlog = new DAOBlog();
            ArrayList<Blog> blogs = daoBlog.loadBlogOrderByDate(4);
            
            //------------------------- FEATURE COURSE ---------------------------------------
            DAOCourse daoCourse = new DAOCourse();
            Vector<Course> featureCourses = daoCourse.getFeatureCourseses(5);
            System.out.println(featureCourses);
            
            
            req.setAttribute("blogs", blogs);
            req.setAttribute("allSliderHome", sliderHome);
            req.setAttribute("featureCourses", featureCourses);
            
            req.getRequestDispatcher("homepage.jsp").forward(req, resp);
        } catch (Exception e) {
            e.printStackTrace();
            resp.sendRedirect("error404.html");
        }
    }
}
