/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package clientController;

import dal.DAOUser;
import entity.User;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import utils.libs;

/**
 *
 * @author ADMIN
 */
@WebServlet(name = "ChangePasswordServlet", urlPatterns = {"/changepassword"})
public class ChangePasswordServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.getRequestDispatcher("changepassword.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        DAOUser daoUser = new DAOUser();
        try {
            //Get user information
            int userId = (Integer) session.getAttribute("userId");
            User userData = daoUser.getUserById(userId);

            //Get input
            String currentPass = request.getParameter("currentPassword");
            String newPassword = request.getParameter("newPassword");
            String confirmPassword = request.getParameter("confirmPassword");
            
            //check if inputed password is equal to current password
            if (!userData.getPassword().equals(currentPass)) {
                request.setAttribute("error", "Current password is incorrect!");
                request.getRequestDispatcher("changepassword.jsp").forward(request, response);
                return;
            }
            
            //check if new password contains letters, digits, special char, not contain whitespace, atleast 8 characters
            if (!libs.isValidPassword(newPassword)) {
                request.setAttribute("error", "Password must contain letters, digits, special characters and at least 8 characters");
                request.getRequestDispatcher("changepassword.jsp").forward(request, response);
                return;
            } 
            
            //Check if confirmPassword and newPassword is equal
            if(!newPassword.equals(confirmPassword)){
                request.setAttribute("error", "Confirm password is not equal to new password");
                request.getRequestDispatcher("changepassword.jsp").forward(request, response);
                return;
            }
            
            //Change password
            daoUser.setPassWord(userId,newPassword);
            request.setAttribute("status", "Change password successful!");
            request.getRequestDispatcher("changepassword.jsp").forward(request, response);
        } catch (Exception e) {
            System.out.println("Change password error: " + e.getMessage());
            request.setAttribute("error", "There has been an error occured, please try again");
            request.getRequestDispatcher("changepassword.jsp").forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
