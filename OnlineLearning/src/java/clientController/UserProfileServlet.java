/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package clientController;

import dal.DAOUser;
import entity.Subject;
import entity.User;
import entity.UserEnrollment;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import utils.libs;

/**
 *
 * @author ADMIN
 */
@WebServlet(name = "UserProfileServlet", urlPatterns = {"/profile"})
public class UserProfileServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        DAOUser userDAO = new DAOUser();
        try {
            //Get user information to display
            int userId = (Integer) session.getAttribute("userId");
            User userData = userDAO.getUserById(userId);
            request.setAttribute("userData", userData);

            //--------------------------------my enrollments------------------------
            //get subject of enrolled course
            ArrayList<Subject> subjects = userDAO.loadUserEnrollmentSubject(userId);
            //set attribute for the subject list
            request.setAttribute("subjects", subjects);

            //get index (page number) 
            int index = 0;
            try {
                index = Integer.parseInt(request.getParameter("index"));
            } catch (Exception e) {
                index = 0;
            }
            int nrpp = 10;
            int offset = index * nrpp; //caculate offset
            int totalCount = 0;
            int totalPages = 0;

            ArrayList<UserEnrollment> enrollments = new ArrayList<>();

            //if user choose a subject, get all enrollments which have courses in this subject
            //else, get all enrollments
            if (request.getParameter("subject") != null) {
                int subjectId = Integer.parseInt(request.getParameter("subject"));

                //set subject = subjectId to use pagination
                request.setAttribute("store", subjectId);

                //get enrollment search results 
                totalCount = userDAO.loadTotalUserEnrollmentBySubjectId(subjectId, userId);
                totalPages = (int) Math.ceil((double) totalCount / nrpp);
                enrollments = userDAO.loadUserEnrollmentBySubjectId(subjectId, userId, offset, nrpp);
            } else {
                //get all enrollments
                totalCount = userDAO.loadTotalUserEnrollment(userId);
                totalPages = (int) Math.ceil((double) totalCount / nrpp);
                enrollments = userDAO.loadUserEnrollment(userId, offset, nrpp);
            }

            request.setAttribute("totalPages", totalPages);
            request.setAttribute("index", index);
            request.setAttribute("enrollments", enrollments);
            request.getRequestDispatcher("./profile.jsp").forward(request, response);
            
        } catch (Exception e) {
            e.printStackTrace();
            response.sendRedirect("homepage");
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            DAOUser daoUser = new DAOUser();
            String id = request.getParameter("id");
            String fullname = request.getParameter("fullname");
            String username = request.getParameter("username");
            String email = request.getParameter("email");
            String phone = request.getParameter("phonenumber");
            //Check if all fields are not empty 
            if (fullname == null || fullname.trim().isEmpty()
                    || username == null || username.trim().isEmpty()
                    || email == null || email.trim().isEmpty()
                    || phone == null || phone.trim().isEmpty()) {
                request.setAttribute("error", "You must fill all field!");
                doGet(request, response);
                return;
            }
            System.out.println(fullname + username + email + phone);
            //Check full name has only alphabetical letter and no space at the beginning
            if (!libs.isValidFullname(fullname)) {
                request.setAttribute("error", "Invalid fullname!");
                doGet(request, response);
                return;
            }
            
            //Check phone number format
            if (!libs.isValidPhoneNumber(phone)) {
                request.setAttribute("error", "Invalid Phone Number!");
                doGet(request, response);
                return;
            }
            
            //If all is valid, update
            daoUser.updateUser(id, username, fullname, phone);
            response.sendRedirect("profile");
        } catch (Exception e) {
            e.printStackTrace();
            response.sendRedirect("profile");
        }
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
