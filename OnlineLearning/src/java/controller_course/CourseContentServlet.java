/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller_course;

import dal.DAOChapter;
import dal.DAOCourse;
import dal.DAOLesson;
import dal.DAOMaterial;
import dal.DAOQuiz;
import dal.DAOUser;
import entity.Chapter;
import entity.Course;
import entity.Lesson;
import entity.Material;
import entity.Quiz;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

/**
 *
 * @author ADMIN
 */
@WebServlet(name = "CourseContent", urlPatterns = {"/learn"})
public class CourseContentServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DAOLesson daoLesson = new DAOLesson();
        DAOQuiz daoQuiz = new DAOQuiz();
        DAOCourse daoCourse = new DAOCourse();
        DAOChapter daoChapter = new DAOChapter();
        DAOMaterial daoMaterial = new DAOMaterial();
        DAOUser daoUser = new DAOUser();

        //Initialize lesson id
        int lessonId = 0;

        //init quiz id 
        int quizId = 0;

//        try {
        try {
            lessonId = Integer.parseInt(request.getParameter("lid"));
        } catch (Exception e) {

        }

        try {
            quizId = Integer.parseInt(request.getParameter("qid"));
        } catch (Exception e) {

        }

        int previousId = 0;
        int previousType = 0;
        int nextId = 0;
        int nextType = 0;
        Course course = new Course();
        Vector<Quiz> quizList = new Vector<>();
        HttpSession session = request.getSession();
        int userId = 0;
        try {
            //get id of current user
            userId = (int) session.getAttribute("userId");
        } catch (Exception e) {
            response.sendRedirect("login");
        }
        //if lesson id != 0 (lid in url), load all course, chapters, 
        //lessons, mat related to it
        if (lessonId != 0) {
            session.setAttribute("lid", lessonId);
            //get lesson info
            Lesson lesson = daoLesson.getLessonById(lessonId);
            request.setAttribute("lesson", lesson);

            //get chapter id by lesson id
            Chapter chap = daoChapter.getChapterByChapterId(lesson.getChapterId());
            request.setAttribute("chapter", chap);

            //get course according to chapterId
            course = daoCourse.loadCourseByCourseId(chap.getCourses_Id());
            request.setAttribute("course", course);

            //check if user enroll this course
            if (daoUser.loadUserEnrollment(course.getCourseID(), userId).getEnrollmentDate() == null) {
                response.sendRedirect("error403.html");
            }

            //load chapter id, name of course according to course id, order by chapter_no
            Vector<Chapter> chapterList = daoChapter.getChaptersByCourseId(course.getCourseID());
            request.setAttribute("chapterList", chapterList);

            //load lesson of course order by lesson_no, chapter_no
            Vector<Lesson> lessonList = daoLesson.getLessonByCourseId(course.getCourseID());
            request.setAttribute("lessonList", lessonList);

            //load quiz of course
            quizList = daoQuiz.getQuizByCourseId(course.getCourseID());
            //sort quiz by chapter id
            Collections.sort(quizList, (o1, o2) -> {
                return Integer.compare(o1.getChapterId(), o2.getChapterId());
            });
            request.setAttribute("quizList", quizList);

            //load material by id
            Material mat = daoMaterial.getMaterialByLessonId(lessonId);
            //convert link to clickable if link is not video
            if (mat.getTypeId() != 3) {
                String[] links = mat.getLink().split("\n");
                StringBuilder output = new StringBuilder();
                for (String part : links) {
                    output.append("<a style=\"display: block;\" href=\"").append(part).append("\" class=\"link-primary\">").append(part).append("</a> ");
                }
                mat.setLink(output.toString());
            }
            request.setAttribute("material", mat);

            Vector<Object> displayList = new Vector<>();
            //loop to each lesson in lessonList to add to displayList
            for (int i = 0; i < lessonList.size(); i++) {
                //check if the lesson is the last lesson of the chapter
                if (i != 0 && lessonList.get(i - 1).getChapterId()
                        != lessonList.get(i).getChapterId()) {
                    //loop to each quiz to get quiz of each chapter
                    for (int j = 0; j < quizList.size(); j++) {
                        //check quiz's chapter id
                        if (quizList.get(j).getChapterId()
                                == lessonList.get(i - 1).getChapterId()) {
                            //add quiz to displayList 
                            displayList.add(quizList.get(j));
                        }
                    }
                    //add lesson to displayList
                    displayList.add(lessonList.get(i));
                } else {
                    //add lesson to displayList
                    displayList.add(lessonList.get(i));
                }
            }

            //loop to each item to get previous and next of current lesson
            for (int i = 0; i < displayList.size(); i++) {
                //get current lesson in displayList
                if (displayList.get(i) instanceof Lesson
                        && ((Lesson) displayList.get(i)).getId() == lessonId) {
                    //check if current is first item
                    if (i != 0) {
                        //check type for previous item of current in displayList
                        if (displayList.get(i - 1) instanceof Lesson) {
                            previousId = ((Lesson) displayList.get(i - 1)).getId();
                            previousType = 1;
                        } else {
                            previousId = ((Quiz) displayList.get(i - 1)).getId();
                            previousType = 2;
                        }
                    }

                    //check if current is last item
                    if (i != displayList.size() - 1) {
                        //check type for next item of current in displayList
                        if (displayList.get(i + 1) instanceof Lesson) {
                            nextId = ((Lesson) displayList.get(i + 1)).getId();
                            nextType = 1;
                        } else {
                            nextId = ((Quiz) displayList.get(i + 1)).getId();
                            nextType = 2;
                        }
                    }
                }
            }

        }

        //if quiz id !=0 (qid in url), load all course, chapters, 
        //lessons, mat related to it
        if (quizId != 0) {
            //get quiz info
            Quiz quiz = daoQuiz.getQuizByQuizId(quizId);
            request.setAttribute("quiz", quiz);

            //get chapter id by lesson id
            Chapter chap = daoChapter.getChapterByChapterId(quiz.getChapterId());
            request.setAttribute("chapter", chap);

            //get course according to chapterId
            course = daoCourse.loadCourseByCourseId(chap.getCourses_Id());
            request.setAttribute("course", course);

            //check if user enroll this course
            if (daoUser.loadUserEnrollment(course.getCourseID(), userId).getEnrollmentDate() == null) {
                response.sendRedirect("error403.html");
            }

            //load chapter id, name of course according to course id, order by chapter_no
            Vector<Chapter> chapterList = daoChapter.getChaptersByCourseId(course.getCourseID());
            request.setAttribute("chapterList", chapterList);

            //load lesson of course order by lesson_no, chapter_no
            Vector<Lesson> lessonList = daoLesson.getLessonByCourseId(course.getCourseID());
            request.setAttribute("lessonList", lessonList);

            //load quiz of course
            quizList = daoQuiz.getQuizByCourseId(course.getCourseID());

            //sort quiz by chapter id
            Collections.sort(quizList, (o1, o2) -> {
                return Integer.compare(o1.getChapterId(), o2.getChapterId());
            });
            request.setAttribute("quizList", quizList);

            //get result of current user for this quiz
            int userResult = daoQuiz.checkQuizResult(quizId, userId);
            request.setAttribute("userResult", userResult);

            Vector<Object> displayList = new Vector<>();
            //loop to each lesson in lessonList to add to displayList
            for (int i = 0; i < lessonList.size(); i++) {
                //check if the lesson is the last lesson of the chapter
                if (i != 0 && lessonList.get(i - 1).getChapterId()
                        != lessonList.get(i).getChapterId()) {
                    //loop to each quiz to get quiz of each chapter
                    for (int j = 0; j < quizList.size(); j++) {
                        //check quiz's chapter id
                        if (quizList.get(j).getChapterId()
                                == lessonList.get(i - 1).getChapterId()) {
                            //add quiz to displayList 
                            displayList.add(quizList.get(j));
                        }
                    }
                    //add lesson to displayList
                    displayList.add(lessonList.get(i));
                } else {
                    //add lesson to displayList
                    displayList.add(lessonList.get(i));
                }
            }

            //loop to each item to get previous and next of current quiz
            for (int i = 0; i < displayList.size(); i++) {
                //get current quiz in displayList
                if (displayList.get(i) instanceof Quiz
                        && ((Quiz) displayList.get(i)).getId() == quizId) {
                    //check if current is first item
                    if (i != 0) {
                        //check type for previous item of current in displayList
                        if (displayList.get(i - 1) instanceof Lesson) {
                            previousId = ((Lesson) displayList.get(i - 1)).getId();
                            previousType = 1;
                        } else {
                            previousId = ((Quiz) displayList.get(i - 1)).getId();
                            previousType = 2;
                        }
                    }

                    //check if current is last item
                    if (i != displayList.size() - 1) {
                        //check type for next item of current in displayList
                        if (displayList.get(i + 1) instanceof Lesson) {
                            nextId = ((Lesson) displayList.get(i + 1)).getId();
                            nextType = 1;
                        } else {
                            nextId = ((Quiz) displayList.get(i + 1)).getId();
                            nextType = 2;
                        }
                    }
                }
            }
        }

        //get result of current user for quizzes
        Set<Integer> quizPassed = new HashSet<>();
        //loop to each quiz
        for (Quiz qui : quizList) {
            //check if user pass this quiz
            if (daoQuiz.checkQuizResult(qui.getId(), userId) == 2) {
                quizPassed.add(qui.getId());
            }
        }
        request.setAttribute("quizPassed", quizPassed);

        int enrollStatus = daoUser.loadUserEnrollment(course.getCourseID(), userId).getStatus();
        //check if user completed this course
        if (enrollStatus == 1) {
            request.setAttribute("coursePassed", true);
        }

        request.setAttribute("previousId", previousId);
        request.setAttribute("previousType", previousType);
        request.setAttribute("nextId", nextId);
        request.setAttribute("nextType", nextType);
//        } catch (Exception e) {
//            response.sendRedirect("courses");
//        }
        request.getRequestDispatcher("course-content.jsp").forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
