/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller_course;

import dal.DAOChapter;
import dal.DAOCourse;
import dal.DAOLesson;
import dal.DAOSubject;
import dal.DAOUser;
import entity.Chapter;
import entity.Course;
import entity.Lesson;
import jakarta.servlet.RequestDispatcher;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.Vector;

/**
 *
 * @author ADMIN
 */
@WebServlet(name = "CourseDetailsServlet", urlPatterns = {"/coursedetails"})
public class CourseDetailsServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    void dispatch(HttpServletRequest request, HttpServletResponse response, String url)
            throws ServletException, IOException {
        RequestDispatcher disp = request.getRequestDispatcher(url);
        //run
        disp.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DAOCourse daoCourse = new DAOCourse();
        DAOSubject daoSub = new DAOSubject();
        DAOChapter daoChapter = new DAOChapter();
        DAOLesson daoLesson = new DAOLesson();
        DAOUser daoUser = new DAOUser();
        HttpSession session = request.getSession(false);
        try ( PrintWriter out = response.getWriter()) {
            try {
                int courseId = Integer.parseInt(request.getParameter("cid"));
                // Retrieve course from the data source
                Vector<Course> vector = daoCourse.getCourseses("select * from Courses where Id = " + courseId);
                if (vector != null) {
                    //Get course data to display
                    Course course = vector.get(0);
                    request.setAttribute("courseDetail", course);

                    //Get 2 courses with related subject
                    Vector<Course> coursesRelated = daoCourse.getCourseses("select TOP 2 * from Courses where Subject_Id = " + course.getSubjectID() + "and Id !=" + courseId);
                    request.setAttribute("coursesRelated", coursesRelated);

                    //Get total number of students of course
                    request.setAttribute("totalStudent", daoCourse.getCourseStudentTotal(courseId));

                    //Get subject name of course
                    request.setAttribute("subjectName", daoSub.getSubjectName(course.getSubjectID()));

                    //load chapter id, name according to course id
                    Vector<Chapter> chapterList = daoChapter.getChaptersByCourseId(course.getCourseID());
                    request.setAttribute("chapterList", chapterList);

                    //load lesson order by lesson_no, chapter_no
                    Vector<Lesson> lessonList = daoLesson.getLessonByCourseId(course.getCourseID());
                    request.setAttribute("lessonList", lessonList);
                }

                //------------------------- COURSE ---------------------------------------
                Vector<Course> featureCourses = daoCourse.getFeatureCourseses(3);
                request.setAttribute("featureCourses", featureCourses);

                if (session != null) {
                    //get id of current user
                    Object userId = session.getAttribute("userId");
                    if (userId != null) {
                        boolean checkEnrolled = daoCourse.checkStudentEnroll(courseId, (int) userId);
                        //Add parameter to check enrollment
                        request.setAttribute("checkEnrolled", checkEnrolled);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
                response.sendRedirect("courses");
            }
            dispatch(request, response, "courses-details.jsp");
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
