package controller_course;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
import dal.DAOCourse;
import entity.Course;
import jakarta.servlet.RequestDispatcher;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.Vector;

/**
 *
 * @author ADMIN
 */
public class CourseListServlet extends HttpServlet {

    private static final int ITEMS_PER_PAGE = 6; // Number of items to display per page

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        DAOCourse dao = new DAOCourse();

        try ( PrintWriter out = response.getWriter()) {
            String go = request.getParameter("go");

            if (go == null) {
                go = "listAll";
            }
            
            if (go.equals("listAll")) {
                // Retrieve all products from the data source
                Vector<Course> vector = dao.getAllActiveCourseses();
                
                int currentPage = 1;
                if (request.getParameter("page") != null) {
                    currentPage = Integer.parseInt(request.getParameter("page"));
                }

                // Calculate the total number of pages
                int totalPages = (int) Math.ceil((double) vector.size() / ITEMS_PER_PAGE);

                // Calculate the starting and ending index for the current page
                int offset = (currentPage - 1) * ITEMS_PER_PAGE;
//                int endIndex = Math.min(startIndex + ITEMS_PER_PAGE, vector.size());

                // Get the sublist of products for the current page
                Vector<Course> courses = dao.getCourseses("SELECT *\n"
                        + "FROM Courses "
                        + "Where Active = 1\n"
                        + "ORDER BY Id\n"
                        + "OFFSET "+ offset +" ROWS\n"
                        + "FETCH NEXT " + ITEMS_PER_PAGE + " ROWS ONLY;");
                // Set the paginated products and pagination information in request attributes
                request.setAttribute("dataCourse", courses);
                request.setAttribute("items_per_page", ITEMS_PER_PAGE);
                request.setAttribute("totalResults", vector.size());
                request.setAttribute("totalPages", totalPages);
                request.setAttribute("currentPage", currentPage);

                // Forward the request to the JSP page for displaying the paginated data
                dispatch(request, response, "courses.jsp");
            }

            // Get the current page number from request parameters
            if (go.equals("search")) {
                String cname = request.getParameter("cname");
                Vector<Course> vector = dao.getCourseses("select * from Courses where Name_course like '%" + cname + "%'");
                
                int currentPage = 1;
                if (request.getParameter("page") != null) {
                    currentPage = Integer.parseInt(request.getParameter("page"));
                }

                // Calculate the total number of pages
                int totalPages = (int) Math.ceil((double) vector.size() / ITEMS_PER_PAGE);

                // Calculate the starting and ending index for the current page
                int startIndex = (currentPage - 1) * ITEMS_PER_PAGE;
                int endIndex = Math.min(startIndex + ITEMS_PER_PAGE, vector.size());

                // Get the sublist of products for the current page
                Vector<Course> courses = new Vector(vector.subList(startIndex, endIndex));

                // Set the paginated products and pagination information in request attributes
                request.setAttribute("dataCourse", courses);
                request.setAttribute("cname", cname);
                request.setAttribute("go", go);
                request.setAttribute("items_per_page", ITEMS_PER_PAGE);
                request.setAttribute("totalResults", vector.size());
                request.setAttribute("totalPages", totalPages);
                request.setAttribute("currentPage", currentPage);

                // Forward the request to the JSP page for displaying the paginated data
                dispatch(request, response, "courses.jsp");
            }
        }
    }

    void dispatch(HttpServletRequest request, HttpServletResponse response, String url)
            throws ServletException, IOException {
        RequestDispatcher disp = request.getRequestDispatcher(url);
        //run
        disp.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
