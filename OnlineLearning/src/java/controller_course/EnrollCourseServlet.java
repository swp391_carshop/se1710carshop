/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller_course;

import dal.DAOCourse;
import dal.DAOLesson;
import dal.DAOUser;
import entity.User;
import jakarta.servlet.RequestDispatcher;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import utils.SendMail;

/**
 *
 * @author ADMIN
 */
@WebServlet(name = "EnrollCourseServlet", urlPatterns = {"/enroll"})
public class EnrollCourseServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            DAOCourse daoCourse = new DAOCourse();
            DAOUser daoUser = new DAOUser();
            DAOLesson daoLesson = new DAOLesson();
            HttpSession session = request.getSession(false); //Creating a session
            int courseId = Integer.parseInt(request.getParameter("cid"));
            
            //If not logged in, back to login
            if (session == null) {
                response.sendRedirect("./login");
                return;
            }
            String username = (String) session.getAttribute("userName");
            if (username == null) {
                response.sendRedirect("./login");
                return;
            }
            
            //Get studentId
            int StudentId = daoUser.findUserId(username);
            //Get user information
            User user = daoUser.getUserById(StudentId);
            
            //Add enroll student to database
            int n = daoCourse.enrollStudent(courseId,StudentId);
            if (n > 0) {
                System.out.println("Enroll Successfully!");
            } else {
                System.err.println("Enroll unsuccessfully!");
            }
            
            
            // Send email
            String subject = "YOU HAVE ENROLLED A NEW COURSE ON EDUBIN";
            String message = "<div style=\"font-family: Helvetica,Arial,sans-serif;min-width:1000px;overflow:auto;line-height:2\">\n"
                    + "  <div style=\"margin:50px auto;width:70%;padding:20px 0\">\n"
                    + "    <div style=\"border-bottom:1px solid #eee\">\n"
                    + "      <a href=\"\" style=\"font-size:1.4em;color: #00466a;text-decoration:none;font-weight:600\">EDUBIN</a>\n"
                    + "    </div>\n"
                    + "    <p style=\"font-size:1.1em\">Hello, </p>\n"
                    + "    <p>You have just enrolled in a new course on our Website</p>\n"
                    + "    <h2 style=\"background: #00466a;margin: 0 auto;width: max-content;padding: 0 10px;color: #fff;border-radius: 4px;\"></h2>\n"
                    + "    <p style=\"font-size:0.9em;\">If it wasn't you who enrolled, please check your account immediately.<br /><b>EDUBIN_ONLINE_LEARNING</b></p>\n"
                    + "    <hr style=\"border:none;border-top:1px solid #eee\" />\n"
                    + "    <div style=\"float:right;padding:8px 0;color:#aaa;font-size:0.8em;line-height:1;font-weight:300\">\n"
                    + "    </div>\n"
                    + "  </div>\n"
                    + "</div>";

            SendMail sendMail = new SendMail();
            sendMail.sendEmail(user.getEmail(), subject, message);
            
            //Get the first lesson of course
            int lessonId = daoLesson.getLessonByCourseId(courseId).get(0).getId();
            
            request.setAttribute("enrollSuccessfull", true);
            request.setAttribute("lessonId", lessonId);
            dispatch(request, response, "./enroll-confirmation.jsp");
        } catch (Exception e) {
            e.printStackTrace();
            response.sendRedirect("courses");
        }
    }

    void dispatch(HttpServletRequest request, HttpServletResponse response, String url)
            throws ServletException, IOException {
        RequestDispatcher disp = request.getRequestDispatcher(url);
        //run
        disp.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
