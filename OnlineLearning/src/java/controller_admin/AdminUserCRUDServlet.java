/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller_admin;

import dal.DAOUser;
import entity.User;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import utils.libs;

/**
 *
 * @author ADMIN
 */
@WebServlet(name = "UserCRUDServlet", urlPatterns = {"/adminuser"})
public class AdminUserCRUDServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DAOUser daoUser = new DAOUser();
        try {
            //Get index for paging
            String indexPage = request.getParameter("index");
            int index = 0;
            if (indexPage == null) {
                index = 1;
            } else {
                try {
                    index = Integer.parseInt(indexPage);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    index = 1;
                }
            }

            //Get go parameter for action
            String go = request.getParameter("go");
            if (go == null) {
                go = "listAll";
            }
            if (go.equals("listAll")) {
                int count = daoUser.getTotalUser();
                int endPage = count / 8;
                if (count % 8 != 0) {
                    endPage++;
                }
                ArrayList<User> userList = (ArrayList<User>) daoUser.pagingUserList(index);

                request.setAttribute("tag", index);
                request.setAttribute("userList", userList);
                request.setAttribute("endP", endPage);
                request.getRequestDispatcher("admin-userlist.jsp").forward(request, response);
            }

            //If searching for a specific user
            if (go.equals("search")) {
                String username = request.getParameter("username");
                int count = daoUser.getTotalSearchUser(username);
                int endPage = count / 8;
                if (count % 8 != 0) {
                    endPage++;
                }
                System.out.println(count);
                ArrayList<User> userList = (ArrayList<User>) daoUser.pagingSearchUserList(username, index);

                request.setAttribute("tag", index);
                request.setAttribute("userList", userList);
                request.setAttribute("endP", endPage);
                System.out.println(userList);
                request.getRequestDispatcher("admin-userlist.jsp").forward(request, response);
            }

            if (go.equals("add")) {
                request.getRequestDispatcher("admin-adduser.jsp").forward(request, response);
            }
            
            if (go.equals("delete")) {
                int id = Integer.parseInt(request.getParameter("id"));
                daoUser.deleteUser(id);
                response.sendRedirect("adminuser");
            }
            
            
            
        } catch (Exception e) {
            e.printStackTrace();
            response.sendRedirect("adminuser");
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String fullName = request.getParameter("fullname");
            String user = request.getParameter("user");
            String pass = request.getParameter("pass");
            String email = request.getParameter("email");
            String phoneNumber = request.getParameter("phone");
            int role = Integer.parseInt(request.getParameter("role"));
            DAOUser daoUser = new DAOUser();

            //Check if all fiels are 
            if (fullName == null || fullName.trim().isEmpty()
                    || user == null || user.trim().isEmpty()
                    || email == null || email.trim().isEmpty()
                    || phoneNumber == null || phoneNumber.trim().isEmpty()
                    || pass == null || pass.trim().isEmpty()) {
                request.setAttribute("error", "You must fill all field!");
                request.getRequestDispatcher("admin-adduser.jsp").forward(request, response);
                return;
            }
            //Check full name has only alphabetical letter and no space at the beginning
            if (!libs.isValidFullname(fullName)) {
                request.setAttribute("fullnameError", fullName);
                request.setAttribute("usernameError", user);
                request.setAttribute("emailError", email);
                request.setAttribute("phoneNumberError", phoneNumber);
                request.setAttribute("error", "Invalid fullname!");
                request.getRequestDispatcher("admin-adduser.jsp").forward(request, response);
                return;
            }
            //check if username has character and number 
            if (user.length() < 10 || !libs.isIncludeAlphabet(user) || !libs.isIncludeDigits(user) || libs.isIncludeSpecialChars(user)) {
                request.setAttribute("fullnameError", fullName);
                request.setAttribute("usernameError", user);
                request.setAttribute("emailError", email);
                request.setAttribute("phoneNumberError", phoneNumber);
                request.setAttribute("error", "Username must contain at most 10 characters and contain only letters and numbers");
                request.getRequestDispatcher("admin-adduser.jsp").forward(request, response);
                return;
            }
            //Check valid email
            if (!libs.isValidEmail(email)) {
                request.setAttribute("fullnameError", fullName);
                request.setAttribute("usernameError", user);
                request.setAttribute("emailError", email);
                request.setAttribute("phoneNumberError", phoneNumber);
                request.setAttribute("error", "Email is not valid!");
                request.getRequestDispatcher("admin-adduser.jsp").forward(request, response);
                return;
            }
            //check existed username and email
            if (daoUser.checkAccountExit(user, email) != null) {
                request.setAttribute("fullnameError", fullName);
                request.setAttribute("usernameError", user);
                request.setAttribute("emailError", email);
                request.setAttribute("phoneNumberError", phoneNumber);
                request.setAttribute("error", "Username or email has already exist!");
                request.getRequestDispatcher("admin-adduser.jsp").forward(request, response);
                return;
            }
            //kiem tra phone number dung
            if (!libs.isValidPhoneNumber(phoneNumber)) {
                request.setAttribute("fullnameError", fullName);
                request.setAttribute("usernameError", user);
                request.setAttribute("emailError", email);
                request.setAttribute("phoneNumberError", phoneNumber);
                request.setAttribute("error", "Invalid Phone Number!");
                request.getRequestDispatcher("admin-adduser.jsp").forward(request, response);
                return;
            }
            //check if password contains letters, digits, special char, not contain whitespace, atleast 8 characters
            if (!libs.isValidPassword(pass)) {
                request.setAttribute("fullnameError", fullName);
                request.setAttribute("usernameError", user);
                request.setAttribute("emailError", email);
                request.setAttribute("phoneNumberError", phoneNumber);
                request.setAttribute("error", "Password must contain letters, digits, special characters and at least 8 characters");
                request.getRequestDispatcher("admin-adduser.jsp").forward(request, response);
                return;
            }
            
            daoUser.register(fullName, user, pass, email, phoneNumber, role);
            response.sendRedirect("adminuser");
            
        } catch (Exception e) {
            e.printStackTrace();
            response.sendRedirect("adminuser");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
