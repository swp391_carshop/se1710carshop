/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller_admin;

import dal.DAOCourse;
import entity.Course;
import entity.CourseSubject;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ADMIN
 */
@WebServlet(name = "AdminCourseServlet", urlPatterns = {"/admincourse"})
public class AdminCourseServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AdminCourseServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AdminCourseServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DAOCourse daoCourse = new DAOCourse();
        try {
            //Get index for paging
            String indexPage = request.getParameter("index");
            int index = 0;
            if (indexPage == null) {
                index = 1;
            } else {
                try {
                    index = Integer.parseInt(indexPage);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    index = 1;
                }
            }

            //Get go parameter for action
            String go = request.getParameter("go");
            if (go == null) {
                go = "listAll";
            }
            if (go.equals("listAll")) {
                int count = daoCourse.getAllCourseses().size();
                int endPage = count / 8;
                if (count % 8 != 0) {
                    endPage++;
                }
                ArrayList<CourseSubject> courseList = (ArrayList<CourseSubject>) daoCourse.pagingCourseSubject(index);
                request.setAttribute("tag", index);
                request.setAttribute("courseList", courseList);
                request.setAttribute("endP", endPage);
                request.getRequestDispatcher("admin-courselist.jsp").forward(request, response);

            }

            //If searching for a specific user
            if (go.equals("search")) {
                String username = request.getParameter("courseName");
                int count = daoCourse.searchCourseSubject("1",username).size();
                int endPage = count / 8;
                if (count % 8 != 0) {
                    endPage++;
                }
                System.out.println(count);
                ArrayList<CourseSubject> courseList = (ArrayList<CourseSubject>) daoCourse.pagingCourseSubject(index);

                request.setAttribute("tag", index);
                request.setAttribute("courseList", courseList);
                request.setAttribute("endP", endPage);
                request.getRequestDispatcher("admin-courselist.jsp").forward(request, response);
            }

            if (go.equals("add")) {
                request.getRequestDispatcher("admin-adduser.jsp").forward(request, response);
            }
            
            if(go.equals("update")){
                int cid = Integer.parseInt(request.getParameter("cid"));
                int active = Integer.parseInt(request.getParameter("active"));
                daoCourse.updateActive(active, cid);
                response.sendRedirect("admincourse");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
