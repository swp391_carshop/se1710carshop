<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="shortcut icon" href="images/favicon.png" type="image/png">
        <title>Add lesson</title>
        <link href="assets/vendor/fontawesome/css/fontawesome.min.css" rel="stylesheet">
        <link href="assets/vendor/fontawesome/css/solid.min.css" rel="stylesheet">
        <link href="assets/vendor/fontawesome/css/brands.min.css" rel="stylesheet">
        <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/css/master.css" rel="stylesheet">
    </head>

    <body>
        <div class="wrapper">
            <jsp:include page="lecturer-sidebar.jsp"/>

            <div id="body" class="active">
                <!-- navbar navigation component -->
                <jsp:include page="lecturer-header.jsp"/>
                <!-- end of navbar navigation -->
                <div class="content">
                    <div class="container">
                        <div class="page-title">
                            <a href="addlesson?chapterId=${chapterData.chapterId}" class="btn-back hidden"><i class="fas fa-arrow-left"></i> Back</a>
                            <h3><i class="fas fa-plus-circle"></i>&ensp;Add lesson to chapter: <span style="color: #FF4136"><b>${chapterData.chapterName}</b></span></h3>
                        </div>
                        <div class="box box-primary">
                            <div class="box-body">
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade active show" id="general" role="tabpanel" aria-labelledby="general-tab">
                                        <div class="col-md-6">

                                            <form id="yourForm" action="addlesson?go=addLesson" method="post">
                                                <div class="mb-3">
                                                    <label class="form-label">Lesson Name<div style="color: red; display: inline;">*</div></label>
                                                    <textarea class="form-control" id="chaperName" name="chapterName" rows="2" cols="30" placeholder="Enter name of lesson" required></textarea>
                                                </div>
                                                <div class="mb-3">
                                                    <label class="form-label">Material Text<div style="color: red; display: inline;">*</div></label>
                                                    <textarea class="form-control" id="id" name="materialText" rows="2" cols="30" placeholder="Enter material text" required></textarea>
                                                </div>
                                                <div class="mb-3">
                                                    <label class="form-label">Material Link<div style="color: red; display: inline;">*</div></label>
                                                    <textarea class="form-control" id="id" name="materialLink" rows="2" cols="30" placeholder="Enter material link" required></textarea>
                                                </div>

                                                <div class="mb-3">
                                                    <label class="form-label">Select material type<div style="color: red; display: inline;">*</div></label>
                                                    <select class="form-select" name="materialType" required>
                                                        <c:forEach items="${materialTypes}" var="mt">
                                                            <option value="${mt.getId()}" id="${mt.getId()}">${mt.getTypeName()}</option>
                                                        </c:forEach>
                                                    </select>
                                                </div>
                                                <input hidden="" value="${chapterData.chapterId}" name="chapterId">

                                                <div class="mb-3 text-end">
                                                    <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#confirmResetModal"><i class="fas fa-times"></i> Reset</button>
                                                    <button class="btn btn-success" type="button" data-bs-toggle="modal" data-bs-target="#confirmAddModal"><i class="fas fa-check"></i> Add</button>
                                                </div>

                                            </form>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="confirmAddModal" tabindex="-1" aria-labelledby="confirmAddModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="confirmAddModalLabel">Confirmation</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure you want to add this LESSON?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                        <button id="confirmAddButton" class="btn btn-success"><i class="fas fa-check"></i> Add</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal xác nhận reset -->
        <div class="modal fade" id="confirmResetModal" tabindex="-1" aria-labelledby="confirmResetModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="confirmResetModalLabel">Confirmation</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure you want to reset the form?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                        <button id="confirmResetButton" class="btn btn-danger"><i class="fas fa-times"></i> Reset</button>
                    </div>
                </div>
            </div>
        </div>
        <style>
            .btn-back {
                position: absolute;
                top: 15px;
                left: 15px;
                font-size: 20px;
                color: #07294D;
                text-decoration: none;
            }

            .btn-back:hover {
                text-decoration: underline;
            }

        </style>  
        <script src="assets/vendor/jquery/jquery.min.js"></script>
        <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script>
            // Lưu trữ trạng thái xác nhận
            // Lưu trữ trạng thái xác nhận
            var confirmAdd = false;
            var confirmReset = false;

            // Xác nhận thêm
            document.getElementById("confirmAddButton").addEventListener("click", function () {
                confirmAdd = true;
                $("#confirmAddModal").modal("hide");
                addItem();
            });

            // Xác nhận reset
            document.getElementById("confirmResetButton").addEventListener("click", function () {
                confirmReset = true;
                $("#confirmResetModal").modal("hide");
                resetForm();
            });

            // Thêm hành động
            function addItem() {
                if (confirmAdd) {
                    var chapterName = document.getElementsByName("chapterName")[0].value;
                    var materialText = document.getElementsByName("materialText")[0].value;
                    var materialLink = document.getElementsByName("materialLink")[0].value;

                    if (chapterName.trim() === "" || materialText.trim() === "" || materialLink.trim() === "") {
                        alert("Please fill in all the required fields.");
                        return;
                    }

                    var form = document.getElementById("yourForm");
                    // Thực hiện hành động thêm bài học
                    form.submit();
                }
            }

            // Reset form
            function resetForm() {
                if (confirmReset) {
                    var form = document.getElementById("yourForm");
                    // Reset các giá trị trong form
                    form.reset();
                }
            }

            // Sự kiện click nút Reset
            document.getElementById("resetButton").addEventListener("click", function () {
                $("#confirmResetModal").modal("show");
            });

            // Sự kiện click nút Add
            document.getElementById("addButton").addEventListener("click", function () {
                var chapterName = document.getElementsByName("chapterName")[0].value;
                var materialText = document.getElementsByName("materialText")[0].value;
                var materialLink = document.getElementsByName("materialLink")[0].value;

                if (chapterName.trim() === "" || materialText.trim() === "" || materialLink.trim() === "") {
                    alert("Please fill in all the required fields.");
                    return;
                }

                // Hiển thị hộp thoại xác nhận
                $("#confirmAddModal").modal("show");
            });


        </script>
    </body>
</html>