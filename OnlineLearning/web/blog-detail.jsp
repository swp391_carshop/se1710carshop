<%-- 
    Document   : blog-detail
    Created on : May 18, 2023, 11:08:04 PM
    Author     : LENOVO
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
    <head>

        <!--====== Required meta tags ======-->
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!--====== Title ======-->
        <title>Edubin - LMS Education HTML Template</title>

        <!--====== Favicon Icon ======-->
        <link rel="shortcut icon" href="images/favicon.png" type="image/png">

        <!--====== Slick css ======-->
        <link rel="stylesheet" href="css/slick.css">

        <!--====== Animate css ======-->
        <link rel="stylesheet" href="css/animate.css">

        <!--====== Nice Select css ======-->
        <link rel="stylesheet" href="css/nice-select.css">

        <!--====== Nice Number css ======-->
        <link rel="stylesheet" href="css/jquery.nice-number.min.css">

        <!--====== Magnific Popup css ======-->
        <link rel="stylesheet" href="css/magnific-popup.css">

        <!--====== Bootstrap css ======-->
        <link rel="stylesheet" href="css/bootstrap.min.css">

        <!--====== Fontawesome css ======-->
        <link rel="stylesheet" href="css/font-awesome.min.css">

        <!--====== Default css ======-->
        <link rel="stylesheet" href="css/default.css">

        <!--====== Style css ======-->
        <link rel="stylesheet" href="css/style.css">

        <!--====== Responsive css ======-->
        <link rel="stylesheet" href="css/responsive.css">


    </head>

    <body>

        <!--====== HEADER PART START ======-->
        <jsp:include page="header.jsp"/>
        <!--====== HEADER PART ENDS ======-->

        <!--====== PAGE BANNER PART START ======-->

        <section id="page-banner" class="pt-105 pb-130 bg_cover" data-overlay="8" style="background-image: url(images/page-banner-4.jpg)">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-banner-cont">
                            <h2>${blog.title}</h2>
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item"><a href="#">Blog</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">${blog.title}</li>
                                </ol>
                            </nav>
                        </div>  <!-- page banner cont -->
                    </div>
                </div> <!-- row -->
            </div> <!-- container -->
        </section>

        <!--====== PAGE BANNER PART ENDS ======-->

        <!--====== BLOG PART START ======-->

        <section id="blog-singel" class="pt-90 pb-120 gray-bg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="blog-details mt-30">
                            <div class="thum">
                                <img src="images/blog/blog-post/${blog.image}" alt="Blog Details">
                            </div>
                            <div class="cont">
                                <h3>${blog.title}</h3>
                                <ul>
                                    <li><a href="#"><i class="fa fa-calendar"></i>${blog.created_at}</a></li>
                                    <!--                                    <li><a href="#"><i class="fa fa-user"></i>Mark anthem</a></li>
                                                                        <li><a href="#"><i class="fa fa-tags"></i>Education</a></li>-->
                                </ul>
                                <p><div style="white-space: pre-line;"> ${blog.content}</div></p>


                                <!--                           <ul class="share">
                                                               <li class="title">Share :</li>
                                                               <li><a href="#"><i class="fa fa-facebook-f"></i></a></li>
                                                               <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                               <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                                               <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                                               <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                                           </ul>-->


                                <!--                           <div class="blog-comment pt-45">
                                                               <div class="title pb-15">
                                                                   <h3>Comment (3)</h3>
                                                               </div>   title 
                                                               <ul>
                                                                   <li>
                                                                       <div class="comment">
                                                                           <div class="comment-author">
                                                                                <div class="author-thum">
                                                                                    <img src="images/review/r-1.jpg" alt="Reviews">
                                                                                </div>
                                                                                <div class="comment-name">
                                                                                    <h6>Bobby Aktar</h6>
                                                                                    <span>April 03, 2019</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="comment-description pt-20">
                                                                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which.</p>
                                                                            </div>
                                                                            <div class="comment-replay">
                                                                                <a href="#">Reply</a>
                                                                            </div>
                                                                        </div>  comment 
                                                                        <ul class="replay">
                                                                           <li>
                                                                               <div class="comment">
                                                                                   <div class="comment-author">
                                                                                        <div class="author-thum">
                                                                                            <img src="images/review/r-2.jpg" alt="Reviews">
                                                                                        </div>
                                                                                        <div class="comment-name">
                                                                                            <h6>Humayun Ahmed</h6>
                                                                                            <span>April 03, 2019</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="comment-description pt-20">
                                                                                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
                                                                                    </div>
                                                                                    <div class="comment-replay">
                                                                                        <a href="#">Reply</a>
                                                                                    </div>
                                                                                </div>  comment 
                                                                           </li>
                                                                       </ul>
                                                                   </li>
                                                                   <li>
                                                                       <div class="comment">
                                                                           <div class="comment-author">
                                                                                <div class="author-thum">
                                                                                    <img src="images/review/r-3.jpg" alt="Reviews">
                                                                                </div>
                                                                                <div class="comment-name">
                                                                                    <h6>Bobby Aktar</h6>
                                                                                    <span>April 03, 2019</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="comment-description pt-20">
                                                                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which.</p>
                                                                            </div>
                                                                            <div class="comment-replay">
                                                                                <a href="#">Reply</a>
                                                                            </div>
                                                                        </div>  comment 
                                                                   </li>
                                                               </ul>
                                                               <div class="title pt-45 pb-25">
                                                                   <h3>Leave a comment</h3>
                                                               </div>  title 
                                                               <div class="comment-form">
                                                                   <form action="#">
                                                                       <div class="row">
                                                                           <div class="col-md-6">
                                                                               <div class="form-singel">
                                                                                   <input type="text" placeholder="Name">
                                                                               </div>  form singel 
                                                                           </div>
                                                                           <div class="col-md-6">
                                                                               <div class="form-singel">
                                                                                   <input type="email" placeholder="email">
                                                                               </div>  form singel 
                                                                           </div>
                                                                           <div class="col-md-12">
                                                                               <div class="form-singel">
                                                                                   <textarea placeholder="Comment"></textarea>
                                                                               </div>  form singel 
                                                                           </div>
                                                                           <div class="col-md-12">
                                                                               <div class="form-singel">
                                                                                   <button class="main-btn">Submit</button>
                                                                               </div>  form singel 
                                                                           </div>
                                                                       </div>  row 
                                                                   </form>
                                                               </div>   comment-form 
                                                           </div>  blog comment -->
                            </div> <!-- cont -->
                        </div> <!-- blog details -->
                    </div>

                    <!--blog sider -->
                    <jsp:include page="blog-sider.jsp"/>
                    <!--blog sider -->

                </div> <!-- row -->
            </div> <!-- container -->
        </section>

        <!--====== BLOG PART ENDS ======-->

        <!--====== FOOTER PART START ======-->
        <jsp:include page="footer.jsp"/>
        <!--====== FOOTER PART ENDS ======-->


        <!--====== jquery js ======-->
        <script src="js/vendor/modernizr-3.6.0.min.js"></script>
        <script src="js/vendor/jquery-1.12.4.min.js"></script>

        <!--====== Bootstrap js ======-->
        <script src="js/bootstrap.min.js"></script>

        <!--====== Slick js ======-->
        <script src="js/slick.min.js"></script>

        <!--====== Magnific Popup js ======-->
        <script src="js/jquery.magnific-popup.min.js"></script>

        <!--====== Counter Up js ======-->
        <script src="js/waypoints.min.js"></script>
        <script src="js/jquery.counterup.min.js"></script>

        <!--====== Nice Select js ======-->
        <script src="js/jquery.nice-select.min.js"></script>

        <!--====== Nice Number js ======-->
        <script src="js/jquery.nice-number.min.js"></script>

        <!--====== Count Down js ======-->
        <script src="js/jquery.countdown.min.js"></script>

        <!--====== Validator js ======-->
        <script src="js/validator.min.js"></script>

        <!--====== Ajax Contact js ======-->
        <script src="js/ajax-contact.js"></script>

        <!--====== Main js ======-->
        <script src="js/main.js"></script>

        <!--====== Map js ======-->
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDC3Ip9iVC0nIxC6V14CKLQ1HZNF_65qEQ"></script>
        <script src="js/map-script.js"></script>

    </body>
</html>
