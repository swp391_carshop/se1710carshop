
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- phan header se bat dau tu phan comment ====== PRELOADER PART START ====== 
den phan ====== SEARCH BOX PART ENDS ======
ae xoa phan header trong trang web ae lam va thay bang 
<-jsp:include page="header.jsp"/> (bo dau '-' truoc chu jsp)
-->
<%@page contentType="text/html" pageEncoding="UTF-8"%>



<!--====== PRELOADER PART START ======-->

<!--<div class="preloader">
    <div class="loader rubix-cube">
        <div class="layer layer-1"></div>
        <div class="layer layer-2"></div>
        <div class="layer layer-3 color-1"></div>
        <div class="layer layer-4"></div>
        <div class="layer layer-5"></div>
        <div class="layer layer-6"></div>
        <div class="layer layer-7"></div>
        <div class="layer layer-8"></div>
    </div>
</div>-->

<!--====== PRELOADER PART END ======-->

<!--====== HEADER PART START ======-->

<header id="header-part">

    <div class="header-top d-none d-lg-block">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="header-contact text-lg-left text-center">
                        <ul>
                            <li><img src="images/all-icon/map.png" alt="icon"><span>Hoa Lac Hi-tech Park, Thach Hoa, Thach That, Ha Noi</span></li>
                            <li><img src="images/all-icon/email.png" alt="icon"><span>anhtthe161427@fpt.edu.vn</span></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="header-opening-time text-lg-right text-center">
<!--                        <p>Opening Hours : Monday to Saturay - 8 Am to 5 Pm</p>-->
                    </div>
                </div>
            </div> <!-- row -->
        </div> <!-- container -->
    </div> <!-- header top -->

    <div class="header-logo-support pt-30 pb-30">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4">
                    <div class="logo">
                        <a href="homepage">
                            <img src="images/logo.png" alt="Logo">
                        </a>
                    </div>
                </div>
                <c:if test="${empty userName}">
                    <div class="col-lg-8 col-md-8">
                        <div class="support-button float-right d-none d-md-block">
                            <div class="support float-left">
                                <div class="icon">
                                    <img src="images/all-icon/support.png" alt="icon">
                                </div>
                                <div class="cont">
                                    <p>Need Help? call us free</p>
                                    <span>096 563 1523</span>
                                </div>
                            </div>                    
                            <div class="button float-left">
                                <a href="login" class="main-btn">Apply Now</a>
                            </div>
                        </div>
                    </div>
                </c:if>
            </div> <!-- row -->
        </div> <!-- container -->
    </div> <!-- header logo support -->

    <div class="navigation">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-md-10 col-sm-9 col-8">
                    <nav class="navbar navbar-expand-lg">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <div class="collapse navbar-collapse sub-menu-bar" id="navbarSupportedContent">
                            <ul class="navbar-nav mr-auto">
                                <li class="nav-item">
                                    <a class="" href="homepage">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a href="about.html">About us</a>
                                </li>
                                <li class="nav-item">
                                    <a href="courses">Courses</a>
                                </li>
                                <li class="nav-item">
                                    <a href="blogs">Blog</a>
                                </li>
                                <c:if test="${empty sessionScope.userName}">
                                    <li class="nav-item">
                                        <a href="login">Login</a>
                                    </li>
                                </c:if>
                                <c:if test="${not empty sessionScope.userName}">
                                    <li class="nav-item">
                                        <a href="./profile">${sessionScope.userName}</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="<%=request.getContextPath()%>/logout">Logout</a>
                                    </li>
                                </c:if>
                            </ul>
                        </div>
                    </nav> <!-- nav -->
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-4">
                    <div class="right-icon text-right">
                        <ul>
                            <li><a href="#" id="search"><i class="fa fa-search"></i></a></li>
                        </ul>
                    </div> <!-- right icon -->
                </div>
            </div> <!-- row -->
        </div> <!-- container -->
    </div>

</header>

<!--====== HEADER PART ENDS ======-->

<!--====== SEARCH BOX PART START ======-->

<div class="search-box">
    <div class="serach-form">
        <div class="closebtn">
            <span></span>
            <span></span>
        </div>
        <form action="./courses" method="GET">
            <input type="hidden" name="go" value="search">
            <input type="text" name="cname" placeholder="Search by keyword">
            <button><i class="fa fa-search"></i></button>
        </form>
    </div> <!-- serach form -->
</div>

<!--====== SEARCH BOX PART ENDS ======-->

