<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<!-- 
* Bootstrap Simple Admin Template
* Version: 2.1
* Author: Alexis Luna
* Website: https://github.com/alexis-luna/bootstrap-simple-admin-template
-->
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="shortcut icon" href="images/favicon.png" type="image/png">
        <title>User List</title>
        <link href="assets/vendor/fontawesome/css/fontawesome.min.css" rel="stylesheet">
        <link href="assets/vendor/fontawesome/css/solid.min.css" rel="stylesheet">
        <link href="assets/vendor/fontawesome/css/brands.min.css" rel="stylesheet">
        <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/vendor/datatables/datatables.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link href="assets/css/master.css" rel="stylesheet">
    </head>

    <body>
        <c:set var="search" value="searchusersale" />
        <div class="wrapper">
            <jsp:include page="sale-sidebar.jsp"/>

            <div id="body" class="active">
                <!-- navbar navigation component -->
                <jsp:include page="sale-header.jsp"/>
                <!-- end of navbar navigation -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <h3><a href="viewuser"><i class="fas fa-user-graduate"></i>&ensp;User List</a></h3>
                            </div>
                            <div class="col-md-6">
                                <form action="viewuser" method="get" class="float-md-end">  
                                    <div class="input-group mt-2 px-3">
                                        <input type="hidden" name="go" value="searchusersale">
                                        <input oninput="searchByUsername(this)" value="${txtB}" name="txtB" type="text" class="form-control" placeholder="Search...."
                                               aria-describedby="basic-addon2">
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-secondary" type="submit">Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="box box-primary">
                            <div id="content" class="box-body">
                                <table width="100%" class="table table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Full Name</th>
                                            <th>Username</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${saleUserList}" begin="${page.begin}" end="${page.end}" var="a" >
                                            <tr>
                                                <td>${a.id}</td>
                                                <td>${a.fullname}</td>
                                                <td>
                                                    <form action="loadusersale" method="post" id="userForm">
                                                        <input type="button" value="${a.username}"
                                                               class="link-button"
                                                               onmouseover="this.style.fontWeight = 'bold'; this.style.color = 'blue'"
                                                               onmouseout="this.style.fontWeight = 'normal'; this.style.color = 'black'"
                                                               onclick="submitForm('${a.id}')">

                                                        <script>
                                                            function submitForm(id) {
                                                                // Gán giá trị id vào trường input ẩn
                                                                document.getElementById('aid').value = id;

                                                                // Submit form
                                                                document.getElementById('userForm').submit();
                                                            }
                                                        </script>

                                                        <input type="hidden" id="aid" name="aid">
                                                    </form>
                                                </td>
                                                <td>${a.email}</td>
                                                <td>${a.phone}</td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                                
                                    <div class="pagination-wrapper">
                                        <ul class="pagination">
                                            <c:if test="${go eq search}">
                                            <c:if test="${page.index!=0}">
                                                <li class="page-item"><a href="viewuser?index=${0}&nrpp=${page.nrpp}&go=searchusersale&txtB=${txtB}"><b>Home</b></a></li>
                                                <li class="page-item"><a href="viewuser?index=${page.index-1}&nrpp=${page.nrpp}&go=searchusersale&txtB=${txtB}"><i class="fas fa-angle-left"></i></a></li>
                                               <!-- <li class="page-item"><a href="viewuser?index=${page.index-5}&nrpp=${page.nrpp}&go=searchusersale&txtB=${txtB}"><b>-5</b></a></li>
                                                 --> </c:if>
                                                
                                                <c:forEach begin="${page.pageStart}" end = "${page.pageEnd}" var="i">
                                                <li class="page-item"><a ${page.index==i?"style='background-color: #FFC600; color: #07294D!important;'":""} href="viewuser?index=${i}&nrpp=${page.nrpp}&go=searchusersale&txtB=${txtB}"><b>${i+1}</b></a></li>
                                                        </c:forEach>
                                                
                                                        <c:if test="${page.index<page.totalPage-1}">
                                                            <!--   <li class="page-item"><a href="viewuser?index=${page.index+5}&nrpp=${page.nrpp}&go=searchusersale&txtB=${txtB}"><b>+5</b></a></li>
                                                     -->  
                                                <li class="page-item"><a href="viewuser?index=${page.index+1}&nrpp=${page.nrpp}&go=searchusersale&txtB=${txtB}" class="page-link"><i class="fas fa-angle-right"></i></a></li>
                                                <!-- <li class="page-item"><a href="viewuser?index=${page.totalPage-1}&nrpp=${page.nrpp}&go=searchusersale&txtB=${txtB}"><b>End</b></a></li>
                                                 -->
                                                </c:if>
                                                </c:if>
                                                
                                            <c:if test="${go ne search}">
                                            <c:if test="${page.index!=0}">
                                                 <li class="page-item"><a href="viewuser?index=${0}&nrpp=${page.nrpp}"><b>Home</b></a></li>
                                                 <li class="page-item"><a href="viewuser?index=${page.index-1}&nrpp=${page.nrpp}"><i class="fas fa-angle-left"></i></a></li>
                                                <!-- <li class="page-item"><a href="viewuser?index=${page.index-5}&nrpp=${page.nrpp}"><b>-5</b></a></li>
                                                 -->
                                            </c:if>
                                                
                                                <c:forEach begin="${page.pageStart}" end = "${page.pageEnd}" var="i">
                                                <li class="page-item"><a ${page.index==i?"style='background-color: #FFC600; color: #07294D!important;'":""} href="viewuser?index=${i}&nrpp=${page.nrpp}"><b>${i+1}</b></a></li>
                                                        </c:forEach>
                                                        <c:if test="${page.index<page.totalPage-1}">
                                                    <!-- <li class="page-item"><a href="viewuser?index=${page.index+5}&nrpp=${page.nrpp}"><b>+5</b></a></li>
                                                 -->
                                                 <li class="page-item"><a href="viewuser?index=${page.index+1}&nrpp=${page.nrpp}" class="page-link"><i class="fas fa-angle-right"></i></a></li>
                                                 <!-- <li class="page-item"><a href="viewuser?index=${page.totalPage-1}&nrpp=${page.nrpp}"><b>End</b></a></li>
                                                --> 
                                                </c:if>
                                                </c:if>
                                                
                                                
                                                
                                        </ul>
                                    </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <style>
            .pagination li {
                display: inline-block;
            }

            .pagination li a {
                display: block;
                padding: 5px 10px;
                text-decoration: none;
                color: #07294D;
            }

            .pagination li a:hover {
                background-color: #FFC600;
            }

            .no-result-found {
                margin-top: 10px;
            }

            .link-button {
                background: none;
                border: none;
                color: inherit;
                cursor: pointer;
                font-size: inherit;
                padding: 0;
                margin: 0;
                font-family: inherit;
                outline: none;
            }

             .pagination-wrapper {
            display: flex;
            justify-content: center;
            margin-top: 20px;
            }
        </style>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script>
                                                            function searchByUsername(param) {
                                                                var txtSearch = param.value;
                                                                $.ajax({
                                                                    url: "/OnlineLearning/viewuser",
                                                                    type: "post", //send it through get method
                                                                    data: {
                                                                        txt: txtSearch
                                                                    },
                                                                    success: function (data) {
                                                                        var row = document.getElementById("content");
                                                                        row.innerHTML = data;
                                                                    },
                                                                    error: function (xhr) {
                                                                        //Do Something to handle error
                                                                    }
                                                                });
                                                            }
        </script>

        <!--
        <script src="assets/vendor/datatables/datatables.min.js"></script>
        <script src="assets/js/initiate-datatables.js"></script>-->
        <script src="assets/vendor/jquery/jquery.min.js"></script>
        <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="assets/js/script.js"></script>
    </body>
</html>
