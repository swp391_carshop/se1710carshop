<%-- 
    Document   : Saler
    Created on : May 25, 2023, 8:30:30 AM
    Author     : Admin MSI
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Saler Page</title>
    </head>

    <body>
        <center><h2>Saler's Home</h2></center>
        Welcome <%=request.getAttribute("userName") %>
        <div style="text-align: right"><a href="<%=request.getContextPath()%>/logout">Logout</a></div>
    </body>
</html>
