<%-- 
    Document   : ShowGrade
    Created on : Mar 8, 2023, 8:58:17 PM
    Author     : Admin MSI
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder" %>
<%@page import="java.util.ArrayList" %>
<%@page import="dal.DAOQuestions" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css">
        <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.3/dist/jquery.slim.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"></script>
        <title>Show Grade</title>
        <link rel="shortcut icon" href="images/favicon.png" type="image/png">
        <style>
            .correct{
                color: #1d7c50;
                font-weight: 700;
            }

            .incorrect{
                color: red;
                font-weight: 700;
            }
        </style>
    </head>
    <body>
        <%
            String sum="";
            if(request.getAttribute("sum")!=null){
                sum=(String)request.getAttribute("sum");
            }
            
//            String Grade="";
//            if(request.getAttribute("Grade")!=null){
//                Grade=(String)request.getAttribute("Grade");
//            }
            
            int count=0;
        %>

        <c:set var="grade" value="${Float.parseFloat(requestScope.Grade)}"/>
        <div class="container my-3  mb-5">
            <form action="" method="post" class="container">
                <div>
                    <c:if test="${grade >= 8}">
                        <h2 class="correct" style="font-size: 28px; font-family: Source Sans Pro,Arial,sans-serif">Congratulations! You passed!</h2>
                    </c:if>

                    <c:if test="${grade < 8}">
                        <h2 class="incorrect" style="font-size: 28px; font-family: Source Sans Pro,Arial,sans-serif">You did not pass the quiz</h2>
                    </c:if>

                </div>
                <div class="row show-condition" style="margin-bottom: 30px">
                    <div class="col-lg-9" style="display: grid; grid-template-columns: repeat(3, 1fr); font-size: 20px; font-family: Source Sans Pro,Arial,sans-serif">
                        <p class="col-md-12">Total Correct Questions: ${requestScope.sum}</p>
                        <p class="col-md-12">Your grade you gain is: ${requestScope.Grade}</p>
                        <p class="col-md-12"> To Pass 80% or higher</p>
                    </div>
                    <div class="col-lg-3">
<!--                            <c@:if test="${grade >= 8}">-->
                        <!--                            Go to next Item-->
                        <!--                            </c@:if>-->
                        <!--                            <c@:if test="${grade < 8}">-->
                        <button class="btn btn-outline-primary" type="button" onclick="location.href = '<%=request.getContextPath()%>/getquestions?quiz=${sessionScope.quizId}';">Try Again</button>
                        <!--                            </c@:if>-->
                    </div>
                </div>

                <div class="row show-quesions">
                    <c:forEach items="${sessionScope.listQuestions}" var="i">
                        <div class="panel-body row" style="margin-left:10px">
                            <input type="hidden" name="idQuestion" value="${i.getId()}">
                            <!-- Hiển thị các câu hỏi -->
                            <h5 style="color: black">Question:  <%=++count%> ${i.getQuestion()}</h5>

                            <!-- Display answer A -->
                            <div class="radio col-md-12">
                                <label>
                                    <input type="radio" name="${i.getId()}" id="rdOptionsA" value="${i.getOptionA()}"
                                           <c:forEach items="${sessionScope.userAnswers}" var="u">
                                               <c:if test="${u == i.getOptionA()}">
                                                   checked="checked"
                                               </c:if>
                                           </c:forEach>
                                           <c:if test="${not empty sessionScope.userAnswers}">disabled</c:if>
                                               />
                                           A: ${i.getOptionA()}
                                    <c:if test="${i.getOptionA().equals(i.getAnswer()) && sessionScope.userAnswers.contains(i.getOptionA())}">
                                        <br> <span class="correct">Correct</span>
                                    </c:if>
                                    <c:if test="${not i.getOptionA().equals(i.getAnswer()) && sessionScope.userAnswers.contains(i.getOptionA())}">
                                        <br><span class="incorrect">Incorrect</span>
                                    </c:if>
                                </label><br>
                            </div>

                            <!-- Display answer B -->
                            <div class="radio col-md-12">
                                <label>
                                    <input type="radio" name="${i.getId()}" id="rdOptionsB" value="${i.getOptionB()}"
                                           <c:forEach items="${sessionScope.userAnswers}" var="u">
                                               <c:if test="${u == i.getOptionB()}">
                                                   checked="checked"
                                               </c:if>
                                           </c:forEach>
                                           <c:if test="${not empty sessionScope.userAnswers}">disabled</c:if>
                                               />
                                           B: ${i.getOptionB()}
                                    <c:if test="${i.getOptionB().equals(i.getAnswer()) && sessionScope.userAnswers.contains(i.getOptionB())}">
                                        <br><span class="correct">Correct</span>
                                    </c:if>
                                    <c:if test="${not i.getOptionB().equals(i.getAnswer()) && sessionScope.userAnswers.contains(i.getOptionB())}">
                                        <br><span class="incorrect">Incorrect</span>
                                    </c:if>
                                </label><br>
                            </div>

                            <!-- Display answer C -->
                            <c:if test="${not empty i.getOptionC()}">
                                <div class="radio col-md-12">
                                    <label>
                                        <input type="radio" name="${i.getId()}" id="rdOptionsC" value="${i.getOptionC()}"
                                               <c:forEach items="${sessionScope.userAnswers}" var="u">
                                                   <c:if test="${u == i.getOptionC()}">
                                                       checked="checked"
                                                   </c:if>
                                               </c:forEach>
                                               <c:if test="${not empty sessionScope.userAnswers}">disabled</c:if>
                                                   />
                                               C: ${i.getOptionC()}
                                        <c:if test="${i.getOptionC().equals(i.getAnswer()) && sessionScope.userAnswers.contains(i.getOptionC())}">
                                            <br><span class="correct">Correct</span>
                                        </c:if>
                                        <c:if test="${not i.getOptionC().equals(i.getAnswer()) && sessionScope.userAnswers.contains(i.getOptionC())}">
                                            <br><span class="incorrect">Incorrect</span>
                                        </c:if>
                                    </label><br>
                                </div>
                            </c:if>        
                            <!-- Display answer D -->
                            <c:if test="${not empty i.getOptionD()}">
                                <div class="radio col-md-12">
                                    <label>
                                        <input type="radio" name="${i.getId()}" id="rdOptionsD" value="${i.getOptionD()}"
                                               <c:forEach items="${sessionScope.userAnswers}" var="u">
                                                   <c:if test="${u == i.getOptionD()}">
                                                       checked="checked"
                                                   </c:if>
                                               </c:forEach>
                                               <c:if test="${not empty sessionScope.userAnswers}">disabled</c:if>
                                                   />
                                               D: ${i.getOptionD()}
                                        <c:if test="${i.getOptionD().equals(i.getAnswer()) && sessionScope.userAnswers.contains(i.getOptionD())}">
                                            <br><span class="correct">Correct</span>
                                        </c:if>
                                        <c:if test="${not i.getOptionD().equals(i.getAnswer()) && sessionScope.userAnswers.contains(i.getOptionD())}">
                                            <br><span class="incorrect">Incorrect</span>
                                        </c:if>
                                    </label><br>
                                </div>
                            </c:if>        
                            <br>
                        </div>
                    </c:forEach>
                </div>


                        <div class="row d-flex align-items-center justify-content-center">
                    <button class="btn btn-link" type="button" onclick="location.href = '<%=request.getContextPath()%>/learn?qid=${sessionScope.quizId}';">Return To Course</button>
                </div>
                <!--                    <button class="col-md-12 btn btn-link" type="button"><a href="<%=request.getContextPath()%>/show-detail.jsp">Show detail</button>-->
            </form> 
        </div>
</body>
                    
                    
                    <button class="col-md-12 btn btn-link" type="button"><a href="<%=request.getContextPath()%>/coursedetails?cid=${requestScope.courseId}">Return Course Page</a></button>
<!--                    <button class="col-md-12 btn btn-link" type="button"><a href="<%=request.getContextPath()%>/show-detail.jsp">Show detail</button>-->
                </form> 
            </div>
        </div>
        <script>
    function disableRadioButtons() {
        var radioButtons = document.querySelectorAll('input[type="radio"]');
        for (var i = 0; i < radioButtons.length; i++) {
            radioButtons[i].disabled = true;
        }
        return true; // Allow form submission
    }

    // Automatically run the function when the page loads
    window.onload = function() {
        disableRadioButtons();
    };
</script>
    </body>
</html>
