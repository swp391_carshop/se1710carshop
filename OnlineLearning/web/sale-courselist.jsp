<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<!-- 
* Bootstrap Simple Admin Template
* Version: 2.1
* Author: Alexis Luna
* Website: https://github.com/alexis-luna/bootstrap-simple-admin-template
-->
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="shortcut icon" href="images/favicon.png" type="image/png">
        <title>Course List</title>
        <link href="assets/vendor/fontawesome/css/fontawesome.min.css" rel="stylesheet">
        <link href="assets/vendor/fontawesome/css/solid.min.css" rel="stylesheet">
        <link href="assets/vendor/fontawesome/css/brands.min.css" rel="stylesheet">
        <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/vendor/datatables/datatables.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link href="assets/css/master.css" rel="stylesheet">
    </head>

    <body>
        <c:set var="search" value="searchcourse" />
        <c:set var="list" value="courselist" />
        <c:set var="sort" value="sortsalecourse" />
        <div class="wrapper">
            <jsp:include page="sale-sidebar.jsp"/>

            <div id="body" class="active">
                <!-- navbar navigation component -->
                <jsp:include page="sale-header.jsp"/>
                <!-- end of navbar navigation -->
                <div class="content">
                    <div class="container">
                        <div class="page-title">
                            <div class="row">
                                <div class="col-md-6">
                                    <h3>
                                        <a href="viewcourse"><i class="fas fa-copyright"></i>&ensp;Course List</a>
                                        <span class="float-end">
                                            <span class="sort-text" style="color: #07294D; font-size: 18px; font-weight: bold;">Sort</span>
                                            <a href="viewcourse?go=sortsalecourse&type=tang" style="background-color: white; color: #07294D; font-size: 12px; padding: 5px 10px; margin-left: 10px; border: 1px solid #AAAAAA; border-radius: 3px;"
                                               onmousedown="this.style.backgroundColor = 'blue'; this.style.color = 'white';"
                                               onmouseup="this.style.backgroundColor = 'white'; this.style.color = '#07294D';"
                                               onmouseout="this.style.backgroundColor = 'white'; this.style.color = '#07294D';"
                                               onmouseover="this.style.backgroundColor = '#FFC600'; this.style.color = '#07294D';"
                                               ><b>Sales <i class="fas fa-arrow-up"></i></b></a>
                                            <a href="viewcourse?go=sortsalecourse&type=giam" style="background-color: white; color: #07294D; font-size: 12px; padding: 5px 10px; margin-left: 10px; border: 1px solid #AAAAAA; border-radius: 3px;"
                                               onmousedown="this.style.backgroundColor = 'blue'; this.style.color = 'white';"
                                               onmouseup="this.style.backgroundColor = 'white'; this.style.color = '#07294D';"
                                               onmouseout="this.style.backgroundColor = 'white'; this.style.color = '#07294D';"
                                               onmouseover="this.style.backgroundColor = '#FFC600'; this.style.color = '#07294D';"
                                               ><b>Sales <i class="fas fa-arrow-down"></i></b></a>
                                        </span>
                                    </h3>
                                </div>
                                <div class="col-md-6">
                                    <form action="viewcourse" method="get" class="float-md-end">
                                        <div class="input-group mt-2 px-3">
                                            <input type="hidden" name="go" value="searchcourse">
                                            <input value="${txtC}" name="txtC" type="text" class="form-control" placeholder="Search...."
                                                   aria-describedby="basic-addon2">
                                            <div class="input-group-append">
                                                <button class="btn btn-outline-secondary" type="submit">Search</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="box box-primary">
                            <div class="box-body">
                                <table width="100%" class="table table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Name Course</th>
                                            <th>Description</th>
                                            <th>Objective</th>
                                            <th>Subject</th>
                                            <th style="width: 5%">Sales</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${saleCourseSubject}" begin="${page.begin}" end="${page.end}" var="c">
                                            <tr>
                                                <td>${c.getId()}</td>
                                                <td>${c.getName_course()}</td>
                                                <td>${c.getDescription()}</td>
                                                <td>${c.getObjective()}</td>
                                                <td>${c.getName()}</td>
                                                <td><div class="popup">
                                                        <span style="color: black; text-decoration: none; cursor: pointer;" onmouseover="this.style.fontWeight = 'bold'" onmouseout="this.style.fontWeight = 'normal'">${c.getSale()}</span> <i class="fas fa-pencil-alt"></i>
                                                        <div class="popup-content">
                                                            <p><b>Sales:</b> ${c.getSale()} </p>
                                                            <p><b>Studying:</b> ${c.getStudying()}&ensp;<i class="fas fa-user"></i></p>
                                                            <p><b>Completed:</b> ${c.getCompleted()}&ensp;<i class="fas fa-user-graduate"></i></p>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="popup">
                                                        <span class="status-icon" style="color: #0D6EFD;">
                                                            <i class="fas fa-eye"></i>
                                                        </span>
                                                        <div class="popup-status">
                                                            <p style="color: #0D6EFD;"><b>Active</b></p>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                                <div class="pagination-wrapper">
                                    <ul class="pagination">
                                        <c:if test="${go eq search}">
                                            <c:if test="${page.index!=0}">
                                                <li class="page-item"><a href="viewcourse?index=${0}&nrpp=${page.nrpp}&go=searchcourse&txtC=${txtC}"><b>Home</b></a></li>
                                                <li class="page-item"><a href="viewcourse?index=${page.index-1}&nrpp=${page.nrpp}&go=searchcourse&txtC=${txtC}"><i class="fas fa-angle-left"></i></a></li>
                                                    </c:if>
                                                    <c:forEach begin="${page.pageStart}" end = "${page.pageEnd}" var="i">
                                                <li class="page-item"><a ${page.index==i?"style='background-color: #FFC600; color: #07294D!important;'":""} href="viewcourse?index=${i}&nrpp=${page.nrpp}&go=searchcourse&txtC=${txtC}"><b>${i+1}</b></a></li>
                                                        </c:forEach>
                                                        <c:if test="${page.index<page.totalPage-1}">
                                                <li class="page-item"><a href="viewcourse?index=${page.index+1}&nrpp=${page.nrpp}&go=searchcourse&txtC=${txtC}" class="page-link"><i class="fas fa-angle-right"></i></a></li>
                                                    </c:if>
                                                </c:if>

                                        <c:if test="${go eq sort}">
                                            <c:if test="${page.index!=0}">
                                                <c:if test="${requestScope.type == 'giam'}">
                                                    <li class="page-item"><a href="viewcourse?index=${0}&nrpp=${page.nrpp}&go=sortsalecourse&type=giam"><b>Home</b></a></li>
                                                    <li class="page-item"><a href="viewcourse?index=${page.index-1}&nrpp=${page.nrpp}&go=sortsalecourse&type=giam"><i class="fas fa-angle-left"></i></a></li>
                                                        </c:if>
                                                        <c:if test="${requestScope.type == 'tang'}">
                                                    <li class="page-item"><a href="viewcourse?index=${0}&nrpp=${page.nrpp}&go=sortsalecourse&type=tang"><b>Home</b></a></li>
                                                    <li class="page-item"><a href="viewcourse?index=${page.index-1}&nrpp=${page.nrpp}&go=sortsalecourse&type=tang"><i class="fas fa-angle-left"></i></a></li>
                                                        </c:if>
                                                    </c:if>
                                                    <c:forEach begin="${page.pageStart}" end = "${page.pageEnd}" var="i">
                                                        <c:if test="${requestScope.type == 'giam'}">
                                                    <li class="page-item"><a ${page.index==i?"style='background-color: #FFC600; color: #07294D!important;'":""} href="viewcourse?index=${i}&nrpp=${page.nrpp}&go=sortsalecourse&type=giam"><b>${i+1}</b></a></li>
                                                            </c:if> 
                                                            <c:if test="${requestScope.type == 'tang'}">
                                                    <li class="page-item"><a ${page.index==i?"style='background-color: #FFC600; color: #07294D!important;'":""} href="viewcourse?index=${i}&nrpp=${page.nrpp}&go=sortsalecourse&type=tang"><b>${i+1}</b></a></li>
                                                            </c:if> 
                                                        </c:forEach>

                                            <c:if test="${page.index<page.totalPage-1}">
                                                <c:if test="${requestScope.type == 'giam'}">
                                                    <li class="page-item"><a href="viewcourse?index=${page.index+1}&nrpp=${page.nrpp}&go=sortsalecourse&type=giam" class="page-link"><i class="fas fa-angle-right"></i></a></li>
                                                        </c:if>    
                                                        <c:if test="${requestScope.type == 'tang'}">
                                                    <li class="page-item"><a href="viewcourse?index=${page.index+1}&nrpp=${page.nrpp}&go=sortsalecourse&type=tang" class="page-link"><i class="fas fa-angle-right"></i></a></li>
                                                        </c:if>  
                                                    </c:if>
                                                </c:if>        

                                        <c:if test="${go eq list}">
                                            <c:if test="${page.index!=0}">
                                                <li class="page-item"><a href="viewcourse?index=${0}&nrpp=${page.nrpp}"><b>Home</b></a></li>
                                                <li class="page-item"><a href="viewcourse?index=${page.index-1}&nrpp=${page.nrpp}"><i class="fas fa-angle-left"></i></a></li>
                                                    </c:if>
                                                    <c:forEach begin="${page.pageStart}" end = "${page.pageEnd}" var="i">
                                                <li class="page-item"><a ${page.index==i?"style='background-color: #FFC600; color: #07294D!important;'":""} href="viewcourse?index=${i}&nrpp=${page.nrpp}"><b>${i+1}</b></a></li>
                                                        </c:forEach>
                                                        <c:if test="${page.index<page.totalPage-1}">
                                                <li class="page-item"><a href="viewcourse?index=${page.index+1}&nrpp=${page.nrpp}" class="page-link"><i class="fas fa-angle-right"></i></a></li>
                                                    </c:if>
                                                </c:if>

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <style>
            .pagination li {
                display: inline-block;
            }

            .pagination li a {
                display: block;
                padding: 5px 10px;
                text-decoration: none;
                color: #07294D;
            }

            .pagination li a:hover {
                background-color: #FFC600;
            }

            ul.pagination li.active a {
                background-color: #FFC600;
            }

            .popup {
                position: relative;
                display: inline-block;
                cursor: pointer;
            }

            .popup .popup-content {
                visibility: hidden;
                position: absolute;
                top: 100%;
                left: 0;
                z-index: 1;
                min-width: 200px;
                padding: 10px;
                background-color: #FFF;
                border: 1px solid #DDD;
                box-shadow: 0 2px 5px rgba(0, 0, 0, 0.3);
                transition: visibility 0.3s ease-in-out;
            }

            .popup:hover .popup-content {
                visibility: visible;
            }

            .popup .popup-status {
                visibility: hidden;
                position: absolute;
                top: -25px; /* Điều chỉnh vị trí top để popup xuất hiện bên cạnh con trỏ chuột */
                left: 290%;
                transform: translateX(-50%);
                z-index: 2;
                background-color: #FFF;
                padding: 15px 5px 0px 5px;
                border: 1px solid #DDD;
                box-shadow: 0 2px 5px rgba(0, 0, 0, 0.3);
                opacity: 0;
                transition: visibility 0s, opacity 0.3s ease-in-out;
                text-align: center;
                line-height: 0;
            }

            .popup:hover .popup-status {
                visibility: visible;
                opacity: 1;
            }

            .pagination-wrapper {
                display: flex;
                justify-content: center;
                margin-top: 20px;
            }
        </style>

        <!--
        <script src="assets/vendor/datatables/datatables.min.js"></script>
        <script src="assets/js/initiate-datatables.js"></script>-->
        <script src="assets/vendor/jquery/jquery.min.js"></script>
        <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="assets/js/script.js"></script>
    </body>

</html>
