        <nav id="sidebar" class="active">
            <div class="sidebar-header">
                <img src="images/logo.png" alt="edubin logo" class="app-logo">
            </div>
            <ul class="list-unstyled components text-secondary">
                <li>
                    <a href="adminuser"><i class="fas fa-user-graduate"></i>User List</a>
                </li>
                <li>
                    <a href="admincourse"><i class="fas fa-copyright"></i>Course List</a>
                </li>
                
            </ul>
        </nav>