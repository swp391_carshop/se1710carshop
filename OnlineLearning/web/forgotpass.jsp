<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en" dir="ltr">

    <head>
        <meta charset="UTF-8">
        <!--====== Title ======-->
        <title>Forgot Password</title>

        <!--====== Favicon Icon ======-->
        <link rel="shortcut icon" href="images/favicon.png" type="image/png">
        <!---<title> Responsive Registration Form | CodingLab </title>--->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/login.css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>

    <body>
        <div class="container">
            <c:choose>
                <c:when test="${empty sendEmailSuccess}">
                    <!-- Code to execute if parameter is null -->
                    <div class="title">Forgot password</div>
                    <br>
                    <c:if test="${not empty error}">
                        <div style="color: red;">${error}</div>
                    </c:if>
                    <div class="content">
                        <form action="forgotpassword" method="post">
                            <div class="user-details">
                                <div class="input-box">
                                    <span class="details">Email</span>
                                    <input type="text" name="email" placeholder="Enter your email" required>
                                </div>
                            </div>

                            <div class="button">
                                <input type="submit" value="Send new password">
                            </div>
                            <div>
                                <p>Don't have account ? <a href="register">Register here</a></p>
                            </div>
                        </form>
                    </div>
                </c:when>
                <c:otherwise>
                    <!-- Code to execute if parameter is not null -->
                    <div class="title">
                        <span class="details">
                            YOUR PASSWORD HAS BEEN 
                            SENT TO YOUR EMAIL. 
                        </span>
                        <span class="details">    
                            PLEASE CHECK YOUR EMAIL
                        </span>
                    </div>
                    <br>
                    <div class="content">
                        <div>
                            <p>Return to <a href="login.jsp">Login</a></p>
                        </div>
                    </div>
                </c:otherwise>
            </c:choose>
        </div>
    </form>

</body>

</html>