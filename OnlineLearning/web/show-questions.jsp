<%-- 
    Document   : show-questions
    Created on : Jun 10, 2023, 11:38:33 PM
    Author     : Admin MSI
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Show Questions</title>

        <!--====== Favicon Icon ======-->
        <link rel="shortcut icon" href="images/favicon.png" type="image/png">

        <!--====== Slick css ======-->
        <link rel="stylesheet" href="css/slick.css">

        <!--====== Animate css ======-->
        <link rel="stylesheet" href="css/animate.css">

        <!--====== Nice Select css ======-->
        <link rel="stylesheet" href="css/nice-select.css">

        <!--====== Nice Number css ======-->
        <link rel="stylesheet" href="css/jquery.nice-number.min.css">

        <!--====== Magnific Popup css ======-->
        <link rel="stylesheet" href="css/magnific-popup.css">

        <!--====== Bootstrap css ======-->
        <link rel="stylesheet" href="css/bootstrap.min.css">

        <!--====== Fontawesome css ======-->
        <link rel="stylesheet" href="css/font-awesome.min.css">

        <!--====== Default css ======-->
        <link rel="stylesheet" href="css/default.css">

        <!--====== Style css ======-->
        <link rel="stylesheet" href="css/style.css">

        <!--====== Responsive css ======-->
        <link rel="stylesheet" href="css/responsive.css">
    </head>
    <body>
        <%
            int count=0;
        %>
        <!--        <form action="getquestions" method="post" style="text-align: center">
                        <p> Enter your number questions you want to practice:
                            <input type="text" name="questionNumber" placeholder="Example: 5">
                        </p>
                        <br>
                        <br>
                        <br>
        </form>-->
        <div class="container">
            <nav class="navbar fixed-top navbar-light bg-light d-flex justify-content-start">
                <div class="mr-5">
                    <button class="btn btn-link" onclick="location.href='<%=request.getContextPath()%>/learn?qid=${sessionScope.quizId}';" >
                        <svg aria-hidden="true" fill="blue" focusable="false" height="20" viewBox="0 0 20 20" width="20">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M3.207 10.5l7.147 7.146-.708.708L1.293 10l8.353-8.354.708.708L3.207 9.5H18v1H3.207z" fill="currentColor"></path>
                        </svg>
                        Back
                    </button>
                </div>
                <div class="mt-1">
                        <h1 style="font-size: 16px; font-family: Source Sans Pro, Arial, sans-serif; font-weight: 600;">Name of Quiz</h1>
                        <p style="font-size: 14px; line-height: 20px; font-family: Source Sans Pro, Arial, sans-serif; color: #636363; margin: 10px 0;">Time: <span style="color: red" id="time"></span></p>
                        <input style="display: none;" id="inputTime" type="text" name="time">
                </div>
            </nav>

            <div class="row mt-100">
                <form action="getresult" method="post" id="form">

                    <c:forEach  items="${requestScope.listQuestions}" var="i" >
                        <div class="question mb-4">
                            <input type="hidden" name="idQuestion" value="${i.getId()}">
                            <!-- Hiển thị các câu hỏi -->
                            <h5 class="mb-2" style="color: black">Questions: <%=++count%> ${i.getQuestion()}</h5>
                            <!-- Display answer A -->
                            <div class="form-check ml-2 mb-1">
                                <input class="form-check-input" type="radio" name="${i.getId()}" id="rdOptionsA${i.getId()}" value="${i.getOptionA()}">
                                <label class="form-check-label" for="rdOptionsA${i.getId()}">A: ${i.getOptionA()}</label>
                            </div>
                            <!-- Display answer B -->
                            <div class="form-check ml-2 mb-1">
                                <input class="form-check-input" type="radio" name="${i.getId()}" id="rdOptionsB${i.getId()}" value="${i.getOptionB()}">
                                <label class="form-check-label" for="rdOptionsB${i.getId()}">B: ${i.getOptionB()}</label><br>
                            </div>
                            <!-- Display answer C -->
                            <c:if test="${not empty i.getOptionC()}">
                                <div class="form-check ml-2 mb-1">
                                    <input class="form-check-input" type="radio" name="${i.getId()}" id="rdOptionsC${i.getId()}" value="${i.getOptionC()}">
                                    <label class="form-check-label" for="rdOptionsC${i.getId()}">C: ${i.getOptionC()}</label><br>
                                </div>
                            </c:if>
                            <!-- Display answer D -->
                            <c:if test="${not empty i.getOptionD()}">
                                <div class="form-check ml-2 mb-1">
                                    <input class="form-check-input" type="radio" name="${i.getId()}" id="rdOptionsD${i.getId()}" value="${i.getOptionD()}">
                                    <label class="form-check-label" for="rdOptionsD${i.getId()}">D: ${i.getOptionD()}</label><br>
                                </div>
                            </c:if>
                        </div>
                    </c:forEach>
            </div>

            <div class="footer row mt-3 mb-5">
                <!--                <input type="checkbox" class="checkbox_bt" value="1" name="checkbox"<p class="text_finish">I want to finish the exam</p>
                                <input type="submit" value="FINISH" style="margin-left: 50%">-->
                <!--<button onclick="goToJSP()">Exit</button>-->
                <div class="col-sm-8 form-check">
                    <input class="form-check-input" type="checkbox" id="myCheckbox" class="checkbox_bt" value="">
                    <label class="form-check-label" for="myCheckbox" style="color: blue;"> I want to finish the quiz</label>
                </div>
                <div class="col-sm-4">
                    <input type="submit" id="submitBtn" class="btn btn-outline-primary" value="Finish">
                </div>
            </div>
        </form>
    </div>
</div>

<!-- This part code countdown time doing quiz -->
<script>

    var countDownTime = new Date().getTime() + (1000 * 60 * 30);
    var x = setInterval(function () {
        var now = new Date().getTime();
        var distance = countDownTime - now;

        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        document.getElementById("time").innerHTML = minutes + ":" + seconds;
        document.getElementById("inputTime").value = minutes + ":" + seconds;
        if (distance <= 0) {
            document.getElementById("inputTime").value = "0" + ":" + "0";
            //                    document.getElementById("time").innerHTML = "nghia";
            document.getElementById("form").submit();
        }
    }, 1000);


    // Lấy checkbox và button submit
    var checkbox = document.getElementById('myCheckbox');
    var submitBtn = document.getElementById('submitBtn');

    // Thêm sự kiện click cho button
    submitBtn.addEventListener('click', function (event) {
        // Kiểm tra checkbox đã được tích chưa
        if (checkbox.checked) {
            // Nếu đã tích, thực hiện hành động của button
            event.target.submit();
        } else {
            alert("Please choose 'I want to finish the quiz' before finish.");
            event.preventDefault();
        }
    });
</script>

<script>
    function goBack() {
        var form = document.createElement('form');
        form.method = 'GET'; // Hoặc 'GET' tùy vào phương thức của Servlet
        form.action = '<%=request.getContextPath()%>/learn?qid=${sessionScope.quizId}'; // Thay 'ten-servlet' bằng URL của Servlet bạn muốn trở về

        document.body.appendChild(form);
        form.submit();
    }
</script>
<!--====== jquery js ======-->
<script src="js/vendor/modernizr-3.6.0.min.js"></script>
<script src="js/vendor/jquery-1.12.4.min.js"></script>

<!--====== Bootstrap js ======-->
<script src="js/bootstrap.min.js"></script>

<!--====== Slick js ======-->
<script src="js/slick.min.js"></script>

<!--====== Magnific Popup js ======-->
<script src="js/jquery.magnific-popup.min.js"></script>

<!--====== Counter Up js ======-->
<script src="js/waypoints.min.js"></script>
<script src="js/jquery.counterup.min.js"></script>

<!--====== Nice Select js ======-->
<script src="js/jquery.nice-select.min.js"></script>

<!--====== Nice Number js ======-->
<script src="js/jquery.nice-number.min.js"></script>

<!--====== Count Down js ======-->
<script src="js/jquery.countdown.min.js"></script>

<!--====== Validator js ======-->
<script src="js/validator.min.js"></script>

<!--====== Ajax Contact js ======-->
<script src="js/ajax-contact.js"></script>

<!--====== Main js ======-->
<script src="js/main.js"></script>

<!--====== Map js ======-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDC3Ip9iVC0nIxC6V14CKLQ1HZNF_65qEQ"></script>
<script src="js/map-script.js"></script>
</body>

</html>
