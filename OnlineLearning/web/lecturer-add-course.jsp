<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="shortcut icon" href="images/favicon.png" type="image/png">
        <title>Add Course</title>
        <link href="assets/vendor/fontawesome/css/fontawesome.min.css" rel="stylesheet">
        <link href="assets/vendor/fontawesome/css/solid.min.css" rel="stylesheet">
        <link href="assets/vendor/fontawesome/css/brands.min.css" rel="stylesheet">
        <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/css/master.css" rel="stylesheet">
    </head>

    <body>
        <div class="wrapper">
            <jsp:include page="lecturer-sidebar.jsp"/>

            <div id="body" class="active">
                <!-- navbar navigation component -->
                <jsp:include page="lecturer-header.jsp"/>
                <!-- end of navbar navigation -->
                <div class="content">
                    <div class="container">
                        <div class="page-title">
                            <h3><i class="fas fa-plus-circle"></i>&ensp;Add Course</h3>
                        </div>
                        <div class="box box-primary">
                            <div class="box-body">

                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade active show" id="general" role="tabpanel" aria-labelledby="general-tab">
                                        <div class="col-md-6">
                                            <form action="addcourse" method="post">
                                                <div class="mb-3">
                                                    <label for="site-title" class="form-label">Subject Name<div style="color: red; display: inline;">*</div></label>
                                                    <select class="form-select" name="subjectId" required>
                                                        <c:forEach items="${requestScope.listSubject}" var="i">
                                                            <option value="${i.getId()}" id="${i.getId()}">${i.getName()}</option>
                                                        </c:forEach>
                                                    </select>
                                                </div>
                                                <div class="mb-3">
                                                    <label for="site-description" class="form-label">Course Name<div style="color: red; display: inline;">*</div></label>
                                                    <textarea class="form-control" name="couresName" rows="1.5" cols="30" placeholder="Introduction to Data Engineering" required></textarea>
                                                </div>
                                                <div class="mb-3">
                                                    <label for="site-description" class="form-label">Description<div style="color: red; display: inline;">*</div></label>
                                                    <textarea class="form-control" name="description" rows="5" cols="30" placeholder="Fundamental Python..." required></textarea>
                                                </div>
                                                <div class="mb-3">
                                                    <label for="site-description" class="form-label">Objective<div style="color: red; display: inline;">*</div></label>
                                                    <textarea class="form-control" name="objective" rows="5" cols="30" placeholder="" required></textarea>
                                                </div>
                                                <div class="mb-3">
                                                    <label for="site-description" class="form-label">Price<div style="color: red; display: inline;">*</div></label>
                                                    <input class="form-control" name="price" placeholder="" required>
                                                </div>

                                                <div class="mb-3 text-end">
                                                    <button class="btn btn-success" type="button" data-bs-toggle="modal" data-bs-target="#confirmAddModal"><i class="fas fa-check"></i> Add</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="confirmAddModal" tabindex="-1" aria-labelledby="confirmAddModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="confirmAddModalLabel">Confirmation</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure you want to add this COURSE?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                        <button id="confirmAddButton" class="btn btn-success"><i class="fas fa-check"></i> Add</button>
                    </div>
                </div>
            </div>
        </div>
        <script src="assets/vendor/jquery/jquery.min.js"></script>
        <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script>
            // Lưu trữ trạng thái xác nhận
            var confirmAdd = false;

// Xác nhận thêm
            document.getElementById("confirmAddButton").addEventListener("click", function () {
                confirmAdd = true;
                $("#confirmAddModal").modal("hide");
                addCourse();
            });

// Thêm khóa học
            function addCourse() {
                if (confirmAdd) {
                    var form = document.querySelector("form");
                    // Thực hiện hành động thêm khóa học
                    form.submit();
                }
            }

// Sự kiện click nút "Add"
            document.getElementById("confirmAddButton").addEventListener("click", function () {
                var courseName = document.getElementsByName("couresName")[0].value;
                var description = document.getElementsByName("description")[0].value;
                var objective = document.getElementsByName("objective")[0].value;
                var price = document.getElementsByName("price")[0].value;

                if (courseName.trim() === "" || description.trim() === "" || objective.trim() === "" || price.trim() === "") {
                    alert("Please fill in all the required fields.");
                    return window.location.href = "addcourse";
                }

                // Chuyển hướng người dùng đến trang thêm khóa học
                //window.location.href = "addcourse";
            });

        </script>        

        <script src="assets/js/script.js"></script>
    </body>

</html>