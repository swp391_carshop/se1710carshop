<%-- 
    Document   : blog-list
    Created on : May 18, 2023, 8:27:21 PM
    Author     : LENOVO
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
    <head>
        <!--====== Required meta tags ======-->
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="assets/vendor/fontawesome/css/all.min.css">

        <!--====== Title ======-->
        <title>Edubin - LMS Education</title>

        <!--====== Favicon Icon ======-->
        <link rel="shortcut icon" href="images/favicon.png" type="image/png">

        <!--====== Slick css ======-->
        <link rel="stylesheet" href="css/slick.css">

        <!--====== Animate css ======-->
        <link rel="stylesheet" href="css/animate.css">

        <!--====== Nice Select css ======-->
        <link rel="stylesheet" href="css/nice-select.css">

        <!--====== Nice Number css ======-->
        <link rel="stylesheet" href="css/jquery.nice-number.min.css">

        <!--====== Magnific Popup css ======-->
        <link rel="stylesheet" href="css/magnific-popup.css">

        <!--====== Bootstrap css ======-->
        <link rel="stylesheet" href="css/bootstrap.min.css">

        <!--====== Fontawesome css ======-->
        <link rel="stylesheet" href="css/font-awesome.min.css">

        <!--====== Default css ======-->
        <link rel="stylesheet" href="css/default.css">

        <!--====== Style css ======-->
        <link rel="stylesheet" href="css/style.css">

        <!--====== Responsive css ======-->
        <link rel="stylesheet" href="css/responsive.css">
    </head>

    <body>
        <c:set var="edit" value="editprofile" />
        <c:set var="change" value="changepass" />
        <a href="adminprofile?go=profiledetail" class="btn-back">
            <i class="fa fa-arrow-left"></i> Back
        </a>
        <!--====== HEADER PART START ======-->
        <div class="header-top d-none d-lg-block">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="header-contact text-lg-left text-center">
                        <ul>
                            <li><img src="images/all-icon/map.png" alt="icon"><span>Hoa Lac Hi-tech Park, Thach Hoa, Thach That, Ha Noi</span></li>
                            <li><img src="images/all-icon/email.png" alt="icon"><span>anhtthe161427@fpt.edu.vn</span></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="header-opening-time text-lg-right text-center">
<!--                        <p>Opening Hours : Monday to Saturay - 8 Am to 5 Pm</p>-->
                    </div>
                </div>
            </div> <!-- row -->
        </div> <!-- container -->
    </div>
        
        <div class="header-logo-support pt-30 pb-30">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4">
                    <div class="logo">
                            <img src="images/logo.png" alt="Logo">
                    </div>
                </div>
            </div> <!-- row -->
        </div> <!-- container -->
    </div>
        <!--====== HEADER PART ENDS ======-->

        <!--====== PROFILE PART START ======-->

        <div class="container px-4 mt-4">
            <!-- Account page navigation-->
            <hr class="mt-0 mb-4">
            <div class="row">
                <div class="col-xl-4">
                    <!--                     Profile picture card-->
                    <div class="card mb-4 mb-xl-0">
                        <div class="card-header">Profile Picture</div>
                        <div class="card-body text-center">
                            <!--                             Profile picture image-->
                            <img class="img-account-profile rounded-circle mb-2" src="images/avatar1.png" alt="">
                            <!--                             Profile picture help block-->
                            <div class="small font-italic text-muted mb-4"></div>
                            <!--                             Profile picture upload button-->
                            <!--                                                        <button class="btn btn-primary" type="button">Upload new image</button>-->
                        </div>
                    </div>
                </div>
                <div class="col-xl-8">
                    <!-- Account details card-->
                    <div class="card mb-4">
                        <div class="card-header">Personal Information</div>
                        <div class="card-body">
                            <div style="color: red">${error}</div>
                            <div style="color: green;">${requestScope.status}</div>
                            <c:if test="${go eq change}">
                                <form id="changepass" action="adminprofile?go=changepass" method="post">

                                    <!-- Form Group (username)-->
                                    <input type="hidden" name="id" value="${requestScope.userData.id}">
                                    <div class="mb-3">
                                        <label class="small mb-1" for="inputUsername">Current Password</label>
                                        <input class="form-control" type="password" id="currentPassword" name="currentPassword" required>
                                    </div>
                                    <!-- Form Row-->
                                    <div class="row gx-3 mb-3">
                                        <!-- Form Group (full name)-->
                                        <div class="col-md-12">
                                            <label class="small mb-1" for="inputFullName">New Password</label>
                                            <input class="form-control" type="password" id="newPassword" name="newPassword" required>
                                        </div>
                                    </div>
                                    <!-- Form Row        -->
                                    <div class="row gx-3 mb-3">
                                        <!-- Form Group (email)-->
                                        <div class="col-md-12">
                                            <label class="small mb-1" for="inputEmailAddress">Confirm New Password</label>
                                            <input class="form-control" type="password" id="confirmPassword" name="confirmPassword"  required>
                                        </div>
                                    </div>
                                    <!-- Form Group (phone)-->

                                    <!-- Save changes button-->
                                    <a href="adminprofile?go=profiledetail"> <button type="button" class="btn btn-secondary"><i class="fas fa-arrow-left"></i>&ensp; Back to profile</button></a>&emsp; &emsp;
                                    <button class="btn btn-primary" type="submit"><i class="fas fa-save"></i> Save</button>
                                </form>
                            </c:if>
                            <c:if test="${go eq edit}">
                                <form id="editprofile" action="adminprofile?go=editprofile" method="post">

                                    <!-- Form Group (username)-->
                                    <input type="hidden" name="id" value="${requestScope.userData.id}">
                                    <div class="mb-3">
                                        <label class="small mb-1" for="inputUsername">Username (how your name will appear to other users on the site)</label>
                                        <input class="form-control" id="inputUsername" type="text" placeholder="Enter your username" value="${requestScope.userData.username}" name="username" readonly>
                                    </div>
                                    <!-- Form Row-->
                                    <div class="row gx-3 mb-3">
                                        <!-- Form Group (full name)-->
                                        <div class="col-md-12">
                                            <label class="small mb-1" for="inputFullName">Full name</label>
                                            <input class="form-control" id="inputFullName" type="text" placeholder="Enter your full name" value="${requestScope.userData.fullname}" name="fullname" required>
                                        </div>
                                    </div>
                                    <!-- Form Row        -->
                                    <div class="row gx-3 mb-3">
                                        <!-- Form Group (email)-->
                                        <div class="col-md-12">
                                            <label class="small mb-1" for="inputEmailAddress">Email</label>
                                            <input class="form-control" id="inputEmailAddress" type="text" placeholder="Enter your email" name="email" value="${requestScope.userData.email}" readonly>
                                        </div>
                                    </div>
                                    <!-- Form Group (phone)-->
                                    <div class="row gx-3 mb-3">
                                        <div class="col-md-12">
                                            <label class="small mb-1" for="inputPhone">Phone Number</label>
                                            <input class="form-control" id="inputPhone" type="text" placeholder="Enter your phone number" name="phonenumber" value="${requestScope.userData.phone}" required>
                                        </div>
                                    </div>
                                    <!-- Save changes button-->
                                    <a href="adminprofile?go=profiledetail"> <button type="button" class="btn btn-secondary"><i class="fas fa-times"></i> Cancel</button></a>&emsp; &emsp;
                                    <button class="btn btn-primary" type="submit"><i class="fas fa-save"></i> Save</button>
                                </form>
                            </c:if> 
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>


    <!--====== PROFILE PART END ======--> 

    <!--====== BLOG PART ENDS ======-->

    <!--====== FOOTER PART START ======-->
    <jsp:include page="footer.jsp"/>
    <!--====== FOOTER PART ENDS ======-->
<style>
        .btn-back {
            position: fixed;
            top: 200px;
            left: 20px;
            font-size: 16px;
            color: #555;
            text-decoration: none;
        }

        .btn-back i {
            margin-right: 5px;
        }
    </style>

    <!--====== jquery js ======-->
    <script src="js/vendor/modernizr-3.6.0.min.js"></script>
    <script src="js/vendor/jquery-1.12.4.min.js"></script>

    <!--====== Bootstrap js ======-->
    <script src="js/bootstrap.min.js"></script>

    <!--====== Slick js ======-->
    <script src="js/slick.min.js"></script>

    <!--====== Magnific Popup js ======-->
    <script src="js/jquery.magnific-popup.min.js"></script>

    <!--====== Counter Up js ======-->
    <script src="js/waypoints.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>

    <!--====== Nice Select js ======-->
    <script src="js/jquery.nice-select.min.js"></script>

    <!--====== Nice Number js ======-->
    <script src="js/jquery.nice-number.min.js"></script>

    <!--====== Count Down js ======-->
    <script src="js/jquery.countdown.min.js"></script>

    <!--====== Validator js ======-->
    <script src="js/validator.min.js"></script>

    <!--====== Ajax Contact js ======-->
    <script src="js/ajax-contact.js"></script>

    <!--====== Main js ======-->
    <script src="js/main.js"></script>

    <!--====== Map js ======-->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDC3Ip9iVC0nIxC6V14CKLQ1HZNF_65qEQ"></script>
    <script src="js/map-script.js"></script>

</body>
</html>
