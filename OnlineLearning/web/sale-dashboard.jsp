<!doctype html>
<!-- 
* Bootstrap Simple Admin Template
* Version: 2.1
* Author: Alexis Luna
* Website: https://github.com/alexis-luna/bootstrap-simple-admin-template
-->
<%@page import="java.util.*" %>

<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="shortcut icon" href="images/favicon.png" type="image/png">
        <title>Sale Dashboard</title>

        <link href="assets/vendor/fontawesome/css/fontawesome.min.css" rel="stylesheet">
        <link href="assets/vendor/fontawesome/css/solid.min.css" rel="stylesheet">
        <link href="assets/vendor/fontawesome/css/brands.min.css" rel="stylesheet">
        <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/css/master.css" rel="stylesheet">
        <link href="assets/vendor/flagiconcss/css/flag-icon.min.css" rel="stylesheet">

    </head>

    <body>

        <div class="wrapper">
            <jsp:include page="sale-sidebar.jsp"/>

            <div id="body" class="active">
                <!-- navbar navigation component -->
                <jsp:include page="sale-header.jsp"/>
                <!-- end of navbar navigation -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 page-header">
                                <div class="page-pretitle">Overview</div>
                                <h2 class="page-title">Dashboard</h2>
                            </div>
                        </div>
                        <div class="row">
                            <!--                        <div class="col-sm-6 col-md-6 col-lg-4 mt-3">
                                                        <div class="card">
                                                            <div class="content">
                                                                <div class="row">
                                                                    <div class="col-sm-4">
                                                                        <div class="icon-big text-center">
                                                                            <i class="violet fas fa-eye"></i>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-8">
                                                                        <div class="detail">
                                                                            <p class="detail-subtitle">Weekly Register</p>
                                                                            <span class="number">28,210</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="footer">
                                                                    <hr />
                                                                    <div class="stats">
                                                                        <i class="fas fa-stopwatch"></i> For this Month
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>-->
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="card">
                                                    <div class="content">

                                                        <div class="detail">
                                                            <p class="detail-subtitle">Weekly Enrollment</p>
                                                            <div class="row">
                                                                <div class="col-sm-4">
                                                                    <h5>${enrollments}</h5>
                                                                </div>
                                                                <div class="col-sm-8 " >
                                                                    <h5 style="display: inline-block; float: right;" class=" ${enrollChangePercent>=0?'text-success':'text-danger'}">${enrollChangePercent}% ${enrollChangePercent>0?'Increased':(enrollChangePercent<0?'Decreased':'No Change')}</h5>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="footer">

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="content">
                                                <div class="head">
                                                    <h5 class="mb-2">Subject Enrollment Overview</h5>
                                                    <select id="subEnrollOption" style="width: 5cm" class="form-select mb-2" required>
                                                        <option value="daily" selected>Last 7 Days</option>
                                                        <option value="monthly">Last 12 Months</option>
                                                        <option value="yearly">Last 5 Years</option>
                                                    </select>                                            
                                                </div>
                                                <div class="canvas-wrapper">
                                                    <canvas class="chart" id="enroll-piechart"></canvas>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="card">
                                                <div class="content">
                                                    <div class="head">
                                                        <h5 class="mb-2">Total Enrollment Overview</h5>
                                                        <select id="revenueOption" style="width: 5cm" class="form-select mb-2" required>
                                                            <option value="daily" selected>Last 7 Days</option>
                                                            <option value="monthly">Last 12 Months</option>
                                                            <option value="yearly">Last 5 Years</option>
                                                        </select>             
                                                    </div>
                                                    <div class="canvas-wrapper">
                                                        <canvas class="chart" id="revenueChart"></canvas>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="card">
                                                <div class="content">
                                                    <div class="head">
                                                        <div class="container">
                                                            <h5 class="mb-2">Course Statistics</h5>
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <select id="dropdownOption" style="width: 6cm" class="form-select mb-2" onchange="shortByDropdown()">
                                                                        <option value="1" selected>Sort by Id</option>
                                                                        <option value="2">Sort by Enrollments</option>
                                                                        <option value="3">Sort by Completion Rates</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <form action="sale-dashboard" method="post" style="display: inline-block; float: right;">
                                                                        <button type="submit" class="btn btn-outline-primary ">Download Sheet</button>
                                                                    </form>                                                            
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!--                                                        <select id="revenueOption" style="width: 5cm" class="form-select mb-2" required>
                                                                                                                    <option value="daily" selected>Sort By</option>
                                                                                                                    <option value="monthly">Last 12 Months</option>
                                                                                                                    <option value="yearly">Last 5 Years</option>
                                                                                                                </select>-->

                                                    </div>
                                                    <table class="table by-id">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col">Id</th>
                                                                <th scope="col">Course Name</th>
                                                                <th scope="col">Subject Name</th>
                                                                <th scope="col">Number of Enrollments</th>
                                                                <th scope="col">Completion rates</th>
                                                                <th scope="col">Active</th>
                                                            </tr>
                                                        </thead>
                                                        <%Vector<Integer> id = (Vector)request.getAttribute("courseId");
                                                        Vector<String> name= (Vector)request.getAttribute("name");
                                                        Vector<String> subject = (Vector)request.getAttribute("subject");
                                                        Vector<Integer> total = (Vector)request.getAttribute("total");
                                                        Vector<Double> complete = (Vector)request.getAttribute("complete");
                                                        Vector<Integer> active = (Vector)request.getAttribute("active");

                                                            for(int i =0; i<id.size();i++){%>
                                                        <tr>
                                                            <th scope="row"><%=id.get(i)%></th>
                                                            <td><%=name.get(i)%></td>
                                                            <td><%=subject.get(i)%></td>
                                                            <td><%=total.get(i)%></td>
                                                            <td><%=complete.get(i)%> %</td>
                                                            <%=active.get(i)==1?"<td class='text-success'>Active</td>":"<td class='text-danger'>Inactive</td>"%></td>
                                                        </tr>
                                                        <%}%>
                                                    </table>
                                                    <table class="table by-enroll" style="display: none;">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col">Id</th>
                                                                <th scope="col">Course Name</th>
                                                                <th scope="col">Subject Name</th>
                                                                <th scope="col">Number of Enrollments</th>
                                                                <th scope="col">Completion rates</th>
                                                                <th scope="col">Active</th>
                                                            </tr>
                                                        </thead>
                                                        <%id = (Vector)request.getAttribute("courseId1");
                                                          name= (Vector)request.getAttribute("name1");
                                                          subject = (Vector)request.getAttribute("subject1");
                                                          total = (Vector)request.getAttribute("total1");
                                                          complete = (Vector)request.getAttribute("complete1");
                                                          active = (Vector)request.getAttribute("active1");

                                                              for(int i =0; i<id.size();i++){%>
                                                        <tr>
                                                            <th scope="row"><%=id.get(i)%></th>
                                                            <td><%=name.get(i)%></td>
                                                            <td><%=subject.get(i)%></td>
                                                            <td><%=total.get(i)%></td>
                                                            <td><%=complete.get(i)%> %</td>
                                                            <%=active.get(i)==1?"<td class='text-success'>Active</td>":"<td class='text-danger'>Inactive</td>"%></td>
                                                        </tr>
                                                        <%}%>
                                                    </table>

                                                    <table class="table by-comp" style="display: none;">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col">Id</th>
                                                                <th scope="col">Course Name</th>
                                                                <th scope="col">Subject Name</th>
                                                                <th scope="col">Number of Enrollments</th>
                                                                <th scope="col">Completion rates</th>
                                                                <th scope="col">Active</th>
                                                            </tr>
                                                        </thead>
                                                    <%id = (Vector)request.getAttribute("courseId2");
                                                    name= (Vector)request.getAttribute("name1");
                                                    subject = (Vector)request.getAttribute("subject2");
                                                    total = (Vector)request.getAttribute("total2");
                                                    complete = (Vector)request.getAttribute("complete2");
                                                    active = (Vector)request.getAttribute("active2");
                                                    for(int i =0; i<id.size();i++){%>
                                                    <tr>
                                                        <th scope="row"><%=id.get(i)%></th>
                                                        <td><%=name.get(i)%></td>
                                                        <td><%=subject.get(i)%></td>
                                                        <td><%=total.get(i)%></td>
                                                        <td><%=complete.get(i)%> %</td>
                                                            <%=active.get(i)==1?"<td class='text-success'>Active</td>":"<td class='text-danger'>Inactive</td>"%></td>
                                                    </tr>
                                                    <%}%>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                            <!-- <div class="col-md-6">
                                <div class="card">
                                    <div class="content">
                                        <div class="head">
                                            <h5 class="mb-0">Top Visitors by Country</h5>
                                            <p class="text-muted">Current year website visitor data</p>
                                        </div>
                                        <div class="canvas-wrapper">
                                            <table class="table table-striped">
                                                <thead class="success">
                                                    <tr>
                                                        <th>Country</th>
                                                        <th class="text-end">Unique Visitors</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td><i class="flag-icon flag-icon-us"></i> United States</td>
                                                        <td class="text-end">27,340</td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="flag-icon flag-icon-in"></i> India</td>
                                                        <td class="text-end">21,280</td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="flag-icon flag-icon-jp"></i> Japan</td>
                                                        <td class="text-end">18,210</td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="flag-icon flag-icon-gb"></i> United Kingdom</td>
                                                        <td class="text-end">15,176</td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="flag-icon flag-icon-es"></i> Spain</td>
                                                        <td class="text-end">14,276</td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="flag-icon flag-icon-de"></i> Germany</td>
                                                        <td class="text-end">13,176</td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="flag-icon flag-icon-br"></i> Brazil</td>
                                                        <td class="text-end">12,176</td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="flag-icon flag-icon-id"></i> Indonesia</td>
                                                        <td class="text-end">11,886</td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="flag-icon flag-icon-ph"></i> Philippines</td>
                                                        <td class="text-end">11,509</td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="flag-icon flag-icon-nz"></i> New Zealand</td>
                                                        <td class="text-end">1,700</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="content">
                                        <div class="head">
                                            <h5 class="mb-0">Most Visited Pages</h5>
                                            <p class="text-muted">Current year website visitor data</p>
                                        </div>
                                        <div class="canvas-wrapper">
                                            <table class="table table-striped">
                                                <thead class="success">
                                                    <tr>
                                                        <th>Page Name</th>
                                                        <th class="text-end">Visitors</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>/about.html <a href="#"><i class="fas fa-link blue"></i></a></td>
                                                        <td class="text-end">8,340</td>
                                                    </tr>
                                                    <tr>
                                                        <td>/special-promo.html <a href="#"><i class="fas fa-link blue"></i></a></td>
                                                        <td class="text-end">7,280</td>
                                                    </tr>
                                                    <tr>
                                                        <td>/products.html <a href="#"><i class="fas fa-link blue"></i></a></td>
                                                        <td class="text-end">6,210</td>
                                                    </tr>
                                                    <tr>
                                                        <td>/documentation.html <a href="#"><i class="fas fa-link blue"></i></a></td>
                                                        <td class="text-end">5,176</td>
                                                    </tr>
                                                    <tr>
                                                        <td>/customer-support.html <a href="#"><i class="fas fa-link blue"></i></a></td>
                                                        <td class="text-end">4,276</td>
                                                    </tr>
                                                    <tr>
                                                        <td>/index.html <a href="#"><i class="fas fa-link blue"></i></a></td>
                                                        <td class="text-end">3,176</td>
                                                    </tr>
                                                    <tr>
                                                        <td>/products-pricing.html <a href="#"><i class="fas fa-link blue"></i></a></td>
                                                        <td class="text-end">2,176</td>
                                                    </tr>
                                                    <tr>
                                                        <td>/product-features.html <a href="#"><i class="fas fa-link blue"></i></a></td>
                                                        <td class="text-end">1,886</td>
                                                    </tr>
                                                    <tr>
                                                        <td>/contact-us.html <a href="#"><i class="fas fa-link blue"></i></a></td>
                                                        <td class="text-end">1,509</td>
                                                    </tr>
                                                    <tr>
                                                        <td>/terms-and-condition.html <a href="#"><i class="fas fa-link blue"></i></a></td>
                                                        <td class="text-end">1,100</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                        <!-- <div class="row">
                            <div class="col-sm-6 col-md-6 col-lg-4">
                                <div class="card">
                                    <div class="content">
                                        <div class="row">
                                            <div class="dfd text-center">
                                                <i class="blue large-icon mb-2 fas fa-thumbs-up"></i>
                                                <h4 class="mb-0">+21,900</h4>
                                                <p class="text-muted">FACEBOOK PAGE LIKES</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-4">
                                <div class="card">
                                    <div class="content">
                                        <div class="row">
                                            <div class="dfd text-center">
                                                <i class="orange large-icon mb-2 fas fa-reply-all"></i>
                                                <h4 class="mb-0">+22,566</h4>
                                                <p class="text-muted">INSTAGRAM FOLLOWERS</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-4">
                                <div class="card">
                                    <div class="content">
                                        <div class="row">
                                            <div class="dfd text-center">
                                                <i class="grey large-icon mb-2 fas fa-envelope"></i>
                                                <h4 class="mb-0">+15,566</h4>
                                                <p class="text-muted">E-MAIL SUBSCRIBERS</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
        <script src="assets/vendor/jquery/jquery.min.js"></script>
        <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="assets/vendor/chartsjs/Chart.min.js"></script>
        <script src="assets/js/script.js"></script>
        ${revenueChart}
        ${enrollmentChart}
        ${subEnrollPieChart}
        <script>
                                                                        function shortByDropdown() {
                                                                            var option = document.getElementById("dropdownOption").value;
                                                                            var sortId = document.getElementsByClassName("by-id");
                                                                            var sortEnr = document.getElementsByClassName("by-enroll");
                                                                            var sortComp = document.getElementsByClassName("by-comp");
                                                                            var idStyle;
                                                                            var enrStyle;
                                                                            var comStyle;
                                                                            if (option === '1') {
                                                                                idStyle = "block";
                                                                                enrStyle = "none";
                                                                                comStyle = "none";
                                                                            } else if (option === '2') {
                                                                                idStyle = "none";
                                                                                enrStyle = "block";
                                                                                comStyle = "none";
                                                                            } else {
                                                                                idStyle = "none";
                                                                                enrStyle = "none";
                                                                                comStyle = "block";
                                                                            }

                                                                            for (const box of sortId) {
                                                                                box.style.display = idStyle;
                                                                            }
                                                                            for (const box of sortEnr) {
                                                                                box.style.display = enrStyle;
                                                                            }
                                                                            for (const box of sortComp) {
                                                                                box.style.display = comStyle;
                                                                            }

                                                                        }
        </script>
    </body>

</html>
