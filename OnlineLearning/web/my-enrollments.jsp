<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html lang="en">
    <head>

        <!--====== Required meta tags ======-->
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!--====== Title ======-->
        <title>Edubin - LMS Education HTML Template</title>

        <!--====== Favicon Icon ======-->
        <link rel="shortcut icon" href="images/favicon.png" type="image/png">

        <!--====== Slick css ======-->
        <link rel="stylesheet" href="css/slick.css">

        <!--====== Animate css ======-->
        <link rel="stylesheet" href="css/animate.css">

        <!--====== Nice Select css ======-->
        <link rel="stylesheet" href="css/nice-select.css">

        <!--====== Nice Number css ======-->
        <link rel="stylesheet" href="css/jquery.nice-number.min.css">

        <!--====== Magnific Popup css ======-->
        <link rel="stylesheet" href="css/magnific-popup.css">

        <!--====== Bootstrap css ======-->
        <link rel="stylesheet" href="css/bootstrap.min.css">

        <!--====== Fontawesome css ======-->
        <link rel="stylesheet" href="css/font-awesome.min.css">

        <!--====== Default css ======-->
        <link rel="stylesheet" href="css/default.css">

        <!--====== Style css ======-->
        <link rel="stylesheet" href="css/style.css">

        <!--====== Responsive css ======-->
        <link rel="stylesheet" href="css/responsive.css">

    </head>

    <body style="position: relative;">




        <jsp:include page="header.jsp"/>
        <!--====== PAGE BANNER PART START ======-->
        <c:if test="${dropSuccess}">
            <div class="alert alert-success alert-dismissible" style="position: fixed; top: 80px; right: 20px; z-index: 6;">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Drop Out Successfully!</strong> 
            </div>
        </c:if>

        <section id="page-banner" class="pt-105 pb-130 bg_cover" data-overlay="8" style="background-image: url(images/page-banner-3.jpg)">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-banner-cont">
                            <h2>My Enrollments</h2>
                            <!--                            <nav aria-label="breadcrumb">
                                                            <ol class="breadcrumb">
                                                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                                                <li class="breadcrumb-item active" aria-current="page">Teachers</li>
                                                            </ol>
                                                        </nav>-->
                        </div> <!-- page banner cont -->
                    </div>
                </div> <!-- row -->
            </div> <!-- container -->
        </section>

        <!--====== PAGE BANNER PART ENDS ======-->

        <!--====== TEACHERS PART START ======-->

        <section id="teachers-singel" class="pt-70 pb-120 gray-bg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9">
                        <c:if test="${!enrollments.isEmpty()}">
                            <table class="table mt-30">
                                <thead class="thead-dark">
                                    <tr>
                                        <th style="width:40%" scope="col">Course</th>
                                        <th scope="col">Subject</th>
                                        <th scope="col">Enrollment Time</th>
                                        <th scope="col">Status</th>
                                        <th scope="col"></th>
                                        <!--                                    <th scope="col">Handle</th>-->
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach var="enrollment" items="${enrollments}">
                                        <tr class="table-light">
                                            <th scope="row">${enrollment.courseName}</th>
                                            <td>${enrollment.subjectName}</td>
                                            <td>${enrollment.enrollmentDate}</td>
                                            <td>${enrollment.status==1?'<p class="text-primary">In Progress</p>':
                                                  (enrollment.status==2?'<p class="text-success">Completed</p>':'<p class="text-danger">Dropped</p>')}
                                            </td>
                                            <td>  
                                                <c:if test="${enrollment.status == 1}">
                                                    <form action="my-enrollments" method="post">
                                                        <input name="dropOut" value="${enrollment.courseId}" style="display: none;">
                                                        <button type="submit" class="btn btn-outline-danger" onclick="return confirmNotification()" >Drop out</button>
                                                    </form>
                                                </c:if>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>

                            <nav class="courses-pagination mt-50">
                                <ul class="pagination justify-content-lg-end justify-content-center">
                                    <c:if test="${index!=0}">
                                        <li class="page-item">
                                            <a href="my-enrollments?index=${index-1}${(store!=null)?'&subject=${store}':''}" aria-label="Previous">
                                                <i class="fa fa-angle-left"></i>
                                            </a>
                                        </li>
                                    </c:if>
                                    <c:forEach var="i" begin="0" end = "${totalPages-1}">
                                        <li class="page-item"><a ${index==i?"class='active'":""} href="my-enrollments?index=${i}${store!=null?'&subject=':''}${store}">${i+1}</a></li>
                                        </c:forEach>
                                        <c:if test="${index < totalPages-1}">
                                        <li class="page-item">
                                            <a href="my-enrollments?index=${index+1}${(store!=null)?'&subject=${store}':''}" aria-label="Next">
                                                <i class="fa fa-angle-right"></i>
                                            </a>
                                        </li>
                                    </c:if>
                                </ul>
                                <input style="display: none" value="${store}">
                            </nav>  <!-- courses pagination -->

                        </c:if>
                    </div>
                    <div class="col-lg-3">
                        <div class="saidbar">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="categories mt-30">
                                        <h4>Subjects</h4>
                                        <ul>
                                            <c:forEach var="subject" items="${subjects}">
                                                <li><a href="my-enrollments?subject=${subject.id}">${subject.name}</a></li>
                                                </c:forEach>
                                        </ul>
                                    </div>
                                </div> <!-- row -->
                            </div> <!-- saidbar -->
                        </div>
                    </div> <!-- row --> 
                </div> <!-- row -->
            </div> <!-- container -->
        </section>

        <!--====== EVENTS PART ENDS ======-->

        <!--====== FOOTER PART START ======-->
        <jsp:include page="footer.jsp"/>



        <!--====== jquery js ======-->
        <script src="js/vendor/modernizr-3.6.0.min.js"></script>
        <script src="js/vendor/jquery-1.12.4.min.js"></script>

        <!--====== Bootstrap js ======-->
        <script src="js/bootstrap.min.js"></script>

        <!--====== Slick js ======-->
        <script src="js/slick.min.js"></script>

        <!--====== Magnific Popup js ======-->
        <script src="js/jquery.magnific-popup.min.js"></script>

        <!--====== Counter Up js ======-->
        <script src="js/waypoints.min.js"></script>
        <script src="js/jquery.counterup.min.js"></script>

        <!--====== Nice Select js ======-->
        <script src="js/jquery.nice-select.min.js"></script>

        <!--====== Nice Number js ======-->
        <script src="js/jquery.nice-number.min.js"></script>

        <!--====== Count Down js ======-->
        <script src="js/jquery.countdown.min.js"></script>

        <!--====== Validator js ======-->
        <script src="js/validator.min.js"></script>

        <!--====== Ajax Contact js ======-->
        <script src="js/ajax-contact.js"></script>

        <!--====== Main js ======-->
        <script src="js/main.js"></script>

        <!--====== Map js ======-->
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDC3Ip9iVC0nIxC6V14CKLQ1HZNF_65qEQ"></script>
        <script src="js/map-script.js"></script>

        <script>
                                                        function confirmNotification()
                                                        {
                                                            if (window.confirm("Do you want to drop out this course ?")) {
                                                                return true;
                                                            } else {
                                                                return false;
                                                            }
                                                        }

        </script>


    </body>

</html>
