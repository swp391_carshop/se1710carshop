<!doctype html>
<html lang="en">

    <head>

        <!--====== Required meta tags ======-->
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!--====== Title ======-->
        <title>Edubin - LMS Education</title>

        <!--====== Favicon Icon ======-->
        <link rel="shortcut icon" href="images/favicon.png" type="image/png">

        <!--====== Slick css ======-->
        <link rel="stylesheet" href="css/slick.css">

        <!--====== Animate css ======-->
        <link rel="stylesheet" href="css/animate.css">

        <!--====== Nice Select css ======-->
        <link rel="stylesheet" href="css/nice-select.css">

        <!--====== Nice Number css ======-->
        <link rel="stylesheet" href="css/jquery.nice-number.min.css">

        <!--====== Magnific Popup css ======-->
        <link rel="stylesheet" href="css/magnific-popup.css">

        <!--====== Bootstrap css ======-->
        <link rel="stylesheet" href="css/bootstrap.min.css">

        <!--====== Fontawesome css ======-->
        <link rel="stylesheet" href="css/font-awesome.min.css">

        <!--====== Default css ======-->
        <link rel="stylesheet" href="css/default.css">

        <!--====== Style css ======-->
        <link rel="stylesheet" href="css/style.css">

        <!--====== Responsive css ======-->
        <link rel="stylesheet" href="css/responsive.css">

        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <%@ page import="java.util.Vector" %>
        <%@ page import="entity.Course"%>
    </head>

    <body>

        <!--====== PRELOADER PART START ======-->
        <c:set var="course" value="${requestScope.courseDetail}" />
        <c:set var="coursesRelated" value="${coursesRelated}" />
        <c:set var="subjectName" value="${subjectName}" />
        <c:set var="chapterTotal" value="${chapterTotal}" />

        <div class="preloader">
            <div class="loader rubix-cube">
                <div class="layer layer-1"></div>
                <div class="layer layer-2"></div>
                <div class="layer layer-3 color-1"></div>
                <div class="layer layer-4"></div>
                <div class="layer layer-5"></div>
                <div class="layer layer-6"></div>
                <div class="layer layer-7"></div>
                <div class="layer layer-8"></div>
            </div>
        </div>


        <!--====== HEADER PART START ======-->
        <jsp:include page="header.jsp"/>
        <!--====== HEADER PART ENDS ======-->


        <!--====== SEARCH BOX PART START ======-->

        <div class="search-box">
            <div class="serach-form">
                <div class="closebtn">
                    <span></span>
                    <span></span>
                </div>
                <form action="./courses" method="GET">
                    <input type="hidden" name="go" value="search">
                    <input type="text" name="cname" placeholder="Search by course name" value="${cname}"> 
                    <button><i class="fa fa-search"></i></button>
                </form>
            </div> <!-- search form -->
        </div>

        <!--====== SEARCH BOX PART ENDS ======-->

        <!--====== PAGE BANNER PART START ======-->

        <section id="page-banner" class="pt-105 pb-110 bg_cover" data-overlay="8" style="background-image: url(images/page-banner-2.jpg)">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-banner-cont">
                            <h2>${course.courseName}</h2>
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="homepage">Home</a></li>
                                    <li class="breadcrumb-item"><a href="courses">Courses</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">${course.courseName}</li>
                                </ol>
                            </nav>
                        </div>  <!-- page banner cont -->
                    </div>
                </div> <!-- row -->
            </div> <!-- container -->
        </section>

        <!--====== PAGE BANNER PART ENDS ======-->

        <!--====== COURSES SINGEl PART START ======-->

        <section id="corses-singel" class="pt-90 pb-120 gray-bg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="corses-singel-left mt-30">
                            <div class="title">
                                <h3>${course.courseName}</h3>
                            </div> <!-- title -->
                            <div class="course-terms">
                                <ul>
                                    <li>
                                        <div class="course-category">
                                            <span>Category</span>
                                            <h6>${subjectName}</h6>
                                        </div>
                                    </li>

                                </ul>
                            </div> <!-- course terms -->

                            <div class="corses-singel-image pt-50">
                                <img src="images/course/cu-1.jpg" alt="Courses">
                            </div> <!-- corses singel image -->

                            <div class="corses-tab mt-30">
                                <ul class="nav nav-justified" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="active" id="overview-tab" data-toggle="tab" href="#overview" role="tab" aria-controls="overview" aria-selected="true">Overview</a>
                                    </li>
                                    <li class="nav-item">
                                        <a id="curriculam-tab" data-toggle="tab" href="#curriculam" role="tab" aria-controls="curriculam" aria-selected="false">Curriculum</a>
                                    </li>
                                    <!--<li class="nav-item">
                                        <a id="instructor-tab" data-toggle="tab" href="#instructor" role="tab" aria-controls="instructor" aria-selected="false">Instructor</a>
                                    </li>-->
                                    <!--<li class="nav-item">
                                                                            <a id="reviews-tab" data-toggle="tab" href="#reviews" role="tab" aria-controls="reviews" aria-selected="false">Reviews</a>
                                                                        </li>-->
                                </ul>

                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview-tab">
                                        <div class="overview-description">
                                            <div class="singel-description pt-40">
                                                <h6>Course Summery</h6>
                                                <p>${course.description}</p>
                                                <h6>Course Objective</h6>
                                                <p>${course.objective}</p>
                                            </div>
                                        </div> <!-- overview description -->
                                    </div>
                                    <div class="tab-pane fade" id="curriculam" role="tabpanel" aria-labelledby="curriculam-tab">
                                        <div class="curriculam-cont">
                                            <div class="title">
                                                <h6>${course.courseName}</h6>
                                            </div>
                                            <div class="accordion" id="accordionExample">
                                                <c:forEach var="chapter" items="${chapterList}" varStatus="loop">
                                                    <div class="card">
                                                        <div class="card-header" id="heading${loop.count}">
                                                            <a href="#" data-toggle="collapse" class="collapsed"  data-target="#collapse${loop.count}" aria-expanded="true" aria-controls="collapse${loop.count}">
                                                                <ul>
                                                                    <li><i class="fa fa-file-o"></i></li>
                                                                    <li><span class="lecture">Chapter ${loop.count}</span></li>
                                                                    <li><span class="head">Chapter ${chapter.chapterName}</span></li>
                                                                    <li><span class="time d-none d-md-block"><i class="fa fa-clock-o"></i></span></li>
                                                                </ul>
                                                            </a>
                                                        </div>

                                                        <div id="collapse${loop.count}" class="collapse" aria-labelledby="heading${loop.count}" data-parent="#accordionExample">
                                                            <c:forEach var="les" items="${lessonList}">
                                                                <c:if test="${chapter.chapterId == les.chapterId}">
                                                                    <div class="card-body">
                                                                        <p><a href="/OnlineLearning/learn?lid=${les.id}" >${les.name}</a></p>
                                                                    </div>
                                                                </c:if>
                                                            </c:forEach>
                                                        </div>
                                                    </div>
                                                </c:forEach>

                                                <!-- <div class="card">
                                                     <div class="card-header" id="headingTow">
                                                         <a href="#" data-toggle="collapse" class="collapsed" data-target="#collapseTow" aria-expanded="true" aria-controls="collapseTow">
                                                             <ul>
                                                                 <li><i class="fa fa-file-o"></i></li>
                                                                 <li><span class="lecture">Lecture 1.2</span></li>
                                                                 <li><span class="head">What is javascirpt</span></li>
                                                                 <li><span class="time d-none d-md-block"><i class="fa fa-clock-o"></i> <span> 00.30.00</span></span></li>
                                                             </ul>
                                                         </a>
                                                     </div>
 
                                                     <div id="collapseTow" class="collapse" aria-labelledby="headingTow" data-parent="#accordionExample">
                                                         <div class="card-body">
                                                             <p>Ut quis scelerisque risus, et viverra nisi. Phasellus ultricies luctus augue, eget maximus felis laoreet quis. Maecenasbibendum tempor eros.</p>
                                                         </div>
                                                     </div>
                                                 </div> -->

                                            </div>
                                        </div> <!-- curriculam cont -->
                                    </div>

                                    <!--                                    <div class="tab-pane fade" id="instructor" role="tabpanel" aria-labelledby="instructor-tab">
                                                                            <div class="instructor-cont">
                                                                                <div class="instructor-author">
                                                                                    <div class="author-thum">
                                                                                        <img src="images/instructor/i-1.jpg" alt="Instructor">
                                                                                    </div>
                                                                                    <div class="author-name">
                                                                                        <a href="#"><h5>Sumon Hasan</h5></a>
                                                                                        <span>Senior WordPress Developer</span>
                                                                                        <ul class="social">
                                                                                            <li><a href="#"><i class="fa fa-facebook-f"></i></a></li>
                                                                                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                                                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                                                                            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="instructor-description pt-25">
                                                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus fuga ratione molestiae unde provident quibusdam sunt, doloremque. Error omnis mollitia, ex dolor sequi. Et, quibusdam excepturi. Animi assumenda, consequuntur dolorum odio sit inventore aliquid, optio fugiat alias. Veritatis minima, dicta quam repudiandae repellat non sit, distinctio, impedit, expedita tempora numquam?</p>
                                                                                </div>
                                                                            </div>  instructor cont 
                                                                        </div>-->
                                    <!--                                    <div class="tab-pane fade" id="reviews" role="tabpanel" aria-labelledby="reviews-tab">
                                                                            <div class="reviews-cont">
                                                                                <div class="title">
                                                                                    <h6>Student Reviews</h6>
                                                                                </div>
                                                                                <ul>
                                                                                    <li>
                                                                                        <div class="singel-reviews">
                                                                                            <div class="reviews-author">
                                                                                                <div class="author-thum">
                                                                                                    <img src="images/review/r-1.jpg" alt="Reviews">
                                                                                                </div>
                                                                                                <div class="author-name">
                                                                                                    <h6>Bobby Aktar</h6>
                                                                                                    <span>April 03, 2019</span>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="reviews-description pt-20">
                                                                                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which.</p>
                                                                                                <div class="rating">
                                                                                                    <ul>
                                                                                                        <li><i class="fa fa-star"></i></li>
                                                                                                        <li><i class="fa fa-star"></i></li>
                                                                                                        <li><i class="fa fa-star"></i></li>
                                                                                                        <li><i class="fa fa-star"></i></li>
                                                                                                        <li><i class="fa fa-star"></i></li>
                                                                                                    </ul>
                                                                                                    <span>/ 5 Star</span>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>  singel reviews 
                                                                                    </li>
                                                                                    <li>
                                                                                        <div class="singel-reviews">
                                                                                            <div class="reviews-author">
                                                                                                <div class="author-thum">
                                                                                                    <img src="images/review/r-2.jpg" alt="Reviews">
                                                                                                </div>
                                                                                                <div class="author-name">
                                                                                                    <h6>Humayun Ahmed</h6>
                                                                                                    <span>April 13, 2019</span>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="reviews-description pt-20">
                                                                                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which.</p>
                                                                                                <div class="rating">
                                                                                                    <ul>
                                                                                                        <li><i class="fa fa-star"></i></li>
                                                                                                        <li><i class="fa fa-star"></i></li>
                                                                                                        <li><i class="fa fa-star"></i></li>
                                                                                                        <li><i class="fa fa-star"></i></li>
                                                                                                        <li><i class="fa fa-star"></i></li>
                                                                                                    </ul>
                                                                                                    <span>/ 5 Star</span>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>  singel reviews 
                                                                                    </li>
                                                                                    <li>
                                                                                        <div class="singel-reviews">
                                                                                            <div class="reviews-author">
                                                                                                <div class="author-thum">
                                                                                                    <img src="images/review/r-3.jpg" alt="Reviews">
                                                                                                </div>
                                                                                                <div class="author-name">
                                                                                                    <h6>Tania Aktar</h6>
                                                                                                    <span>April 20, 2019</span>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="reviews-description pt-20">
                                                                                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which.</p>
                                                                                                <div class="rating">
                                                                                                    <ul>
                                                                                                        <li><i class="fa fa-star"></i></li>
                                                                                                        <li><i class="fa fa-star"></i></li>
                                                                                                        <li><i class="fa fa-star"></i></li>
                                                                                                        <li><i class="fa fa-star"></i></li>
                                                                                                        <li><i class="fa fa-star"></i></li>
                                                                                                    </ul>
                                                                                                    <span>/ 5 Star</span>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>  singel reviews 
                                                                                    </li>
                                                                                </ul>
                                                                                <div class="title pt-15">
                                                                                    <h6>Leave A Comment</h6>
                                                                                </div>
                                                                                <div class="reviews-form">
                                                                                    <form action="#">
                                                                                        <div class="row">
                                                                                            <div class="col-md-6">
                                                                                                <div class="form-singel">
                                                                                                    <input type="text" placeholder="Fast name">
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-6">
                                                                                                <div class="form-singel">
                                                                                                    <input type="text" placeholder="Last Name">
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-lg-12">
                                                                                                <div class="form-singel">
                                                                                                    <div class="rate-wrapper">
                                                                                                        <div class="rate-label">Your Rating:</div>
                                                                                                        <div class="rate">
                                                                                                            <div class="rate-item"><i class="fa fa-star" aria-hidden="true"></i></div>
                                                                                                            <div class="rate-item"><i class="fa fa-star" aria-hidden="true"></i></div>
                                                                                                            <div class="rate-item"><i class="fa fa-star" aria-hidden="true"></i></div>
                                                                                                            <div class="rate-item"><i class="fa fa-star" aria-hidden="true"></i></div>
                                                                                                            <div class="rate-item"><i class="fa fa-star" aria-hidden="true"></i></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-lg-12">
                                                                                                <div class="form-singel">
                                                                                                    <textarea placeholder="Comment"></textarea>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-lg-12">
                                                                                                <div class="form-singel">
                                                                                                    <button type="button" class="main-btn">Post Comment</button>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>  row 
                                                                                    </form>
                                                                                </div>
                                                                            </div>  reviews cont 
                                                                        </div>-->
                                </div> <!-- tab content -->
                            </div>
                        </div> <!-- corses singel left -->

                    </div>
                    <div class="col-lg-4">
                        <div class="row">
                            <div class="col-lg-12 col-md-6">
                                <div class="course-features mt-30">
                                    <h4>Course Features </h4>
                                    <ul>
                                        <li><i class="fa fa-clone"></i>Chapters : <span>${chapterList.size()}</span></li>
                                        <li><i class="fa fa-user-o"></i>Students :  <span>${totalStudent}</span></li>
                                    </ul>
                                    <div class="price-button pt-10">
                                        <c:choose>
                                            <c:when test = "${course.price > 0}">
                                                <span>Price : <b>$${course.price}</b></span>
                                            </c:when>
                                            <c:when test = "${course.price == 0}">
                                                <span>Price : <b>FREE</b></span>
                                            </c:when>    
                                        </c:choose>
                                        <c:choose>
                                            <c:when test = "${requestScope.checkEnrolled == true}">
                                                <c:if test = "${lessonList.size() != 0}">
                                                    <a href="./learn?lid=${lessonList.get(0).id}" class="main-btn">Go to course</a>
                                                </c:if>
                                            </c:when>
                                            <c:otherwise>
                                                <c:if test = "${lessonList.size() != 0}">
                                                    <a href="enroll?cid=${course.courseID}" class="main-btn">Enroll Now</a>
                                                </c:if>
                                            </c:otherwise> 
                                        </c:choose>

                                    </div>
                                </div> <!-- course features -->
                            </div>
                            <div class="col-lg-12 col-md-6">
                                <div class="You-makelike mt-30">
                                    <h4>You may like </h4> 
                                    <c:forEach items="${requestScope.featureCourses}"  var="feaCourse" varStatus="loop" >
                                    <div class="singel-makelike mt-20">
                                        <div class="image">
                                            <img src="images/your-make/y-1.jpg" alt="Image">
                                        </div>
                                        <div class="cont">
                                            <a href="coursedetails?cid=${feaCourse.courseID}"><h4>${feaCourse.courseName}</h4></a>
                                            <ul>
                                                <li>FREE</li>
                                            </ul>
                                        </div>
                                    </div>
                                    </c:forEach>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- row -->
                <div class="row">
                    <div class="col-lg-8">
                        <div class="releted-courses pt-95">
                            <div class="title">
                                <h3>Related Courses</h3>
                            </div>
                            <div class="row">
                                <c:forEach var="courseRelate" items="${coursesRelated}" varStatus="loop">
                                    <div class="col-md-6">
                                        <div class="singel-course mt-30">
                                            <div class="thum">
                                                <div class="image">
                                                    <img src="images/course/cu-${loop.count}.jpg" alt="Course">
                                                </div>
                                                <div class="price">
                                                    <span>$${courseRelate.price}</span>
                                                </div>
                                            </div>
                                            <div class="cont">
                                                <a href="coursedetails?cid=${courseRelate.courseID}"><h4>${courseRelate.courseName}</h4></a>
                                            </div>
                                        </div> <!-- singel course -->
                                    </div>
                                </c:forEach>
                            </div> <!-- row -->
                        </div> <!-- releted courses -->
                    </div>
                </div> <!-- row -->
            </div> <!-- container -->
        </section>

        <!--====== COURSES SINGEl PART ENDS ======-->

        <!--====== FOOTER PART START ======-->
        <jsp:include page="footer.jsp"/>
        <!--====== FOOTER PART ENDS ======-->

        <!--====== BACK TO TP PART START ======-->

        <a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>

        <!--====== BACK TO TP PART ENDS ======-->








        <!--====== jquery js ======-->
        <script src="js/vendor/modernizr-3.6.0.min.js"></script>
        <script src="js/vendor/jquery-1.12.4.min.js"></script>

        <!--====== Bootstrap js ======-->
        <script src="js/bootstrap.min.js"></script>

        <!--====== Slick js ======-->
        <script src="js/slick.min.js"></script>

        <!--====== Magnific Popup js ======-->
        <script src="js/jquery.magnific-popup.min.js"></script>

        <!--====== Counter Up js ======-->
        <script src="js/waypoints.min.js"></script>
        <script src="js/jquery.counterup.min.js"></script>

        <!--====== Nice Select js ======-->
        <script src="js/jquery.nice-select.min.js"></script>

        <!--====== Nice Number js ======-->
        <script src="js/jquery.nice-number.min.js"></script>

        <!--====== Count Down js ======-->
        <script src="js/jquery.countdown.min.js"></script>

        <!--====== Validator js ======-->
        <script src="js/validator.min.js"></script>

        <!--====== Ajax Contact js ======-->
        <script src="js/ajax-contact.js"></script>

        <!--====== Main js ======-->
        <script src="js/main.js"></script>

        <!--====== Map js ======-->
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDC3Ip9iVC0nIxC6V14CKLQ1HZNF_65qEQ"></script>
        <script src="js/map-script.js"></script>

    </body>
</html>
