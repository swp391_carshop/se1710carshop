<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="shortcut icon" href="images/favicon.png" type="image/png">
        <title>View Lesson</title>
        <link href="assets/vendor/fontawesome/css/fontawesome.min.css" rel="stylesheet">
        <link href="assets/vendor/fontawesome/css/solid.min.css" rel="stylesheet">
        <link href="assets/vendor/fontawesome/css/brands.min.css" rel="stylesheet">
        <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/vendor/datatables/datatables.min.css" rel="stylesheet">
        <link href="assets/css/master.css" rel="stylesheet">
    </head>

    <body>
        <div class="wrapper">
            <jsp:include page="lecturer-sidebar.jsp"/>

            <div id="body" class="active">
                <!-- navbar navigation component -->
                <jsp:include page="lecturer-header.jsp"/>
                <!-- end of navbar navigation -->
                <div class="content">
                    <div class="container">
                        <div class="page-title">
                            <a href="addchapter?cid=${chapterData.getCourses_Id()}" class="btn-back hidden"><i class="fas fa-arrow-left"></i> Back</a>
                            <h3>
                                <i class="fas fa-bookmark"></i>&ensp;Lesson list of chapter: <span style="color: #FF4136"><b>${chapterData.chapterName}</b></span>
                                <span class="float-end">
                                    <span class="sort-text" style="color: #07294D; font-size: 18px; font-weight: bold;">ADD: </span>
                                    <a href="addlesson?go=addLesson&chapterId=${chapterData.chapterId}" style="background-color: white; color: #07294D; font-size: 12px; padding: 5px 10px; margin-left: 10px; border: 1px solid #AAAAAA; border-radius: 3px;"
                                       onmousedown="this.style.backgroundColor = 'blue'; this.style.color = 'white';"
                                       onmouseup="this.style.backgroundColor = 'white'; this.style.color = '#07294D';"
                                       onmouseout="this.style.backgroundColor = 'white'; this.style.color = '#07294D';"
                                       onmouseover="this.style.backgroundColor = '#FFC600'; this.style.color = '#07294D';"
                                       ><i class="fas fa-plus-circle"></i>&ensp; ADD NEW LESSON</a>
                                    <a href="#" style="background-color: white; color: #07294D; font-size: 12px; padding: 5px 10px; margin-left: 10px; border: 1px solid #AAAAAA; border-radius: 3px;"
                                       onmousedown="this.style.backgroundColor = 'blue'; this.style.color = 'white';"
                                       onmouseup="this.style.backgroundColor = 'white'; this.style.color = '#07294D';"
                                       onmouseout="this.style.backgroundColor = 'white'; this.style.color = '#07294D';"
                                       onmouseover="this.style.backgroundColor = '#FFC600'; this.style.color = '#07294D';"
                                       data-bs-toggle="modal" data-bs-target="#addQuizModal"
                                       ><i class="fas fa-plus-circle"></i>&ensp; ADD NEW QUIZ</a>
                                </span>    
                            </h3>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-lg-12" style="padding-bottom: 20px">
                                <div class="card">
                                    <div class="card-header">Lesson</div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Lesson Name</th>
                                                        <th>Lesson no</th>
                                                        <th>Material text</th>
                                                        <th>Material link</th>
                                                        <th style="text-align: right">Option</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <c:forEach items="${lessonList}" var="lesson" >
                                                        <tr>
                                                            <td>${lesson.name}</td>
                                                            <td>${lesson.lessonNo}</td>
                                                            <c:forEach items="${materialList}" var="material" >
                                                                <c:if test ="${lesson.id == material.lessonId}">
                                                                    <td>${material.text}</td>
                                                                    <td>${material.link}</td>
                                                                </c:if>
                                                            </c:forEach>
                                                            <td style="text-align: right">
                                                                <a href="#" class="btn btn-outline-danger btn-rounded"
                                                                   onclick="confirmDeleteLesson(${lesson.id})"><i
                                                                        class="fas fa-trash"></i></a></td>
                                                        </tr>
                                                    </c:forEach>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-lg-12" style="padding-bottom: 20px">
                                <div class="card">
                                    <div class="card-header">Quiz</div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Quiz name</th>
                                                        <th style="text-align: right">Option</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <c:forEach items="${quizList}" var="quiz">
                                                        <tr>
                                                            <td><a href="viewquestion?go=viewQuiz&quizId=${quiz.id}&quizName=${quiz.name}&chapterId=${chapterData.chapterId}">${quiz.name}</a></td>
                                                            <td style="text-align: right">
                                                                <a href="#" class="btn btn-outline-danger btn-rounded" onclick="confirmDeleteQuiz(${quiz.id}, '${quiz.name}', ${chapterData.chapterId})">
                                                                    <i class="fas fa-trash"></i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    </c:forEach>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="addQuizModal" tabindex="-1" aria-labelledby="addQuizModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="addQuizModalLabel"><i class="fas fa-plus-circle"></i>&ensp;Add New Quiz</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form action="addquiz" method="POST">
                            <div class="mb-3">
                                <label for="quizName" class="form-label">Quiz Name<div style="color: red; display: inline;">*</div></label>
                                <textarea type="text" class="form-control" id="quizName" name="quiz-name" placeholder="Enter the quiz name" required></textarea>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <input hidden="" name="chapterId" value="${chapterData.chapterId}">
                                <button type="submit" class="btn btn-primary">Next&ensp;<i class="fas fa-arrow-right"></i></button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <div class="modal fade" id="confirmDeleteLessonModal" tabindex="-1" aria-labelledby="confirmDeleteModalLabel"
             aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="confirmDeleteModalLabel">Confirmation</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure you want to delete this LESSON?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                        <a id="deleteLink" href="#" class="btn btn-danger" onclick="deleteLessonItem()">Delete</a>
                    </div>

                </div>
            </div>
        </div>

        <div class="modal fade" id="confirmDeleteQuizModal" tabindex="-1" aria-labelledby="confirmDeleteModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="confirmDeleteModalLabel">Confirmation</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure you want to delete this QUIZ?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                        <a id="deleteLink" href="#" class="btn btn-danger" onclick="deleteQuizItem()">Delete</a>
                    </div>
                </div>
            </div>
        </div>
        <style>
            .btn-back {
                position: absolute;
                top: 15px;
                left: 15px;
                font-size: 20px;
                color: #07294D;
                text-decoration: none;
            }

            .btn-back:hover {
                text-decoration: underline;
            }
            a:hover {
            color: blue; 
            font-weight: bold; 
         
        }
        </style>                         
        <script>
            var deleteLessonItemId;
            var deleteQuizItemId;
            var deleteQuizItemName;
            var deleteQuizItemChapterId;


            function confirmDeleteLesson(itemId) {
                deleteLessonItemId = itemId;
                $('#confirmDeleteLessonModal').modal('show');
            }

            function deleteLessonItem() {
                var link = "addlesson?go=delete&lid=" + deleteLessonItemId;
                window.location.href = link;
                $('#confirmDeleteLessonModal').modal('hide');
            }
            
            function confirmDeleteQuiz(itemId, itemName, itemChapterId) {
                deleteQuizItemId = itemId;
                deleteQuizItemName = itemName;
                deleteQuizItemChapterId = itemChapterId;
                $('#confirmDeleteQuizModal').modal('show');
            }

            function deleteQuizItem() {
                
                var link = "viewquestion?go=delete&quizId=" + deleteQuizItemId + "&quizName=" + deleteQuizItemName + "&chapterId=" + deleteQuizItemChapterId;
                window.location.href = link;
                $('#confirmDeleteQuizModal').modal('hide');
            }

            
            $('#confirmDeleteQuizModal').on('hidden.bs.modal', function () {
                deleteQuizItemId = null;
            });

        </script>
        <script src="assets/vendor/jquery/jquery.min.js"></script>
        <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="assets/vendor/datatables/datatables.min.js"></script>
        <script src="assets/js/initiate-datatables.js"></script>
        <script src="assets/js/script.js"></script>
    </body>

</html>