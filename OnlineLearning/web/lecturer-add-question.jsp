<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="shortcut icon" href="images/favicon.png" type="image/png">
        <title>Add Quiz</title>

        <link href="assets/vendor/fontawesome/css/fontawesome.min.css" rel="stylesheet">
        <link href="assets/vendor/fontawesome/css/solid.min.css" rel="stylesheet">
        <link href="assets/vendor/fontawesome/css/brands.min.css" rel="stylesheet">
        <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/css/master.css" rel="stylesheet">
    </head>

    <body>
        <div class="wrapper">
            <jsp:include page="lecturer-sidebar.jsp"/>

            <div id="body" class="active">
                <!-- navbar navigation component -->
                <jsp:include page="lecturer-header.jsp"/>
                <!-- end of navbar navigation -->
                <div class="content">
                    <div class="container">
                        <div class="page-title">
                            <a href="addlesson?chapterId=${chapterData.chapterId}" class="btn-back hidden"><i class="fas fa-arrow-left"></i> Back</a>
                            <h3><i class="fas fa-plus-circle"></i>&ensp; Add Question</h3>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">

                                    <div class="card-body">

                                        <form id="addQuestionForm" action="addquestion" method="POST">
                                            <input hidden="" name="quizId" value="${requestScope.quizId}">
                                            <input hidden="" name="chapterId" value="${requestScope.chapterId}">
                                            <div id="questionsContainer">
                                                <div class="row g-2 question">
                                                    <div class="mb-3">
                                                        <label for="question" class="form-label"><b>Question</b></label>
                                                        <textarea type="text" class="form-control" name="question" placeholder="Enter question" rows="2" cols="30" required></textarea>
                                                    </div>
                                                    <div class="mb-3 col-md-6">
                                                        <label for="optionA" class="form-label"><b>Option A</b></label>
                                                        <textarea type="text" class="form-control" name="optionA" placeholder="A" rows="1" cols="30" required></textarea>
                                                    </div>
                                                    <div class="mb-3 col-md-6">
                                                        <label for="optionB" class="form-label"><b>Option B</b></label>
                                                        <textarea type="text" class="form-control" name="optionB" placeholder="B" rows="1" cols="30" required></textarea>
                                                    </div>
                                                    <div class="mb-3 col-md-6">
                                                        <label for="optionC" class="form-label"><b>Option C</b></label>
                                                        <textarea type="text" class="form-control" name="optionC" placeholder="C" rows="1" cols="30" required></textarea>
                                                    </div>
                                                    <div class="mb-3 col-md-6">
                                                        <label for="optionD" class="form-label"><b>Option D</b></label>
                                                        <textarea type="text" class="form-control" name="optionD" placeholder="D" rows="1" cols="30" required></textarea>
                                                    </div>
                                                    <div class="mb-3 row" style="padding:15px 0px 15px 0px">
                                                        <label class="col-sm-2" for="correctAnswer"><b>Correct Answer</b></label>
                                                        <div class="col-sm-10 select">
                                                            <select name="correctChoice" class="form-select">
                                                                <option value="A">Option A</option>
                                                                <option value="B">Option B</option>
                                                                <option value="C">Option C</option>
                                                                <option value="D">Option D</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-info"
                                                    id="addQuestionBtn"><i class="fas fa-plus-circle"></i> Add Another Question</button>
                                            &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                                            <button type="submit" class="btn btn-success"><i class="fas fa-check"></i>Add</button>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <style>
            .btn-back {
                position: absolute;
                top: 15px;
                left: 15px;
                font-size: 20px;
                color: #07294D;
                text-decoration: none;
            }

            .btn-back:hover {
                text-decoration: underline;
            }

        </style>
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script>
            $(document).ready(function () {
                $("#addQuestionBtn").click(function () {
                    var questionBlock = $(".question").first().clone(); // Sao chép khối câu hỏi đầu tiên
                    questionBlock.find("textarea, input").val(""); // Xóa giá trị đầu vào của textarea
                    $("#questionsContainer").append(questionBlock); // Thêm khối câu hỏi mới vào container
                });
            });

        </script>
        <script src="assets/vendor/jquery/jquery.min.js"></script>
        <script src="assets/js/form-validator.js"></script>
        <script src="assets/js/script.js"></script>

    </body>

</html>