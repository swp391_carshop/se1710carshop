<%-- 
    Document   : blog-list
    Created on : May 18, 2023, 8:27:21 PM
    Author     : LENOVO
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
    <head>
        <!--====== Required meta tags ======-->
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!--====== Title ======-->
        <title>Edubin - LMS Education</title>

        <!--====== Favicon Icon ======-->
        <link rel="shortcut icon" href="images/favicon.png" type="image/png">

        <!--====== Slick css ======-->
        <link rel="stylesheet" href="css/slick.css">

        <!--====== Animate css ======-->
        <link rel="stylesheet" href="css/animate.css">

        <!--====== Nice Select css ======-->
        <link rel="stylesheet" href="css/nice-select.css">

        <!--====== Nice Number css ======-->
        <link rel="stylesheet" href="css/jquery.nice-number.min.css">

        <!--====== Magnific Popup css ======-->
        <link rel="stylesheet" href="css/magnific-popup.css">

        <!--====== Bootstrap css ======-->
        <link rel="stylesheet" href="css/bootstrap.min.css">

        <!--====== Fontawesome css ======-->
        <link rel="stylesheet" href="css/font-awesome.min.css">

        <!--====== Default css ======-->
        <link rel="stylesheet" href="css/default.css">

        <!--====== Style css ======-->
        <link rel="stylesheet" href="css/style.css">

        <!--====== Responsive css ======-->
        <link rel="stylesheet" href="css/responsive.css">
    </head>

    <body>
        <!--====== HEADER PART START ======-->
        <jsp:include page="header.jsp"/>
        <!--====== HEADER PART ENDS ======-->

        <!--====== PAGE BANNER PART START ======-->

        <section id="page-banner" class="pt-105 pb-130 bg_cover" data-overlay="8" style="background-image: url(images/page-banner-4.jpg)">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-banner-cont">
                            <h2>Blogs</h2>
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="homepage">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Blogs</li>
                                </ol>
                            </nav>
                        </div>  <!-- page banner cont -->
                    </div>
                </div> <!-- row -->
            </div> <!-- container -->
        </section>

        <!--====== PAGE BANNER PART ENDS ======-->

        <!--====== BLOG PART START ======-->

        <section id="blog-page" class="pt-90 pb-120 gray-bg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <c:if test="${!blogs.isEmpty()}">
                            <!-- singel blog -->
                            <c:forEach var="blog" items="${blogs}">
                                <div class="singel-blog mt-30">
                                    <div class="blog-thum">
                                        <img src="images/blog/blog-post/${blog.image}" alt="Blog">
                                    </div>
                                    <div class="blog-cont">
                                        <a href="blogdetail?id=${blog.id}"><h3>${blog.title}</h3></a>
                                        <ul>
                                            <li><a href="#"><i class="fa fa-calendar"></i>${blog.created_at}</a></li>
                                        </ul>
                                        <p>${blog.description}</p>
                                    </div>
                                </div> 
                                <!-- singel blog end -->
                            </c:forEach>


                            <nav class="courses-pagination mt-50">
                                <ul class="pagination justify-content-lg-end justify-content-center">
                                    <c:if test="${index!=0}">
                                        <li class="page-item">
                                            <a href="blogs?index=${index-1}${(texts!=null)?'&search=${textS}':''}" aria-label="Previous">
                                                <i class="fa fa-angle-left"></i>
                                            </a>
                                        </li>
                                    </c:if>
                                    <c:forEach var="i" begin="0" end = "${totalPages-1}">
                                        <li class="page-item"><a ${index==i?"class='active'":""} href="blogs?index=${i}${(texts!=null)?'&search=${textS}':''}">${i+1}</a></li>
                                        </c:forEach>
                                        <c:if test="${index < totalPages-1}">
                                        <li class="page-item">
                                            <a href="blogs?index=${index+1}${(texts!=null)?'&search=${textS}':''}" aria-label="Next">
                                                <i class="fa fa-angle-right"></i>
                                            </a>
                                        </li>
                                    </c:if>
                                </ul>
                            </nav>  <!-- courses pagination -->
                        </c:if>
                        <c:if test="${blogs.isEmpty()}">
                                <a><h3>No Result Found</h3></a>
                        </c:if>
                    </div>

                    <!--blog sider -->
                    <jsp:include page="blog-sider.jsp"/>
                    <!--blog sider -->
                </div> <!-- row -->
            </div> <!-- container -->
        </section>

        <!--====== BLOG PART ENDS ======-->

        <!--====== FOOTER PART START ======-->
        <jsp:include page="footer.jsp"/>
        <!--====== FOOTER PART ENDS ======-->


        <!--====== jquery js ======-->
        <script src="js/vendor/modernizr-3.6.0.min.js"></script>
        <script src="js/vendor/jquery-1.12.4.min.js"></script>

        <!--====== Bootstrap js ======-->
        <script src="js/bootstrap.min.js"></script>

        <!--====== Slick js ======-->
        <script src="js/slick.min.js"></script>

        <!--====== Magnific Popup js ======-->
        <script src="js/jquery.magnific-popup.min.js"></script>

        <!--====== Counter Up js ======-->
        <script src="js/waypoints.min.js"></script>
        <script src="js/jquery.counterup.min.js"></script>

        <!--====== Nice Select js ======-->
        <script src="js/jquery.nice-select.min.js"></script>

        <!--====== Nice Number js ======-->
        <script src="js/jquery.nice-number.min.js"></script>

        <!--====== Count Down js ======-->
        <script src="js/jquery.countdown.min.js"></script>

        <!--====== Validator js ======-->
        <script src="js/validator.min.js"></script>

        <!--====== Ajax Contact js ======-->
        <script src="js/ajax-contact.js"></script>

        <!--====== Main js ======-->
        <script src="js/main.js"></script>

        <!--====== Map js ======-->
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDC3Ip9iVC0nIxC6V14CKLQ1HZNF_65qEQ"></script>
        <script src="js/map-script.js"></script>

    </body>
</html>

