<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" href="images/favicon.png" type="image/png">
    <title>Lecturer Dashboard</title>
    <link href="assets/vendor/fontawesome/css/fontawesome.min.css" rel="stylesheet">
    <link href="assets/vendor/fontawesome/css/solid.min.css" rel="stylesheet">
    <link href="assets/vendor/fontawesome/css/brands.min.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/master.css" rel="stylesheet">
    <link href="assets/vendor/flagiconcss/css/flag-icon.min.css" rel="stylesheet">
</head>

<body>
    <div class="wrapper">
        <jsp:include page="lecturer-sidebar.jsp"/>

            <div id="body" class="active">
                <!-- navbar navigation component -->
                <jsp:include page="lecturer-header.jsp"/>
            <!-- end of navbar navigation -->
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 page-header">
                            <h2 class="page-title"><i class="fas fa-graduation-cap"></i>&ensp;All Course</h2>
                        </div>
                    </div>
                    <div class="row">
                        <c:forEach items="${requestScope.listCourse}" var="i">
                        <div class="col-sm-6 col-md-6 col-lg-3 mt-3">
                            <div class="card">
                                <div class="content">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="icon-big text-center">
                                                <a href="<%=request.getContextPath()%>/addchapter?cid=${i.getCourseID()}"><i class="teal fas fa-graduation-cap"></i></a> 
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="detail">
                                                <span class="number" style="font-size: 20px; color: #07294D"><a href="<%=request.getContextPath()%>/addchapter?cid=${i.getCourseID()}">${i.getCourseName()}</a></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="footer">
                                        <hr />
                                        <div class="stats" >
                                                <a href="<%=request.getContextPath()%>/addchapter?cid=${i.getCourseID()}"><span style="color: #0074D9"><i class="fas fa-file-alt"></i>&nbsp;Description: </span> <span style="color: black">
                                                        <% int maxLength = 100; %>
                                                        <c:set var="content" value="${i.description}" />
                                                        <% if (((String)pageContext.getAttribute("content")).length() > maxLength) { %>
                                                        <%= ((String)pageContext.getAttribute("content")).substring(0, maxLength) + "..." %>
                                                        <% } else { %>
                                                        <%= pageContext.getAttribute("content") %>
                                                        <% } %></span></a>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </c:forEach>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="assets/vendor/jquery/jquery.min.js"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/vendor/chartsjs/Chart.min.js"></script>
    <script src="assets/js/dashboard-charts.js"></script>
    <script src="assets/js/script.js"></script>
</body>

</html>
