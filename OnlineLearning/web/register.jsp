<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">

    <head>

        <!--====== Required meta tags ======-->
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!--====== Title ======-->
        <title>Register</title>

        <!--====== Favicon Icon ======-->
        <link rel="shortcut icon" href="images/favicon.png" type="image/png">

        <!--====== Slick css ======-->
        <link rel="stylesheet" href="css/slick.css">

        <!--====== Animate css ======-->
        <link rel="stylesheet" href="css/animate.css">

        <!--====== Nice Select css ======-->
        <link rel="stylesheet" href="css/nice-select.css">

        <!--====== Nice Number css ======-->
        <link rel="stylesheet" href="css/jquery.nice-number.min.css">

        <!--====== Magnific Popup css ======-->
        <link rel="stylesheet" href="css/magnific-popup.css">

        <!--====== Bootstrap css ======-->
        <link rel="stylesheet" href="css/bootstrap.min.css">

        <!--====== Fontawesome css ======-->
        <link rel="stylesheet" href="css/font-awesome.min.css">

        <!--====== Default css ======-->
        <link rel="stylesheet" href="css/default.css">

        <!--====== Style css ======-->
        <link rel="stylesheet" href="css/style.css">

        <!--====== Responsive css ======-->
        <link rel="stylesheet" href="css/responsive.css">
    </head>

    <body>

        <jsp:include page="header.jsp"/>

        <section id="page-banner" class="pt-105 pb-110 bg_cover" data-overlay="8"
                 style="background-image: url(./images/page-banner-2.jpg)">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-banner-cont">
                            <h2>Register</h2>
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="homepage">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Register</li>
                                </ol>
                            </nav>
                        </div> <!-- page banner cont -->
                    </div>    
                </div> <!-- container -->
            </div> <!-- container -->
        </section>

        <!--====== PAGE BANNER PART ENDS ======-->

        <!--====== COURSES PART START ======-->

        <section id="contact-page" class="pt-90 pb-120 gray-bg">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-6">
                        <div class="contact-from mt-30">
                            <div class="section-title">
                                <h5>Register</h5>
                            </div> <!-- section title -->
                            <div style="color: red">${error}</div>
                            <div class="main-form pt-45">
                                <form id="contact-form" action="register" method="post" data-toggle="validator">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="singel-form form-group">
                                                <input type="text" name="fullname" placeholder="Enter your name" value="${fullnameError}" data-error="Please,enter your fullname." required="required">
                                                <div class="help-block with-errors"></div>
                                            </div> <!-- singel form -->
                                        </div>
                                        <div class="col-md-12">
                                            <div class="singel-form form-group">
                                                <input type="text" name="user" placeholder="Enter your username" value="${usernameError}" data-error="Please,enter your username." required="required">
                                                <div class="help-block with-errors"></div>
                                            </div> <!-- singel form -->
                                        </div>
                                        <div class="col-md-12">
                                            <div class="singel-form form-group">
                                                <input type="text" name="email" placeholder="Enter your email" value="${emailError}" data-error="Please,enter your gmail." required="required">
                                                <div class="help-block with-errors"></div>
                                            </div> <!-- singel form -->
                                        </div>
                                        <div class="col-md-12">
                                            <div class="singel-form form-group">
                                                <input type="tel" name="phone" placeholder="Enter your phone number" name="phone" value="${phoneNumberError}" data-error="Please,enter your phone number." required="required">
                                                <div class="help-block with-errors"></div>
                                            </div> <!-- singel form -->
                                        </div>
                                        <div class="col-md-12">
                                            <div class="singel-form form-group">
                                                <input type="password" name="pass" placeholder="Enter your password" data-error="Please,enter your password." required="required">
                                                <div class="help-block with-errors"></div>
                                            </div> <!-- singel form -->
                                        </div>
                                        <div class="col-md-12">
                                            <div class="singel-form form-group">
                                                <input type="password" name="repass" placeholder="Confirm your password" data-error="Please,confirm your password." required="required">
                                                <div class="help-block with-errors"></div>
                                            </div> <!-- singel form -->
                                        </div>
                                        <p class="form-message"></p>
                                        <div class="col-md-12">
                                            <div class="singel-form">
                                                <button type="submit" class="main-btn">Sign up</button>&emsp;&emsp;&emsp;
                                                <a href="login" class="forgot-password"><b>Login</b></a>
                                            </div> <!-- singel form -->
                                        </div> 
                                    </div> <!-- row -->
                                </form>
                            </div> <!-- main form -->
                        </div> <!--  contact from -->
                    </div>
                </div> <!-- row -->
            </div> <!-- container -->
        </section>

        <!--====== COURSES PART ENDS ======-->

        <!--====== FOOTER PART START ======-->
        <jsp:include page="footer.jsp"/>
        <!--====== FOOTER PART ENDS ======-->
        <style>
            a.forgot-password {
                color: #07294D;
            }


            a.forgot-password:hover {
                color: #FFC600;

            }
        </style>
        <!--====== BACK TO TP PART START ======-->

        <a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>

        <!--====== BACK TO TP PART ENDS ======-->

        <!--====== jquery js ======-->
        <script src="js/vendor/modernizr-3.6.0.min.js"></script>
        <script src="js/vendor/jquery-1.12.4.min.js"></script>

        <!--====== Bootstrap js ======-->
        <script src="js/bootstrap.min.js"></script>

        <!--====== Slick js ======-->
        <script src="js/slick.min.js"></script>

        <!--====== Magnific Popup js ======-->
        <script src="js/jquery.magnific-popup.min.js"></script>

        <!--====== Counter Up js ======-->
        <script src="js/waypoints.min.js"></script>
        <script src="js/jquery.counterup.min.js"></script>

        <!--====== Nice Select js ======-->
        <script src="js/jquery.nice-select.min.js"></script>

        <!--====== Nice Number js ======-->
        <script src="js/jquery.nice-number.min.js"></script>

        <!--====== Count Down js ======-->
        <script src="js/jquery.countdown.min.js"></script>

        <!--====== Validator js ======-->
        <script src="js/validator.min.js"></script>

        <!--====== Main js ======-->
        <script src="js/main.js"></script>

        <!--====== Map js ======-->
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDC3Ip9iVC0nIxC6V14CKLQ1HZNF_65qEQ"></script>
        <script src="js/map-script.js"></script>

    </body>
