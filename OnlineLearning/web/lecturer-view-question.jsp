<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="shortcut icon" href="images/favicon.png" type="image/png">
        <title>View Lesson</title>
        <link href="assets/vendor/fontawesome/css/fontawesome.min.css" rel="stylesheet">
        <link href="assets/vendor/fontawesome/css/solid.min.css" rel="stylesheet">
        <link href="assets/vendor/fontawesome/css/brands.min.css" rel="stylesheet">
        <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/vendor/datatables/datatables.min.css" rel="stylesheet">
        <link href="assets/css/master.css" rel="stylesheet">
    </head>

    <body>
        <div class="wrapper">
            <jsp:include page="lecturer-sidebar.jsp"/>

            <div id="body" class="active">
                <!-- navbar navigation component -->
                <jsp:include page="lecturer-header.jsp"/>
                <!-- end of navbar navigation -->
                <div class="content">
                    <div class="container">
                        <div class="page-title">
                            <a href="addlesson?chapterId=${requestScope.chapterId}" class="btn-back hidden"><i class="fas fa-arrow-left"></i>Back</a>
                            <h3>
                                <i class="fas fa-question-circle"></i>&ensp;Question List

                            </h3>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-lg-12" style="padding-bottom: 20px">
                                <div class="card">
                                    <div class="card-header">Question</div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Question</th>
                                                        <th>Option A</th>
                                                        <th>Option B</th>
                                                        <th>Option C</th>
                                                        <th>Option D</th>
                                                        <th style="color: black"><b>Answer</b></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <c:forEach items="${requestScope.listQuestion}" var="i">
                                                        <tr>
                                                            <td>${i.getQuestion()}</td>
                                                            <td>${i.getOptionA()}</td>
                                                            <td>${i.getOptionB()}</td>
                                                            <td>${i.getOptionC()}</td>
                                                            <td>${i.getOptionD()}</td>
                                                            <td style="color: green"><b>${i.getAnswer()}</b></td>
                                                        </tr>
                                                    </c:forEach>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <style>
            .btn-back {
                position: absolute;
                top: 15px;
                left: 15px;
                font-size: 20px;
                color: #07294D;
                text-decoration: none;
            }

            .btn-back:hover {
                text-decoration: underline;
            }

        </style>                         
       
        <script src="assets/vendor/jquery/jquery.min.js"></script>
        <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="assets/vendor/datatables/datatables.min.js"></script>
        <script src="assets/js/initiate-datatables.js"></script>
        <script src="assets/js/script.js"></script>
    </body>

</html>