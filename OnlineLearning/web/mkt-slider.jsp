<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<!-- 
* Bootstrap Simple Admin Template
* Version: 2.1
* Author: Alexis Luna
* Website: https://github.com/alexis-luna/bootstrap-simple-admin-template
-->
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="shortcut icon" href="images/favicon.png" type="image/png">
        <title>Slider Manager</title>
        <link href="assets/vendor/fontawesome/css/fontawesome.min.css" rel="stylesheet">
        <link href="assets/vendor/fontawesome/css/solid.min.css" rel="stylesheet">
        <link href="assets/vendor/fontawesome/css/brands.min.css" rel="stylesheet">
        <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/vendor/datatables/datatables.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link href="assets/css/master.css" rel="stylesheet">
    </head>

    <body>
        <div class="wrapper">
            <jsp:include page="mkt-sidebar.jsp"/>

            <div id="body" class="active">
                <!-- navbar navigation component -->
                <jsp:include page="mkt-header.jsp"/>
                <!-- end of navbar navigation -->
                <div class="content">
                    <div class="container">
                        <div class="page-title d-flex justify-content-between">
                            <h3>Slider Manager</h3>
                            <div class="d-flex align-items-center">
                                <form action="searchslider" method="post" class="form-inline" style="margin-right: 200px">
                                    <div class="input-group">
                                        <input value="${txtS}" name="txt" type="text" class="form-control" placeholder="Search...." aria-describedby="basic-addon2">
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-secondary" type="submit">Search</button>
                                        </div>
                                    </div>
                                </form>
                                <span class="float-end">
                                    <span class="sort-text" style="color: #07294D; font-size: 18px; font-weight: bold;">ADD: </span>
                                    <a href="add-slider.jsp" style="background-color: white; color: #07294D; font-size: 15px; padding: 5px 10px; margin-left: 10px; border: 1px solid #AAAAAA; border-radius: 3px;"
                                       onmousedown="this.style.backgroundColor = 'blue'; this.style.color = 'white';"
                                       onmouseup="this.style.backgroundColor = 'white'; this.style.color = '#07294D';"
                                       onmouseout="this.style.backgroundColor = 'white'; this.style.color = '#07294D';"
                                       onmouseover="this.style.backgroundColor = '#FFC600'; this.style.color = '#07294D';"
                                       class="btn btn-sm btn-outline-warning"><i class="fas fa-plus-circle"></i>&ensp; ADD SLIDER</a>
                                </span>
                            </div>
                        </div>
                        <div class="box box-primary">
                            <div class="box-body">
                                <table width="100%" class="table table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Title</th>
                                            <th>Description</th>
                                            <th>Image</th>
                                            <th>Link</th>
                                            <th>Status</th>
                                            <th>Option</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${requestScope.allSlider}" var="s">
                                            <tr>
                                                <td>${s.getId()}</td>
                                                <td>${s.getTitle()}</td>
                                                <td>${s.getDescription()}</td>
                                                <td><img style="width: 110px;height: 67px; object-fit: cover;border: 1px solid #fff;" src="images/slider/${s.getImage()}"></td>
                                                <td>${s.getLink()}</td>
                                                <td>
                                                    <c:if test="${s.getStatus() == 1}">
                                                        <i class="fas fa-eye" style="color: #0D6EFD;"></i>
                                                    </c:if>
                                                    <c:if test="${s.getStatus() == 0}">
                                                        <i class="fas fa-eye-slash"></i>
                                                    </c:if>
                                                </td>
                                                <td class="text-end">
                                                    <a href="loadslider?sid=${s.getId()}" name="editslider" class="btn btn-outline-info btn-rounded"><i class="fas fa-pen"></i></a>
                                                   <a href="#" class="btn btn-outline-danger btn-rounded"
                                                                   onclick="confirmDeleteSlider(${s.id})"><i
                                                                        class="fas fa-trash"></i></a>
                                                     <!--<a href="deleteslider?sid=${s.getId()}" name="deleteslider" class="btn btn-outline-danger btn-rounded"><i class="fas fa-trash"></i></a>
                                              -->  </td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                                <div class="pagination-wrapper">
                                    <ul class="pagination">
                                        <c:if test="${tag>1}">
                                            <li class="page-item"><a href="manageslider?index=${tag-1}" class="page-link"><i class="fas fa-angle-left"></i></a></li>
                                                </c:if>
                                                <c:forEach begin="1" end="${endP}" var="i">
                                            <li class="${tag == i?"active":""}"><a href="manageslider?index=${i}"><b>${i}</b></a></li>
                                                    </c:forEach>
                                                    <c:if test="${tag < endP}">
                                            <li class="page-item"><a href="manageslider?index=${tag+1}" class="page-link"><i class="fas fa-angle-right"></i></a></li>
                                                </c:if>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="confirmDeleteSliderModal" tabindex="-1" aria-labelledby="confirmDeleteModalLabel"
             aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="confirmDeleteModalLabel">Confirmation</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure you want to delete this SLIDER?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                        <a id="deleteLink" href="#" class="btn btn-danger" onclick="deleteSliderItem()">Delete</a>
                    </div>

                </div>
            </div>
        </div>
        <style>
            .pagination li {
                display: inline-block;
            }

            .pagination li a {
                display: block;
                padding: 5px 10px;
                text-decoration: none;
                color: #07294D;
            }

            .pagination li a:hover {
                background-color: #FFC600;
            }

            ul.pagination li.active a {
                background-color: #FFC600;
            }

            .pagination-wrapper {
                display: flex;
                justify-content: center;
                margin-top: 20px;
            }

        </style>
        <script>
            var deleteSliderItemId;


            function confirmDeleteSlider(itemId) {
                deleteSliderItemId = itemId;
                $('#confirmDeleteSliderModal').modal('show');
            }

            function deleteSliderItem() {
                var link = "deleteslider?sid=" + deleteSliderItemId;
                window.location.href = link;
                $('#confirmDeleteSliderModal').modal('hide');
            }


        </script>                                

        <!--
        <script src="assets/vendor/datatables/datatables.min.js"></script>
        <script src="assets/js/initiate-datatables.js"></script>-->
        <script src="assets/vendor/jquery/jquery.min.js"></script>
        <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="assets/js/script.js"></script>
    </body>

</html>