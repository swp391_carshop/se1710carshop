<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">

        <link rel="shortcut icon" href="images/favicon.png" type="image/png">
        <title>View User Detail</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
         <link rel="stylesheet" href="css/style.css">
         <link rel="stylesheet" href="css/default.css">
        
    </head>
    <body>
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
        <div class="header-top d-none d-lg-block">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="header-contact text-lg-left text-center">
                        <ul>
                            <li style="padding-right: 100px"><img src="images/all-icon/map.png" alt="icon"><span>Hoa Lac Hi-tech Park, Thach Hoa, Thach That, Ha Noi</span></li>
                            <li style="padding-right: 320px"><img src="images/all-icon/email.png" alt="icon"><span>anhtthe161427@fpt.edu.vn</span></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="header-opening-time text-lg-right text-center">
<!--                        <p>Opening Hours : Monday to Saturay - 8 Am to 5 Pm</p>-->
                    </div>
                </div>
            </div> <!-- row -->
        </div> <!-- container -->
    </div>
        
        <div class="header-logo-support pt-30 pb-30">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4">
                    <div class="logo">
                        <a href="homepage">
                            <img src="images/logo.png" alt="Logo">
                        </a>
                    </div>
                </div>
            </div> <!-- row -->
        </div> <!-- container -->
    </div> 
        <div class="container bootstrap snippets bootdey">
            <div class="row">
                <div class="profile-nav col-md-3">
                    <a href="viewuser" class="btn-back">
                        <i class="fa fa-arrow-left"></i> Back
                    </a>
                    <div class="panel">
                        <div class="user-heading round">
                            <a href="#">
                                <img src="https://bootdey.com/img/Content/avatar/avatar3.png" alt>
                            </a>
                            <h1>${requestScope.userDetail.getFullname()}</h1>
                            <p><a href="" style="color: white" class="__cf_email__" data-cfemail="e682839f82839fa6928e83a38b878f8ac885898b">${requestScope.userDetail.getEmail()}</a></p>
                        </div>
                        <ul class="nav nav-pills nav-stacked">
                            <li class="active"><a href="#"> <i class="fa fa-user"></i> ${requestScope.userDetail.getUsername()}</a></li>
                            <li><a href="#"> <i class="fa fa-calendar"></i> Recent Activity <span class="label label-warning pull-right r-activity">9</span></a></li>
                        </ul>
                    </div>
                </div>
                <div class="profile-info col-md-9">

                    <div class="panel">
                        <div class="bio-graph-heading">
                            <b style="font-size: 30px;">EDUBIN!</b><br>
                            <b>"Exploring Boundless Knowledge: Your Gateway to Online Learning"</b>
                        </div>
                        <div class="panel-body bio-graph-info">
                            <span style="font-size: 20px; color: #000000; padding-bottom: 15px">Personal Information</span>
                            <form action="editslider" method="post"> 
                                <div class="row">
                                    <div class="bio-row">
                                        <p style="color: #000000;"><span>Full Name </span>${requestScope.userDetail.getFullname()}</p>
                                    </div>
                                    <div class="bio-row">
                                        <p style="color: #000000;"><span>Username </span>${requestScope.userDetail.getUsername()}</p>
                                    </div>
                                    <div class="bio-row">
                                        <p style="color: #000000;"><span>Email </span>${requestScope.userDetail.getEmail()}</p>
                                    </div>
                                    <div class="bio-row">
                                        <p style="color: #000000;"><span>Phone</span>${requestScope.userDetail.getPhone()}</p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                                    <div>
                                        <b style="font-size: 18px; color: #000000">Courses attended</b>
                                    </div>                
                    <div>
                        <div class="row">
                            <c:forEach items="${requestScope.courseUserDetail}" var="c">
                                <div class="col-md-7">
                                    <div class="panel">
                                        <div class="panel-body">
                                            <div class="bio-desk">
                                                <h4 class="red"><span style="color: #FF4136"><b>${c.getNameCourse()}</b></span></h4>
                                                <p><span style="color: #0074D9"><b>Description : </b></span><span style="color: black">${c.getDescription()}</span></p>
                                                <p><span style="color: #0074D9"><b>Started : </b></span><span style="color: black">${c.getEnrollmentDate()}</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         <jsp:include page="footer.jsp"/>   
         <style type="text/css">
            body {
                color: #797979;
                background: #f1f2f7;
                font-family: 'Open Sans', sans-serif;
                padding: 0px !important;
                margin: 0px !important;
                font-size: 13px;
                text-rendering: optimizeLegibility;
                -webkit-font-smoothing: antialiased;
                -moz-font-smoothing: antialiased;
            }

            .profile-nav, .profile-info{
                margin-top:30px;
            }

            .profile-nav .user-heading {
                background: #07294D;
                color: #fff;
                border-radius: 4px 4px 0 0;
                -webkit-border-radius: 4px 4px 0 0;
                padding: 30px;
                text-align: center;
            }

            .profile-nav .user-heading.round a  {
                border-radius: 50%;
                -webkit-border-radius: 50%;
                border: 10px solid rgba(255,255,255,0.3);
                display: inline-block;
            }

            .profile-nav .user-heading a img {
                width: 112px;
                height: 112px;
                border-radius: 50%;
                -webkit-border-radius: 50%;
            }

            .profile-nav .user-heading h1 {
                font-size: 22px;
                font-weight: 300;
                margin-bottom: 5px;
            }

            .profile-nav .user-heading p {
                font-size: 12px;
            }

            .profile-nav ul {
                margin-top: 1px;
            }

            .profile-nav ul > li {
                border-bottom: 1px solid #ebeae6;
                margin-top: 0;
                line-height: 30px;
            }

            .profile-nav ul > li:last-child {
                border-bottom: none;
            }

            .profile-nav ul > li > a {
                border-radius: 0;
                -webkit-border-radius: 0;
                color: black;
                border-left: 5px solid #fff;
            }

            .profile-nav ul > li > a:hover, .profile-nav ul > li > a:focus, .profile-nav ul li.active  a {
                background: #f8f7f5 !important;
                border-left: 5px solid #07294D;
                color: black !important;
            }

            .profile-nav ul > li:last-child > a:last-child {
                border-radius: 0 0 4px 4px;
                -webkit-border-radius: 0 0 4px 4px;
            }

            .profile-nav ul > li > a > i{
                font-size: 16px;
                padding-right: 10px;
                color: #bcb3aa;
            }

            .r-activity {
                margin: 6px 0 0;
                font-size: 12px;
            }


            .p-text-area, .p-text-area:focus {
                border: none;
                font-weight: 300;
                box-shadow: none;
                color: #c3c3c3;
                font-size: 16px;
            }

            .profile-info .panel-footer {
                background-color:#f8f7f5 ;
                border-top: 1px solid #e7ebee;
            }

            .profile-info .panel-footer ul li a {
                color: #7a7a7a;
            }

            .bio-graph-heading {
                background: #07294D;
                color: #fff;
                text-align: center;
                font-style: italic;
                padding: 40px 110px;
                border-radius: 4px 4px 0 0;
                -webkit-border-radius: 4px 4px 0 0;
                font-size: 16px;
                font-weight: 300;
            }

            .bio-graph-info {
                color: black;
            }

            .bio-graph-info h1 {
                font-size: 22px;
                font-weight: 300;
                margin: 0 0 20px;
            }

            .bio-row {
                width: 50%;
                float: left;
                margin-bottom: 10px;
                padding:0 15px;
            }

            .bio-row p span {
                width: 100px;
                display: inline-block;
            }

            .bio-chart, .bio-desk {
                float: left;
            }

            .bio-chart {
                width: 40%;
            }

            .bio-desk {
                width: 60%;
            }

            .bio-desk h4 {
                font-size: 15px;
                font-weight:400;
            }

            .bio-desk h4.terques {
                color: #4CC5CD;
            }

            .bio-desk h4.red {
                color: #e26b7f;
            }

            .bio-desk h4.green {
                color: #97be4b;
            }

            .bio-desk h4.purple {
                color: #caa3da;
            }

            .file-pos {
                margin: 6px 0 10px 0;
            }

            .profile-activity h5 {
                font-weight: 300;
                margin-top: 0;
                color: #c3c3c3;
            }

            .summary-head {
                background: #ee7272;
                color: #fff;
                text-align: center;
                border-bottom: 1px solid #ee7272;
            }

            .summary-head h4 {
                font-weight: 300;
                text-transform: uppercase;
                margin-bottom: 5px;
            }

            .summary-head p {
                color: rgba(255,255,255,0.6);
            }

            ul.summary-list {
                display: inline-block;
                padding-left:0 ;
                width: 100%;
                margin-bottom: 0;
            }

            ul.summary-list > li {
                display: inline-block;
                width: 19.5%;
                text-align: center;
            }

            ul.summary-list > li > a > i {
                display:block;
                font-size: 18px;
                padding-bottom: 5px;
            }

            ul.summary-list > li > a {
                padding: 10px 0;
                display: inline-block;
                color: #818181;
            }

            ul.summary-list > li  {
                border-right: 1px solid #eaeaea;
            }

            ul.summary-list > li:last-child  {
                border-right: none;
            }

            .activity {
                width: 100%;
                float: left;
                margin-bottom: 10px;
            }

            .activity.alt {
                width: 100%;
                float: right;
                margin-bottom: 10px;
            }

            .activity span {
                float: left;
            }

            .activity.alt span {
                float: right;
            }
            .activity span, .activity.alt span {
                width: 45px;
                height: 45px;
                line-height: 45px;
                border-radius: 50%;
                -webkit-border-radius: 50%;
                background: #eee;
                text-align: center;
                color: #fff;
                font-size: 16px;
            }

            .activity.terques span {
                background: #8dd7d6;
            }

            .activity.terques h4 {
                color: #8dd7d6;
            }
            .activity.purple span {
                background: #b984dc;
            }

            .activity.purple h4 {
                color: #b984dc;
            }
            .activity.blue span {
                background: #90b4e6;
            }

            .activity.blue h4 {
                color: #90b4e6;
            }
            .activity.green span {
                background: #aec785;
            }

            .activity.green h4 {
                color: #aec785;
            }

            .activity h4 {
                margin-top:0 ;
                font-size: 16px;
            }

            .activity p {
                margin-bottom: 0;
                font-size: 13px;
            }

            .activity .activity-desk i, .activity.alt .activity-desk i {
                float: left;
                font-size: 18px;
                margin-right: 10px;
                color: #bebebe;
            }

            .activity .activity-desk {
                margin-left: 70px;
                position: relative;
            }

            .activity.alt .activity-desk {
                margin-right: 70px;
                position: relative;
            }

            .activity.alt .activity-desk .panel {
                float: right;
                position: relative;
            }

            .activity-desk .panel {
                background: #F4F4F4 ;
                display: inline-block;
            }


            .activity .activity-desk .arrow {
                border-right: 8px solid #F4F4F4 !important;
            }
            .activity .activity-desk .arrow {
                border-bottom: 8px solid transparent;
                border-top: 8px solid transparent;
                display: block;
                height: 0;
                left: -7px;
                position: absolute;
                top: 13px;
                width: 0;
            }

            .activity-desk .arrow-alt {
                border-left: 8px solid #F4F4F4 !important;
            }

            .activity-desk .arrow-alt {
                border-bottom: 8px solid transparent;
                border-top: 8px solid transparent;
                display: block;
                height: 0;
                right: -7px;
                position: absolute;
                top: 13px;
                width: 0;
            }

            .activity-desk .album {
                display: inline-block;
                margin-top: 10px;
            }

            .activity-desk .album a{
                margin-right: 10px;
            }

            .activity-desk .album a:last-child{
                margin-right: 0px;
            }

            .btn-back {
                position: fixed;
                top: 75px;
                left: 20px;
                font-size: 16px;
                color: #555;
                text-decoration: none;
            }

            .btn-back i {
                margin-right: 5px;
            }

        </style>
        <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
        <script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <script type="text/javascript">

        </script>
    </body>
</html>