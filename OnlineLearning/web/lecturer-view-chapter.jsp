<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="shortcut icon" href="images/favicon.png" type="image/png">
        <title>View Chapter</title>
        <link href="assets/vendor/fontawesome/css/fontawesome.min.css" rel="stylesheet">
        <link href="assets/vendor/fontawesome/css/solid.min.css" rel="stylesheet">
        <link href="assets/vendor/fontawesome/css/brands.min.css" rel="stylesheet">
        <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/css/master.css" rel="stylesheet">
        <link href="assets/vendor/flagiconcss/css/flag-icon.min.css" rel="stylesheet">
    </head>

    <body>
        <div class="wrapper">
            <jsp:include page="lecturer-sidebar.jsp"/>

            <div id="body" class="active">
                <!-- navbar navigation component -->
                <jsp:include page="lecturer-header.jsp"/>
                <!-- end of navbar navigation -->
                <div class="content">

                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 page-header">
                                <a href="loaddatalecturer" class="btn-back hidden"><i class="fas fa-arrow-left"></i> Back</a>
                                <h2 class="page-title"><i class="fas fa-book"></i>&ensp;All Chapter of Course</h2>
                                <span class="float-end">
                                    <span class="sort-text" style="color: #07294D; font-size: 18px; font-weight: bold;">ADD: </span>
                                    <a href="addchapter?go=add&cid=${requestScope.cid}"" style="background-color: white; color: #07294D; font-size: 12px; padding: 5px 10px; margin-left: 10px; border: 1px solid #AAAAAA; border-radius: 3px;"
                                       onmousedown="this.style.backgroundColor = 'blue'; this.style.color = 'white';"
                                       onmouseup="this.style.backgroundColor = 'white'; this.style.color = '#07294D';"
                                       onmouseout="this.style.backgroundColor = 'white'; this.style.color = '#07294D';"
                                       onmouseover="this.style.backgroundColor = '#FFC600'; this.style.color = '#07294D';"
                                       ><i class="fas fa-plus-circle"></i>&ensp; ADD CHAPTER</a>
                                </span> 
                            </div>
                        </div>
                        <div class="row">
                            <c:forEach items="${requestScope.listChapter}" var="i">
                                <div class="col-sm-6 col-md-6 col-lg-6 mt-3">
                                    <div class="card">
                                        <div class="content">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="icon-big text-center">
                                                        <a href="<%=request.getContextPath()%>/addlesson?chapterId=${i.getChapterId()}"><i class="orange fas fa-book"></i></a> 
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="detail" style="padding-bottom: 5px">
                                                        <span class="number" style="font-size: 20px; color: #07294D"><a href="<%=request.getContextPath()%>/addlesson?chapterId=${i.getChapterId()}">Chapter ${i.getChapterNo()}</a></span>
                                                    </div>
                                                    <div class="stats" >
                                                        <a href="<%=request.getContextPath()%>/addlesson?chapterId=${i.getChapterId()}"><span style="color: black">${i.getChapterName()}</span></a>  
                                                    </div>                                                       
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="icon-big text-center">
                                                        <a href="#" class="btn btn-outline-danger btn-rounded"
                                                           onclick="confirmDeleteChapter(${i.getChapterId()})"><i
                                                                class="fas fa-trash"></i>
                                                        </a>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>

                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!<!-- Confirm delete chapter form -->  
        <div class="modal fade" id="confirmDeleteChapterModal" tabindex="-1" aria-labelledby="confirmDeleteModalLabel"
             aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="confirmDeleteModalLabel">Confirmation</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure you want to delete this CHAPTER?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                        <a id="deleteLink" href="#" class="btn btn-danger" onclick="deleteChapter()">Delete</a>
                    </div>
                </div>
            </div>
        </div>

        <style>
            .btn-back {
                position: absolute;
                top: 15px;
                left: 15px;
                font-size: 20px;
                color: #07294D;
                text-decoration: none;
            }

            .btn-back:hover {
                text-decoration: underline;
            }

        </style>
                
        <script>
            var deleteChapterId;


            function confirmDeleteChapter(itemId) {
                deleteChapterId = itemId;
                $('#confirmDeleteChapterModal').modal('show');
            }

            function deleteChapter() {
                var link = "addchapter?go=delete&cid=${requestScope.cid}&chapterId=" + deleteChapterId;
                window.location.href = link;
                deleteChapterId = null;
                $('#confirmDeleteLessonModal').modal('hide');
            }

        </script>
        <script src="assets/vendor/jquery/jquery.min.js"></script>
        <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="assets/vendor/chartsjs/Chart.min.js"></script>
        <script src="assets/js/dashboard-charts.js"></script>
        <script src="assets/js/script.js"></script>
    </body>

</html>
