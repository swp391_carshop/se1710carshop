<%-- 
    Document   : show-detail
    Created on : Jun 24, 2023, 11:54:01 PM
    Author     : Admin MSI
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <title>Detail Quiz</title>
        <style>
            .correct {
                color: green;
                font-weight: 700;
            }

            .incorrect {
                color: red;
                font-weight: 700;
            }
        </style>
    </head>
    <body>
        this is show quiz correct 
        <%
int count=0;
        %>

        <c:forEach items="${sessionScope.listQuestions}" var="i">
            <div class="panel-body row" style="margin-left:10px">
                <input type="hidden" name="idQuestion" value="${i.getId()}">
                <!-- Hiển thị các câu hỏi -->
                <h5 style="color: black">Question:  <%=++count%> ${i.getQuestion()}</h5>

                <!-- Display answer A -->
                <div class="radio col-md-12">
                    <label>
                        <input type="radio" name="${i.getId()}" id="rdOptionsA" value="${i.getOptionA()}"
                               <c:forEach items="${sessionScope.userAnswers}" var="u">
                                   <c:if test="${u == i.getOptionA()}">
                                       checked="checked"
                                   </c:if>
                               </c:forEach>
                               <c:if test="${not empty sessionScope.userAnswers}">disabled</c:if>
                               />
                        A: ${i.getOptionA()}
                        <c:if test="${i.getOptionA().equals(i.getAnswer()) && sessionScope.userAnswers.contains(i.getOptionA())}">
                            <br> <span class="correct">Correct</span>
                        </c:if>
                        <c:if test="${not i.getOptionA().equals(i.getAnswer()) && sessionScope.userAnswers.contains(i.getOptionA())}">
                            <br><span class="incorrect">Incorrect</span>
                        </c:if>
                    </label><br>
                </div>

                <!-- Display answer B -->
                <div class="radio col-md-12">
                    <label>
                        <input type="radio" name="${i.getId()}" id="rdOptionsB" value="${i.getOptionB()}"
                               <c:forEach items="${sessionScope.userAnswers}" var="u">
                                   <c:if test="${u == i.getOptionB()}">
                                       checked="checked"
                                   </c:if>
                               </c:forEach>
                               <c:if test="${not empty sessionScope.userAnswers}">disabled</c:if>
                               />
                        B: ${i.getOptionB()}
                        <c:if test="${i.getOptionB().equals(i.getAnswer()) && sessionScope.userAnswers.contains(i.getOptionB())}">
                            <br><span class="correct">Correct</span>
                        </c:if>
                        <c:if test="${not i.getOptionB().equals(i.getAnswer()) && sessionScope.userAnswers.contains(i.getOptionB())}">
                            <br><span class="incorrect">Incorrect</span>
                        </c:if>
                    </label><br>
                </div>

                <!-- Display answer C -->
                <div class="radio col-md-12">
                    <label>
                        <input type="radio" name="${i.getId()}" id="rdOptionsC" value="${i.getOptionC()}"
                               <c:forEach items="${sessionScope.userAnswers}" var="u">
                                   <c:if test="${u == i.getOptionC()}">
                                       checked="checked"
                                   </c:if>
                               </c:forEach>
                                <c:if test="${not empty sessionScope.userAnswers}">disabled</c:if>
                               />
                        C: ${i.getOptionC()}
                        <c:if test="${i.getOptionC().equals(i.getAnswer()) && sessionScope.userAnswers.contains(i.getOptionC())}">
                            <br><span class="correct">Correct</span>
                        </c:if>
                        <c:if test="${not i.getOptionC().equals(i.getAnswer()) && sessionScope.userAnswers.contains(i.getOptionC())}">
                            <br><span class="incorrect">Incorrect</span>
                        </c:if>
                    </label><br>
                </div>

                <!-- Display answer D -->
                <div class="radio col-md-12">
                    <label>
                        <input type="radio" name="${i.getId()}" id="rdOptionsD" value="${i.getOptionD()}"
                               <c:forEach items="${sessionScope.userAnswers}" var="u">
                                   <c:if test="${u == i.getOptionD()}">
                                       checked="checked"
                                   </c:if>
                               </c:forEach>
                                <c:if test="${not empty sessionScope.userAnswers}">disabled</c:if>
                               />
                        D: ${i.getOptionD()}
                        <c:if test="${i.getOptionD().equals(i.getAnswer()) && sessionScope.userAnswers.contains(i.getOptionD())}">
                            <br><span class="correct">Correct</span>
                        </c:if>
                        <c:if test="${not i.getOptionD().equals(i.getAnswer()) && sessionScope.userAnswers.contains(i.getOptionD())}">
                            <br><span class="incorrect">Incorrect</span>
                        </c:if>
                    </label><br>
                </div>

                <br>
            </div>
        </c:forEach>
        <button type="submit"><a href="<%=request.getContextPath()%>/quizlist">Back to quiz list</a></button>
    </body>
</html>
