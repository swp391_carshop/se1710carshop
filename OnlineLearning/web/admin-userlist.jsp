<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<!-- 
* Bootstrap Simple Admin Template
* Version: 2.1
* Author: Alexis Luna
* Website: https://github.com/alexis-luna/bootstrap-simple-admin-template
-->
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="shortcut icon" href="images/favicon.png" type="image/png">
        <title>User List</title>
        <link href="assets/vendor/fontawesome/css/fontawesome.min.css" rel="stylesheet">
        <link href="assets/vendor/fontawesome/css/solid.min.css" rel="stylesheet">
        <link href="assets/vendor/fontawesome/css/brands.min.css" rel="stylesheet">
        <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/vendor/datatables/datatables.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link href="assets/css/master.css" rel="stylesheet">
    </head>

    <body>
        <div class="wrapper">
            <jsp:include page="admin-sidebar.jsp"/>

            <div id="body" class="active">
                <!-- navbar navigation component -->
                <jsp:include page="admin-header.jsp"/>
                <!-- end of navbar navigation -->
                <div class="content">
                    <div class="container">
                        <div class="page-title">
                            <h3><i class="fas fa-user-graduate"></i>&ensp;User List
                                <span class="float-end">
                                    <span class="sort-text" style="color: #07294D; font-size: 18px; font-weight: bold;">ADD: </span>
                                    <a href="adminuser?go=add" style="background-color: white; color: #07294D; font-size: 15px; padding: 5px 10px; margin-left: 10px; border: 1px solid #AAAAAA; border-radius: 3px;"
                                       onmousedown="this.style.backgroundColor = 'blue'; this.style.color = 'white';"
                                       onmouseup="this.style.backgroundColor = 'white'; this.style.color = '#07294D';"
                                       onmouseout="this.style.backgroundColor = 'white'; this.style.color = '#07294D';"
                                       onmouseover="this.style.backgroundColor = '#FFC600'; this.style.color = '#07294D';"
                                       class="btn btn-sm btn-outline-warning"><i class="fas fa-plus-circle"></i>&ensp; ADD NEW USER</a>
                                </span>
                                <span class="float-end">
                                    <form class="float-end" action="adminuser" method="GET">
                                        <input type="hidden" name="go" value="search">
                                        <input type="text" class="form-control" placeholder="Search Username"
                                               name="username">
                                    </form>
                                </span>   

                            </h3>
                        </div>
                        <div class="box box-primary">
                            <div class="box-body">
                                <table width="100%" class="table table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Full Name</th>
                                            <th>Username</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>Role</th>
                                            <th>Delete</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${userList}" var="a">
                                            <tr>
                                                <td>${a.id}</td>
                                                <td>${a.fullname}</td>
                                                <td>${a.username}</td>
                                                <td>${a.email}</td>
                                                <td>${a.phone}</td>
                                                <c:if  test = "${a.role == 1}">
                                                    <td>Admin</td>
                                                </c:if>
                                                <c:if  test = "${a.role == 2}">
                                                    <td>Marketer</td>
                                                </c:if>
                                                <c:if  test = "${a.role == 3}">
                                                    <td>Sale</td>
                                                </c:if>
                                                <c:if  test = "${a.role == 4}">
                                                    <td>Lecturer</td>
                                                </c:if>
                                                <c:if  test = "${a.role == 5}">
                                                    <td>User</td>
                                                </c:if>
                                                <td>
                                                    <a href="#" class="btn btn-outline-danger btn-rounded"
                                                       onclick="confirmDeleteUser(${a.id})"><i
                                                            class="fas fa-trash"></i></a>
                                                <!--    <a href="adminuser?go=delete&id=${a.id}" class="btn btn-outline-danger btn-rounded"><i class="fas fa-trash"></i></a>
                                                    --> </td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                                <div class="pagination-wrapper">
                                    <ul class="pagination">
                                        <c:choose>
                                            <c:when test="${requestScope.go == search}">
                                                <c:if test="${tag>1}">
                                                    <li class="page-item"><a href="adminuser?go=search&username=${param.username}&index=${tag-1}" class="page-link"><i class="fas fa-angle-left"></i></a></li>
                                                        </c:if>
                                                        <c:if test = "${tag <= 2 && (tag + 4) <= endP}">
                                                            <c:forEach begin="1" end="${tag + 4}" var="i">
                                                        <li class="${tag == i?"active":""}"><a href="adminuser?go=search&username=${param.username}&index=${i}"><b>${i}</b></a></li>
                                                                </c:forEach>
                                                            </c:if>
                                                            <c:if test = "${tag <= 2 && (tag + 4) > endP}">
                                                                <c:forEach begin="1" end="${endP}" var="i">
                                                        <li class="${tag == i?"active":""}"><a href="adminuser?go=search&username=${param.username}&index=${i}"><b>${i}</b></a></li>
                                                                </c:forEach>
                                                            </c:if>    
                                                            <c:if test = "${tag > 2 && (tag + 2) <= endP}">
                                                                <c:forEach begin="${tag - 2}" end="${tag + 2}" var="i">
                                                        <li class="${tag == i?"active":""}"><a href="adminuser?go=search&username=${param.username}&index=${i}"><b>${i}</b></a></li>
                                                                </c:forEach>
                                                            </c:if>
                                                            <c:if test = "${tag > 2 && (tag + 2) > endP}">
                                                                <c:forEach begin="${tag - 2}" end="${endP}" var="i">
                                                        <li class="${tag == i?"active":""}"><a href="adminuser?go=search&username=${param.username}&index=${i}"><b>${i}</b></a></li>
                                                                </c:forEach>
                                                            </c:if>   
                                                            <c:if test="${tag < endP}">
                                                    <li class="page-item"><a href="adminuser?go=search&username=${param.username}&index=${tag+1}" class="page-link"><i class="fas fa-angle-right"></i></a></li>
                                                        </c:if>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <c:if test="${tag>1}">
                                                    <li class="page-item"><a href="adminuser?index=${tag-1}" class="page-link"><i class="fas fa-angle-left"></i></a></li>
                                                        </c:if>
                                                        <c:if test = "${tag <= 2 && (tag + 4) <= endP}">
                                                            <c:forEach begin="1" end="${tag + 4}" var="i">
                                                        <li class="${tag == i?"active":""}"><a href="adminuser?index=${i}"><b>${i}</b></a></li>
                                                                </c:forEach>
                                                            </c:if>
                                                            <c:if test = "${tag <= 2 && (tag + 4) > endP}">
                                                                <c:forEach begin="1" end="${endP}" var="i">
                                                        <li class="${tag == i?"active":""}"><a href="adminuser?index=${i}"><b>${i}</b></a></li>
                                                                </c:forEach>
                                                            </c:if>    
                                                            <c:if test = "${tag > 2 && (tag + 2) <= endP}">
                                                                <c:forEach begin="${tag - 2}" end="${tag + 2}" var="i">
                                                        <li class="${tag == i?"active":""}"><a href="adminuser?index=${i}"><b>${i}</b></a></li>
                                                                </c:forEach>
                                                            </c:if>
                                                            <c:if test = "${tag > 2 && (tag + 2) > endP}">
                                                                <c:forEach begin="${tag - 2}" end="${endP}" var="i">
                                                        <li class="${tag == i?"active":""}"><a href="adminuser?index=${i}"><b>${i}</b></a></li>
                                                                </c:forEach>
                                                            </c:if>   
                                                            <c:if test="${tag < endP}">
                                                    <li class="page-item"><a href="adminuser?index=${tag+1}" class="page-link"><i class="fas fa-angle-right"></i></a></li>
                                                        </c:if>
                                                    </c:otherwise>
                                        </c:choose>

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="confirmDeleteUserModal" tabindex="-1" aria-labelledby="confirmDeleteModalLabel"
             aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="confirmDeleteModalLabel">Confirmation</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure you want to delete this USER?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                        <a id="deleteLink" href="#" class="btn btn-danger" onclick="deleteUserItem()">Delete</a>
                    </div>

                </div>
            </div>
        </div>    
        <style>
            .pagination li {
                display: inline-block;
            }

            .pagination li a {
                display: block;
                padding: 5px 10px;
                text-decoration: none;
                color: #07294D;
            }

            .pagination li a:hover {
                background-color: #FFC600;
            }

            ul.pagination li.active a {
                background-color: #FFC600;
            }

            .pagination-wrapper {
                display: flex;
                justify-content: center;
                margin-top: 20px;
            }

        </style>

        <script>
            var deleteUserItemId;


            function confirmDeleteUser(itemId) {
                deleteUserItemId = itemId;
                $('#confirmDeleteUserModal').modal('show');
            }

            function deleteUserItem() {
                var link = "adminuser?go=delete&id=" + deleteUserItemId;
                window.location.href = link;
                $('#confirmDeleteUserModal').modal('hide');
            }


        </script>
        <!--
        <script src="assets/vendor/datatables/datatables.min.js"></script>
        <script src="assets/js/initiate-datatables.js"></script>-->
        <script src="assets/vendor/jquery/jquery.min.js"></script>
        <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="assets/js/script.js"></script>
    </body>

</html>