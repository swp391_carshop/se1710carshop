var revenueOption = document.getElementById("revenueOption");
var revenueChart = document.getElementById("revenueChart");
var revContainer = null;

revenueDailyChart();

function revenueDailyChart() {
    revContainer = new Chart(revenueChart, {
        type: 'line',
        data: {
            labels: ['2023-05-06', '2023-06-06', '2023-07-06', '2023-08-06', '2023-09-06', '2023-10-06', '2023-11-06'],
            datasets: [{
                    data: ['2', '3', '6', '8', '4', '4', '2'],
                    backgroundColor: "rgba(48, 164, 255, 0.2)",
                    borderColor: "rgba(48, 164, 255, 0.8)",
                    fill: true,
                    borderWidth: 1
                }]
        },
        options: {
            animation: {
                duration: 2000,
                easing: 'easeOutQuart',
            },
            plugins: {
                legend: {
                    display: false,
                    position: 'right',
                },
                title: {
                    display: true,
                    text: 'Number of Visitors',
                    position: 'left',
                },
            },
        }
    });
}

function revenueMonthlyChart() {
    revContainer = new Chart(revenueChart, {
        type: 'line',
        data: {
            labels: ['2022-07', '2022-08', '2022-09', '2022-10', '2022-11', '2022-12', '2023-01', '2023-02', '2023-03', '2023-04', '2023-05', '2023-06'],
            datasets: [{
                    data: ["10", "8", "10", "9", "5", "6", "4", "12", "11", "15", "10", "10"],
                    backgroundColor: "rgba(48, 164, 255, 0.2)",
                    borderColor: "rgba(48, 164, 255, 0.8)",
                    fill: true,
                    borderWidth: 1
                }]
        },
        options: {
            animation: {
                duration: 2000,
                easing: 'easeOutQuart',
            },
            plugins: {
                legend: {
                    display: false,
                    position: 'right',
                },
                title: {
                    display: true,
                    text: 'Number of Visitors',
                    position: 'left',
                },
            },
        }
    });
}

function revenueYearlyChart() {
    revContainer = new Chart(revenueChart, {
        type: 'line',
        data: {
            labels: ['2019', '2020', '2021', '2022', '2023'],
            datasets: [{
                    data: ["280", "300", "400", "600", "450"],
                    backgroundColor: "rgba(48, 164, 255, 0.2)",
                    borderColor: "rgba(48, 164, 255, 0.8)",
                    fill: true,
                    borderWidth: 1
                }]
        },
        options: {
            animation: {
                duration: 2000,
                easing: 'easeOutQuart',
            },
            plugins: {
                legend: {
                    display: false,
                    position: 'right',
                },
                title: {
                    display: true,
                    text: 'Number of Visitors',
                    position: 'left',
                },
            },
        }
    });
}

// Event listener for dropdown change
revenueOption.addEventListener('change', function () {
    // Clear the previous chart if it exists
    if (revContainer !== null) {
        revContainer.destroy();
    }

    // Create the selected chart based on the dropdown value
    if (this.value === 'daily') {
        revenueDailyChart();
    } else if (this.value === 'monthly') {
        revenueMonthlyChart();
    } else {
        revenueYearlyChart();
    }
});


var enrollOption = document.getElementById("enrollOption");
var enrollChart = document.getElementById("enrollChart");
var enrollContainer = null;

enrollDailyChart();

function enrollDailyChart() {
    enrollContainer = new Chart(enrollChart, {
        type: 'bar',
        data: {
            labels: ['Calculus 101', 'British Literature', 'Human Anatomy and Physiology', 'Introduction to Art History', 'Organic Chemistry 101', 'Classical Mechanics 101', 'World History: Ancient Civilizations to the Renaissance'],
            datasets: [{
                    label: 'Income',
                    data: ["280", "300", "400", "600", "450", "400", "500", "550", "450", "650", "950", "1000"],
                    backgroundColor: "rgba(76, 175, 80, 0.5)",
                    borderColor: "#6da252",
                    borderWidth: 1,
                }]
        },
        options: {
            animation: {
                duration: 2000,
                easing: 'easeOutQuart',
            },
            plugins: {
                legend: {
                    display: false,
                    position: 'top',
                },
                title: {
                    display: true,
                    text: 'Number of Sales',
                    position: 'left',
                },
            },
        }
    });
}

function enrollMonthlyChart() {
    enrollContainer = new Chart(enrollChart, {
        type: 'bar',
        data: {
            labels: ['Calculus 101', 'British Literature', 'Human Anatomy and Physiology', 'Introduction to Art History', 'Organic Chemistry 101', 'Classical Mechanics 101', 'World History: Ancient Civilizations to the Renaissance'],
            datasets: [{
                    label: 'Income',
                    data: ["5280", "3100", "4010", "6010", "4150", "4100", "5400", "5450", "4150", "6150", "5950", "5000"],
                    backgroundColor: "rgba(76, 175, 80, 0.5)",
                    borderColor: "#6da252",
                    borderWidth: 1,
                }]
        },
        options: {
            animation: {
                duration: 2000,
                easing: 'easeOutQuart',
            },
            plugins: {
                legend: {
                    display: false,
                    position: 'top',
                },
                title: {
                    display: true,
                    text: 'Number of Sales',
                    position: 'left',
                },
            },
        }
    });
}

function enrollYearlyChart() {
    enrollContainer = new Chart(enrollChart, {
        type: 'bar',
        data: {
            labels: ['Calculus 101', 'British Literature', 'Human Anatomy and Physiology', 'Introduction to Art History', 'Organic Chemistry 101', 'Classical Mechanics 101', 'World History: Ancient Civilizations to the Renaissance'],
            datasets: [{
                    label: 'Income',
                    data: ["15280", "13100", "14010", "16010", "14150", "14100", "15400", "15450", "14150", "16150", "15950", "15000"],
                    backgroundColor: "rgba(76, 175, 80, 0.5)",
                    borderColor: "#6da252",
                    borderWidth: 1,
                }]
        },
        options: {
            animation: {
                duration: 2000,
                easing: 'easeOutQuart',
            },
            plugins: {
                legend: {
                    display: false,
                    position: 'top',
                },
                title: {
                    display: true,
                    text: 'Number of Sales',
                    position: 'left',
                },
            },
        }
    });
}

// Event listener for dropdown change
enrollOption.addEventListener('change', function () {
    // Clear the previous chart if it exists
    if (enrollContainer !== null) {
        enrollContainer.destroy();
    }

    // Create the selected chart based on the dropdown value
    if (this.value === 'daily') {
        enrollDailyChart();
    } else if (this.value === 'monthly') {
        enrollMonthlyChart();
    } else {
        enrollYearlyChart();
    }
});


var subOption = document.getElementById("subEnrollOption");
var subChart = document.getElementById("enroll-piechart");
var revContainer = null;

subDailyChart();

function subDailyChart() {
    revContainer = new Chart(subChart, {
        type: 'pie',
        data: {
            labels: ["Engineering", "Customer Support", "Operations", "Marketing", "R and D"],
            datasets: [{
                    data: ["62", "80", "30", "25", "17"],
                    backgroundColor: ["#009688", "#795548", "#673AB7", "#2196F3", "#6da252"],
                    hoverOffset: 4
                }]
        },
        options: {
            animation: {
                duration: 2000,
                easing: 'easeOutQuart',
            },
            plugins: {
                legend: {
                    display: true,
                    position: 'right',
                },
                title: {
                    display: false,
                    text: 'Total Value',
                    position: 'left',
                },
            },
        }
    });
}

function subMonthlyChart() {
    revContainer = new Chart(subChart, {
        type: 'pie',
        data: {
            labels: ["Engineering", "Customer Support", "Operations", "Marketing", "R and D"],
            datasets: [{
                    data: ["62", "80", "30", "25", "17"],
                    backgroundColor: ["#009688", "#795548", "#673AB7", "#2196F3", "#6da252"],
                    hoverOffset: 4
                }]
        },
        options: {
            animation: {
                duration: 2000,
                easing: 'easeOutQuart',
            },
            plugins: {
                legend: {
                    display: true,
                    position: 'right',
                },
                title: {
                    display: false,
                    text: 'Total Value',
                    position: 'left',
                },
            },
        }
    });
}

function subYearlyChart() {
    revContainer = new Chart(subChart, {
        type: 'pie',
        data: {
            labels: ["Engineering", "Customer Support", "Operations", "Marketing", "R and D"],
            datasets: [{
                    data: ["62", "80", "30", "25", "17"],
                    backgroundColor: ["#009688", "#795548", "#673AB7", "#2196F3", "#6da252"],
                    hoverOffset: 4
                }]
        },
        options: {
            animation: {
                duration: 2000,
                easing: 'easeOutQuart',
            },
            plugins: {
                legend: {
                    display: true,
                    position: 'right',
                },
                title: {
                    display: false,
                    text: 'Total Value',
                    position: 'left',
                },
            },
        }
    });
}

// Event listener for dropdown change
revenueOption.addEventListener('change', function () {
    // Clear the previous chart if it exists
    if (revContainer !== null) {
        revContainer.destroy();
    }

    // Create the selected chart based on the dropdown value
    if (this.value === 'daily') {
        subDailyChart();
    } else if (this.value === 'monthly') {
        subMonthlyChart();
    } else {
        subYearlyChart();
    }
});
