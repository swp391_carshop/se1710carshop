$(document).ready(function() {
    $('#changePasswordForm').submit(function(event) {
        var newPassword = $('#newPassword').val();
        var confirmPassword = $('#confirmPassword').val();

        if (newPassword != confirmPassword) {
            event.preventDefault();
            alert('New password and confirm password must match.');
        }
    });
});
