// Lấy thông tin người dùng từ backend API
function getUserProfile() {
  // Gửi yêu cầu GET đến API để lấy thông tin người dùng
  // Ví dụ: '/api/user/profile'
  // Xử lý kết quả nhận được và hiển thị lên màn hình User Profile
  fetch('/api/user/profile')
    .then(response => response.json())
    .then(data => {
      // Hiển thị thông tin hồ sơ người dùng
      displayUserProfile(data);
    })
    .catch(error => {
      console.error('Lỗi khi lấy thông tin người dùng:', error);
    });
}

// Cập nhật thông tin hồ sơ người dùng
function updateProfile() {
  // Lấy các giá trị đã chỉnh sửa từ các trường input trên màn hình User Profile
  var name = document.getElementById('name').value;
  var address = document.getElementById('address').value;
  var phoneNumber = document.getElementById('phoneNumber').value;

  // Gửi yêu cầu PUT đến API để cập nhật thông tin hồ sơ người dùng
  // Ví dụ: '/api/user/profile'
  // Truyền các giá trị đã chỉnh sửa trong payload của yêu cầu
  fetch('/api/user/profile', {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      name: name,
      address: address,
      phoneNumber: phoneNumber
    })
  })
    .then(response => response.json())
    .then(data => {
      // Kiểm tra kết quả và thông báo thành công hoặc lỗi cho người dùng
      if (data.success) {
        alert('Thông tin hồ sơ đã được cập nhật thành công!');
      } else {
        alert('Lỗi khi cập nhật thông tin hồ sơ. Vui lòng thử lại sau.');
      }
    })
    .catch(error => {
      console.error('Lỗi khi cập nhật thông tin hồ sơ:', error);
    });
}

// Hiển thị thông tin hồ sơ người dùng
function displayUserProfile(user) {
  // Lấy các phần tử DOM để hiển thị thông tin hồ sơ
  var nameField = document.getElementById('name');
  var addressField = document.getElementById('address');
  var phoneNumberField = document.getElementById('phoneNumber');
  var avatarImage = document.getElementById('avatar');

  // Hiển thị thông tin hồ sơ người dùng lên các trường input và hình ảnh đại diện
  nameField.value = user.name;
  addressField.value = user.address;
  phoneNumberField.value = user.phoneNumber;
  avatarImage.src = user.avatarUrl;
}

// Gọi hàm getUserProfile khi trang web được tải
window.addEventListener('load', function() {
  getUserProfile();
});