<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="shortcut icon" href="images/favicon.png" type="image/png">
        <title>Add New User</title>
        <link href="assets/vendor/fontawesome/css/fontawesome.min.css" rel="stylesheet">
        <link href="assets/vendor/fontawesome/css/solid.min.css" rel="stylesheet">
        <link href="assets/vendor/fontawesome/css/brands.min.css" rel="stylesheet">
        <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/css/master.css" rel="stylesheet">
    </head>

    <body>
        <div class="wrapper">
            <jsp:include page="admin-sidebar.jsp"/>

            <div id="body" class="active">
                <!-- navbar navigation component -->
                <jsp:include page="admin-header.jsp"/>
                <!-- end of navbar navigation -->
                <div class="content">
                    <div class="container">
                        <div class="page-title">
                            <h3><i class="fas fa-plus-circle"></i>&ensp;Add User</h3>
                        </div>
                        <div class="box box-primary">
                            <div class="box-body">
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade active show" id="general" role="tabpanel" aria-labelledby="general-tab">
                                        <div class="col-md-6">
                                            <div style="color: red">${error}</div>
                                            <form action="adminuser?go=submit" method="POST">
                                                <div class="mb-3">
                                                    <label class="form-label">Full Name<div style="color: red; display: inline;">*</div></label>
                                                    <input class="form-control" type="text" name="fullname" placeholder="Enter name" value="${fullnameError}" required>
                                                </div>

                                                <div class="mb-3">
                                                    <label class="form-label">Username<div style="color: red; display: inline;">*</div></label>
                                                    <input class="form-control" type="text" name="user" placeholder="Enter username" value="${usernameError}" required>
                                                </div>
                                                <div class="mb-3">
                                                    <label class="form-label">Password<div style="color: red; display: inline;">*</div></label>
                                                    <input class="form-control" type="password" name="pass" placeholder="Enter password" required>
                                                </div>
                                                <div class="mb-3">
                                                    <label class="form-label">Email<div style="color: red; display: inline;">*</div></label>
                                                    <input class="form-control" name="email" placeholder="Enter email" value="${emailError}" required>
                                                </div>
                                                <div class="mb-3">
                                                    <label class="form-label">Phone Number<div style="color: red; display: inline;">*</div></label>
                                                    <input class="form-control" type="tel" placeholder="Enter phone number" name="phone" value="${phoneNumberError}" required>
                                                </div>
                                                <div class="mb-3">
                                                    <label for="site-title" class="form-label">Role<div style="color: red; display: inline;">*</div></label>
                                                    <select class="form-select" name="role" id="role">;
                                                        <option value="1" "selected"> Admin </option>
                                                        <option value="2" "selected"> Marketer </option>
                                                        <option value="3" "selected"> Sale </option>
                                                        <option value="4" "selected"> Lecturer </option>
                                                        <option value="5" "selected"> User </option>
                                                    </select>
                                                </div>
                                                <div class="mb-3 text-end">
                                                    <a href="adminuser"><button type="button" class="btn btn-warning"><i class="fas fa-times"></i> Cancel</button></a> &emsp;&emsp;
                                                    <button type="reset" class="btn btn-secondary"><i class="fas fa-undo-alt"></i> Reset</button> &emsp;&emsp;
                                                    <button class="btn btn-success" type="submit"><i class="fas fa-check"></i> Add</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <style>
            .textarea-label {
                display: inline-block;
                vertical-align: top;
                margin-bottom: 0;
            }

            .textarea-input {
                display: inline-block;
                vertical-align: top;
            }

        </style>
        <script src="assets/vendor/jquery/jquery.min.js"></script>
        <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="assets/js/script.js"></script>
    </body>

</html>