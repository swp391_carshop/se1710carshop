<%-- 
    Document   : successfulRegister
    Created on : Jun 4, 2023, 12:12:42 AM
    Author     : Asus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="shortcut icon" href="images/favicon.png" type="image/png">
        <title>Register Successful</title>
        <link rel="stylesheet" href="css/successRegister.css">
    </head>
    <body>
        <div class="success-message">
            <div class="icon-container">
                <img src="images/success-icon.png" alt="Success Icon">
            </div>
            <h1>Register Successful!</h1>
            <p>Thank you for registering your account.</p>
            <a href="homepage">Back to home</a>
        </div>
    </body>
</html>
