<%-- 
    Document   : blog-list
    Created on : May 18, 2023, 8:27:21 PM
    Author     : LENOVO
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
    <head>
        <!--====== Required meta tags ======-->
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!--====== Title ======-->
        <title>Edubin - LMS Education</title>

        <!--====== Favicon Icon ======-->
        <link rel="shortcut icon" href="images/favicon.png" type="image/png">

        <!--====== Slick css ======-->
        <link rel="stylesheet" href="css/slick.css">

        <!--====== Animate css ======-->
        <link rel="stylesheet" href="css/animate.css">

        <!--====== Nice Select css ======-->
        <link rel="stylesheet" href="css/nice-select.css">

        <!--====== Nice Number css ======-->
        <link rel="stylesheet" href="css/jquery.nice-number.min.css">

        <!--====== Magnific Popup css ======-->
        <link rel="stylesheet" href="css/magnific-popup.css">

        <!--====== Bootstrap css ======-->
        <link rel="stylesheet" href="css/bootstrap.min.css">

        <!--====== Fontawesome css ======-->
        <link rel="stylesheet" href="css/font-awesome.min.css">

        <!--====== Default css ======-->
        <link rel="stylesheet" href="css/default.css">

        <!--====== Style css ======-->
        <link rel="stylesheet" href="css/style.css">

        <!--====== Responsive css ======-->
        <link rel="stylesheet" href="css/responsive.css">
    </head>

    <body>
        <!--====== HEADER PART START ======-->
        <jsp:include page="header.jsp"/>
        <!--====== HEADER PART ENDS ======-->

        <!--====== PROFILE PART START ======-->

        <div class="container px-4 mt-4">
            <!-- Account page navigation-->
            <hr class="mt-0 mb-4">
            <div class="row">
                <div class="col-xl-4">
                    <!--                     Profile picture card-->
                    <div class="card mb-4 mb-xl-0">
                        <div class="card-header">Profile Picture</div>
                        <div class="card-body text-center">
                            <!--                             Profile picture image-->
                            <img class="img-account-profile rounded-circle mb-2" src="http://bootdey.com/img/Content/avatar/avatar1.png" alt="">
                            <!--                             Profile picture help block-->
                            <div class="small font-italic text-muted mb-4"></div>
                            <!--                             Profile picture upload button-->
                            <!--                                                        <button class="btn btn-primary" type="button">Upload new image</button>-->
                        </div>
                    </div>
                </div>
                <div class="col-xl-8">
                    <!-- Account details card-->
                    <div class="card mb-4">
                        <div class="card-header">Account Details</div>
                        <div class="card-body">
                            <div style="color: red">${error}</div>
                            <form action="profile" method="Post">
                                <input type="hidden" name="go" value="update">
                                <!-- Form Group (username)-->
                                <input type="hidden" name="id" value="${requestScope.userData.id}">
                                <div class="mb-3">
                                    <label class="small mb-1" for="inputUsername">Username (how your name will appear to other users on the site)</label>
                                    <input class="form-control" id="inputUsername" type="text" placeholder="Enter your username" value="${requestScope.userData.username}" name="username" readonly>
                                </div>
                                <!-- Form Row-->
                                <div class="row gx-3 mb-3">
                                    <!-- Form Group (full name)-->
                                    <div class="col-md-12">
                                        <label class="small mb-1" for="inputFullName">Full name</label>
                                        <input class="form-control" id="inputFullName" type="text" placeholder="Enter your full name" value="${requestScope.userData.fullname}" name="fullname">
                                    </div>
                                </div>
                                <!-- Form Row        -->
                                <div class="row gx-3 mb-3">
                                    <!-- Form Group (email)-->
                                    <div class="col-md-12">
                                        <label class="small mb-1" for="inputEmailAddress">Email</label>
                                        <input class="form-control" id="inputEmailAddress" type="text" placeholder="Enter your email" name="email" value="${requestScope.userData.email}" readonly>
                                    </div>
                                </div>
                                <!-- Form Group (phone)-->
                                <div class="row gx-3 mb-3">
                                    <div class="col-md-12">
                                        <label class="small mb-1" for="inputPhone">Phone Number</label>
                                        <input class="form-control" id="inputPhone" type="text" placeholder="Enter your phone number" name="phonenumber" value="${requestScope.userData.phone}">
                                    </div>
                                </div>
                                <!-- Save changes button-->
                                <button class="btn btn-primary" type="submit">Save changes</button>
                                <!-- Change passwordbutton-->
                                <a href="changepassword" class="btn btn-primary" style="background-color: orange">Change password</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mb-4">
                <div class="card-header">My Enrollments</div>
                <div class="container mb-2">
                    <div class="row">
                        <div class="col-lg-8">
                            <c:if test="${!enrollments.isEmpty()}">
                                <table class="table table-hover mt-30">
                                    <thead>
                                        <tr>
                                            <th style="width:45%" scope="col">Course</th>
                                            <th scope="col">Subject</th>
                                            <th scope="col">Enrollment Time</th>
                                            <th scope="col">Status</th>
                                            <!--                                    <th scope="col">Handle</th>-->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach var="enrollment" items="${enrollments}">
                                            <tr class="table-light">
                                                <th scope="row">${enrollment.courseName}</th>
                                                <td>${enrollment.subjectName}</td>
                                                <td>${enrollment.enrollmentDate}</td>
                                                <td>${enrollment.status==0?'<p class="text-primary">In Progress</p>':'<p class="text-success">Completed</p>'}
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>

                                <nav class="courses-pagination mt-2">
                                    <ul class="pagination justify-content-lg-end justify-content-center">
                                        <c:if test="${totalPages>1}">
                                            <c:if test="${index!=0}">
                                                <li class="page-item">
                                                    <a href="profile?index=${index-1}${(store!=null)?'&subject=${store}':''}" aria-label="Previous">
                                                        <i class="fa fa-angle-left"></i>
                                                    </a>
                                                </li>
                                            </c:if>
                                            <c:forEach var="i" begin="0" end = "${totalPages-1}">
                                                <li class="page-item"><a ${index==i?"class='active'":""} href="profile?index=${i}${store!=null?'&subject=':''}${store}">${i+1}</a></li>
                                                </c:forEach>
                                                <c:if test="${index < totalPages-1}">
                                                <li class="page-item">
                                                    <a href="profile?index=${index+1}${(store!=null)?'&subject=${store}':''}" aria-label="Next">
                                                        <i class="fa fa-angle-right"></i>
                                                    </a>
                                                </li>
                                            </c:if>
                                        </c:if>
                                    </ul>
                                    <input style="display: none" value="${store}">
                                </nav>  <!-- courses pagination -->

                            </c:if>
                        </div>
                        <div class="col-lg-4">
                            <div class="saidbar">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="categories mt-10">
                                            <h4>Subjects</h4>
                                            <ul>
                                                <c:forEach var="subject" items="${subjects}">
                                                    <li><a href="profile?subject=${subject.id}">${subject.name}</a></li>
                                                    </c:forEach>
                                            </ul>
                                        </div>
                                    </div> <!-- row -->
                                </div> <!-- saidbar -->
                            </div>
                        </div> <!-- row --> 
                    </div> <!-- row -->
                </div> <!-- container -->
            </div>
        </div>
    </div>


    <!--====== PROFILE PART END ======--> 

    <!--====== BLOG PART ENDS ======-->

    <!--====== FOOTER PART START ======-->
    <jsp:include page="footer.jsp"/>
    <!--====== FOOTER PART ENDS ======-->


    <!--====== jquery js ======-->
    <script src="js/vendor/modernizr-3.6.0.min.js"></script>
    <script src="js/vendor/jquery-1.12.4.min.js"></script>

    <!--====== Bootstrap js ======-->
    <script src="js/bootstrap.min.js"></script>

    <!--====== Slick js ======-->
    <script src="js/slick.min.js"></script>

    <!--====== Magnific Popup js ======-->
    <script src="js/jquery.magnific-popup.min.js"></script>

    <!--====== Counter Up js ======-->
    <script src="js/waypoints.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>

    <!--====== Nice Select js ======-->
    <script src="js/jquery.nice-select.min.js"></script>

    <!--====== Nice Number js ======-->
    <script src="js/jquery.nice-number.min.js"></script>

    <!--====== Count Down js ======-->
    <script src="js/jquery.countdown.min.js"></script>

    <!--====== Validator js ======-->
    <script src="js/validator.min.js"></script>

    <!--====== Ajax Contact js ======-->
    <script src="js/ajax-contact.js"></script>

    <!--====== Main js ======-->
    <script src="js/main.js"></script>

    <!--====== Map js ======-->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDC3Ip9iVC0nIxC6V14CKLQ1HZNF_65qEQ"></script>
    <script src="js/map-script.js"></script>

</body>
</html>

