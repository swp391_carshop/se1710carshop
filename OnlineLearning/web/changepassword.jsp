<%-- 
    Document   : Login
    Created on : May 22, 2023, 2:47:16 PM
    Author     : Admin MSI
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link rel="shortcut icon" href="images/favicon.png" type="image/png">
        <title>Change Password</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/changepassword.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/login.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    </head>

    <body>
        <div class="container">
            <div style="color: red;">${error}</div>
            <h1 class="text-center mb-4">Change Password</h1>
            <form id="changePasswordForm" action="changepassword" method="post">
                <input type="hidden" name="id" value="${requestScope.id}">
                <div class="form-group">
                    <label for="currentPassword">Current Password:</label>
                    <input type="password" id="currentPassword" name="currentPassword" class="form-control" required>
                </div>

                <div class="form-group">
                    <label for="newPassword">New Password:</label>
                    <input type="password" id="newPassword" name="newPassword" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="confirmPassword">Confirm New Password:</label>
                    <input type="password" id="confirmPassword" name="confirmPassword" class="form-control" required>
                </div>

                <button type="submit" class="btn btn-primary">Change Password</button>
            </form>
            <div style="color: green;">${requestScope.status}</div>
            <div>
                <p>Forgot your password ? <a href="forgotpass.jsp">Retrieve password</a></p>
            </div>
            <div>
                <a href="profile"><button type="button" class="color-button">Back to profile</button> </a></p>
            </div>

        </div>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <script src="js/changepassword.js"></script>
    </body>

</html>



