<%-- 
    Document   : blog-list
    Created on : May 18, 2023, 8:27:21 PM
    Author     : LENOVO
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
    <head>
        <!--====== Required meta tags ======-->
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="assets/vendor/fontawesome/css/all.min.css">

        <!--====== Title ======-->
        <title>Edubin - LMS Education</title>

        <!--====== Favicon Icon ======-->
        <link rel="shortcut icon" href="images/favicon.png" type="image/png">

        <!--====== Slick css ======-->
        <link rel="stylesheet" href="css/slick.css">

        <!--====== Animate css ======-->
        <link rel="stylesheet" href="css/animate.css">

        <!--====== Nice Select css ======-->
        <link rel="stylesheet" href="css/nice-select.css">

        <!--====== Nice Number css ======-->
        <link rel="stylesheet" href="css/jquery.nice-number.min.css">

        <!--====== Magnific Popup css ======-->
        <link rel="stylesheet" href="css/magnific-popup.css">

        <!--====== Bootstrap css ======-->
        <link rel="stylesheet" href="css/bootstrap.min.css">

        <!--====== Fontawesome css ======-->
        <link rel="stylesheet" href="css/font-awesome.min.css">

        <!--====== Default css ======-->
        <link rel="stylesheet" href="css/default.css">

        <!--====== Style css ======-->
        <link rel="stylesheet" href="css/style.css">

        <!--====== Responsive css ======-->
        <link rel="stylesheet" href="css/responsive.css">
    </head>

    <body>
        <a href="manageblog" class="btn-back">
                        <i class="fa fa-arrow-left"></i> Back
                    </a>
        <!--====== HEADER PART START ======-->
        <div class="header-top d-none d-lg-block">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="header-contact text-lg-left text-center">
                        <ul>
                            <li><img src="images/all-icon/map.png" alt="icon"><span>Hoa Lac Hi-tech Park, Thach Hoa, Thach That, Ha Noi</span></li>
                            <li><img src="images/all-icon/email.png" alt="icon"><span>anhtthe161427@fpt.edu.vn</span></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="header-opening-time text-lg-right text-center">
<!--                        <p>Opening Hours : Monday to Saturay - 8 Am to 5 Pm</p>-->
                    </div>
                </div>
            </div> <!-- row -->
        </div> <!-- container -->
    </div>
        
        <div class="header-logo-support pt-30 pb-30">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4">
                    <div class="logo">
                        
                            <img src="images/logo.png" alt="Logo">
                        
                    </div>
                </div>
            </div> <!-- row -->
        </div> <!-- container -->
    </div>
        <!--====== HEADER PART ENDS ======-->

        <!--====== PROFILE PART START ======-->

        <div class="container px-4 mt-4">
            <!-- Account page navigation-->
            <hr class="mt-0 mb-4">
            <div class="row">
                <div class="col-xl-4">
                    <!--                     Profile picture card-->
                    <div class="card mb-4 mb-xl-0">
                        <div class="card-header">Profile Picture</div>
                        <div class="card-body text-center">
                            <!--                             Profile picture image-->
                            <img class="img-account-profile rounded-circle mb-2" src="images/avatar1.png" alt="">
                            <!--                             Profile picture help block-->
                            <div class="small font-italic text-muted mb-4"></div>
                            <!--                             Profile picture upload button-->
                            <!--                                                        <button class="btn btn-primary" type="button">Upload new image</button>-->
                        </div>
                    </div>
                </div>
                <div class="col-xl-8">
                    <!-- Account details card-->
                    <div class="card mb-4">
                        <div class="card-header">Personal Information</div>
                        <div class="card-body">
                                <!-- Form Group (username)-->
                                <input type="hidden" name="id" value="${requestScope.userData.id}">
                                <div class="mb-3">
                                <p style="color: #000000;"><span>Username:&emsp;&emsp;&emsp;&emsp;&nbsp;  </span>${requestScope.userData.getUsername()}</p>   
                                </div>
                                <!-- Form Row-->
                                <div class="row gx-3 mb-3">
                                    <!-- Form Group (full name)-->
                                    <div class="col-md-12">
                                       <p style="color: #000000;"><span>Full Name:&emsp;&emsp;&emsp;&emsp; </span>${requestScope.userData.getFullname()}</p>
                                    </div>
                                </div>
                                <!-- Form Row        -->
                                <div class="row gx-3 mb-3">
                                    <!-- Form Group (email)-->
                                    <div class="col-md-12">
                                           <p style="color: #000000;"><span>Email:&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; </span>${requestScope.userData.getEmail()}</p>
                                     </div>
                                </div>
                                <!-- Form Group (phone)-->
                                <div class="row gx-3 mb-3">
                                    <div class="col-md-12">
                                      <p style="color: #000000;"><span>Phone Number:&emsp;&emsp; </span>${requestScope.userData.getPhone()}</p>  
                                    </div>
                                </div>
                                <!-- Save changes button-->
                                <a href="mktprofile?go=changepass"><button type="button" class="btn btn-warning"><i class="fas fa-lock-open"></i> Change Password</button></a>&emsp; &emsp;
                                <a href="mktprofile?go=editprofile"> <button class="btn btn-primary" type="button"><i class="fas fa-edit"></i> Edit Profile</button></a>
                            
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    


    <!--====== PROFILE PART END ======--> 

    <!--====== BLOG PART ENDS ======-->

    <!--====== FOOTER PART START ======-->
    <jsp:include page="footer.jsp"/>
    <!--====== FOOTER PART ENDS ======-->

    <style>
        .btn-back {
                position: fixed;
                top: 200px;
                left: 20px;
                font-size: 16px;
                color: #555;
                text-decoration: none;
            }

            .btn-back i {
                margin-right: 5px;
            }
    </style>
    <!--====== jquery js ======-->
    <script src="js/vendor/modernizr-3.6.0.min.js"></script>
    <script src="js/vendor/jquery-1.12.4.min.js"></script>

    <!--====== Bootstrap js ======-->
    <script src="js/bootstrap.min.js"></script>

    <!--====== Slick js ======-->
    <script src="js/slick.min.js"></script>

    <!--====== Magnific Popup js ======-->
    <script src="js/jquery.magnific-popup.min.js"></script>

    <!--====== Counter Up js ======-->
    <script src="js/waypoints.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>

    <!--====== Nice Select js ======-->
    <script src="js/jquery.nice-select.min.js"></script>

    <!--====== Nice Number js ======-->
    <script src="js/jquery.nice-number.min.js"></script>

    <!--====== Count Down js ======-->
    <script src="js/jquery.countdown.min.js"></script>

    <!--====== Validator js ======-->
    <script src="js/validator.min.js"></script>

    <!--====== Ajax Contact js ======-->
    <script src="js/ajax-contact.js"></script>

    <!--====== Main js ======-->
    <script src="js/main.js"></script>

    <!--====== Map js ======-->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDC3Ip9iVC0nIxC6V14CKLQ1HZNF_65qEQ"></script>
    <script src="js/map-script.js"></script>

</body>
</html>
