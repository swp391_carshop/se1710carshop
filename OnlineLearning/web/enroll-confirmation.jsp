<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en" dir="ltr">

    <head>
        <meta charset="UTF-8">
        <!--====== Title ======-->
        <title>Course Enroll Confirmation</title>

        <!--====== Favicon Icon ======-->
        <link rel="shortcut icon" href="images/favicon.png" type="image/png">
        <!---<title> Responsive Registration Form | CodingLab </title>--->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/login.css">
        <!--====== Style css ======-->
        <link rel="stylesheet" href="css/style.css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>

    <body>
        <div class="container">
            <!-- Code to execute if parameter is not null -->
            <div class="title">
                <c:choose>
                    <c:when test ="${empty enrollSuccessfull}">
                        <span class="details">
                            There is some mistake, please try again
                        </span>
                    </c:when>
                    <c:otherwise>
                        <span class="details">
                            Enrollment Successfully. 
                        </span>
                        <span class="details">    
                            Thank you for enrolling in the course!
                        </span>
                    </c:otherwise>
                </c:choose>
            </div>
            <br>
            <div class="content">
                <c:if test ="${not empty enrollSuccessfull}">
                    <div>
                        <a href="./learn?lid=${lessonId}" class="main-btn" style="text-decoration: none">Go to course</a>
                    </div>
                </c:if>
                <div>
                    <a href="courses" class="main-btn" style="text-decoration: none; margin-top: 1rem">Browse other courses</a>
                </div>
            </div>
        </div>

    </body>

</html>