<%-- 
    Document   : blog-list
    Created on : May 18, 2023, 8:27:21 PM
    Author     : LENOVO
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
    <head>
        <!--====== Required meta tags ======-->
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!--====== Title ======-->
        <title>Edubin - LMS Education</title>

        <!--====== Favicon Icon ======-->
        <link rel="shortcut icon" href="images/favicon.png" type="image/png">

        <!--====== Slick css ======-->
        <link rel="stylesheet" href="css/slick.css">

        <!--====== Animate css ======-->
        <link rel="stylesheet" href="css/animate.css">

        <!--====== Nice Select css ======-->
        <link rel="stylesheet" href="css/nice-select.css">

        <!--====== Nice Number css ======-->
        <link rel="stylesheet" href="css/jquery.nice-number.min.css">

        <!--====== Magnific Popup css ======-->
        <link rel="stylesheet" href="css/magnific-popup.css">

        <!--====== Bootstrap css ======-->
        <link rel="stylesheet" href="css/bootstrap.min.css">

        <!--====== Fontawesome css ======-->
        <link rel="stylesheet" href="css/font-awesome.min.css">

        <!--====== Default css ======-->
        <link rel="stylesheet" href="css/default.css">

        <!--====== Style css ======-->
        <link rel="stylesheet" href="css/style.css">

        <!--====== Responsive css ======-->
        <link rel="stylesheet" href="css/responsive.css">
        <link rel="stylesheet" href="css/sidebar-style.css">
        <style>
            .hover-list:hover{
                background-color: #f7f7f7;
                border-left: 6px solid #2a73cc;
            }
        </style>

    </head>

    <body>
        <!--====== HEADER PART START ======-->
        <jsp:include page="header.jsp"/>
        <!--====== HEADER PART ENDS ======-->

        <!--====== PAGE BANNER PART START ======-->

        <!--        <section id="page-banner" class="pt-105 pb-130 bg_cover" data-overlay="8" style="background-image: url(images/page-banner-4.jpg)">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="page-banner-cont">
                                    <h2>Blogs</h2>
                                    <nav aria-label="breadcrumb">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="homepage">Home</a></li>
                                            <li class="breadcrumb-item active" aria-current="page">Blogs</li>
                                        </ol>
                                    </nav>
                                </div>   page banner cont 
                            </div>
                        </div>  row 
                    </div>  container 
                </section>-->

        <!--====== PAGE BANNER PART ENDS ======-->

        <!--====== BLOG PART START ======-->

        <section id="blog-page" class=" pb-120" style="border-top: 1px solid #cecece;">
            <div class="container-fluid">

                <div class="row" >
                    <div class="col-lg-9">
                        <nav aria-label="breadcrumb" class="pl-3">
                            <ol class="breadcrumb" style="background-color: white">
                                <li class="breadcrumb-item"><a href="/OnlineLearning/coursedetails?cid=${course.courseID}">${course.courseName}</a></li>
                                <li class="breadcrumb-item">${chapter.chapterName}</li>
                                <li class="breadcrumb-item active" aria-current="page">${lesson!=null?lesson.name:quiz.name}</li>
                            </ol>
                        </nav>
                    </div>
                    <div class="col-lg-3 d-flex align-items-center justify-content-end">
                        <c:if test="${previousId != 0}">
                            <button type="button" class="btn btn-outline-primary mr-4" onclick="location.href = './learn?${previousType==1?'lid':'qid'}=${previousId}';">
                                Previous 
                            </button>
                        </c:if>
                        <c:if test="${nextId != 0}">
                            <button type="button" class="btn btn-outline-primary mr-4" onclick="location.href = './learn?${nextType==1?'lid':'qid'}=${nextId}';">
                                Next
                            </button>
                        </c:if>
                    </div>
                </div>



                <div class="row">
                    <div class="col-lg-3">
                        <!-- sidebar -->
                        <nav id="sidebar" style="border-right: 1px solid rgba(0, 0, 0, 0.05); ">
                            <div class="p-4 ">
                                <ul class="list-unstyled components mb-5">
                                    <h3>Chapters</h3>
                                    <c:forEach var="chapter" items="${chapterList}">
                                        <li>
                                            <a href="#pageSubmenu${chapter.chapterNo}" data-toggle="collapse" aria-expanded="${lesson!=null?(lesson.chapterId==chapter.chapterId?'true':'false'):(quiz.chapterId==chapter.chapterId?'true':'false')}" class="dropdown-toggle" style ="white-space: wrap">
                                                ${chapter.chapterName}
                                            </a>
                                            <ul class="collapse ${lesson!=null?(lesson.chapterId==chapter.chapterId?'show':''):(quiz.chapterId==chapter.chapterId?'show':'')} list-unstyled" id="pageSubmenu${chapter.chapterNo}">
                                                <!--Load lessons for each chapter-->
                                                <c:forEach var="les" items="${lessonList}">
                                                    <c:if test="${chapter.chapterId == les.chapterId}">
                                                        <li ${les.id == lesson.id?'style="background-color: #f7f7f7; border-left: 6px solid #2a73cc;"':''} class="hover-list"><a class="ml-2 mr-2" href="/OnlineLearning/learn?lid=${les.id}" style="display: inline-block;">${les.name}</a></li>
                                                        </c:if>
                                                    </c:forEach>
                                                <!--Load quiz for each chapter-->        
                                                <c:forEach var="qui" items="${quizList}">
                                                    <c:if test="${qui.chapterId == chapter.chapterId}">
                                                        <li ${qui.id == quiz.id?'style="background-color: #f7f7f7; border-left: 6px solid #2a73cc;"':''} class="hover-list"><a class="ml-2" href="/OnlineLearning/learn?qid=${qui.id}" style="display: inline-block;">${qui.name}${quizPassed.contains(qui.getId())?'<span class="badge badge-success ml-2" style="color:white;">Completed</span>':''}</a></li>
                                                        </c:if>
                                                    </c:forEach>        
                                            </ul>
                                        </li>
                                    </c:forEach>
                                    <!--
                                                                        <li>
                                                                            <a href="#pageSubmenu3" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Accessories</a>
                                                                            <ul class="collapse list-unstyled" id="pageSubmenu3">
                                                                                <li><a href="#"><span class="fa fa-chevron-right mr-2"></span> Nicklace</a></li>
                                                                                <li><a href="#"><span class="fa fa-chevron-right mr-2"></span> Ring</a></li>
                                                                                <li><a href="#"><span class="fa fa-chevron-right mr-2"></span> Bag</a></li>
                                                                                <li><a href="#"><span class="fa fa-chevron-right mr-2"></span> Sacks</a></li>
                                                                                <li><a href="#"><span class="fa fa-chevron-right mr-2"></span> Lipstick</a></li>
                                                                            </ul>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#pageSubmenu4" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Clothes</a>
                                                                            <ul class="collapse list-unstyled" id="pageSubmenu4">
                                                                                <li><a href="#"><span class="fa fa-chevron-right mr-2"></span> Jeans</a></li>
                                                                                <li><a href="#"><span class="fa fa-chevron-right mr-2"></span> T-shirt</a></li>
                                                                                <li><a href="#"><span class="fa fa-chevron-right mr-2"></span> Jacket</a></li>
                                                                                <li><a href="#"><span class="fa fa-chevron-right mr-2"></span> Shoes</a></li>
                                                                                <li><a href="#"><span class="fa fa-chevron-right mr-2"></span> Sweater</a></li>
                                                                            </ul>
                                                                        </li>-->
                                </ul>
                                <!--                                <div class="mb-5">
                                                                    <h5>Tag Cloud</h5>
                                                                    <div class="tagcloud">
                                                                        <a href="#" class="tag-cloud-link">dish</a>
                                                                        <a href="#" class="tag-cloud-link">menu</a>
                                                                        <a href="#" class="tag-cloud-link">food</a>
                                                                        <a href="#" class="tag-cloud-link">sweet</a>
                                                                        <a href="#" class="tag-cloud-link">tasty</a>
                                                                        <a href="#" class="tag-cloud-link">delicious</a>
                                                                        <a href="#" class="tag-cloud-link">desserts</a>
                                                                        <a href="#" class="tag-cloud-link">drinks</a>
                                                                    </div>
                                                                </div>
                                                                <div class="mb-5">
                                                                    <h5>Newsletter</h5>
                                                                    <form action="#" class="subscribe-form">
                                                                        <div class="form-group d-flex">
                                                                            <div class="icon"><span class="icon-paper-plane"></span></div>
                                                                            <input type="text" class="form-control" placeholder="Enter Email Address">
                                                                        </div>
                                                                    </form>
                                                                </div>-->
                            </div>
                        </nav>
                        <!-- sidebar -->
                    </div>

                    <div class="col-lg-9" >
                        <!-- content -->
                        <div class="blog-details" >
                            <div class="cont pt-4">
                                <c:if test="${coursePassed!=null}">
                                <div class="alert alert-success" role="alert">
                                    <p class="mb-0"><i class="fa-regular fa-circle-check"></i>  You have completed this course!</p>
                                </div>
                                </c:if>
                                <!-- lesson -->
                                <c:if test="${lesson !=null}">
                                    <h1>${lesson.name}</h1>
                                    <!-- Textbook -->
                                    <c:if test="${material.typeId==1}">
                                        <p><div style="white-space: pre-line;">${material.text}</div></p>
                                        ${material.link}
                                    </c:if>

                                    <!-- Lecture Notes -->
                                    <c:if test="${material.typeId==2}">
                                        <p><div style="white-space: pre-line;">${material.text}</div></p>
                                        ${material.link}
                                    </c:if>

                                    <!-- Video -->
                                    <c:if test="${material.typeId==3}">
                                        <p><div style="white-space: pre-line;">${material.text}</div></p>
                                        <div style="position: relative; overflow: hidden; width: 100%; padding-top: 56.25%;">
                                            <iframe style="
                                                    position: absolute;
                                                    top: 0;
                                                    left: 0;
                                                    bottom: 0;
                                                    right: 0;
                                                    width: 100%;
                                                    height: 100%;"
                                                    src="${material.link}" 
                                                    title="YouTube video player" 
                                                    frameborder="0" 
                                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" 
                                                    allowfullscreen>
                                            </iframe>
                                        </div>
                                    </c:if>

                                    <!-- Reference -->
                                    <c:if test="${material.typeId==4}">
                                        <p><div style="white-space: pre-line;">${material.text}</div></p>
                                        ${material.link}
                                    </c:if>
                                </c:if>
                                <!-- quiz -->
                                <c:if test="${quiz!=null}">
                                    <h1 class="pb-3">${quiz.name}</h1>
                                    ${userResult==0?'':(userResult==1?'<h4 class="text-danger">Not passed</h4>':'<h4 class="text-success">You have passed</h4>')}
                                    <p><strong>To do:</strong> 80% or higher</p>
                                    <button type="button" class="btn btn-outline-primary mt-5" onclick="location.href = '/OnlineLearning/getquestions?quiz=${quiz.id}';">
                                        ${userResult==0?'Do Quiz':'Try Again'}
                                    </button>
                                </c:if>

                                <!-- cont -->
                            </div> <!-- blog details -->

                            <!-- !content -->
                        </div>
                    </div>
                </div>
            </div> <!-- container -->
        </section>

        <!--====== BLOG PART ENDS ======-->

        <!--====== FOOTER PART START ======-->
        <jsp:include page="footer.jsp"/>
        <!--====== FOOTER PART ENDS ======-->


        <!--====== jquery js ======-->
        <script src="js/vendor/modernizr-3.6.0.min.js"></script>
        <script src="js/vendor/jquery-1.12.4.min.js"></script>

        <!--====== Bootstrap js ======-->
        <script src="js/bootstrap.min.js"></script>

        <!--====== Slick js ======-->
        <script src="js/slick.min.js"></script>

        <!--====== Magnific Popup js ======-->
        <script src="js/jquery.magnific-popup.min.js"></script>

        <!--====== Counter Up js ======-->
        <script src="js/waypoints.min.js"></script>
        <script src="js/jquery.counterup.min.js"></script>

        <!--====== Nice Select js ======-->
        <script src="js/jquery.nice-select.min.js"></script>

        <!--====== Nice Number js ======-->
        <script src="js/jquery.nice-number.min.js"></script>

        <!--====== Count Down js ======-->
        <script src="js/jquery.countdown.min.js"></script>

        <!--====== Validator js ======-->
        <script src="js/validator.min.js"></script>

        <!--====== Ajax Contact js ======-->
        <script src="js/ajax-contact.js"></script>

        <!--====== Main js ======-->
        <script src="js/main.js"></script>

        <!--====== Map js ======-->
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDC3Ip9iVC0nIxC6V14CKLQ1HZNF_65qEQ"></script>
        <script src="js/map-script.js"></script>
        <script src="https://kit.fontawesome.com/ad151c54e9.js" crossorigin="anonymous"></script>   
    </body>
</html>

