<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="shortcut icon" href="images/favicon.png" type="image/png">
        <title>Edit Slider</title>
        <link href="assets/vendor/fontawesome/css/fontawesome.min.css" rel="stylesheet">
        <link href="assets/vendor/fontawesome/css/solid.min.css" rel="stylesheet">
        <link href="assets/vendor/fontawesome/css/brands.min.css" rel="stylesheet">
        <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/css/master.css" rel="stylesheet">
    </head>

    <body>
        <div class="wrapper">
            <jsp:include page="mkt-sidebar.jsp"/>

            <div id="body" class="active">
                <!-- navbar navigation component -->
                <jsp:include page="mkt-header.jsp"/>
                <!-- end of navbar navigation -->
                <div class="content">
                    <div class="container">
                        <div class="page-title">
                            <h3><i class="fas fa-edit"></i>&ensp;EDIT SLIDER</h3>
                        </div>
                        <div class="box box-primary">
                            <div class="box-body">
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade active show" id="general" role="tabpanel" aria-labelledby="general-tab">
                                        <div class="col-md-6">
                                            <form id="addQuestionForm" action="editslider" method="post">
                                                <div class="mb-3">
                                                    <label class="form-label">ID</label>
                                                    <input class="form-control" value="${requestScope.detail.getId()}" name="sid" type="text" readonly>
                                                </div>
                                                <div class="mb-3">
                                                    <label class="form-label">Title<div style="color: red; display: inline;">*</div></label>
                                                    <input class="form-control" value="${requestScope.detail.getTitle()}" name="title" type="text" placeholder="Enter Title" required>
                                                </div>
                                                <div class="mb-3">
                                                    <label class="form-label">Description<div style="color: red; display: inline;">*</div></label>
                                                    <textarea class="form-control" name="describe" type="text" placeholder="Enter Description" required rows="3" cols="30">${requestScope.detail.getDescription()}</textarea>
                                                </div>
                                                <div class="mb-3">
                                                    <label class="form-label">Image<div style="color: red; display: inline;">*</div></label>
                                                    <input class="form-control" value="${requestScope.detail.getImage()}" name="image" type="file" required>
                                                </div>
                                                <div class="mb-3">
                                                    <label class="form-label">Link<div style="color: red; display: inline;">*</div></label>
                                                    <input class="form-control" value="${requestScope.detail.getLink()}" name="link" type="text" placeholder="Enter Link" required>
                                                </div>
                                                <div class="mb-3">
                                                    <label for="site-title" class="form-label">Status<div style="color: red; display: inline;">*</div></label>
                                                    <select class="form-select" name="status" required>
                                                        <option value="1">Active</option>
                                                        <option value="0">Inactive</option>
                                                    </select>
                                                </div>
                                                <div class="btn-group">
                                                    &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                                                    <a href="manageslider"><button type="button" class="btn btn-secondary"><i class="fas fa-times"></i> Cancel</button></a>&emsp; &emsp; &emsp;
                                                    <button class="btn btn-primary" type="submit"><i class="fas fa-save"></i> Save</button>
                                                </div>

                                            </form>
                                               
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <style>
            .textarea-label {
                display: inline-block;
                vertical-align: top;
                margin-bottom: 0;
            }

            .textarea-input {
                display: inline-block;
                vertical-align: top;
            }
            .form-control {
                width: 200%; /* Adjust the width as needed, 100% will make it take the full width of the container */
            }
            .form-select{
                width: 200%;
            }

            .form-label {
                display: block;
                margin-bottom: 5px;
            }

            .btn-group {
                display: flex;
                justify-content: flex-end;
                width: 200%;
            }

            .btn-group .btn {
                margin-left: 10px;
            }
            
            .form-control[readonly] {
                background-color: #f0f0f0;
        font-weight: bold;
    }
        </style>
        <script src="assets/vendor/jquery/jquery.min.js"></script>
        <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="assets/js/script.js"></script>
    </body>

</html>