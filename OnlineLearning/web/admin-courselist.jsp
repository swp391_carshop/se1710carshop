<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<!-- 
* Bootstrap Simple Admin Template
* Version: 2.1
* Author: Alexis Luna
* Website: https://github.com/alexis-luna/bootstrap-simple-admin-template
-->
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="shortcut icon" href="images/favicon.png" type="image/png">
        <title>Course List</title>
        <link href="assets/vendor/fontawesome/css/fontawesome.min.css" rel="stylesheet">
        <link href="assets/vendor/fontawesome/css/solid.min.css" rel="stylesheet">
        <link href="assets/vendor/fontawesome/css/brands.min.css" rel="stylesheet">
        <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/vendor/datatables/datatables.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link href="assets/css/master.css" rel="stylesheet">
    </head>

    <body>
        <div class="wrapper">
            <jsp:include page="admin-sidebar.jsp"/>

            <div id="body" class="active">
                <!-- navbar navigation component -->
                <jsp:include page="admin-header.jsp"/>
                <!-- end of navbar navigation -->
                <div class="content">
                    <div class="container">
                        <div class="page-title">
                            <h3>
                                <i class="fas fa-copyright"></i>&ensp;Course List
                                <!--                                <span class="float-end">
                                                                    <span class="sort-text" style="color: #22A1F9; font-size: 18px; font-weight: bold;">Sort</span>
                                                                    <a href="searchcoursesale?type=tang" style="background-color: white; color: blue; font-size: 12px; padding: 5px 10px; margin-left: 10px; border: 1px solid blue; border-radius: 3px;"
                                                                       onmousedown="this.style.backgroundColor = 'blue'; this.style.color = 'white';"
                                                                       onmouseup="this.style.backgroundColor = 'white'; this.style.color = 'blue';"
                                                                       onmouseout="this.style.backgroundColor = 'white'; this.style.color = 'blue';"
                                                                       onmouseover="this.style.backgroundColor = 'lightblue'; this.style.color = 'blue';"
                                                                       >Ascending</a>
                                                                    <a href="searchcoursesale?type=giam" style="background-color: white; color: blue; font-size: 12px; padding: 5px 10px; margin-left: 10px; border: 1px solid blue; border-radius: 3px;"
                                                                       onmousedown="this.style.backgroundColor = 'blue'; this.style.color = 'white';"
                                                                       onmouseup="this.style.backgroundColor = 'white'; this.style.color = 'blue';"
                                                                       onmouseout="this.style.backgroundColor = 'white'; this.style.color = 'blue';"
                                                                       onmouseover="this.style.backgroundColor = 'lightblue'; this.style.color = 'blue';"
                                                                       >Descending</a>
                                                                </span>-->
                            </h3>
                        </div>







                        <div class="box box-primary">
                            <div class="box-body">
                                <table width="100%" class="table table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Name Course</th>
                                            <th>Description</th>
                                            <th>Objective</th>
                                            <th>Subject</th>
                                            <th style="width: 5%">Sales</th>
                                            <th>Status</th>
                                            <th>Update</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${requestScope.courseList}" var="c">
                                            <tr>
                                                <td>${c.getId()}</td>
                                                <td>${c.getName_course()}</td>
                                                <td>${c.getDescription()}</td>
                                                <td>${c.getObjective()}</td>
                                                <td>${c.getName()}</td>
                                                <td>${c.getSale()} <i class="fas fa-pencil-alt"></i></td>
                                                <td>
                                                    <c:if test = "${c.getActive() eq '1'}"><i class="fas fa-eye" style="color: #0D6EFD;"></i></c:if> 
                                                    <c:if test = "${c.getActive() eq '0'}"><i class="fas fa-eye-slash"></i></c:if> 
                                                </td>
                                                <td>
                                                    <c:if test = "${c.getActive() eq '1'}">
                                                        <a href="admincourse?go=update&cid=${c.getId()}&active=0" class="btn btn-outline-dark btn-rounded"><i class="fas fa-eye-slash"></i></a>
                                                    </c:if>
                                                    <c:if test = "${c.getActive() eq '0'}">
                                                        <a href="admincourse?go=update&cid=${c.getId()}&active=1" class="btn btn-outline-primary btn-rounded"><i class="fas fa-eye"></i></a>
                                                    </c:if>   
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                                <div class="pagination-wrapper">
                                    <ul class="pagination">
                                        <c:if test="${tag>1}">
                                            <li class="page-item"><a href="admincourse?index=${tag-1}" class="page-link"><i class="fas fa-angle-left"></i></a></li>
                                        </c:if>
                                            <c:if test = "${tag <= 2 && (tag + 4) <= endP}">
                                                <c:forEach begin="1" end="${tag + 4}" var="i">
                                                <li class="${tag == i?"active":""}"><a href="admincourse?index=${i}"><b>${i}</b></a></li>
                                                </c:forEach>
                                            </c:if>
                                            <c:if test = "${tag <= 2 && (tag + 4) > endP}">
                                                <c:forEach begin="1" end="${endP}" var="i">
                                                <li class="${tag == i?"active":""}"><a href="admincourse?index=${i}"><b>${i}</b></a></li>
                                                </c:forEach>
                                            </c:if>    
                                            <c:if test = "${tag > 2 && (tag + 2) <= endP}}">
                                                <c:forEach begin="${tag - 2}" end="${tag + 2}" var="i">
                                                <li class="${tag == i?"active":""}"><a href="admincourse?index=${i}"><b>${i}</b></a></li>
                                                </c:forEach>
                                            </c:if>
                                            <c:if test = "${tag > 2 && (tag + 2) > endP}">
                                                <c:forEach begin="${tag - 2}" end="${endP}" var="i">
                                                <li class="${tag == i?"active":""}"><a href="admincourse?index=${i}"><b>${i}</b></a></li>
                                                </c:forEach>
                                            </c:if>    
                                        <c:if test="${tag < endP}">
                                        <li class="page-item"><a href="admincourse?index=${tag+1}" class="page-link"><i class="fas fa-angle-right"></i></a></li>
                                        </c:if>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <style>
            .pagination li {
                display: inline-block;
            }

            .pagination li a {
                display: block;
                padding: 5px 10px;
                text-decoration: none;
                color: #07294D;
            }

            .pagination li a:hover {
                background-color: #FFC600;
            }

            ul.pagination li.active a {
                background-color: #FFC600;
            }

            .pagination-wrapper {
                display: flex;
                justify-content: center;
                margin-top: 20px;
            }

        </style>
        <!--
        <script src="assets/vendor/datatables/datatables.min.js"></script>
        <script src="assets/js/initiate-datatables.js"></script>-->
        <script src="assets/vendor/jquery/jquery.min.js"></script>
        <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="assets/js/script.js"></script>
    </body>

</html>