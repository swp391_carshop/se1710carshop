<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="shortcut icon" href="images/favicon.png" type="image/png">
        <title>Add Chapter</title>
        <link href="assets/vendor/fontawesome/css/fontawesome.min.css" rel="stylesheet">
        <link href="assets/vendor/fontawesome/css/solid.min.css" rel="stylesheet">
        <link href="assets/vendor/fontawesome/css/brands.min.css" rel="stylesheet">
        <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/css/master.css" rel="stylesheet">
        <link href="assets/vendor/flagiconcss/css/flag-icon.min.css" rel="stylesheet">
    </head>

    <body>
        <div class="wrapper">
            <jsp:include page="lecturer-sidebar.jsp"/>

            <div id="body" class="active">
                <!-- navbar navigation component -->
                <jsp:include page="lecturer-header.jsp"/>
                <!-- end of navbar navigation -->
                <div class="content">

                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 page-header">
                                <a href="loaddatalecturer" class="btn-back hidden"><i class="fas fa-arrow-left"></i> Back</a>
                                <h2 class="page-title"><i class="fas fa-plus-circle"></i>&ensp;Add Chapter</h2>
                            </div>
                        </div>
                            <div class="box box-primary">
                                <div class="box-body">

                                    <div class="tab-content" id="myTabContent">
                                        <div class="tab-pane fade active show" id="general" role="tabpanel" aria-labelledby="general-tab">
                                            <div class="col-md-6">
                                                <form id="yourForm" action="addchapter" method="post">
                                                    <div class="mb-3">
                                                        <label class="form-label">Enter Name Of Chapter<div style="color: red; display: inline;">*</div></label>
                                                        <textarea id="id" class="form-control" name="chapter-name" rows="2" cols="30" placeholder="Chapter Name" required></textarea>
                                                    </div>
                                                    <div class="mb-3">
                                                        <label id="id" class="form-label">Enter Chapter No<div style="color: red; display: inline;">*</div></label>
                                                        <input class="form-control" name="chapter-no" placeholder="Chapter No" required>
                                                    </div>
                                                    <div class="mb-3">
                                                        <label id="id" class="form-label">Enter Objective Of Chapter<div style="color: red; display: inline;">*</div></label>
                                                        <textarea class="form-control" name="chapter-objective" rows="2" cols="30" placeholder="Objective Of Chapter" required></textarea>
                                                    </div>

                                                    <input hidden="" value="${requestScope.courseId}" name="courseId">

                                                    <div class="mb-3 text-end">
                                                        <button class="btn btn-success" type="button" data-bs-toggle="modal" data-bs-target="#confirmAddModal"><i class="fas fa-check"></i> Add</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="confirmAddModal" tabindex="-1" aria-labelledby="confirmAddModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="confirmAddModalLabel">Confirmation</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure you want to add this CHAPTER?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                        <button id="confirmAddButton" class="btn btn-success"><i class="fas fa-check"></i> Add</button>
                    </div>
                </div>
            </div>
        </div>

        <style>
            .btn-back {
                position: absolute;
                top: 15px;
                left: 15px;
                font-size: 20px;
                color: #07294D;
                text-decoration: none;
            }

            .btn-back:hover {
                text-decoration: underline;
            }

        </style>
        <script src="assets/vendor/jquery/jquery.min.js"></script>
        <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script>
            // Lưu trữ trạng thái xác nhận
            var confirmAdd = false;

// Xác nhận thêm
            document.getElementById("confirmAddButton").addEventListener("click", function () {
                confirmAdd = true;
                $("#confirmAddModal").modal("hide");
                addChapter();
            });

// Thêm chương
            function addChapter() {
                if (confirmAdd) {
                    var chapterName = document.getElementsByName("chapter-name")[0].value;
                    var chapterNo = document.getElementsByName("chapter-no")[0].value;
                    var chapterObjective = document.getElementsByName("chapter-objective")[0].value;

                    if (chapterName.trim() === "" || chapterNo.trim() === "" || chapterObjective.trim() === "") {
                        alert("Please fill in all the required fields.");
                    return;
                    }
                    // Get the courseId value from the URL parameter &cid=
            var urlParams = new URLSearchParams(window.location.search);
            var courseId = urlParams.get('cid');

            if (!courseId) {
                alert("Error: courseId not found in URL parameter &cid=");
                return;
            }

                var form = document.getElementById("yourForm");
                // Thêm courseId vào action của form
                form.action = "addchapter?cid=" + courseId;
                // Thực hiện hành động thêm bài học
                form.submit();
                }
            }


        // Sự kiện click nút "Add"
            document.getElementById("addButton").addEventListener("click", function () {
                var chapterName = document.getElementsByName("chapter-name")[0].value;
                var chapterNo = document.getElementsByName("chapter-no")[0].value;
                var chapterObjective = document.getElementsByName("chapter-objective")[0].value;

                if (chapterName.trim() === "" || chapterNo.trim() === "" || chapterObjective.trim() === "") {
                    alert("Please fill in all the required fields.");
                    return;
                }

                // Hiển thị hộp thoại xác nhận
                $("#confirmAddModal").modal("show");
            });

        </script>  
        <script src="assets/vendor/chartsjs/Chart.min.js"></script>
        <script src="assets/js/dashboard-charts.js"></script>
        <script src="assets/js/script.js"></script>
    </body>

</html>
