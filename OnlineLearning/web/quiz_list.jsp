<!doctype html>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
    <head>
        <!--====== Required meta tags ======-->
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!--====== Title ======-->
        <title>Quiz</title>
        <!--====== Favicon Icon ======-->
        <link rel="shortcut icon" href="images/favicon.png" type="image/png">
        <!--====== Slick css ======-->
        <link rel="stylesheet" href="css/slick.css">
        <!--====== Animate css ======-->
        <link rel="stylesheet" href="css/animate.css">
        <!--====== Nice Select css ======-->
        <link rel="stylesheet" href="css/nice-select.css">
        <!--====== Nice Number css ======-->
        <link rel="stylesheet" href="css/jquery.nice-number.min.css">
        <!--====== Magnific Popup css ======-->
        <link rel="stylesheet" href="css/magnific-popup.css">
        <!--====== Bootstrap css ======-->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!--====== Fontawesome css ======-->
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <!--====== Default css ======-->
        <link rel="stylesheet" href="css/default.css">
        <!--====== Style css ======-->
        <link rel="stylesheet" href="css/style.css">
        <!--====== Responsive css ======-->
        <link rel="stylesheet" href="css/responsive.css">
    </head>

    <body>
        <!--====== PRELOADER PART START ======-->
        <div class="preloader">
            <div class="loader rubix-cube">
                <div class="layer layer-1"></div>
                <div class="layer layer-2"></div>
                <div class="layer layer-3 color-1"></div>
                <div class="layer layer-4"></div>
                <div class="layer layer-5"></div>
                <div class="layer layer-6"></div>
                <div class="layer layer-7"></div>
                <div class="layer layer-8"></div>
            </div>
        </div>
        <!--====== PRELOADER PART START ======-->
        <!--====== HEADER PART START ======-->
        <header>
            <jsp:include page="header.jsp" />
        </header>
        <!--====== HEADER PART ENDS ======-->
        <!--====== SEARCH BOX PART START ======-->
        <div class="search-box">
            <div class="serach-form">
                <div class="closebtn">
                    <span></span>
                    <span></span>
                </div>
                <form action="#">
                    <input type="text" placeholder="Search by keyword">
                    <button><i class="fa fa-search"></i></button>
                </form>
            </div> <!-- serach form -->
        </div>
        <!--====== SEARCH BOX PART ENDS ======-->
        <!--====== PAGE BANNER PART START ======-->
        <section id="page-banner" class="pt-105 pb-110 bg_cover" data-overlay="8"
            style="background-image: url(images/page-banner-4.jpg)">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-banner-cont">
                            <h2>Quiz List</h2>
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="homepage">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Quiz</li>
                                </ol>
                            </nav>
                        </div> <!-- page banner cont -->
                    </div>
                </div> <!-- row -->
            </div> <!-- container -->
        </section>
        <!--====== PAGE BANNER PART ENDS ======-->
        <!--====== QUIZ LIST PART START ======-->
        <section id="quiz-list" class="pt-90 pb-120 gray-bg">
            <div class="quiz">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8">
                            <form action="getquestions" method="get">
                                <ul>
                                    <c:forEach items="${quizzes}" var="quiz">
                                        <li>
                                            <a href="getquestions?quiz=${quiz.getId()}">${quiz.getName()}</a>
                                        </li>
                                    </c:forEach>
                                </ul>
                            </form>
                        </div>
                        <div class="col-lg-4"></div>
                    </div>
        </section>
        <!--====== QUIZ LIST PART ENDS ======-->
        <!--====== FOOTER PART START ======-->
        <jsp:include page="footer.jsp" />
        <!--====== FOOTER PART ENDS ======-->
        <!--====== BACK TO TP PART START ======-->
        <a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>
        <!--====== BACK TO TP PART ENDS ======-->
        <!--====== jquery js ======-->
        <script src="js/vendor/modernizr-3.6.0.min.js"></script>
        <script src="js/vendor/jquery-1.12.4.min.js"></script>
        <!--====== Bootstrap js ======-->
        <script src="js/bootstrap.min.js"></script>
        <!--====== Slick js ======-->
        <script src="js/slick.min.js"></script>
        <!--====== Magnific Popup js ======-->
        <script src="js/jquery.magnific-popup.min.js"></script>
        <!--====== Counter Up js ======-->
        <script src="js/waypoints.min.js"></script>
        <script src="js/jquery.counterup.min.js"></script>
        <!--====== Nice Select js ======-->
        <script src="js/jquery.nice-select.min.js"></script>
        <!--====== Nice Number js ======-->
        <script src="js/jquery.nice-number.min.js"></script>
        <!--====== Count Down js ======-->
        <script src="js/jquery.countdown.min.js"></script>
        <!--====== Validator js ======-->
        <script src="js/validator.min.js"></script>
        <!--====== Ajax Contact js ======-->
        <script src="js/ajax-contact.js"></script>
        <!--====== Main js ======-->
        <script src="js/main.js"></script>
        <!--====== Map js ======-->
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDC3Ip9iVC0nIxC6V14CKLQ1HZNF_65qEQ"></script>
        <script src="js/map-script.js"></script>
    </body>

</html>